(function ($) {
    var app = {};

    app.menu = {

        trigger: $("#sidebar .b-sidebar__btn"),
        isClosed: false,
        width: 220,

        init: function () {
            this.events();
            this.startUpOpen();
        },

        events: function () {
            let that = this;

            this.trigger.click(function () {
                that.toggleCross();
                $("#sidebar").toggleClass('toggled');
            });
        },

        startUpOpen: function () {
            if ($.cookie('menuClosed') == 'false') this.trigger.click();
        },

        toggleCross: function () {
            if (this.isClosed == true) {
                this.trigger
                    .removeClass('is-open')
                    .addClass('is-closed')
                    .animate({
                        left: "0",
                    }, 300);

                this.setContainerWidth();
                $.cookie('menuClosed', true, {expires: 120, path: '/admin'});
                this.isClosed = false;
            } else {
                this.trigger
                    .removeClass('is-closed')
                    .addClass('is-open')
                    .animate({
                        left: "+=" + this.width,
                    }, 300);

                this.setContainerWidth();
                $.cookie('menuClosed', false, {expires: 120, path: '/admin'});
                this.isClosed = true;
            }
        },

        setContainerWidth: function () {
            let that = this;
            if (this.isClosed) {
                $(".b-content").animate({
                    "width": "100%",
                    "margin-left": "0px",
                }, 300);

                return;
            }

            $(".b-content").animate({
                "width": ($(".b-content").parent().width() - that.width) + "px",
                "margin-left": (that.width) + "px",
            }, 300);

        },


    };

    app.mainPage = {
        init: function () {
            app.utils.tinymceImage('#b-main-menu__background');
            this.events();
            app.utils.filemanagerCallback();
        },

        events: function () {
            var that = this;
            $('.b-main-interior__link').fancybox({
                'width': 900,
                'height': 600,
                'type': 'iframe',
                'autoScale': false
            });

            $('#b-main-interior__background').change(function () {
                $.fancybox.close();
                that.searchPhoto($(this).val());
            });

            this.removePhotoEvent();
        },

        removePhotoEvent: function () {
            $('.b-main_interior-remove').click(function () {
                $(this).parent().remove();
            })
        },


        clientMenu: function () {
            $(".b-menu input[name=item_id]").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "item_id[" + $(value).val() + "]").val($(value).data('parent'));
                $('.b-menu_btn__order').append($(input));
            });

            $('.b-menu_btn__order').submit();

            return true;
        },


        searchPhoto: function (url) {
            let that = this;

            if (url.length < 5) return;

            $.ajax({
                url: '/admin/ajax/searchPhoto',
                method: 'POST',
                dataType: "json",
                data: {
                    term: url
                },
            })
                .done(function (obj) {
                    if (obj.length < 1) return;
                    that.addNewPhoto({
                        location: obj[0].path,
                        tn: obj[0].thumbnail_path,
                        id: obj[0].id,
                    });

                })

        },

        addNewPhoto: function (obj) {
            var html = '<li> <a href="/' + obj.location + '" data-fancybox="group" data-caption="">' +
                '<img src="/' + obj.tn + '"></a>' +
                '<a href="#"title="Удалить" class="btn btn-default b-main_interior-remove">' +
                '<span class="glyphicon glyphicon-remove"></span></a>' +
                '<input type="hidden" name="photo_ids[]" value="' + obj.id + '"></li>';

            $(".b-main_interior-list ul").prepend(html);

            this.removePhotoEvent();
        }


    };

    app.news = {
        init: function () {
            app.utils.tinymce('#b-news_tinymce');
            this.tinymceImage();
        },

        updateOrder: function () {
            $(".b-news input[name=news_id]").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "item_ids[]").val($(value).val());
                $('.b-news_btn__order').append($(input));
            });

            $('.b-news_btn__order').submit();

            return true;
        },

        tinymceImage: function () {
            app.utils.tinymceImage('#b-news_tinymce-image');
        },
    };

    app.photos = {
        init: function () {
            this.dropzone();
        },

        multiDelete: function () {
            $(".b-photos .b-photos_gallery input:checked").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "ids[]").val($(value).data('id'));
                $(input).insertAfter(".b-photos .b-photos__delete");
            })

            $(".b-photos .b-photos__delete").parent().submit();
        },

        dropzone: function () {
            $(".b-photos #addPhotos").dropzone(
                {
                    url: '/admin/photos/ajax',
                    paramName: 'file',
                    maxFilesize: 2,
                    acceptedFiles: '.jpg, .jpeg, .png, .bmp',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    dictDefaultMessage: 'Для загрузки файлов перетащите файлы сюда.',
                });
        }


    };

    app.utils = {
        copyToClipboard: function (val) {
            var el = document.createElement("input");
            el.setAttribute("value", val);
            document.body.appendChild(el);
            el.select();
            document.execCommand("copy");

            document.body.removeChild(el);
        },

        tinymce: function (selector) {
            tinymce.init({
                selector: selector,
                language: 'ru',
                height: 500,
                theme: 'modern',
                paste_data_images: true,
                image_advtab: true,
                relative_urls: false,
                force_br_newlines: true,
                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc help responsivefilemanager'
                ],

                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager',
                toolbar2: 'fontselect fontsizeselect | print preview media | forecolor backcolor emoticons | codesample help',
                fontsize_formats: '4pt 6pt 8pt 10pt 12pt 14pt 16pt 18pt 20pt 24pt 30pt 36pt',

                style_formats: [
                    {
                        title: 'Рамка вокруг блока',
                        block: 'div',
                        styles: {
                            border: '1px solid #e7e7e7',
                            'font-size': '16px',
                            color: '#7a7a7a',
                            padding: '10px 20px',
                        }
                    },

                    {
                        title: 'Подчеркивание у заглавия',
                        block: 'span',
                        styles: {
                            'border-bottom': '1px solid #f8d39a',
                            'font-size': '35px',
                            color: '#3a2f2e',
                            padding: '10px 0px',
                            'padding-bottom': '0',
                            'margin-bottom': '10px',
                            display: 'inline-block',
                        },
                    },
                ],

                file_browser_callback_types: 'file image media',

                image_title: true,
                // enable automatic uploads of images represented by blob or data URIs
                automatic_uploads: true,
                // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
                images_upload_url: '/admin/ajax/imageUpload',
                // here we add custom filepicker only to Image dialog
                file_picker_types: 'image',
                file_picker_callback: function (cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function () {
                            // Note: Now we need to register the blob in TinyMCEs image blob
                            // registry. In the next release this part hopefully won't be
                            // necessary, as we are looking to handle it internally.
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            // call the callback and populate the Title field with the file name
                            cb(blobInfo.blobUri(), {title: file.name});
                        };
                    };

                    input.click();
                },

                external_filemanager_path: "/admin_assets/filemanager/",
                filemanager_title: "Файл менеджер",
                external_plugins: {"filemanager": "/admin_assets/filemanager/plugin.min.js"}
            })
        },

        tinymceImage: function (selector) {
            tinymce.init({
                selector: selector,
                language: 'ru',
                height: 500,
                theme: 'modern',
                plugins: [
                    'image imagetools responsivefilemanager',
                ],
                toolbar1: 'image responsivefilemanager',
                menu: {},
                invalid_elements: 'strong,em,div,p',
                paste_data_images: true,
                image_advtab: true,
                relative_urls: false,
                force_br_newlines: true,

                file_browser_callback_types: 'file image media',

                image_title: true,
                // enable automatic uploads of images represented by blob or data URIs
                automatic_uploads: true,
                // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
                images_upload_url: '/admin/ajax/imageUpload',
                // here we add custom filepicker only to Image dialog
                file_picker_types: 'image',
                file_picker_callback: function (cb, value, meta) {
                    var input = document.createElement('input');
                    input.setAttribute('type', 'file');
                    input.setAttribute('accept', 'image/*');

                    input.onchange = function () {
                        var file = this.files[0];

                        var reader = new FileReader();
                        reader.readAsDataURL(file);
                        reader.onload = function () {
                            // Note: Now we need to register the blob in TinyMCEs image blob
                            // registry. In the next release this part hopefully won't be
                            // necessary, as we are looking to handle it internally.
                            var id = 'blobid' + (new Date()).getTime();
                            var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                            var base64 = reader.result.split(',')[1];
                            var blobInfo = blobCache.create(id, file, base64);
                            blobCache.add(blobInfo);

                            // call the callback and populate the Title field with the file name
                            cb(blobInfo.blobUri(), {title: file.name});
                        };
                    };

                    input.click();
                },

                external_filemanager_path: "/admin_assets/filemanager/",
                filemanager_title: "Файл менеджер",
                external_plugins: {"filemanager": "/admin_assets/filemanager/plugin.min.js"}

            })
        },

        confirmDelete: function (selector) {
            $(selector).click(function (e) {
                let that = this;
                e.preventDefault();
                swal({
                        title: "Удалить?",
                        type: "info",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        cancelButtonText: 'Отмена',
                        confirmButtonText: "Да!",
                        closeOnConfirm: true,
                    },
                    function () {
                        $(that).parent().submit();
                    });
            });
        },

        csrf: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },

        filemanagerCallback: function () {
            let that = this;
            //not good, but plugin filemanager requires a function in global namespace
            window.responsive_filemanager_callback = function (selector) {
                $('#' + selector).trigger("change");
            }
        },


    };

    app.slider = {
        init: function () {
            this.tinymceForImage();
            this.tinymceText();
        },

        tinymceForImage: function () {
            app.utils.tinymceImage('#b-slider__tinymce-image');
        },

        tinymceText: function () {
            app.utils.tinymce('#b-slider__tinymce-text');
        },

        updateOrder: function () {
            $(".b-slider input[name=slide_id]").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "slide_ids[]").val($(value).val());
                $('.b-slider_order-form').append($(input));
            });

            $('.b-slider_order-form').submit();

            return true;
        }
    };

    app.info_page = {
        init: function () {
            this.tinymceInit();
            this.tinymceImage();
        },

        tinymceInit: function () {
            app.utils.tinymce('#b-info-page_tinymce');
        },

        tinymceImage: function () {
            app.utils.tinymceImage('#b-info-page_tinymce-image');
        },


    };

    app.product = {

        init: function () {
            this.events();
            this.autoComplete();
            this.autoCompleteColor();
        },

        events: function () {
            let that = this;

            $(".b-product .b-product_tabs").tabs();
            app.utils.tinymce('.b-product_short-info');

            app.utils.tinymce('.b-product #tab_description');
            app.utils.tinymce('.b-product #tab_instruction');

            //dropzone for photos upload
            $(".b-product-photos_dropzone").dropzone({
                url: '/admin/photos/ajax',
                paramName: 'file',
                maxFilesize: 2,
                acceptedFiles: '.jpg, .jpeg, .png, .bmp',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dictDefaultMessage: 'Для загрузки файлов перетащите файлы сюда.',
                init: function () {
                    this.on("success", function (file, response) {
                        that.addNewPhoto(response);
                    });
                }
            });

            $(".b-product .b-product_current-photos").sortable();
            $(".b-product .b-product_current-colors").sortable();

            that.removePhotoColorEvent();

            //dropzone for colors
            $(".b-product-colors_dropzone").dropzone({
                url: '/admin/photos/ajax',
                paramName: 'file',
                maxFilesize: 2,
                acceptedFiles: '.jpg, .jpeg, .png, .bmp',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                dictDefaultMessage: 'Для загрузки файлов перетащите файлы сюда.',
                init: function () {
                    this.on("success", function (file, response) {
                        that.addNewColor(response);
                    });
                }
            });

            app.utils.confirmDelete('.b-product_delete button');

            $('.b-product .b-product_photos a, .b-product .b-product_colors a').fancybox({
                'width': 900,
                'height': 600,
                'type': 'iframe',
                'autoScale': false
            });

            $('#b-product-photos_filemanager').change(function () {
                that.searchPhoto($(this).val());
            });

            $('#b-product-colors_filemanager').change(function () {
                that.searchColor($(this).val());
            });

            this.filemanagerCallback();

        },

        removePhoto: function (that) {
            $(that).parent().remove();
        },

        addNewPhoto: function (photo) {
            let html = '<li><a href="/' + photo.location + '"' +
                ' data-fancybox="group">' +
                '<img src="' + photo.tn + '">' +
                '</a><a href="#" title="Удалить"' +
                ' class="btn btn-default b-product_photos-remove">' +
                '<span class="glyphicon glyphicon-remove"></span>' +
                '</a><input type="hidden" name="photos[]" value="' + photo.id + '"></li>';
            $(".b-product .b-product_current-photos").prepend(html);

            this.removePhotoColorEvent();
        },

        removePhotoColorEvent: function () {
            let that = this;
            $(".b-product .b-product_photos-remove, .b-product .b-product_colors-remove").click(function (e) {
                e.preventDefault();
                that.removePhoto(this);
            });
        },

        autoComplete: function () {
            let that = this;
            let obj = $(".b-product #b-product_search-photo").autocomplete({
                source: "/admin/ajax/searchPhoto",
                minLength: 2,
                select: function (event, ui) {
                    let photo = {
                        location: ui.item.path,
                        tn: '/' + ui.item.thumbnail_path,
                        id: ui.item.id,
                    };

                    that.addNewPhoto(photo);
                }
            });

            if (obj.length == 0) return;

            obj.autocomplete("instance")._renderItem = function (ul, item) {
                return $('<li class="b-product_autocomplete">')
                    .append('<img height="70" src="/' + item.thumbnail_path + '">' + item.name)
                    .appendTo(ul);
            };
        },

        autoCompleteColor: function () {
            let that = this;
            let obj = $(".b-product #b-product_search-color").autocomplete({
                source: "/admin/ajax/searchPhoto",
                minLength: 2,
                select: function (event, ui) {
                    let photo = {
                        location: ui.item.path,
                        tn: '/' + ui.item.thumbnail_path,
                        id: ui.item.id,
                    };

                    that.addNewColor(photo);
                }
            });

            if (obj.length == 0) return;

            obj.autocomplete("instance")._renderItem = function (ul, item) {
                return $('<li class="b-product_autocomplete_color">')
                    .append('<img height="70" src="/' + item.thumbnail_path + '">' + item.name)
                    .appendTo(ul);
            };
        },

        addNewColor: function (photo) {
            let html = '<li><input type="text" ' +
                'name="colors[' + photo.id + '][color_name]" placeholder="Артикул"' +
                ' class="form-control" value="">' +
                '<a href="/' + photo.location + '"' +
                ' data-fancybox="group">' +
                '<img src="' + photo.tn + '"></a>' +
                '<a href="#" title="Удалить"' +
                ' class="btn btn-default b-product_colors-remove">' +
                '<span class="glyphicon glyphicon-remove"></span></a>' +
                '<input type="hidden" name="colors[' +
                photo.id + '][photo_id]" value="' + photo.id + '"></li>';
            $(".b-product .b-product_current-colors").prepend(html);

            this.removePhotoColorEvent();
        },

        filemanagerCallback: function () {
            let that = this;
            //not good, but plugin filemanager requires a function in global namespace
            window.responsive_filemanager_callback = function (selector) {
                $('#' + selector).trigger("change");
            }
        },

        searchPhoto: function (url) {
            let that = this;
            $(".b-product_photos-loading").show();

            parent.$.fancybox.close();

            if (url.length < 5) return;

            $.ajax({
                url: '/admin/ajax/searchPhoto',
                method: 'POST',
                dataType: "json",
                data: {
                    term: url
                },
            })
                .done(function (obj) {
                    $(".b-product_photos-loading").hide();

                    if (obj.length < 1) return;
                    that.addNewPhoto({
                        location: obj[0].path,
                        tn: '/' + obj[0].thumbnail_path,
                        id: obj[0].id,
                    });

                })

        },

        searchColor: function (url) {
            let that = this;
            $(".b-product_colors-loading").show();

            parent.$.fancybox.close();

            if (url.length < 5) return;

            $.ajax({
                url: '/admin/ajax/searchPhoto',
                method: 'POST',
                dataType: "json",
                data: {
                    term: url
                },
            })
                .done(function (obj) {
                    $(".b-product_colors-loading").hide();

                    if (obj.length < 1) return;
                    that.addNewColor({
                        location: obj[0].path,
                        tn: '/' + obj[0].thumbnail_path,
                        id: obj[0].id,
                    });

                })

        }


    };

    app.category = {
        init: function () {
            this.events();
        },

        events: function () {
            let that = this;
            app.utils.tinymceImage("#b-category_tinymce-miniature");
            app.utils.tinymceImage("#b-category_tinymce-image");
            $(".b-category_sortable_level0").sortable().disableSelection();
            $(".b-category_sortable_level1").sortable().disableSelection();
            $(".b-category_sortable_level2").sortable().disableSelection();

            $(".b-category .b-category_btn__order button").click(function () {
                that.updateOrder();
            });

            app.utils.confirmDelete('.b-category form .btn-danger');

        },

        updateOrder: function () {
            $(".b-category input[name=item_id]").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "item_id[" + $(value).val() + "]").val($(value).data('parent'));
                $('.b-category_btn__order').append($(input));
            });

            $('.b-category_btn__order').submit();

            return true;
        },

    };

    app.brand = {
        init: function () {
            app.utils.tinymceImage('#b-brand_tinymce-image');
            app.utils.confirmDelete('.b-brand_delete button');
        },

        updateOrder: function () {
            $(".b-brand input[name=brand_id]").each(function (index, value) {
                let input = $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "brand_ids[]").val($(value).val());
                $('.b-brand_btn__order').append($(input));
            });

            $('.b-brand_btn__order').submit();

            return true;
        }
    };


    app.events = function () {
        app.utils.csrf();

        $(".b-content .b-menu tbody").sortable();
        $(".b-content .b-news tbody").sortable();
        $(".b-content .b-slider tbody").sortable();


        $(".b-menu_btn__order button").click(function (e) {
            e.preventDefault();
            app.mainPage.clientMenu(this);
        });

        $(".b-photos_gallery .b-photos_copy").click(function (e) {
            e.preventDefault();
            app.utils.copyToClipboard($(this).attr('href'));
            swal({
                title: "Адрес скопирован",
                type: "info",
                timer: 3000,
            });
        });

        $(".b-photos .b-photos_remove").click(function (e) {
            var that = this;
            e.preventDefault();

            swal({
                    title: "Удалить фото",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: 'Отмена',
                    confirmButtonText: "Да, удалить",
                    closeOnConfirm: true,
                },
                function () {
                    $(that).parent().submit();
                });
        });

        $(".b-news .b-news_delete button").click(function (e) {
            var that = this;
            e.preventDefault();

            swal({
                    title: "Удалить новость",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: 'Отмена',
                    confirmButtonText: "Да, удалить",
                    closeOnConfirm: true,
                },
                function () {
                    $(that).parent().submit();
                });
        });


        $(".b-news_btn__order button").click(function (e) {
            e.preventDefault();
            app.news.updateOrder();
        });

        $(".b-photos .b-photos__multi").click(function (e) {
            e.preventDefault();
            $(".b-photos .b-photos__checkbox, .b-photos .b-photos__delete").toggleClass('hidden');
        });

        $(".b-photos .b-photos__delete").click(function (e) {
            e.preventDefault();
            app.photos.multiDelete();
        })

        $(".b-slider_order-form button").click(function (e) {
            e.preventDefault();
            app.slider.updateOrder();
        });

        $(".b-info-page_delete button, " +
            ".b-subscriber_delete button, " +
            ".b-order_delete button," +
            ".b-callback_delete button").click(function (e) {

            var that = this;
            e.preventDefault();

            swal({
                    title: "Удалить?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: 'Отмена',
                    confirmButtonText: "Да!",
                    closeOnConfirm: true,
                },
                function () {
                    $(that).parent().submit();
                });
        });

        $(".b-menu_sortable_level0").sortable().disableSelection();
        $(".b-menu_sortable_level1").sortable().disableSelection();
        $(".b-menu_sortable_level2").sortable().disableSelection();

        $(".b-brand tbody").sortable();

        $(".b-brand_btn__order button").click(function (e) {
            e.preventDefault();
            app.brand.updateOrder();
        });

        //settings
        app.utils.tinymce('#b-settings__offer');

        //design-studio
        app.utils.tinymceImage('#b-design_studio-image');
        app.utils.confirmDelete('.b-design-studio .btn-danger');


    };


    // Disabling autoDiscover, otherwise Dropzone may try to attach twice.
    Dropzone.autoDiscover = false;

    app.menu.init();
    app.photos.init();
    app.news.init();
    app.slider.init();
    app.info_page.init();
    app.mainPage.init();
    app.product.init();
    app.category.init();
    app.brand.init();
    app.events();


})(jQuery)




