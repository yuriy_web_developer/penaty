-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'Garn Decor',75,NULL,NULL,'2017-07-29 12:28:06','2017-07-29 12:28:06'),(2,'OriginalStyle',76,'/',NULL,'2017-07-29 12:28:34','2017-07-29 12:28:34');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `callbacks`
--

DROP TABLE IF EXISTS `callbacks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `callbacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `repair_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `callbacks`
--

LOCK TABLES `callbacks` WRITE;
/*!40000 ALTER TABLE `callbacks` DISABLE KEYS */;
INSERT INTO `callbacks` VALUES (1,'name','123456','test@test.com',NULL,'city',NULL,'2017-08-12 15:02:17','2017-08-12 15:02:17');
/*!40000 ALTER TABLE `callbacks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `photo_id` int(11) DEFAULT NULL,
  `order_num` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (6,149,0,79,1,'2017-07-09 16:56:20','2017-08-02 18:20:24'),(7,150,0,3,15,'2017-07-09 16:57:51','2017-08-04 21:52:41'),(8,151,0,NULL,18,'2017-07-09 16:58:29','2017-07-16 09:19:52'),(9,152,0,NULL,19,'2017-07-09 16:58:37','2017-07-16 09:19:52'),(10,153,0,56,20,'2017-07-09 16:59:02','2017-07-16 09:19:52'),(11,154,14,57,3,'2017-07-09 17:26:00','2017-07-16 09:19:52'),(12,155,7,NULL,16,'2017-07-09 17:26:19','2017-07-16 09:19:52'),(13,156,7,NULL,17,'2017-07-09 17:26:45','2017-07-16 09:19:52'),(14,157,6,NULL,2,'2017-07-09 17:28:02','2017-07-16 09:19:52'),(15,158,6,NULL,8,'2017-07-09 17:28:24','2017-07-16 09:19:52'),(16,165,6,NULL,12,'2017-07-11 14:27:35','2017-07-16 09:19:52'),(17,166,6,NULL,13,'2017-07-11 14:27:51','2017-07-16 09:19:52'),(18,167,6,NULL,14,'2017-07-11 14:28:18','2017-07-16 09:19:52'),(19,168,14,NULL,5,'2017-07-11 14:29:19','2017-07-16 09:19:52'),(20,169,14,NULL,6,'2017-07-11 14:29:33','2017-07-16 09:19:52'),(21,170,14,NULL,7,'2017-07-11 14:29:50','2017-07-16 09:19:52'),(22,171,15,NULL,9,'2017-07-11 14:32:11','2017-07-16 09:19:52'),(23,172,15,NULL,10,'2017-07-11 14:32:35','2017-07-16 09:19:52'),(24,173,15,NULL,11,'2017-07-11 14:32:48','2017-07-16 09:19:52'),(25,180,14,NULL,4,'2017-07-16 09:17:58','2017-07-16 09:19:52'),(26,182,6,0,0,'2017-08-18 19:55:32','2017-08-18 19:55:32'),(27,183,6,0,0,'2017-08-18 19:56:10','2017-08-18 19:56:10');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `design_studios`
--

DROP TABLE IF EXISTS `design_studios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `design_studios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `photo_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `design_studios`
--

LOCK TABLES `design_studios` WRITE;
/*!40000 ALTER TABLE `design_studios` DISABLE KEYS */;
INSERT INTO `design_studios` VALUES (3,'КВАРТИРЫ','/',103,'2017-08-19 19:02:10','2017-08-19 19:15:59'),(4,'КОТТЕДЖИ','/',104,'2017-08-19 19:04:12','2017-08-19 19:16:12'),(5,'КАФЕ РЕСТОРАНЫ','/',106,'2017-08-19 19:04:38','2017-08-19 19:16:26'),(6,'ОБЩЕСТВЕННЫЕ ИНТЕРЬЕРЫ','/',105,'2017-08-19 19:05:10','2017-08-19 19:16:40');
/*!40000 ALTER TABLE `design_studios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main_page`
--

DROP TABLE IF EXISTS `main_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `meta_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main_page`
--

LOCK TABLES `main_page` WRITE;
/*!40000 ALTER TABLE `main_page` DISABLE KEYS */;
INSERT INTO `main_page` VALUES (1,'contacts','{\"phone_header\":\"(343) 355-18-441\",\"email_header\":\"salon_penat@mail.ru\",\"address_header\":\"\\u0433. \\u0415\\u043a\\u0430\\u0442\\u0435\\u0440\\u0438\\u043d\\u0431\\u0443\\u0440\\u0433, \\u0443\\u043b. \\u0411\\u0430\\u0436\\u043e\\u0432\\u0430, 89\",\"address_footer\":\"620075, \\u0433. \\u0415\\u043a\\u0430\\u0442\\u0435\\u0440\\u0438\\u043d\\u0431\\u0443\\u0440\\u0433 \\r\\n\\u0443\\u043b. \\u0411\\u0430\\u0436\\u043e\\u0432\\u0430, 89.\",\"time_footer\":\"\\u041f\\u043d\\u2013\\u041f\\u0442: \\u0441 10 \\u0434\\u043e 19 \\u0447\\u0430\\u0441\\u043e\\u0432; \\r\\n\\u0421\\u0431-\\u0412\\u0441: \\u0432\\u044b\\u0445\\u043e\\u0434\\u043d\\u043e\\u0439.\",\"email_footer\":\"salon_penat@mail.ru\",\"phone_footer\":\"(343) 355-18-44 \\r\\n358-92-36 \\r\\n358-97-64\"}','2017-06-19 17:01:04','2017-07-25 16:41:33'),(4,'interior','{\"photo_ids\":[\"102\",\"90\",\"88\"],\"link\":\"dizajn\",\"inEffect\":\"fadeInLeftBig\",\"outEffect\":\"fadeOutRightBig\",\"delay\":\"0.5\"}','2017-07-06 12:16:29','2017-08-19 14:31:45');
/*!40000 ALTER TABLE `main_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_num` smallint(6) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
INSERT INTO `menu` VALUES (7,'О компании','o-kompanii',1,'2017-06-19 17:52:01','2017-07-05 16:59:12',0),(8,'Каталог','/catalog',2,'2017-06-19 17:53:18','2017-08-10 13:06:29',0),(9,'Галерея','/',8,'2017-06-19 17:53:28','2017-07-11 17:08:21',0),(10,'Дизайн-студия','dizajn',9,'2017-06-19 17:53:38','2017-07-11 17:08:21',0),(11,'Публикации','/',10,'2017-06-19 17:53:47','2017-07-11 17:08:21',0),(12,'Оптовым партнерам','/',11,'2017-06-19 17:53:56','2017-07-11 17:08:21',0),(13,'Контакты','kontakty',12,'2017-06-19 17:54:04','2017-07-11 17:08:21',0),(14,'Акции','/',13,'2017-06-19 17:54:11','2017-07-11 17:08:21',0),(18,'Декоративные штукатурки и покрытия','/catalog/dekorativnye-shtukaturki',3,'2017-07-05 16:42:16','2017-07-11 17:08:21',8),(19,'Лепнина','/catalog/lepnina',4,'2017-07-05 16:42:31','2017-07-11 17:08:21',8),(20,'Деревянный декор','/catalog/derevyannyj-dekor',5,'2017-07-05 16:43:43','2017-07-11 17:08:21',8),(21,'Обои','/catalog/oboi',6,'2017-07-05 16:43:59','2017-07-11 17:08:21',8),(23,'Керамическая плитка','/catalog/keramicheskaya-plitka',7,'2017-07-11 17:08:16','2017-07-11 17:08:21',8),(24,'Отзывы','/',14,'2017-08-10 12:36:51','2017-08-10 12:36:51',0);
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (3,'2014_10_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_resets_table',1),(5,'2017_06_19_102202_create_menu_table',2),(7,'2017_06_19_162657_create_main_page_table',3),(10,'2017_06_19_185924_create_news_table',4),(11,'2017_06_20_182036_create_photo_table',5),(13,'2017_06_25_160926_create_sliders_table',6),(20,'2017_06_26_122654_create_pages_table',7),(21,'2017_06_27_174808_create_subscribers_table',8),(22,'2017_06_28_154713_create_products_table',9),(24,'2017_07_04_083354_update_menu_table_add_parent_id',10),(25,'2017_07_05_171304_update_pages_table_add_photo_id_column',11),(29,'2017_07_06_151922_update_products_add_short_info_and_page_id',12),(30,'2017_07_06_152225_update_products_delete_url',13),(32,'2017_07_06_170648_update_products_add_columns_for_tabs',14),(33,'2017_07_08_095932_create_product_image_table',15),(35,'2017_07_08_103013_create_product_colors_table',16),(36,'2017_07_08_135347_create_product_photo_table',17),(37,'2017_07_08_184225_update_products_add_producer_column',18),(38,'2017_07_08_193204_update_product_colors_table_addbigint_increment',19),(39,'2017_07_08_193256_update_product_photos_table_addbigint_increment',19),(44,'2017_07_09_095350_create_categories_table',20),(45,'2017_07_09_153650_create_product_categories_table',21),(46,'2017_07_10_230342_update_products_add_volume',22),(47,'2017_07_16_132020_create_orders_table',23),(51,'2017_07_16_132406_crate_order_product_table',24),(53,'2017_07_19_160513_create_callbacks_table',25),(65,'2017_07_25_180732_create_brands_table',26),(67,'2017_08_12_135713_create_settings_table',27),(68,'2017_08_14_215924_update_products_table_add_hit',28),(70,'2017_08_19_150522_add_meta_column_to_pages_table',29),(71,'2017_08_19_184105_create_design_studios_table',29);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text_short` text COLLATE utf8mb4_unicode_ci,
  `text` text COLLATE utf8mb4_unicode_ci,
  `order_num` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'test',NULL,NULL,3,'2017-06-19 22:13:37','2017-06-24 20:01:29'),(2,'Новость 1',NULL,'<div style=\"border-bottom: 1px solid #f8d39a; font-size: 35px; color: #3a2f2e; padding: 10px 0px; margin-bottom: 10px; display: inline-block; text-align: left;\">jhgkjhg</div>',2,'2017-06-24 16:49:37','2017-06-24 20:01:29'),(3,'КопияНовость 1',NULL,'<div style=\"border-bottom: 1px solid #f8d39a; font-size: 35px; color: #3a2f2e; padding: 10px 0px; margin-bottom: 10px; display: inline-block; text-align: left;\">jhgkjhg</div>',1,'2017-06-24 20:00:44','2017-06-24 20:00:44');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_product`
--

DROP TABLE IF EXISTS `order_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `volume` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_product`
--

LOCK TABLES `order_product` WRITE;
/*!40000 ALTER TABLE `order_product` DISABLE KEYS */;
INSERT INTO `order_product` VALUES (8,22,15,61,1,1,'2017-07-19 15:54:06','2017-07-19 15:54:06'),(9,22,16,65,1,10,'2017-07-19 15:54:06','2017-07-19 15:54:06'),(10,22,17,71,1,15,'2017-07-19 15:54:06','2017-07-19 15:54:06'),(11,23,15,61,1,1,'2017-07-29 14:28:39','2017-07-29 14:28:39'),(12,24,15,61,1,1,'2017-07-29 14:41:34','2017-07-29 14:41:34'),(13,25,15,61,1,1,'2017-07-29 14:42:59','2017-07-29 14:42:59'),(14,26,15,61,1,1,'2017-07-29 14:44:58','2017-07-29 14:44:58'),(15,27,15,61,1,1,'2017-07-29 15:48:58','2017-07-29 15:48:58'),(16,28,15,61,1,1,'2017-07-29 15:49:55','2017-07-29 15:49:55'),(17,29,15,61,1,1,'2017-07-29 19:04:51','2017-07-29 19:04:51'),(18,29,15,61,1,1,'2017-07-31 18:13:16','2017-07-31 18:13:16'),(19,30,15,61,1,1,'2017-07-31 20:14:24','2017-07-31 20:14:24'),(20,31,15,61,1,1,'2017-07-31 20:36:42','2017-07-31 20:36:42'),(21,29,15,61,1,1,'2017-08-02 16:21:45','2017-08-02 16:21:45'),(22,29,15,61,1,1,'2017-08-03 16:13:31','2017-08-03 16:13:31');
/*!40000 ALTER TABLE `order_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (28,'test','123456','test@test.com','city',NULL,'2017-07-29 15:49:55','2017-07-29 15:49:55');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `short_content` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  `meta` text COLLATE utf8mb4_unicode_ci,
  `photo_id` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `archived` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (71,'/','Главная страница','ключевые слова','описание главное страницы',NULL,NULL,NULL,NULL,NULL,'main',0,0,'2017-07-04 16:11:01','2017-07-04 16:18:58'),(72,'o-kompanii','О компании','ключевые фразы','описание',NULL,'<table style=\"height: 19px; width: 787px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 322px;\"><img src=\"/img/uploaded/full/2017/07/2017_07_05_16_52_32-penaty.jpg\" alt=\"\" width=\"250\" height=\"187\" /></td>\r\n<td style=\"width: 451px;\">\r\n<p><em>Скрипнут колеса<br /></em><em>Древней телеги времен<br /></em><em>Что-то вернется,&nbsp;<br />Что-то растает как сон.<br />Песня тревоги...&nbsp;<br />Знаки судьбы на песке<br />Древние боги<br />С</em><em>&nbsp;нами идут по земле</em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>\r\n<div style=\"border: 1px solid #e7e7e7; font-size: 16px; color: #7a7a7a; padding: 10px 20px;\"><span style=\"border-bottom: 1px solid #f8d39a; font-size: 24pt; color: #3a2f2e; padding: 10px 0px 0px; margin-bottom: 10px; display: inline-block;\">О ГЛАВНОМ</span><br />&nbsp; &nbsp;ООО \"Пенаты\" образованы в 2003 году. Собственный салон в центре города. Профиль работы - продажа эксклюзивных отделочных материалов для стен, полов и потолков, для создания неповторимого дизайна интерьеров в Екатеринбурге. Салон \"Пенаты\" предлагает вниманию специалистов, практикующих в своей работе, индивидуальный подход и широкий ассортимент продукции многих мировых брендов.<br /><br />&nbsp; &nbsp;Компания \"Пенаты\" - официальный представитель в Уральском регионе продукции \"FabellonDecor\" (в прошлом \"Gaudi Decor\"), крупнейшего производителя лепных украшений из пенополиуретана, и всемирно известной итальянской фабрики декоративных покрытий \"SanMarco\". С помощью лепнины и декоративных покрытий можно создать неповторимый дизайн интерьера. &nbsp; &nbsp;&nbsp;<br /><br />&nbsp; &nbsp;Гордостью салона \"Пенаты\" является самая большая в Екатеринбурге коллекция текстильных и бумажных обоев производства Италии, Франции, Бельгии, Англии.&nbsp;<br /><br />&nbsp; &nbsp;Салон &laquo;Пенаты&raquo; поможет создать неповторимый дизайн интерьера в городе Екатеринбург.<br /><br />&nbsp; &nbsp;Только в салоне \"Пенаты\" - полная коллекции керамической плитки английской фабрики \"Original style\" для ванных комнат и кухни, а также \"викторианские полы\" - мозаичная напольная плитка геометрических форм. Панно с репродукциями работ известных всему миру художников, а также с характерной для кухонь и ванных комнат тематикой, глазурованные вручную.</div>\r\n<div>&nbsp;</div>\r\n<div style=\"border: 1px solid #e7e7e7; font-size: 16px; color: #7a7a7a; padding: 10px 20px;\"><span style=\"border-bottom: 1px solid #f8d39a; font-size: 24pt; color: #3a2f2e; padding: 10px 0px 0px; margin-bottom: 10px; display: inline-block;\">НАША ПОЛИТИКА</span><br />&nbsp; &nbsp;Мы отлично знаем свою продукцию и материалы наших конкурентов.<br /><br />&nbsp; &nbsp;В \"Пенатах\" мы советуем выбрать именно те материалы, которые подходят для Вас в каждом конкретном случае, с обязательным соотношением цены и качества!<br />&nbsp; &nbsp;Не всегда дешевле - это лучше. И наоборот! Мы всегда поймем, что действительно нужно нашим клиентам и дадим полную и правдивую информацию по применению и использованию наших материалов.<br /><br />&nbsp; &nbsp;Специалисты салона \"Пенаты\" осуществляют все необходимые консультации по подготовительным работам, предшествующим использованию наших отделочных материалов. Мы поддерживаем профессиональные отношения с покупателем и после осуществления покупки-продажи, мы отслеживаем доставку, укладку и дальнейшую \"судьбу\" нашей продукции. Наши мастера и специалисты принимают непосредственное участие в нанесении декоративных штукатурок, наклейке обоев и укладке мозаичных полов. <br /><br />&nbsp; &nbsp;Мы несем ответственность за выполненные работы и всегда добиваемся превосходных результатов. Салон \"Пенаты\" - это профессиональный дизайн интерьеров в городе Екатеринбург.</div>\r\n<div>&nbsp;</div>\r\n<div style=\"border: 1px solid #e7e7e7; font-size: 16px; color: #7a7a7a; padding: 10px 20px;\"><span style=\"border-bottom: 1px solid #f8d39a; font-size: 24pt; color: #3a2f2e; padding: 10px 0px 0px; margin-bottom: 10px; display: inline-block;\">НАШИ КЛИЕНТЫ</span><br />&nbsp; &nbsp;Каждый человек, обратившийся в Пенаты, для нас - Очень Важная Персона.<br /><br />&nbsp; &nbsp;Кто Вы и чем занимаетесь - это не влияет на наше внимательное отношение.<br /><br />&nbsp; &nbsp;Многим нашим покупателям весьма интересно, где воочию можно увидеть работу наших мастеров? Мы всегда готовы показать результаты нашей работы и не боимся трудностей.<br /><br />&nbsp; &nbsp;Благодаря этому мы сохраняем позитивные партнерские отношения с большинством из наших клиентов на долгое время, многие возвращаются к нам спустя несколько лет и рекомендуют \"Пенаты\" своим знакомым.</div>\r\n<div>&nbsp;</div>\r\n<div style=\"border: 1px solid #e7e7e7; font-size: 16px; color: #7a7a7a; padding: 10px 20px;\"><span style=\"border-bottom: 1px solid #f8d39a; font-size: 24pt; color: #3a2f2e; padding: 10px 0px 0px; margin-bottom: 10px; display: inline-block;\">НАША РЕПУТАЦИЯ</span><br />&nbsp; &nbsp;Мы имеем успешный и многолетний опыт работы с дизайнерами и архитекторами Уральского региона, который основан на индивидуальном подходе и неравнодушном отношении к конечному результату нашего совместного творчества.<br /><br />&nbsp; &nbsp;Специалисты компании обеспечивают выполнение всех технологических процессов под \"ключ\", с соблюдением всех норм и требований, обуславливающих в дальнейшем гарантию качества выполненных работ и материалов.</div>\r\n<div>&nbsp;</div>\r\n<div style=\"border: 1px solid #e7e7e7; font-size: 16px; color: #7a7a7a; padding: 10px 20px;\"><span style=\"border-bottom: 1px solid #f8d39a; font-size: 24pt; color: #3a2f2e; padding: 10px 0px 0px; margin-bottom: 10px; display: inline-block;\">...И НЕМНОГО ИСТОРИИ</span><br />&nbsp; &nbsp;Кто такие пенаты? Пенаты &mdash; древние боги-хранители, культ которых связан с обожествлением предков. Они хранили дом и кладовые дома. Каждый римлянин выбирал свободно из числа всех богов Рима тех, которых он чтил у себя дома как особых своих покровителей. Среди богов-пенатов были и Юпитер, и Венера, и Фортуна, и Марс, и другие боги. Пенатов можно было взять с собой при переезде на другое место. Пенаты хранились в особом шкафчике возле очага, где собирались все члены семьи.<br /><br />&nbsp; &nbsp;При радостных событиях в семье пенатам приносились благодарственные жертвы. Вместе с тем были пенаты всего римского государства, которых чтили там, где находится очаг всего государства, а именно у круглого храма Весты - богини покровительницы семейного очага и жертвенного огня на римском форуме.&nbsp; &nbsp;&nbsp;<br />&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />&nbsp; &nbsp;Древние римляне свято верили в добрых, уютных божков, живших в каждом доме и охранявших его. Пенаты издавна стали символом дома, родины. Выражение \"Вернуться к своим пенатам\" означающее: возвратиться под родную кровлю, устойчиво закрепилось в нашем сознании.<br />&nbsp;<br />&nbsp;</div>',NULL,2,NULL,'info',1,0,'2017-07-05 16:47:27','2017-07-06 13:11:37'),(74,'dizajn','Дизайн-студия',NULL,NULL,NULL,'<!--<div>\r\n<table style=\"width: 870px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 627px;\">\r\n<p><span style=\"color: #c92300; font-size: 14pt;\">Интерьеры квартир.</span></p>\r\n<p>Личное пространство постоянно находится в динамике, изменяется, трансформируется под потребности своих хозяев. Эту особенность очень важно понимать при создании интерьера. Задача состоит в том, чтобы не просто помочь сделать квартиру красивой, соблюсти чистоту стиля и правильность композиционных решений, но и сделать интерьер функциональным и удобным для жизни как в данный момент времени, так и в будущем. Необходимо учесть, насколько это возможно, предстоящие перемены и создать возможность менять что-либо без ущерба для психики всех хозяев.</p>\r\n</td>\r\n<td style=\"width: 263px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src=\"/img/uploaded/full/2017/07/2017_07_05_18_31_07-Iukuhni.jpg\" alt=\"\" width=\"175\" height=\"133\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>&nbsp;</p>\r\n<table style=\"width: 859px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 838px;\">\r\n<p><span style=\"color: #c92300; font-size: 14pt;\">Интерьеры коттеджей.</span></p>\r\n<p>Бесспорно, дизайн коттеджа имеет свои особенности. Во-первых, коттедж может включать в себя такие помещения, которые трудно представить себе в обычной квартире: бассеин, мансарда, винный погреб и другие. Во-вторых, практически неограниченный полет фантазии: хотите русскую печку ставьте, хотите зимний сад разбивайте или стройте стеклянную крышу. И третье, интерьер коттеджа можно выгодно дополнить его экстерьером: задекорировать фасад здания под стиль его внутреннего убранства.</p>\r\n</td>\r\n<td style=\"width: 266px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"/img/uploaded/full/2017/07/2017_07_05_18_31_08-Susert_sad.jpg\" width=\"175\" height=\"133\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>&nbsp;</p>\r\n<table style=\"width: 861px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 607px;\">\r\n<p><span style=\"color: #c92300; font-size: 14pt;\">Интерьеры ресторанов, кафе.</span></p>\r\n<p>Дизайн ресторанов и кафе должен полностью соответствовать мировоззрению их посетителей. К примеру, в спортивном баре фанатов Спартака не место флагам Зенита, а в студенческом бистро - классической мебели с позолотой. От того оправдаются ли надежды посетителя при входе в ресторан, зависит останется ли он здесь или побредет дальше в поисках более комфортного места пусть и с менее заманчивым меню.</p>\r\n</td>\r\n<td style=\"width: 242px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"/img/uploaded/full/2017/07/2017_07_05_18_31_07-fish.jpg\" width=\"175\" height=\"133\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>&nbsp;</p>\r\n<table style=\"width: 858px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 604px;\">\r\n<p><span style=\"font-size: 14pt; color: #c92300;\">Интерьеры общественных помещений.</span></p>\r\n<p>У общественных помещений могут быть абсолютно разные предназначения. Это может быть магазин, гостиница или офис. Всех их объединяет одно: от интерьера помещения во многом зависят имидж и репутация фирмы, а значит и её успех в мире бизнеса.</p>\r\n</td>\r\n<td style=\"width: 242px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"/img/uploaded/full/2017/07/2017_07_05_18_45_53-direkto_r2.jpg\" width=\"175\" height=\"133\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>&nbsp;</p>\r\n<table style=\"width: 858px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 598px;\">\r\n<p><span style=\"color: #c92300; font-size: 14pt;\">Интересные детали в итерьере.</span></p>\r\n<p>Уют складывается из мелочей: каждая вещь, каждый предмет влияют на восприятие всего пространства. В данном разделе вы можете увидеть интересные детали, способные полностью преобразить интерьер..</p>\r\n</td>\r\n<td style=\"width: 248px;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<img src=\"/img/uploaded/full/2017/07/2017_07_05_18_31_08-t_87bec31d371d838e9451616afbfe0226.jpg\" width=\"175\" height=\"133\" /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<hr />\r\n<p>&nbsp;</p>\r\n</div>-->',NULL,0,NULL,'info',1,0,'2017-07-05 18:34:28','2017-08-19 18:27:26'),(75,'kontakty','Контакты',NULL,NULL,NULL,'<p>Вы можете приобрести наши материалы, увидеть образцы декоративных красок и покрытий, получить необходимые консультации по их нанесению и эксплуатации, а так же рекомендации по наклеиванию всех видов обоев с использованию специальных инструментов, применению изделий из полиуретана.</p>\r\n<p>Специалисты нашего салона всегда рады предоставить максимальное количество информации по любому вопросу касающемуся наших материалов и их применению.</p>\r\n<p>&nbsp;</p>\r\n<p><span style=\"font-size: 20pt;\">Офис на Бажова</span></p>\r\n<table style=\"height: 66px; width: 509px;\">\r\n<tbody>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 191px; height: 18px;\">Адрес:</td>\r\n<td style=\"width: 304px; height: 18px;\">620075, г. Екатеринбург, ул. Бажова, 89.</td>\r\n</tr>\r\n<tr style=\"height: 48px;\">\r\n<td style=\"width: 191px; height: 48px;\">Время работы:</td>\r\n<td style=\"width: 304px; height: 48px;\">Пн&ndash;Пт: с 10 до 19 часов;<br />Сб-Вс: выходной.</td>\r\n</tr>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 191px; height: 18px;\">Телефоны:</td>\r\n<td style=\"width: 304px; height: 18px;\">(343) 355-18-44, 358-92-36, 358-97-64.</td>\r\n</tr>\r\n<tr style=\"height: 18px;\">\r\n<td style=\"width: 191px; height: 18px;\">E-mail:</td>\r\n<td style=\"width: 304px; height: 18px;\">info@salon-penat.ru</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p>Схема проезда:&nbsp;</p>\r\n<p><iframe src=\"http://maps.google.com/maps/ms?ie=UTF8&amp;hl=ru&amp;msa=0&amp;msid=103709962939214058174.00044a908f0705a41a668&amp;ll=56.842588,60.626893&amp;spn=0.007042,0.021415&amp;z=15&amp;output=embed\" width=\"800\" height=\"300\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\"></iframe></p>',NULL,NULL,NULL,'info',1,0,'2017-07-05 19:00:28','2017-07-05 19:02:30'),(76,'fabello-decor','Новости - Fabello Decor',NULL,NULL,NULL,'<p>Внимание! Производитель всемирно изверного бренда Gaudi Decor, сменил название на Fabello Decor. Каталог лепного декора остается с прежними артикулами и привычными названиями.</p>\r\n<p>Система работы прежняя. Ждем клиентов и дизайнеров за новыми каталогами.</p>',NULL,2,1,'news',1,0,'2017-07-05 22:38:27','2017-07-25 16:58:58'),(77,'novyj-katalog-oboev','Новый каталог обоев!',NULL,NULL,NULL,'<p><span style=\"font-size: 14pt;\">В салон Пенаты поступил новый каталог (обои) ProSpero &laquo;Space&raquo;.</span> <br /><strong>Материал</strong>: флизелин/акрил <br /><strong>Ширина</strong>: 90см, поставка погонными метрами.<br /> <strong>Цена</strong> от 1100 до 1800р/пог.м.<br /><br />Необычный для флизелиновых обоев \"рельефный\" объём флористического рисунка, а-ля Линкруста. Обои ProSpero Space элегантно сочетает винтажный шик и минималистичный аскетизм. Обои нежно обволакивают стены, не перегружая пространство. Они одинаково хорошо подходят и для создания мягких акцентов в интерьерах в стиле хайтек и лофт, и уютной атмосферы в классических спальнях и гостиных. Великолепные металлизированные рельефные дамаски и цветочный орнамент контрастно проявляются на фонах, исполненных в глубоких пастельных тонах. Фактурный дизайн жатка &ndash; актуальный тренд в современном дизайне &mdash; представлен в 11 цветах от кораллового и темно-фиолетового до оттенка шампанского и насыщенного гиацинтового.</p>\r\n<p><img title=\"Space_.jpg\" src=\"/img/uploaded/full/2017/07/2017_07_05_22_44_28-blobid1499294665214.jpg\" alt=\"\" width=\"359\" height=\"375\" /></p>',NULL,NULL,2,'news',1,0,'2017-07-05 22:45:21','2017-07-05 22:48:20'),(78,'novyj-master-klass-tehniki-zolocheniya-zapis-otkryta','Новый мастер-класс \"Техники золочения\".',NULL,NULL,NULL,'<table style=\"width: 1021px;\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 288px;\"><img title=\"image-30-07-14-15-46.jpeg\" src=\"/img/uploaded/full/2017/07/2017_07_05_22_46_48-blobid1499294805252.jpg\" alt=\"\" width=\"247\" height=\"329\" /></td>\r\n<td style=\"width: 735px; vertical-align: top;\">\r\n<p>Спешите записаться на новый мастер-класс от художника салона \"Пенаты\"&nbsp;Симаковой Анны! Мастер-класс состоит из 3 занятий по 1,5 часа каждое.&nbsp;На мастер-классе вы познакомитесь с патиной и освоите несколько техник золочения. На последнем занятии вы закрепите свои навыки и самостоятельно задекорируете деревянную шкатулку.&nbsp;</p>\r\n<p>Стоимость курса&nbsp;\"Техники золочения\" - 3500 рублей. В стоимость включены все необходимые материалы и инструменты, в том числе деревянная шкатулка.</p>\r\n<p>Первое занятие состоится уже 7 августа.</p>\r\n<p>Количество мест ограничено!</p>\r\n<p>Запишитесь по телефону:&nbsp;<strong>355-18-44.</strong></p>\r\n<p>Хорошее настроение и полезные практические навыки гарантируем!</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>',NULL,NULL,3,'news',1,0,'2017-07-05 22:48:13','2017-07-05 22:50:02'),(79,'postavka-materialov-dlya-dekorativnoj-otdelki','Поставка материалов',NULL,NULL,NULL,'<p>Поставка материалов для декоративной отделки</p>',NULL,NULL,NULL,'info',1,0,'2017-07-05 23:39:48','2017-07-05 23:43:46'),(80,'shef-dizajn-proektov','Шеф-дизайн  проектов',NULL,NULL,NULL,'<p>Шеф-дизайн&nbsp;проектов</p>',NULL,NULL,NULL,'info',1,0,'2017-07-05 23:40:07','2017-07-05 23:40:07'),(81,'otdelochnye-raboty-i-dekorirovanie','Отделочные работы  и декорирование',NULL,NULL,NULL,'<p>Отделочные работы&nbsp;и декорирование</p>',NULL,NULL,NULL,'info',1,0,'2017-07-05 23:40:20','2017-07-05 23:40:20'),(149,'dekorativnye-shtukaturki','Декоративные  штукатурки','штукатурка','штукатурка',NULL,NULL,NULL,2,NULL,'category',0,0,'2017-07-09 16:56:20','2017-08-18 19:56:55'),(150,'lepnina','Лепнина','Лепнина','Лепнина',NULL,NULL,NULL,84,NULL,'category',0,0,'2017-07-09 16:57:51','2017-08-04 21:58:32'),(151,'derevyannyj-dekor','Деревянный декор','Деревянный декор','Деревянный декор',NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 16:58:29','2017-07-09 16:58:29'),(152,'oboi','Обои',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 16:58:37','2017-07-09 16:58:37'),(153,'keramicheskaya-plitka','Керамическая плитка',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 16:59:02','2017-07-09 16:59:02'),(154,'dekorativnye-shtukaturki','Декоративные штукатурки',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 17:26:00','2017-07-16 09:20:02'),(155,'venecianskaya-shtukaturka','Венецианская штукатурка',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 17:26:19','2017-07-09 17:26:19'),(156,'fakturnye-pokrytiya','Фактурные покрытия',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 17:26:45','2017-07-09 17:26:45'),(157,'pokrytiya-dlya-vnutrennej-otdelki','Покрытия для внутренней отделки (\"обычной\" стойкости)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 17:28:02','2017-07-11 14:27:07'),(158,'pokrytiya-dlya-fasadov-obshestvennyh-pomeshenij-agressivnyh-sred','Покрытия для фасадов,  общественных помещений, агрессивных сред',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-09 17:28:24','2017-07-11 14:27:22'),(164,'catalog','Каталог',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,'2017-07-10 23:42:43','2017-07-10 23:42:43'),(165,'pokrytiya-dlya-pola-i-cementa','Покрытия для пола и цемента',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:27:35','2017-07-11 14:27:35'),(166,'propitki-i-zashitnye-sostavy-dlya-agressivnyh-sred','Пропитки и защитные составы для агрессивных сред',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:27:51','2017-07-11 14:27:51'),(167,'sistema-kolerovki','Система колеровки',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:28:18','2017-07-11 14:28:18'),(168,'gruntovki','Грунтовки',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:29:19','2017-07-11 14:29:19'),(169,'lessiruyushie-finitury-i-laki','Лессирующие финитуры и  лаки',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:29:33','2017-07-11 14:29:33'),(170,'zashitnye-sostavy-i-finishnye-pokrytiya','Защитные составы и финишные покрытия',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:29:50','2017-07-11 14:29:50'),(171,'dekorativnye-shtukaturki-i-kraski','Декоративные штукатурки и краски',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:32:11','2017-07-11 14:32:11'),(172,'zashitnye-sostavy-i-finishnye-pokrytiya','Защитные составы и финишные покрытия',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:32:35','2017-07-11 14:32:35'),(173,'gruntovki','Грунтовки',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-11 14:32:48','2017-07-11 14:32:48'),(174,'intonachino-minerale','Intonachino Minerale',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'product',0,0,'2017-07-11 14:47:39','2017-07-11 14:47:39'),(175,'defer','Отложить',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,'2017-07-11 15:01:01','2017-07-11 15:01:01'),(176,'antica-calce-plus','Antica Calce plus',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'product',0,0,'2017-07-11 16:39:48','2017-07-11 16:39:48'),(177,'cadoro','Cadoro',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'product',0,0,'2017-07-11 16:44:47','2017-07-11 16:44:47'),(178,'decorfond','Decorfond',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'product',0,0,'2017-07-11 16:57:57','2017-07-11 16:57:57'),(179,'news','Новости',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,'2017-07-12 18:38:38','2017-07-12 18:43:05'),(180,'dekorativnye-kraski','Декоративные краски',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'category',0,0,'2017-07-16 09:17:58','2017-07-16 09:17:58'),(181,'testovyy-produkt','Тестовый продукт',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'product',0,0,'2017-08-04 21:15:26','2017-08-04 21:15:26'),(182,'tonkosloynaya-shtukaturka','Тонкослойная штукатурка',NULL,NULL,NULL,NULL,NULL,0,NULL,'category',0,0,'2017-08-18 19:55:32','2017-08-18 19:55:32'),(183,'fakturnaya-shtukaturka','Фактурная штукатурка',NULL,NULL,NULL,NULL,NULL,0,NULL,'category',0,0,'2017-08-18 19:56:10','2017-08-18 19:56:10');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('admin@admin.com','$2y$10$hh3fvfMVlWKAjiocTkVIS.2RsSU/3srhwH9OROPfiSL19F4ZJiF3u','2017-06-15 22:43:10');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uploaded_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,'img/uploaded/full/2017/07/2017_07_05_16_52_32-penaty.jpg','img/uploaded/tn/2017/07/2017_07_05_16_52_32-penaty.jpg','2017_07_05_16_52_32-penaty.jpg',1,'2017-07-05 16:52:32','2017-07-05 16:52:32'),(2,'img/uploaded/full/2017/07/2017_07_05_17_19_37-blobid1499275170434.jpg','img/uploaded/tn/2017/07/2017_07_05_17_19_37-blobid1499275170434.jpg','2017_07_05_17_19_37-blobid1499275170434.jpg',1,'2017-07-05 17:19:37','2017-07-05 17:19:37'),(3,'img/uploaded/full/2017/07/2017_07_05_18_31_07-Iukuhni.jpg','img/uploaded/tn/2017/07/2017_07_05_18_31_07-Iukuhni.jpg','2017_07_05_18_31_07-Iukuhni.jpg',1,'2017-07-05 18:31:07','2017-07-05 18:31:07'),(4,'img/uploaded/full/2017/07/2017_07_05_18_31_07-fish.jpg','img/uploaded/tn/2017/07/2017_07_05_18_31_07-fish.jpg','2017_07_05_18_31_07-fish.jpg',1,'2017-07-05 18:31:08','2017-07-05 18:31:08'),(5,'img/uploaded/full/2017/07/2017_07_05_18_31_08-Susert_sad.jpg','img/uploaded/tn/2017/07/2017_07_05_18_31_08-Susert_sad.jpg','2017_07_05_18_31_08-Susert_sad.jpg',1,'2017-07-05 18:31:08','2017-07-05 18:31:08'),(6,'img/uploaded/full/2017/07/2017_07_05_18_31_08-t_87bec31d371d838e9451616afbfe0226.jpg','img/uploaded/tn/2017/07/2017_07_05_18_31_08-t_87bec31d371d838e9451616afbfe0226.jpg','2017_07_05_18_31_08-t_87bec31d371d838e9451616afbfe0226.jpg',1,'2017-07-05 18:31:08','2017-07-05 18:31:08'),(7,'img/uploaded/full/2017/07/2017_07_05_18_45_53-direkto_r2.jpg','img/uploaded/tn/2017/07/2017_07_05_18_45_53-direkto_r2.jpg','2017_07_05_18_45_53-direkto_r2.jpg',1,'2017-07-05 18:45:53','2017-07-05 18:45:53'),(8,'img/uploaded/full/2017/07/2017_07_05_19_34_59-slider2.jpg','img/uploaded/tn/2017/07/2017_07_05_19_34_59-slider2.jpg','2017_07_05_19_34_59-slider2.jpg',1,'2017-07-05 19:34:59','2017-07-05 19:34:59'),(9,'img/uploaded/full/2017/07/2017_07_05_19_34_59-slider.jpg','img/uploaded/tn/2017/07/2017_07_05_19_34_59-slider.jpg','2017_07_05_19_34_59-slider.jpg',1,'2017-07-05 19:34:59','2017-07-05 19:34:59'),(10,'img/uploaded/full/2017/07/2017_07_05_19_35_00-slider4.jpg','img/uploaded/tn/2017/07/2017_07_05_19_35_00-slider4.jpg','2017_07_05_19_35_00-slider4.jpg',1,'2017-07-05 19:35:00','2017-07-05 19:35:00'),(11,'img/uploaded/full/2017/07/2017_07_05_19_35_00-slider3.jpg','img/uploaded/tn/2017/07/2017_07_05_19_35_00-slider3.jpg','2017_07_05_19_35_00-slider3.jpg',1,'2017-07-05 19:35:00','2017-07-05 19:35:00'),(12,'img/uploaded/full/2017/07/2017_07_05_19_35_01-slider5.jpg','img/uploaded/tn/2017/07/2017_07_05_19_35_01-slider5.jpg','2017_07_05_19_35_01-slider5.jpg',1,'2017-07-05 19:35:01','2017-07-05 19:35:01'),(13,'img/uploaded/full/2017/07/2017_07_05_21_25_03-slider_circle2.jpg','img/uploaded/tn/2017/07/2017_07_05_21_25_03-slider_circle2.jpg','2017_07_05_21_25_03-slider_circle2.jpg',1,'2017-07-05 21:25:03','2017-07-05 21:25:03'),(14,'img/uploaded/full/2017/07/2017_07_05_21_25_04-slider_circle.jpg','img/uploaded/tn/2017/07/2017_07_05_21_25_04-slider_circle.jpg','2017_07_05_21_25_04-slider_circle.jpg',1,'2017-07-05 21:25:04','2017-07-05 21:25:04'),(15,'img/uploaded/full/2017/07/2017_07_05_21_25_04-slider_circle3.jpg','img/uploaded/tn/2017/07/2017_07_05_21_25_04-slider_circle3.jpg','2017_07_05_21_25_04-slider_circle3.jpg',1,'2017-07-05 21:25:04','2017-07-05 21:25:04'),(16,'img/uploaded/full/2017/07/2017_07_05_21_25_04-slider_circle4.jpg','img/uploaded/tn/2017/07/2017_07_05_21_25_04-slider_circle4.jpg','2017_07_05_21_25_04-slider_circle4.jpg',1,'2017-07-05 21:25:05','2017-07-05 21:25:05'),(17,'img/uploaded/full/2017/07/2017_07_05_22_01_33-blobid1499292088495.jpg','img/uploaded/tn/2017/07/2017_07_05_22_01_33-blobid1499292088495.jpg','2017_07_05_22_01_33-blobid1499292088495.jpg',1,'2017-07-05 22:01:33','2017-07-05 22:01:33'),(18,'img/uploaded/full/2017/07/2017_07_05_22_03_00-blobid1499292177338.jpg','img/uploaded/tn/2017/07/2017_07_05_22_03_00-blobid1499292177338.jpg','2017_07_05_22_03_00-blobid1499292177338.jpg',1,'2017-07-05 22:03:00','2017-07-05 22:03:00'),(19,'img/uploaded/full/2017/07/2017_07_05_22_04_45-blobid1499292282026.jpg','img/uploaded/tn/2017/07/2017_07_05_22_04_45-blobid1499292282026.jpg','2017_07_05_22_04_45-blobid1499292282026.jpg',1,'2017-07-05 22:04:45','2017-07-05 22:04:45'),(21,'img/uploaded/full/2017/07/2017_07_05_22_44_28-blobid1499294665214.jpg','img/uploaded/tn/2017/07/2017_07_05_22_44_28-blobid1499294665214.jpg','2017_07_05_22_44_28-blobid1499294665214.jpg',1,'2017-07-05 22:44:28','2017-07-05 22:44:28'),(22,'img/uploaded/full/2017/07/2017_07_05_22_46_48-blobid1499294805252.jpg','img/uploaded/tn/2017/07/2017_07_05_22_46_48-blobid1499294805252.jpg','2017_07_05_22_46_48-blobid1499294805252.jpg',1,'2017-07-05 22:46:49','2017-07-05 22:46:49'),(23,'img/uploaded/full/2017/07/2017_07_06_12_48_49-design_background.png','img/uploaded/tn/2017/07/2017_07_06_12_48_49-design_background.png','2017_07_06_12_48_49-design_background.png',1,'2017-07-06 12:48:50','2017-07-06 12:48:50'),(35,'img/uploaded/full/2017/07/2017_07_06_18_21_58-product.jpg','img/uploaded/tn/2017/07/2017_07_06_18_21_58-product.jpg','2017_07_06_18_21_58-product.jpg',1,'2017-07-06 18:21:58','2017-07-06 18:21:58'),(54,'img/uploaded/full/2017/07/2017_07_09_10_21_11-blobid1499595667552.jpg','img/uploaded/tn/2017/07/2017_07_09_10_21_11-blobid1499595667552.jpg','2017_07_09_10_21_11-blobid1499595667552.jpg',1,'2017-07-09 10:21:11','2017-07-09 10:21:11'),(55,'img/uploaded/full/2017/07/2017_07_09_16_52_41-blobid1499619156390.jpg','img/uploaded/tn/2017/07/2017_07_09_16_52_41-blobid1499619156390.jpg','2017_07_09_16_52_41-blobid1499619156390.jpg',1,'2017-07-09 16:52:41','2017-07-09 16:52:41'),(56,'img/uploaded/full/2017/07/2017_07_09_16_59_01-blobid1499619537877.jpg','img/uploaded/tn/2017/07/2017_07_09_16_59_01-blobid1499619537877.jpg','2017_07_09_16_59_01-blobid1499619537877.jpg',1,'2017-07-09 16:59:01','2017-07-09 16:59:01'),(57,'img/uploaded/full/2017/07/2017_07_10_17_26_31-blobid1499707587673.jpg','img/uploaded/tn/2017/07/2017_07_10_17_26_31-blobid1499707587673.jpg','2017_07_10_17_26_31-blobid1499707587673.jpg',1,'2017-07-10 17:26:32','2017-07-10 17:26:32'),(58,'img/uploaded/full/2017/07/2017_07_10_23_10_49-color.jpg','img/uploaded/tn/2017/07/2017_07_10_23_10_49-color.jpg','2017_07_10_23_10_49-color.jpg',1,'2017-07-10 23:10:49','2017-07-10 23:10:49'),(59,'img/uploaded/full/2017/07/2017_07_11_14_42_13-GMcVelaturaA369.JPG','img/uploaded/tn/2017/07/2017_07_11_14_42_13-GMcVelaturaA369.JPG','2017_07_11_14_42_13-GMcVelaturaA369.JPG',1,'2017-07-11 14:42:14','2017-07-11 14:42:14'),(60,'img/uploaded/full/2017/07/2017_07_11_14_42_42-GFcVelaturaA369A366.JPG','img/uploaded/tn/2017/07/2017_07_11_14_42_42-GFcVelaturaA369A366.JPG','2017_07_11_14_42_42-GFcVelaturaA369A366.JPG',1,'2017-07-11 14:42:42','2017-07-11 14:42:42'),(61,'img/uploaded/full/2017/07/2017_07_11_14_43_06-GFcVelaturaA366A506.JPG','img/uploaded/tn/2017/07/2017_07_11_14_43_06-GFcVelaturaA366A506.JPG','2017_07_11_14_43_06-GFcVelaturaA366A506.JPG',1,'2017-07-11 14:43:06','2017-07-11 14:43:06'),(62,'img/uploaded/full/2017/07/2017_07_11_14_47_37-GMc501506.JPG','img/uploaded/tn/2017/07/2017_07_11_14_47_37-GMc501506.JPG','2017_07_11_14_47_37-GMc501506.JPG',1,'2017-07-11 14:47:37','2017-07-11 14:47:37'),(63,'img/uploaded/full/2017/07/2017_07_11_16_38_48-4011.JPG','img/uploaded/tn/2017/07/2017_07_11_16_38_48-4011.JPG','2017_07_11_16_38_48-4011.JPG',1,'2017-07-11 16:38:48','2017-07-11 16:38:48'),(64,'img/uploaded/full/2017/07/2017_07_11_16_38_55-4654.JPG','img/uploaded/tn/2017/07/2017_07_11_16_38_55-4654.JPG','2017_07_11_16_38_55-4654.JPG',1,'2017-07-11 16:38:55','2017-07-11 16:38:55'),(65,'img/uploaded/full/2017/07/2017_07_11_16_39_02-4026369.JPG','img/uploaded/tn/2017/07/2017_07_11_16_39_02-4026369.JPG','2017_07_11_16_39_02-4026369.JPG',1,'2017-07-11 16:39:02','2017-07-11 16:39:02'),(66,'img/uploaded/full/2017/07/2017_07_11_16_39_24-antica-calce-plus_banka.jpg','img/uploaded/tn/2017/07/2017_07_11_16_39_24-antica-calce-plus_banka.jpg','2017_07_11_16_39_24-antica-calce-plus_banka.jpg',1,'2017-07-11 16:39:24','2017-07-11 16:39:24'),(67,'img/uploaded/full/2017/07/2017_07_11_16_41_53-001.jpg','img/uploaded/tn/2017/07/2017_07_11_16_41_53-001.jpg','2017_07_11_16_41_53-001.jpg',1,'2017-07-11 16:41:53','2017-07-11 16:41:53'),(68,'img/uploaded/full/2017/07/2017_07_11_16_41_53-037.jpg','img/uploaded/tn/2017/07/2017_07_11_16_41_53-037.jpg','2017_07_11_16_41_53-037.jpg',1,'2017-07-11 16:41:53','2017-07-11 16:41:53'),(69,'img/uploaded/full/2017/07/2017_07_11_16_41_54-045.jpg','img/uploaded/tn/2017/07/2017_07_11_16_41_54-045.jpg','2017_07_11_16_41_54-045.jpg',1,'2017-07-11 16:41:54','2017-07-11 16:41:54'),(70,'img/uploaded/full/2017/07/2017_07_11_16_41_54-055.jpg','img/uploaded/tn/2017/07/2017_07_11_16_41_54-055.jpg','2017_07_11_16_41_54-055.jpg',1,'2017-07-11 16:41:54','2017-07-11 16:41:54'),(71,'img/uploaded/full/2017/07/2017_07_11_16_41_54-038.jpg','img/uploaded/tn/2017/07/2017_07_11_16_41_54-038.jpg','2017_07_11_16_41_54-038.jpg',1,'2017-07-11 16:41:54','2017-07-11 16:41:54'),(72,'img/uploaded/full/2017/07/2017_07_11_16_44_04-cadoro.jpg','img/uploaded/tn/2017/07/2017_07_11_16_44_04-cadoro.jpg','2017_07_11_16_44_04-cadoro.jpg',1,'2017-07-11 16:44:04','2017-07-11 16:44:04'),(73,'img/uploaded/full/2017/07/2017_07_11_16_55_33-decorfond.jpg','img/uploaded/tn/2017/07/2017_07_11_16_55_33-decorfond.jpg','2017_07_11_16_55_33-decorfond.jpg',1,'2017-07-11 16:55:33','2017-07-11 16:55:33'),(74,'img/uploaded/full/2017/07/2017_07_25_22_01_17-brand.png','img/uploaded/tn/2017/07/2017_07_25_22_01_17-brand.png','2017_07_25_22_01_17-brand.png',1,'2017-07-25 22:01:17','2017-07-25 22:01:17'),(75,'img/uploaded/full/2017/07/2017_07_25_22_01_17-brand2.png','img/uploaded/tn/2017/07/2017_07_25_22_01_17-brand2.png','2017_07_25_22_01_17-brand2.png',1,'2017-07-25 22:01:17','2017-07-25 22:01:17'),(76,'img/uploaded/full/2017/07/2017_07_25_22_01_18-brand4.png','img/uploaded/tn/2017/07/2017_07_25_22_01_18-brand4.png','2017_07_25_22_01_18-brand4.png',1,'2017-07-25 22:01:18','2017-07-25 22:01:18'),(77,'img/uploaded/full/2017/07/2017_07_25_22_01_18-brand3.png','img/uploaded/tn/2017/07/2017_07_25_22_01_18-brand3.png','2017_07_25_22_01_18-brand3.png',1,'2017-07-25 22:01:18','2017-07-25 22:01:18'),(78,'img/uploaded/full/2017/07/2017_07_25_22_01_19-brand5.png','img/uploaded/tn/2017/07/2017_07_25_22_01_19-brand5.png','2017_07_25_22_01_19-brand5.png',1,'2017-07-25 22:01:19','2017-07-25 22:01:19'),(79,'img/uploaded/full/2017/07/2017_07_30_20_20_04-category_item.jpg','img/uploaded/tn/2017/07/2017_07_30_20_20_04-category_item.jpg','2017_07_30_20_20_04-category_item.jpg',NULL,'2017-07-30 20:20:04','2017-07-30 20:20:04'),(80,'img/uploaded/full/2017/07/2017_07_30_20_20_53-services3.jpg','img/uploaded/tn/2017/07/2017_07_30_20_20_53-services3.jpg','2017_07_30_20_20_53-services3.jpg',NULL,'2017-07-30 20:20:53','2017-07-30 20:20:53'),(83,'img/uploaded/full/2017/07/2017_07_30_22_04_01-slider4.jpg','img/uploaded/tn/2017/07/2017_07_30_22_04_01-slider4.jpg','2017_07_30_22_04_01-slider4.jpg',1,'2017-07-30 22:04:01','2017-07-30 22:04:01'),(84,'img/uploaded/full/2017/08/2017_08_04_21_58_31-imagetools0.jpg','img/uploaded/tn/2017/08/2017_08_04_21_58_31-imagetools0.jpg','2017_08_04_21_58_31-imagetools0.jpg',1,'2017-08-04 21:58:31','2017-08-04 21:58:31'),(85,'img/uploaded/full/2017/08/2017_08_04_22_11_00-product.jpg','img/uploaded/tn/2017/08/2017_08_04_22_11_00-product.jpg','2017_08_04_22_11_00-product.jpg',1,'2017-08-04 22:11:00','2017-08-04 22:11:00'),(86,'img/uploaded/full/2017/08/2017_08_12_15_30_42-blobid1502551839871.jpg','img/uploaded/tn/2017/08/2017_08_12_15_30_42-blobid1502551839871.jpg','2017_08_12_15_30_42-blobid1502551839871.jpg',1,'2017-08-12 15:30:42','2017-08-12 15:30:42'),(88,'img/uploaded/full/2017/08/2017_08_12_17_07_50-imagetools0.jpg','img/uploaded/tn/2017/08/2017_08_12_17_07_50-imagetools0.jpg','2017_08_12_17_07_50-imagetools0.jpg',1,'2017-08-12 17:07:50','2017-08-12 17:07:50'),(90,'img/uploaded/full/2017/08/2017_08_12_17_10_35-imagetools0.jpg','img/uploaded/tn/2017/08/2017_08_12_17_10_35-imagetools0.jpg','2017_08_12_17_10_35-imagetools0.jpg',1,'2017-08-12 17:10:35','2017-08-12 17:10:35'),(91,'img/uploaded/full/2017/08/2017_08_17_16_19_57-foto 2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_19_57-foto 2.jpg','2017_08_17_16_19_57-foto 2.jpg',1,'2017-08-17 16:19:58','2017-08-17 16:19:58'),(92,'img/uploaded/full/2017/08/2017_08_17_16_19_57-Гостиная 4.jpg','img/uploaded/tn/2017/08/2017_08_17_16_19_57-Гостиная 4.jpg','2017_08_17_16_19_57-Гостиная 4.jpg',1,'2017-08-17 16:19:58','2017-08-17 16:19:58'),(93,'img/uploaded/full/2017/08/2017_08_17_16_19_59-декоративные покрытия.jpg','img/uploaded/tn/2017/08/2017_08_17_16_19_59-декоративные покрытия.jpg','2017_08_17_16_19_59-декоративные покрытия.jpg',1,'2017-08-17 16:19:59','2017-08-17 16:19:59'),(94,'img/uploaded/full/2017/08/2017_08_17_16_20_00-обои.jpg','img/uploaded/tn/2017/08/2017_08_17_16_20_00-обои.jpg','2017_08_17_16_20_00-обои.jpg',1,'2017-08-17 16:20:00','2017-08-17 16:20:00'),(95,'img/uploaded/full/2017/08/2017_08_17_16_20_00-сантехника.jpg','img/uploaded/tn/2017/08/2017_08_17_16_20_00-сантехника.jpg','2017_08_17_16_20_00-сантехника.jpg',1,'2017-08-17 16:20:00','2017-08-17 16:20:00'),(96,'img/uploaded/full/2017/08/2017_08_17_16_20_01-художественные работы.jpg','img/uploaded/tn/2017/08/2017_08_17_16_20_01-художественные работы.jpg','2017_08_17_16_20_01-художественные работы.jpg',1,'2017-08-17 16:20:02','2017-08-17 16:20:02'),(97,'img/uploaded/full/2017/08/2017_08_17_16_24_43-г11_2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_24_43-г11_2.jpg','2017_08_17_16_24_43-г11_2.jpg',1,'2017-08-17 16:24:43','2017-08-17 16:24:43'),(98,'img/uploaded/full/2017/08/2017_08_17_16_24_43-stolovaja2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_24_43-stolovaja2.jpg','2017_08_17_16_24_43-stolovaja2.jpg',1,'2017-08-17 16:24:43','2017-08-17 16:24:43'),(99,'img/uploaded/full/2017/08/2017_08_17_16_24_44-мозаичные полы_2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_24_44-мозаичные полы_2.jpg','2017_08_17_16_24_44-мозаичные полы_2.jpg',1,'2017-08-17 16:24:44','2017-08-17 16:24:44'),(100,'img/uploaded/full/2017/08/2017_08_17_16_24_45-плитка_2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_24_45-плитка_2.jpg','2017_08_17_16_24_45-плитка_2.jpg',1,'2017-08-17 16:24:45','2017-08-17 16:24:45'),(101,'img/uploaded/full/2017/08/2017_08_17_16_24_45-лепной декор_2.jpg','img/uploaded/tn/2017/08/2017_08_17_16_24_45-лепной декор_2.jpg','2017_08_17_16_24_45-лепной декор_2.jpg',1,'2017-08-17 16:24:45','2017-08-17 16:24:45'),(102,'img/uploaded/full/2017/08/2017_08_19_14_31_35-design_photo.jpg','img/uploaded/tn/2017/08/2017_08_19_14_31_35-design_photo.jpg','2017_08_19_14_31_35-design_photo.jpg',1,'2017-08-19 14:31:35','2017-08-19 14:31:35'),(103,'img/uploaded/full/2017/08/2017_08_19_19_01_40-design_studio.jpg','img/uploaded/tn/2017/08/2017_08_19_19_01_40-design_studio.jpg','2017_08_19_19_01_40-design_studio.jpg',1,'2017-08-19 19:01:40','2017-08-19 19:01:40'),(104,'img/uploaded/full/2017/08/2017_08_19_19_01_40-design_studio2.jpg','img/uploaded/tn/2017/08/2017_08_19_19_01_40-design_studio2.jpg','2017_08_19_19_01_40-design_studio2.jpg',1,'2017-08-19 19:01:40','2017-08-19 19:01:40'),(105,'img/uploaded/full/2017/08/2017_08_19_19_01_41-design_studio4.jpg','img/uploaded/tn/2017/08/2017_08_19_19_01_41-design_studio4.jpg','2017_08_19_19_01_41-design_studio4.jpg',1,'2017-08-19 19:01:41','2017-08-19 19:01:41'),(106,'img/uploaded/full/2017/08/2017_08_19_19_01_41-design_studio3.jpg','img/uploaded/tn/2017/08/2017_08_19_19_01_41-design_studio3.jpg','2017_08_19_19_01_41-design_studio3.jpg',1,'2017-08-19 19:01:41','2017-08-19 19:01:41');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_categories`
--

DROP TABLE IF EXISTS `product_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_categories`
--

LOCK TABLES `product_categories` WRITE;
/*!40000 ALTER TABLE `product_categories` DISABLE KEYS */;
INSERT INTO `product_categories` VALUES (1,4,7,NULL,NULL),(2,3,7,NULL,NULL),(4,2,6,NULL,NULL),(11,10,3,NULL,NULL),(14,7,12,NULL,NULL),(16,7,11,NULL,NULL),(17,6,12,NULL,NULL),(18,6,11,NULL,NULL),(19,9,11,NULL,NULL),(20,11,11,NULL,NULL),(21,12,11,NULL,NULL),(22,13,11,NULL,NULL),(23,14,11,NULL,NULL),(24,15,11,NULL,NULL),(25,16,11,NULL,NULL),(27,18,19,NULL,NULL),(28,15,20,NULL,NULL),(29,18,11,NULL,NULL),(30,19,11,NULL,NULL),(32,17,22,NULL,NULL);
/*!40000 ALTER TABLE `product_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_colors`
--

DROP TABLE IF EXISTS `product_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_colors` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `color_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=197 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_colors`
--

LOCK TABLES `product_colors` WRITE;
/*!40000 ALTER TABLE `product_colors` DISABLE KEYS */;
INSERT INTO `product_colors` VALUES (170,61,15,'A366, A506','2017-08-14 22:08:31','2017-08-14 22:08:31'),(171,60,15,'A369, A366','2017-08-14 22:08:31','2017-08-14 22:08:31'),(172,59,15,'A369','2017-08-14 22:08:31','2017-08-14 22:08:31'),(173,65,16,'4026','2017-08-14 22:09:23','2017-08-14 22:09:23'),(174,64,16,'4654','2017-08-14 22:09:23','2017-08-14 22:09:23'),(175,63,16,'4011','2017-08-14 22:09:23','2017-08-14 22:09:23'),(186,73,18,'0111','2017-08-14 22:09:43','2017-08-14 22:09:43'),(192,71,17,'038','2017-08-14 23:05:43','2017-08-14 23:05:43'),(193,70,17,'055','2017-08-14 23:05:43','2017-08-14 23:05:43'),(194,69,17,'045','2017-08-14 23:05:43','2017-08-14 23:05:43'),(195,68,17,'037','2017-08-14 23:05:43','2017-08-14 23:05:43'),(196,67,17,'001','2017-08-14 23:05:43','2017-08-14 23:05:43');
/*!40000 ALTER TABLE `product_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_images`
--

DROP TABLE IF EXISTS `product_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_images`
--

LOCK TABLES `product_images` WRITE;
/*!40000 ALTER TABLE `product_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_photos`
--

DROP TABLE IF EXISTS `product_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_photos` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `photo_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=355 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_photos`
--

LOCK TABLES `product_photos` WRITE;
/*!40000 ALTER TABLE `product_photos` DISABLE KEYS */;
INSERT INTO `product_photos` VALUES (346,15,35,'2017-08-14 22:08:31','2017-08-14 22:08:31'),(347,15,62,'2017-08-14 22:08:31','2017-08-14 22:08:31'),(348,16,66,'2017-08-14 22:09:23','2017-08-14 22:09:23'),(351,18,73,'2017-08-14 22:09:43','2017-08-14 22:09:43'),(352,19,85,'2017-08-14 22:09:46','2017-08-14 22:09:46'),(354,17,72,'2017-08-14 23:05:43','2017-08-14 23:05:43');
/*!40000 ALTER TABLE `product_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `short_info` text COLLATE utf8mb4_unicode_ci,
  `basement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `instrument` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `consumption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tab_description` text COLLATE utf8mb4_unicode_ci,
  `tab_instruction` text COLLATE utf8mb4_unicode_ci,
  `tab_video` text COLLATE utf8mb4_unicode_ci,
  `dilution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `producer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available` smallint(6) NOT NULL DEFAULT '0',
  `published` smallint(6) NOT NULL DEFAULT '0',
  `hit` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `volume` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (15,NULL,174,'INTONACHINO MINERALE -это настенное покрытие для внешних и внутренних работ, на основе минеральных связующих веществ, которое позволяет добиться отделки очень подобной цветной штукатурке, которая представлена на фасадах зданий Венеции, произведено в округе Veneto, применение которого основано на многолетнем опыте использования натуральных штукатурок в Венеции.','вода','продукт может быть нанесен с помощью мастерка из нержавеющей стали или из пластика в горизонтальном или вертикальном направлении, либо круговыми движениями.','1л','в 1 слой - на гладкую и ровную поверхность; в 2 слоя- на поверхность грубую и необработанную.','<p>&nbsp;</p>\r\n<p>INTONACHINO MINERALE -это настенное покрытие для внешних и внутренних работ, на основе минеральных связующих веществ, которое позволяет добиться отделки очень подобной цветной штукатурке, которая представлена на фасадах зданий Венеции, произведено в округе Veneto, применение которого основано на многолетнем опыте использования натуральных штукатурок в Венеции. Поставляется INTONACHINO MINERALE двух разновидностей с гранулированным кварцевым наполнителем 0,7 мм (серия 939) и 1,4 мм (серия 938), что позволяет достичь, различных декоративных эффектов: &laquo;под кору дуба&raquo;, &laquo;с трещинками&raquo;, завитками и проч.</p>\r\n<p>INTONACHINO MINERALE &ndash; наносится слоем достаточно плотным, оказывая высокое сопротивление к атмосферным осадкам и обладает повышенной воздухопроницаемостью. Этот результат особенно подходит для работ в регионах с неблагоприятными климатическими условиями.</p>\r\n<p>Связующее вещество - известь воздушного свойства.&nbsp;</p>','<p>&nbsp;</p>\r\n<p>INTONACHINO MINERALE -это настенное покрытие для внешних и внутренних работ, на основе минеральных связующих веществ, которое позволяет добиться отделки очень подобной цветной штукатурке, которая представлена на фасадах зданий Венеции, произведено в округе Veneto, применение которого основано на многолетнем опыте использования натуральных штукатурок в Венеции. Поставляется INTONACHINO MINERALE двух разновидностей с гранулированным кварцевым наполнителем 0,7 мм (серия 939) и 1,4 мм (серия 938), что позволяет достичь, различных декоративных эффектов: &laquo;под кору дуба&raquo;, &laquo;с трещинками&raquo;, завитками и проч.</p>\r\n<p>INTONACHINO MINERALE &ndash; наносится слоем достаточно плотным, оказывая высокое сопротивление к атмосферным осадкам и обладает повышенной воздухопроницаемостью. Этот результат особенно подходит для работ в регионах с неблагоприятными климатическими условиями.</p>\r\n<p>Связующее вещество - известь воздушного свойства.&nbsp;</p>','<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/jpFxRkDrtVI?ecver=1\" frameborder=\"0\" allowfullscreen></iframe>','готов к употреблению.','San Marco',0,0,1,'2017-07-11 14:47:39','2017-08-14 22:08:31','[\"1\",\"3\",\"4\"]'),(16,NULL,176,'\"ANTICA CALCE PLUS\" – настенное декоративное известковое покрытие для внутренних работ с эффектом состаренной штукатурки. Обладает высокой пластичностью, прекрасно глянцуется, позволяет создать тонкую рельефную штукатурку, сочетающую бархатистость углубленных частей и глянец выступающих поверхностей.','вода','валик или кисточка','1л. на 4 м. кв.','нанесение в 1-2 слоя.','<p>&nbsp;</p>\r\n<p>\"ANTICA CALCE PLUS\" &ndash; настенное декоративное известковое покрытие для внутренних работ с эффектом состаренной штукатурки. Обладает высокой пластичностью, прекрасно глянцуется, позволяет создать тонкую рельефную штукатурку, сочетающую бархатистость углубленных частей и глянец выступающих поверхностей.</p>\r\n<p><br />Дополнительно декорируется финишными красками \"CADORO\", \"VELATURA\", \"PERLACEO\". Нанесенное покрытие образует слой значительной толщины с повышенной воздухопроницаемостью.. Натуральные связующие вещества воздушного свойства - известь.</p>','<p>&nbsp;</p>\r\n<p>\"ANTICA CALCE PLUS\" &ndash; настенное декоративное известковое покрытие для внутренних работ с эффектом состаренной штукатурки. Обладает высокой пластичностью, прекрасно глянцуется, позволяет создать тонкую рельефную штукатурку, сочетающую бархатистость углубленных частей и глянец выступающих поверхностей.</p>\r\n<p><br />Дополнительно декорируется финишными красками \"CADORO\", \"VELATURA\", \"PERLACEO\". Нанесенное покрытие образует слой значительной толщины с повышенной воздухопроницаемостью.. Натуральные связующие вещества воздушного свойства - известь.</p>','<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/jpFxRkDrtVI?ecver=1\" frameborder=\"0\" allowfullscreen></iframe>','вода','San Marco',0,0,1,'2017-07-11 16:39:48','2017-08-14 22:09:23','[\"4\",\"10\"]'),(17,NULL,177,'Изящное декоративное покрытие с золотыми или серебряными частицами, с муаровым эффектом для внутренних работ. CADORO создает на стенах утонченный рисунок подобный мягкому, переливчатому шелку. Натуральные связующие вещества - акриловый сополимер в водной эмульсии.','вода','шпатель','4-6 кв.м./ л. в два слоя.','2','<p>&nbsp;</p>\r\n<p>Изящное декоративное покрытие с золотыми или серебряными частицами, с муаровым эффектом для внутренних работ. CADORO создает на стенах утонченный рисунок подобный мягкому, переливчатому шелку. Натуральные связующие вещества - акриловый сополимер в водной эмульсии. Ca\'D\'Oro (Золотой дом) - одно из красивейших палаццо, расположенных на Гранд Канал в Венеции.</p>\r\n<p>По мотивам средневековой золотой росписи и создано это изящное декоративное покрытие с золотыми или серебряными частичками, &nbsp;муаровым эффектом. Акриловая штукатурка на водной основе &nbsp;CADORO поставляется в базах ORO (золото) и Argento (серебро). CADOO - простота в нанесении и реставрации. Она долговечна, моется мыльным раствором.</p>','<p>&nbsp;</p>\r\n<p>Изящное декоративное покрытие с золотыми или серебряными частицами, с муаровым эффектом для внутренних работ. CADORO создает на стенах утонченный рисунок подобный мягкому, переливчатому шелку. Натуральные связующие вещества - акриловый сополимер в водной эмульсии. Ca\'D\'Oro (Золотой дом) - одно из красивейших палаццо, расположенных на Гранд Канал в Венеции.</p>\r\n<p>По мотивам средневековой золотой росписи и создано это изящное декоративное покрытие с золотыми или серебряными частичками, &nbsp;муаровым эффектом. Акриловая штукатурка на водной основе &nbsp;CADORO поставляется в базах ORO (золото) и Argento (серебро). CADOO - простота в нанесении и реставрации. Она долговечна, моется мыльным раствором.</p>','<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/jpFxRkDrtVI?ecver=1\" frameborder=\"0\" allowfullscreen></iframe>','вода','San Marco',0,0,1,'2017-07-11 16:44:47','2017-08-14 22:09:40','[\"2\",\"9\",\"15\"]'),(18,NULL,178,'Супер моющая основа, специально разработана для подготовки настенной поверхности под нанесение продуктов DECORI CLASSICI, AQUASIL VELATURA и AQUASIL PERLACEО. Натуральные связующие вещества - акриловые сополимеры в водоэмульсионном растворе.','вода','кисточка, валик, разбрызгиватель и распылитель','вода','1-2','<p>&nbsp;</p>\r\n<p>Супер моющая основа, специально разработана для подготовки настенной поверхности под нанесение продуктов DECORI CLASSICI, AQUASIL VELATURA и AQUASIL PERLACEО. Натуральные связующие вещества - акриловые сополимеры в водоэмульсионном растворе.</p>','<p>&nbsp;</p>\r\n<p>Супер моющая основа, специально разработана для подготовки настенной поверхности под нанесение продуктов DECORI CLASSICI, AQUASIL VELATURA и AQUASIL PERLACEО. Натуральные связующие вещества - акриловые сополимеры в водоэмульсионном растворе.</p>','<iframe width=\"100%\" height=\"315\" src=\"https://www.youtube.com/embed/jpFxRkDrtVI?ecver=1\" frameborder=\"0\" allowfullscreen></iframe>','вода','San Marco',0,0,1,'2017-07-11 16:57:57','2017-08-14 22:09:43','[\"2\",\"20\"]'),(19,NULL,181,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,1,'2017-08-04 21:15:26','2017-08-14 22:09:46',NULL);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'special_offer','<p><img title=\"karat-vesennjaya-akciya.jpg\" src=\"/img/uploaded/full/2017/08/2017_08_12_15_30_42-blobid1502551839871.jpg\" alt=\"\" width=\"494\" height=\"386\" /></p>\r\n<p style=\"text-align: center;\"><strong>Спец предложение!</strong></p>','2017-08-12 14:50:14','2017-08-12 15:30:44');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `photo_id` int(11) DEFAULT NULL,
  `order_num` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (3,'main2','kjhg','jkhgjhghgjhg','<p>fgjf</p>',40,NULL,'2017-06-25 18:56:22','2017-06-25 18:56:22'),(5,'interior','Интерьеры квартир.','/','<p>Личное пространство постоянно находится в динамике, изменяется, трансформируется под потребности своих хозяев. Эту особенность</p>',16,1,'2017-06-25 21:03:15','2017-07-05 23:28:17'),(6,'main','Лепнина','/','<p>Лепнина</p>',101,8,'2017-06-26 11:56:18','2017-08-17 17:11:54'),(7,'main','Краски и декоративные','/','<p>Краски и декоративные</p>',93,6,'2017-06-26 11:56:55','2017-08-17 17:11:54'),(8,'main','Обои','/','<p>Обои</p>',94,7,'2017-06-26 11:57:16','2017-08-17 17:11:54'),(9,'main','Керамическая плитка','/','<p>Керамическая плитка</p>',95,4,'2017-07-05 19:40:04','2017-08-17 17:11:54'),(10,'main','Мозаичные полы','/','<p>Мозаичные полы</p>',99,5,'2017-07-05 19:42:55','2017-08-17 17:11:54'),(11,'interior','Интерьеры коттеджей.','/','<p>Бесспорно, дизайн коттеджа имеет свои особенности.<br /> Во-первых, коттедж может включать в себя такие помещения, которые</p>',13,2,'2017-07-05 21:29:12','2017-07-05 23:28:17'),(12,'interior','Интерьеры ресторанов, кафе.','/','<p>Дизайн ресторанов и кафе должен полностью соответствовать мировоззрению<br /> их посетителей.</p>',15,3,'2017-07-05 21:29:57','2017-07-05 23:28:17'),(13,'interior','Интерьеры общественных помещений.','/','<p>У общественных помещений могут быть абсолютно разные предназначения.</p>',16,4,'2017-07-05 21:31:05','2017-07-05 23:28:17'),(14,'interior','Интересные детали в интерьере.','/','<p>Уют складывается из мелочей: каждая вещь, каждый предмет влияют на восприятие всего пространства. В данном разделе вы&nbsp;</p>',16,5,'2017-07-05 21:50:32','2017-07-05 23:29:09'),(15,'about_us','Мария Алексеевна Наумова','/','<p><strong>Мария Алексеевна Наумова</strong><br />Шеф -дизайнер, директор салона.<br />Высшее образование, УралГАХА, специальность дизайн интерьеров 2000 год.<br />Преподаватель кафедры дизайн среды УралГАХА с 2003 года. Руководитель творческого лектива с 2003 года.</p>',17,1,'2017-07-05 22:02:25','2017-07-05 22:16:37'),(16,'about_us','Анна Вадимовна Ласкина','/','<p><strong>Анна Вадимовна Ласкина</strong>, дизайнер салона \"Пенаты\".<br />Высшее образование, УралГАХА, специальность: архитектор, 2010.</p>',18,2,'2017-07-05 22:04:15','2017-07-05 22:17:01'),(17,'about_us','Ольга Александровна Бочкарева','/','<p><strong>Ольга Александровна Бочкарева</strong> Ведущий специалист салона Пенаты</p>',19,3,'2017-07-05 22:05:07','2017-07-05 22:17:15'),(18,'main','Сантехника','/','<p>Сантехника</p>',95,2,'2017-08-17 16:52:39','2017-08-17 17:11:54'),(19,'main','Гостиная','/','<p>Гостиная</p>',92,3,'2017-08-17 16:53:10','2017-08-17 17:11:54'),(20,'main','Интерьер','/','<p>Интерьер</p>',91,1,'2017-08-17 16:54:28','2017-08-17 17:11:54');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` VALUES (1,'jast.oceane@gmail.com','+1-267-993-0319','2017-06-28 16:08:19','2017-06-28 16:08:19'),(2,'cbode@nolan.info','786.624.4229 x650','2017-06-28 16:08:19','2017-06-28 16:08:19'),(3,'sanford79@bartoletti.com','781.267.9891 x7802','2017-06-28 16:08:19','2017-06-28 16:08:19'),(4,'swiza@hotmail.com','567.774.9328 x98293','2017-06-28 16:08:19','2017-06-28 16:08:19'),(5,'rolfson.orland@gmail.com','270.236.0624 x2487','2017-06-28 16:08:19','2017-06-28 16:08:19'),(6,'abshire.joe@yahoo.com','597-558-4811','2017-06-28 16:08:19','2017-06-28 16:08:19'),(7,'harvey.jermey@collier.com','+19085656059','2017-06-28 16:08:19','2017-06-28 16:08:19'),(8,'herman.edmond@rutherford.com','969-257-5956 x98937','2017-06-28 16:08:19','2017-06-28 16:08:19'),(9,'lysanne45@okon.com','1-564-330-9774 x74245','2017-06-28 16:08:19','2017-06-28 16:08:19'),(10,'ogerlach@gmail.com','902-351-2423 x1859','2017-06-28 16:08:19','2017-06-28 16:08:19'),(11,'scollins@stroman.com','1-631-946-0833','2017-06-28 16:08:19','2017-06-28 16:08:19'),(12,'jamarcus.bins@gmail.com','1-906-518-1724','2017-06-28 16:08:19','2017-06-28 16:08:19'),(13,'mheaney@gmail.com','1-813-758-3545','2017-06-28 16:08:19','2017-06-28 16:08:19'),(14,'zella50@yahoo.com','(646) 812-4646','2017-06-28 16:08:19','2017-06-28 16:08:19'),(15,'earnestine74@goodwin.com','337.207.9371 x492','2017-06-28 16:08:19','2017-06-28 16:08:19'),(16,'hillary37@hotmail.com','386-887-9540 x46548','2017-06-28 16:08:19','2017-06-28 16:08:19'),(17,'roel.hirthe@oberbrunner.com','(513) 467-6303 x6163','2017-06-28 16:08:19','2017-06-28 16:08:19'),(18,'luna16@funk.com','(863) 640-4690 x9402','2017-06-28 16:08:19','2017-06-28 16:08:19'),(19,'nina35@brakus.info','360-591-0577','2017-06-28 16:08:19','2017-06-28 16:08:19'),(20,'nicola69@gmail.com','203-357-2727','2017-06-28 16:08:19','2017-06-28 16:08:19'),(21,'durgan.kip@carter.com','(317) 790-7229 x430','2017-06-28 16:08:19','2017-06-28 16:08:19'),(22,'kbergstrom@cremin.com','+1 (720) 251-9487','2017-06-28 16:08:19','2017-06-28 16:08:19'),(23,'yundt.teagan@bartell.org','1-345-799-7644','2017-06-28 16:08:19','2017-06-28 16:08:19'),(24,'wjakubowski@yahoo.com','985.948.1406 x0754','2017-06-28 16:08:19','2017-06-28 16:08:19'),(25,'aiden24@casper.com','248-707-8565','2017-06-28 16:08:19','2017-06-28 16:08:19'),(26,'alowe@yahoo.com','931.880.8505 x7121','2017-06-28 16:08:19','2017-06-28 16:08:19'),(27,'stanton.emmy@altenwerth.com','321.627.6613 x81446','2017-06-28 16:08:19','2017-06-28 16:08:19'),(28,'caitlyn02@pollich.com','329-634-8773','2017-06-28 16:08:19','2017-06-28 16:08:19'),(29,'koss.miguel@yahoo.com','+1 (957) 247-4221','2017-06-28 16:08:19','2017-06-28 16:08:19'),(30,'scotty.prosacco@wehner.net','(937) 399-0676','2017-06-28 16:08:19','2017-06-28 16:08:19'),(31,'kautzer.junior@gmail.com','776.567.9513 x909','2017-06-28 16:08:19','2017-06-28 16:08:19'),(32,'abshire.luisa@douglas.com','+1-826-546-8020','2017-06-28 16:08:19','2017-06-28 16:08:19'),(33,'lboyle@moore.com','(397) 570-2421','2017-06-28 16:08:19','2017-06-28 16:08:19'),(34,'jett.olson@yahoo.com','+1-884-855-9782','2017-06-28 16:08:19','2017-06-28 16:08:19'),(35,'sdooley@yahoo.com','570.291.5006 x5874','2017-06-28 16:08:19','2017-06-28 16:08:19'),(36,'andreane.bernier@hotmail.com','(218) 519-5648 x5327','2017-06-28 16:08:19','2017-06-28 16:08:19'),(37,'jermain60@yahoo.com','335.248.7166','2017-06-28 16:08:19','2017-06-28 16:08:19'),(38,'wunsch.devonte@smith.com','1-278-366-5389 x9289','2017-06-28 16:08:19','2017-06-28 16:08:19'),(39,'schuppe.darby@hotmail.com','715.739.7130 x3633','2017-06-28 16:08:19','2017-06-28 16:08:19'),(40,'jeanne07@gmail.com','952-296-7828','2017-06-28 16:08:19','2017-06-28 16:08:19'),(41,'payton.berge@gmail.com','824-942-9831 x24129','2017-06-28 16:08:19','2017-06-28 16:08:19'),(42,'gbahringer@mertz.com','935.944.7434 x57520','2017-06-28 16:08:19','2017-06-28 16:08:19'),(43,'celestino.koepp@hotmail.com','1-597-692-7326','2017-06-28 16:08:19','2017-06-28 16:08:19'),(44,'crist.ella@hotmail.com','839.516.6192 x16566','2017-06-28 16:08:19','2017-06-28 16:08:19'),(45,'hackett.shanel@hotmail.com','(945) 904-1994','2017-06-28 16:08:19','2017-06-28 16:08:19'),(46,'norma.vonrueden@gmail.com','(841) 715-4860','2017-06-28 16:08:19','2017-06-28 16:08:19'),(47,'jlangworth@romaguera.net','667.966.0303 x686','2017-06-28 16:08:19','2017-06-28 16:08:19'),(48,'garry.strosin@yahoo.com','+1-601-742-6884','2017-06-28 16:08:19','2017-06-28 16:08:19'),(49,'romaguera.cindy@orn.com','(328) 923-8588 x2337','2017-06-28 16:08:19','2017-06-28 16:08:19'),(50,'pjacobi@gmail.com','1-393-504-9051','2017-06-28 16:08:19','2017-06-28 16:08:19'),(51,'lourdes75@yahoo.com','362.972.4789','2017-06-28 16:08:19','2017-06-28 16:08:19'),(52,'jamie25@yahoo.com','(950) 226-4526 x51316','2017-06-28 16:08:19','2017-06-28 16:08:19'),(53,'columbus47@stanton.com','+1-767-980-7481','2017-06-28 16:08:19','2017-06-28 16:08:19'),(54,'qshields@johnston.com','1-518-928-6294','2017-06-28 16:08:19','2017-06-28 16:08:19'),(55,'wwalker@krajcik.com','(661) 902-7271 x23039','2017-06-28 16:08:19','2017-06-28 16:08:19'),(56,'kayla.feil@yahoo.com','1-471-946-3056','2017-06-28 16:08:19','2017-06-28 16:08:19'),(57,'weimann.heidi@gmail.com','+1 (969) 464-6336','2017-06-28 16:08:19','2017-06-28 16:08:19'),(58,'jaeden11@macejkovic.net','287.583.0892 x99096','2017-06-28 16:08:19','2017-06-28 16:08:19'),(59,'anissa04@wolf.com','1-276-769-2565 x50330','2017-06-28 16:08:19','2017-06-28 16:08:19'),(60,'toy.hester@gmail.com','(810) 297-5611','2017-06-28 16:08:19','2017-06-28 16:08:19'),(61,'mwilderman@gmail.com','390-317-9195 x709','2017-06-28 16:08:19','2017-06-28 16:08:19'),(62,'uschneider@hotmail.com','+18029017510','2017-06-28 16:08:19','2017-06-28 16:08:19'),(63,'allie45@gmail.com','1-652-934-3239 x3065','2017-06-28 16:08:19','2017-06-28 16:08:19'),(64,'kevin.schneider@hotmail.com','(470) 866-4286 x1990','2017-06-28 16:08:19','2017-06-28 16:08:19'),(65,'zhuel@gmail.com','1-892-528-4986 x865','2017-06-28 16:08:19','2017-06-28 16:08:19'),(66,'salvador37@yahoo.com','+1-547-775-6787','2017-06-28 16:08:19','2017-06-28 16:08:19'),(67,'ulices41@aufderhar.com','917-231-9335','2017-06-28 16:08:19','2017-06-28 16:08:19'),(68,'murray.glover@gmail.com','+1-521-971-9250','2017-06-28 16:08:19','2017-06-28 16:08:19'),(69,'nzboncak@shields.com','837-794-9083','2017-06-28 16:08:19','2017-06-28 16:08:19'),(70,'adam.romaguera@hotmail.com','1-958-234-3236 x6301','2017-06-28 16:08:19','2017-06-28 16:08:19'),(71,'mbartoletti@hotmail.com','(853) 850-6156','2017-06-28 16:08:19','2017-06-28 16:08:19'),(72,'zzemlak@von.net','+1 (261) 468-4676','2017-06-28 16:08:19','2017-06-28 16:08:19'),(73,'aileen.wehner@kozey.com','208-560-3787','2017-06-28 16:08:19','2017-06-28 16:08:19'),(74,'fletcher.christiansen@walter.org','1-525-422-2770','2017-06-28 16:08:19','2017-06-28 16:08:19'),(75,'bpfeffer@gmail.com','314.351.2951 x7098','2017-06-28 16:08:19','2017-06-28 16:08:19'),(76,'sabryna.cummings@gmail.com','(760) 406-7649 x14617','2017-06-28 16:08:19','2017-06-28 16:08:19'),(77,'luciano.ferry@hagenes.com','+1-887-750-5039','2017-06-28 16:08:19','2017-06-28 16:08:19'),(78,'ugoodwin@yahoo.com','436-618-5758 x4180','2017-06-28 16:08:19','2017-06-28 16:08:19'),(79,'ezboncak@yahoo.com','1-389-639-9111','2017-06-28 16:08:19','2017-06-28 16:08:19'),(80,'janie.hyatt@gmail.com','+19493125229','2017-06-28 16:08:19','2017-06-28 16:08:19'),(81,'hhessel@yahoo.com','(669) 202-8599 x453','2017-06-28 16:08:19','2017-06-28 16:08:19'),(82,'schumm.kaylah@gmail.com','1-495-908-9458','2017-06-28 16:08:19','2017-06-28 16:08:19'),(83,'josh.hahn@kuhic.org','(905) 926-4826','2017-06-28 16:08:19','2017-06-28 16:08:19'),(84,'rrippin@aufderhar.com','(530) 689-4680 x411','2017-06-28 16:08:19','2017-06-28 16:08:19'),(85,'wilbert.hegmann@lowe.com','+1-447-679-7144','2017-06-28 16:08:19','2017-06-28 16:08:19'),(86,'lehner.judy@yahoo.com','1-371-586-1160 x280','2017-06-28 16:08:19','2017-06-28 16:08:19'),(87,'nikko.dibbert@hotmail.com','1-308-735-1412','2017-06-28 16:08:19','2017-06-28 16:08:19'),(88,'jmueller@blick.info','(394) 225-0336 x68833','2017-06-28 16:08:19','2017-06-28 16:08:19'),(89,'kilback.brycen@grimes.com','378-684-0496 x17231','2017-06-28 16:08:19','2017-06-28 16:08:19'),(90,'devonte.pollich@gmail.com','1-361-468-1374 x895','2017-06-28 16:08:19','2017-06-28 16:08:19'),(91,'hallie.bergstrom@yahoo.com','385-341-7094','2017-06-28 16:08:19','2017-06-28 16:08:19'),(92,'providenci95@dooley.com','1-659-214-0985','2017-06-28 16:08:19','2017-06-28 16:08:19'),(93,'schneider.alyson@yahoo.com','(653) 609-8597','2017-06-28 16:08:19','2017-06-28 16:08:19'),(94,'little.beulah@beahan.com','+1 (627) 246-1851','2017-06-28 16:08:19','2017-06-28 16:08:19'),(95,'isidro29@kautzer.com','+12259514662','2017-06-28 16:08:19','2017-06-28 16:08:19'),(96,'fae69@cartwright.biz','1-385-920-5677 x728','2017-06-28 16:08:19','2017-06-28 16:08:19'),(97,'eldred92@predovic.com','380-402-8852','2017-06-28 16:08:19','2017-06-28 16:08:19'),(98,'qhermiston@bauch.org','1-970-257-4847 x57153','2017-06-28 16:08:19','2017-06-28 16:08:19'),(99,'swift.aurelia@swaniawski.com','1-786-934-6389 x03022','2017-06-28 16:08:19','2017-06-28 16:08:19'),(100,'madyson.langosh@hotmail.com','(408) 263-5555 x662','2017-06-28 16:08:19','2017-06-28 16:08:19'),(101,'t144est@test.co',NULL,'2017-07-02 11:03:10','2017-07-02 11:03:10'),(103,NULL,'email@email.com','2017-07-29 13:02:44','2017-07-29 13:02:44');
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@admin.com','$2y$10$O6.KUepuzs1Jux4mwyBBlOzi0jRImW7r/zHC7ocnjrYU8XRtdcmh.','ZjcWvxDS25xbbmPyURYdZeQoZSp8WrylWs2OeeAMqP0BYJ0was9DpADZvsFC','2017-06-09 17:03:08','2017-06-09 17:03:08'),(2,'admin2','admin2@admin.com','$2y$10$jNQfP67mlfqh4RkkhrhvGOeJ9gdnWIB5KCCq0kvtal8GTNu2UsILS','IsH62zb6DtuyrXa1bXLxPkZ1xvNjY6T01md5yCfVFXQ4m2dTzCkl0zishRo6','2017-06-15 23:15:12','2017-06-15 23:15:12'),(37,'Elton Leffler','ilebsack@example.org','$2y$10$slRF/w9YPnqiKF6Fdzxslu2kS5RNU182kjrw6RF2unuU1QNBYi5Cy','TZQnzAh1Q3vJpp79qLLeyiMTJp4eO26tAYhOhqX6WA6M2K4dwEdfOaXrWhvx','2017-07-29 19:02:35','2017-07-29 19:02:35'),(38,'Noble Zboncak','bernier.davion@example.com','$2y$10$0xXBxg4T9ZgtEd7v6aG7qei5Tkr9GjTAjxLwRskcqJj4wEP49GHTq','rRLjGXPIPIeVdd6QHXjvmv4Y3qfAhghyT2BISxj4uDzkJZhl0oH7pyg3khYv','2017-07-31 16:46:28','2017-07-31 16:46:28'),(40,'Triston Leuschke','champlin.forrest@example.org','$2y$10$te4dQ2Hl9QKISP2j0xwn2uKx6/ccfb5iR3sjs6Gx.PuzYm2WHfHSi','5SylNlBWkFYkdwvwSHtJL0f5KLeywIhHhyawiAfW2phPeVZPwxn9MrsDpHnC','2017-07-31 16:49:03','2017-07-31 16:49:03'),(42,'Haleigh Paucek','rmayer@example.net','$2y$10$UaaSrUp22wf4AzuvabWm1.QuIbhAfz8Unh6FOtomTsO5ySTEqoQh2','5MPd9SOuC7wl5TxXGm9Hsqbbxo5yrHMrEoCI0lpkkSuLZNqiGWUXcXN63UbQ','2017-07-31 16:51:11','2017-07-31 16:51:11'),(44,'Syble Ferry','carmel.armstrong@example.org','$2y$10$tGBpm0hxUakTB0TELUYEMeFvtfAM8ZWCuxEdKunMEVHHeqtkhvyQy','yAmvGBeiRektVll2jokjcfW9nNi1Ph9qjRWB5loifd8uhLOasa8pVrqBXCx4','2017-07-31 17:53:27','2017-07-31 17:53:27'),(46,'Morton Kuhn','xledner@example.net','$2y$10$V2H/rDJRXSombovM119yO.ZYUV5A1kvggtvkVcQNisjKZ4wD176iu','A1W3L482B8GoD7U6mmPPmQ9qRK8M1wE1xU6h2DsLVcJu7Vxb1pbDOlSzkrID','2017-07-31 18:01:25','2017-07-31 18:01:25'),(48,'Jolie Bartoletti','abigayle.champlin@example.com','$2y$10$9/aliRMOQ4dMqz.uqPeJleIXnNZC9iPup8c0sNf2kDHnRvEHIkSHW','dMiS8DSQPegRLk6xrAU5eFwytU2OKk47eFcjtKM5b7FtCIPdMW9F3CHP0IJ0','2017-07-31 18:08:22','2017-07-31 18:08:22'),(50,'Mr. Eldred Dibbert','jterry@example.com','$2y$10$kA4R9eTctJEgZqquKuhoPepm.I.tgUObKOBh.5BgtyejwrYqkXU.W','jhKAiAYNvHUbkMzOvyN6KS9EyCJDCdAiuIQmnIVpXxgxerDK7jLK210WLTDK','2017-07-31 18:12:11','2017-07-31 18:12:11'),(52,'Kamren Watsica','monroe.gaylord@example.org','$2y$10$TgBDUOqEwNl7pm9/UtE3j.3CsdyK6um.AitxanJWhCgOO2.fF2.Bq','4GCI5OJmkwWGR7RDshZhHmST5DX5mo6hTmdjOeZDYaGzHrfonN8biXCR9cO2','2017-07-31 20:13:18','2017-07-31 20:13:18'),(54,'Dawn Orn II','barry.kirlin@example.com','$2y$10$PwG99ejPvEB8FB0O0LzTKeSPz2teHXmnWpm8J1onI2dwVZswFlFbW','LHaVrLhTh4iSxV9BNNhvjCrgeyEhDKM5xkNtcvgBr2EGohFggCvmiJWN4bJM','2017-07-31 20:35:16','2017-07-31 20:35:16'),(55,'Mr. Cornelius Hilll MD','uankunding@example.org','$2y$10$0/XmJa/ZQ3uEpZ8K9QQLbOQdvcb.wqYiLJ6ntbnot1FKRxCkw7pIG','jCWps4aUykIjuJymyT8O01mWoakfekIfbpSzah8x5tmsMiZY2UJjIMjaxglX','2017-08-02 16:20:22','2017-08-02 16:20:22'),(56,'Prof. Mina Klocko III','xondricka@example.org','$2y$10$cwYv0EdL9trd3ApRu6knD.3UOtTBfxMPAV0Ptgv00m.G4hMT9cFFq','D0E6kGrtfFGFwvVndPgANnsarvhnNleaDUZL3wwF5ECJMGE8oV8QKZDN8z0k','2017-08-03 16:11:56','2017-08-03 16:11:56');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-19 19:20:40
