(function ($) {
    var app = {};

    app.page = {
        main: {
            slider: {
                name: 'mainPage',
                slider: ".b-slider",
                slide: ".b-slider .b-slide",
                container: ".b-slider .b-slider_container",
                sliderGroup: ".b-slider .b-slider_group",
                activeGroup: ".b-slider .b-slider_group.active",
                switchContainer: ".b-slider .b-slider_switch",
                switchBtn: ".b-slider .b-slider_switch div",
                rightArr: ".b-slider__arrow-right",
                leftArr: ".b-slider__arrow-left",

                init: function () {
                    app.shared.slider.init(this);
                },

            },

            sliderInterior: {
                name: "interior",
                slider: ".b-slider-interior",
                slide: ".b-slider-interior .b-slide",
                container: ".b-slider-interior .b-slider-interior_container",
                switchContainer: ".b-slider-interior .b-slider_switch",
                switchBtn: ".b-slider-interior .b-slider_switch div",
                sliderGroup: ".b-slider-interior .b-slider_group",
                activeGroup: ".b-slider-interior .b-slider_group.active",
                rightArr: ".b-slider-interior__arrow-right",
                leftArr: ".b-slider-interior__arrow-left",

                init: function () {
                    app.shared.slider.init(this);

                }
            },

            sliderAboutUs: {
                name: "aboutUs",
                slider: ".b-about",
                slide: ".b-about .b-slide",
                container: ".b-about .b-about_slider",
                switchContainer: ".b-about .b-about_switch",
                switchBtn: ".b-about .b-about_switch div",
                sliderGroup: ".b-about .b-slider_group",
                activeGroup: ".b-about .b-slider_group.active",
                rightArr: ".b-about_slide__left",
                leftArr: ".b-about_slide__right",

                init: function () {
                    app.shared.slider.init(this);

                }
            },

            toggleSubscribe: function () {
                let email = $(".b-subscribe .b-subscribe_email");
                let phone = $(".b-subscribe .b-subscribe_phone");
                phone.toggleClass("active");
                email.toggleClass("active");

                if (phone.hasClass("active")) {
                    $(".b-subscribe #subscribe_field").attr({
                        "name": "phone",
                        "placeholder": "Телефон",
                    })
                }

                if (email.hasClass("active")) {
                    $(".b-subscribe #subscribe_field").attr({
                        "name": "email",
                        "placeholder": "Email",
                    })
                }
            },

            subscribe: function () {
                //validate if conditions are checked
                $(".b-subscribe .checkbox").css('border', 'none');
                checked = $(".b-subscribe #conditions_agreed").is(":checked");

                if (!checked) {
                    $(".b-subscribe .checkbox").css('border', '1px solid red');
                    return;
                }

                if (!app.shared.captchaChecked()) {
                    app.shared.captchaShow($('.b-header .b-subscribe .b-subscribe_form'));
                    return;
                }
                $.fancybox.close();

                if (!app.page.main.subscribeData()) return;


                if ($(".b-subscribe .b-subscribe_email").hasClass("active")) {
                    var email = $(".b-subscribe #subscribe_field").val();
                }

                $.ajax({
                    url: '/ajax/subscribe',
                    method: 'POST',
                    dataType: "json",
                    data: app.page.main.subscribeData(),
                })
                    .done(function (msg) {
                        if (email != undefined) {
                            window.location.href = "/subscribe/email?email=" + email;
                            return;
                        }

                        swal({
                            title: "Вы удачно подписаны",
                            type: "info",
                            timer: 3000,
                        });

                        $(".b-subscribe .btn-subscribe").prop("disabled", true);
                    })
                    .fail(function (msg) {
                        swal({
                            title: "Указаны неверные данные или такой пользователь уже существует",
                            type: "error",
                            timer: 3000,
                        });
                    });

                grecaptcha.reset();
            },

            subscribeData: function () {
                //phone or email is inserted
                let phone, email = '';

                if ($(".b-subscribe .b-subscribe_phone").hasClass("active")) {
                    phone = $(".b-subscribe #subscribe_field").val();
                    if (phone.length < 5) {
                        swal({
                            title: "Неверный номер телефона",
                            type: "error",
                            timer: 3000,
                        });
                        return false;
                    }
                }

                if ($(".b-subscribe .b-subscribe_email").hasClass("active")) {
                    email = $(".b-subscribe #subscribe_field").val();

                    if (!app.shared.validEmail(email)) {
                        swal({
                            title: "Неверный email",
                            type: "error",
                            timer: 3000,
                        });
                        return false;
                    }
                }

                return {
                    phone: phone,
                    email: email,
                    'g-recaptcha-response': app.shared.captchaField(),
                };
            },

            interior: function () {
                var delay = $(".b-design .b-design_photo").data('delay');
                var inEffect = $(".b-design .b-design_photo").data('ineffect');
                var outEffect = $(".b-design .b-design_photo").data('outeffect');

                setInterval(function () {
                    var animation = function (inEffect, outEffect, delay) {
                        let visible = $(".b-design .b-design_photo img:visible").first();
                        let next = visible.next().length ? visible.next() : $(".b-design .b-design_photo img").first();

                        visible.animate({
                                opacity: 0
                            }, {
                                duration: (delay * 1000),
                                queue: false,
                                complete: function () {
                                    $(this).addClass('hidden').css({'opacity': 1});
                                    next.removeClass('transparent hidden ' + outEffect + ' animated');
                                }
                            }
                        );

                        $(".b-design .animated").css({'animation-duration': +delay + 's'});
                        visible.animateCss(outEffect);
                        next.animateCss(inEffect);

                    };

                    animation(inEffect, outEffect, delay);


                }, 5000);
            },
        },

        catalog: {
            toggleCategories: function (that) {
                $(that).toggleClass("closed");

                let parent = $(that).parent();

                if (parent.data('level') == 1)
                    parent.nextAll('.b-catalog_content_lev2').removeClass("hidden");

                if (parent.data('level') == 2)
                    parent.next('.b-catalog_content_lev3').removeClass("hidden");


                if ($(that).hasClass("closed")) {
                    if (parent.data('level') == 1)
                        parent
                            .nextAll()
                            .addClass("hidden")
                            .find(".opener")
                            .addClass("closed");

                    if (parent.data('level') == 2)
                        parent.next().addClass("hidden");
                }
            },

            toggleSidebar: function (that) {
                $(that).find("> .opener").toggleClass("closed");
                if ($(that).next().hasClass("b-left-child"))
                    $(that).next().toggleClass("hidden").next();


                //set height of the opened list
                let child = $(that).parent();

                if ($(that).hasClass("b-left-sidebar_lev1")) {
                    child = $(that).next().find(">ul");
                }

                child.closest(".b-left-child").css('height', child.height() + 10);
            },

            sliders: {
                init: function () {
                    //get all containers with categories
                    // where sliders should be initialized
                    $(".b-category .b-category-items").each(function (key, slider) {
                        var index = $(slider).data('index');
                        var obj = {
                            name: "categories" + index,
                            slider: ".b-category_slider" + index,
                            slide: ".b-category_slider" + index + " .b-slide",
                            container: ".b-category_slider" + index + " .b-category-items_container",
                            switchContainer: "",
                            switchBtn: "",
                            sliderGroup: ".b-category_slider" + index + " .b-slider_group",
                            activeGroup: ".b-category_slider" + index + " .b-slider_group.active",
                            rightArr: ".b-category_slider" + index + " .b-category-items__arrow-right",
                            leftArr: ".b-category_slider" + index + " .b-category-items__arrow-left",
                            offset: true,
                        };

                        app.shared.slider.init(obj);
                    });

                    //init filter sliders
                    $(".b-category .b-category-filter").each(function (key, slider) {
                        var index = $(slider).data('index');
                        var obj = {
                            name: "hit" + index,
                            slider: ".b-category-filter-slider" + index,
                            slide: ".b-category-filter-slider" + index + " .b-slide",
                            container: ".b-category-filter-slider" + index + " .b-category-filter_container",
                            switchContainer: "",
                            switchBtn: "",
                            sliderGroup: ".b-category-filter-slider" + index + " .b-slider_group",
                            activeGroup: ".b-category-filter-slider" + index + " .b-slider_group.active",
                            rightArr: ".b-category-filter-slider" + index + " .b-category-filter__arrow-right",
                            leftArr: ".b-category-filter-slider" + index + " .b-category-filter__arrow-left",
                            offset: true,
                            add: false,
                            lastFull: true,
                            touch: false,
                        };

                        app.shared.slider.init(obj);
                        app.page.catalog.stretchSlide(".b-category-filter-slider" + index);
                    });
                },
            },

            stretchSlide: function (selector) {
                //let filter = '.b-category .b-category-filter';
                let filter = '.b-category ' + selector;
                $(filter).removeClass('stretch');
                $(filter + '.stretch .b-slide, ' +
                    filter + '.stretch .b-category-filter__content')
                    .removeAttr('style');

                //if more than one group with sliders exists
                if ($(filter + ' .b-slider_group').length > 1) return;

                //container width
                let containerWidth = $(filter).width();
                let slidersWidth = $(filter + ' .b-slider_group').first().width();
                if (slidersWidth > containerWidth) return;

                $(filter).addClass('stretch');

                let slidesNum = $(filter + ' .b-slide').length;

                $(filter + '.stretch .b-slide, ' +
                    filter + '.stretch .b-category-filter__content')
                    .css('width', containerWidth / slidesNum);
            },


        },

        category: {
            toggleCategory: function (that) {
                $(that).toggleClass("closed");
                let parent = $(that).parent();
                let next = parent.next();
                if (next.hasClass("b-category-items") || next.hasClass("b-category-filter")) {
                    next.toggleClass("hidden");
                    $(that).next().toggleClass("hidden");
                }
            }
        },

        product: {
            setQuantity: function (that, increment) {
                increment = increment == undefined ? true : false;
                let parent = $(that).parent();
                let input = parent.find("input");
                let current = parseInt(input.val());
                let new_quantity = increment ? ++current : --current;

                if (new_quantity <= 0 || new_quantity >= 100) return;

                input.val(new_quantity);
                parent.find("div").html((new_quantity) + "<span>шт</span>");
            },

            tabs: function (that) {
                let container = $(that).parent().parent();
                let tabsContainer = $(that).parent();
                let index = $(that).index();

                tabsContainer.find("a")
                    .removeClass("active")
                    .eq(index)
                    .addClass("active");

                container.find(".b-product_tab")
                    .removeClass("active")
                    .eq(index)
                    .addClass("active");

            },

            requestForm: function (that) {
                $(that).find('input').css('border-color', '#e1e1e1');

                let name = $(that).find('input[name=client_name]').val();
                if (name.length < 3) {
                    $(that).find('input[name=client_name]').css('border-color', 'red');
                    return false;
                }

                let email = $(that).find('input[name=email]').val();
                if (!app.shared.validEmail(email)) {
                    $(that).find('input[name=email]').css('border-color', 'red');
                    return false;
                }

                let phone = $(that).find('input[name=phone]').val();
                if (phone.length < 3) {
                    $(that).find('input[name=phone]').css('border-color', 'red');
                    return false;
                }

                let city = $(that).find('input[name=city]').val();
                if (city.length < 3) {
                    $(that).find('input[name=city]').css('border-color', 'red');
                    return false;
                }

                if (!app.shared.captchaChecked()) {
                    app.shared.captchaShow($(that));
                    return;
                }
                $.fancybox.close();

                $.ajax({
                    url: '/ajax/contact',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        client_name: name,
                        phone: phone,
                        email: email,
                        city: city,
                        'g-recaptcha-response': app.shared.captchaField(),
                        comment: $(that).find("textarea").val(),
                    }
                })
                    .done(function (msg) {
                        $(that).find(".fancybox-close-small").click();
                        $('.b-product .b-product_btn_request').off();

                        swal({
                            title: "С Вами скоро свяжутся наши специалисты",
                            type: "info",
                            timer: 3000,
                        });
                    })
                    .fail(function (msg) {
                        swal({
                            title: "Указаны неверные данные",
                            type: "error",
                            timer: 3000,
                        });
                    });

                grecaptcha.reset();
            },

            requestProduct: function (that) {
                $(that).find('input').css('border-color', '#e1e1e1');

                let name = $(that).find('input[name=client_name]').val();
                if (name.length < 3) {
                    $(that).find('input[name=client_name]').css('border-color', 'red');
                    return false;
                }

                let email = $(that).find('input[name=email]').val();
                if (!app.shared.validEmail(email)) {
                    $(that).find('input[name=email]').css('border-color', 'red');
                    return false;
                }

                let phone = $(that).find('input[name=phone]').val();
                if (phone.length < 3) {
                    $(that).find('input[name=phone]').css('border-color', 'red');
                    return false;
                }

                let city = $(that).find('input[name=city]').val();
                if (city.length < 3) {
                    $(that).find('input[name=city]').css('border-color', 'red');
                    return false;
                }

                if (!app.shared.captchaChecked()) {
                    app.shared.captchaShow($(that));
                    return;
                }
                $.fancybox.close();

                var volume = $(".b-product_info .b-product_btn__vol.active").data('volume');
                volume = volume ? volume : $(".b-product_info .b-product_btn__vol:first").data('volume');
                var color = $(".b-product_info .b-product_btn__color").text();
                var color_id = $(".b-product_tab a[data-caption='" + $.trim(color) + "']").data('id');
                var product_id = $("[name='product_id']").val();

                $.ajax({
                    url: '/ajax/request',
                    method: 'POST',
                    dataType: "json",
                    data: {
                        client_name: name,
                        phone: phone,
                        email: email,
                        city: city,
                        'g-recaptcha-response': app.shared.captchaField(),
                        comment: $(that).find("textarea").val(),
                        product_id: product_id,
                        color_id: color_id,
                        volume: volume,
                        quantity: $(".b-product_btn_quantity").find('[name=quantity]').val(),
                    }
                })
                    .done(function (msg) {
                        $(that).find(".fancybox-close-small").click();
                        $('.b-product .b-product_btn_request').off().attr('href', '');

                        swal({
                            title: "С Вами скоро свяжутся наши специалисты",
                            type: "info",
                            timer: 3000,
                        });
                    })
                    .fail(function (msg) {
                        swal({
                            title: "Указаны неверные данные",
                            type: "error",
                            timer: 3000,
                        });
                    });

                grecaptcha.reset();
            },

            selectColor: function (that) {
                let val = $(that).val();
                let link = $(".b-product_tab a[data-caption='" + val + "']");
                let src = $(link).find('img').attr('src');

                $(".b-product .b-product_btn__color").html('<img src="' + src + '">' + val);
            },


            sliderRelated: {
                name: "related",
                slider: ".b-product-related",
                slide: ".b-product-related .b-slide",
                container: ".b-product-related .b-product-related_container",
                switchContainer: "",
                switchBtn: "",
                sliderGroup: ".b-product-related .b-slider_group",
                activeGroup: ".b-product-related .b-slider_group.active",
                rightArr: ".b-product-related .b-product-related__arrow-right",
                leftArr: ".b-product-related .b-product-related__arrow-left",

                init: function () {
                    app.shared.slider.init(this);
                }
            },

        },

        menu: {
            onHover: function (that) {
                //position an element
                this.positionMenu(that);

                $(that)
                    .find("> ul")
                    .show()
                    .end()
                    .find(" > .no-link")
                    .addClass("active");
            },

            offHover: function (that) {
                $(that)
                    .find("> ul")
                    .hide()
                    .end()
                    .find(" > a")
                    .removeClass("active");
            },

            positionMenu: function (that) {
                let sublist = $(that).find("> ul");

                if (sublist.length == 0) return;

                let container = {
                    width: $(".container").width(),
                    left: $(".container").offset().left,
                    right: $(".container").offset().left + $(".container").width()
                };

                let element = {
                    width: sublist.width(),
                    left: $(that).offset().left,
                    right: $(that).offset().left + sublist.width(),
                };

                if (container.right < element.right) {
                    let offset = container.right - element.right - 20;
                    sublist.css('left', offset + "px");
                }

                //for the second level sublist
                if ($(that).parent().hasClass("b-menu_level1")
                    && element.right + element.width > container.right) {
                    sublist.css('left', "-240px");
                }
            }
        },

        portfolio: {
            next: function () {
                let container = ".b-object .b-object_images_container";
                let tn = ".b-object .b-object_images_tn";
                let current = $(tn + " .active");
                let next = current.next();

                next = next.length ? next : $(tn + " div").first();

                //remove/add active class
                $(tn + " div").removeClass('active');
                next.addClass('active');

                let nextUrl = next.find('a').attr('href');
                //get the position
                let left = $(container + " a[href='" + nextUrl + "']")
                    .position()
                    .left;

                $(container).css('left', '-' + left + 'px');
            },

            prev: function () {
                let container = ".b-object .b-object_images_container";
                let tn = ".b-object .b-object_images_tn";
                let current = $(tn + " .active");
                let prev = current.prev();

                prev = prev.length ? prev : $(tn + " div").last();

                //remove/add active class
                $(tn + " div").removeClass('active');
                prev.addClass('active');

                let nextUrl = prev.find('a').attr('href');
                //get the position
                let left = $(container + " a[href='" + nextUrl + "']")
                    .position()
                    .left;

                $(container).css('left', '-' + left + 'px');
            }
        },
    };

    app.shared = {

        slider: {
            slidesNumCanView: 1,
            //current slider
            current: {},
            //all sliders
            all: {},

            init: function (obj) {
                this.current = obj;
                this.all[obj.name] = obj;

                //prevent multi initializations
                if (!this.checkDelay()) return;

                this.groupSlides();
                this.makeSliderBtn();
                this.makeDraggable();
                this.events();
            },
            //move to the next right slide
            moveRight: function (obj) {
                obj = obj || this.current;
                var current = $(obj.activeGroup);
                var next = current.next();
                next = next.length ? next : $(obj.sliderGroup).first();

                current.removeClass('active');
                next.addClass('active');
                this.changeBtnState(next.index(), obj);

                $(obj.container).css('left', '-' + (next.position().left) + 'px');
            },

            //move to the next left slide
            moveLeft: function (obj) {
                obj = obj || this.current;
                var current = $(obj.activeGroup);
                var next = current.prev();
                next = next.length ? next : $(obj.sliderGroup).last();

                current.removeClass('active');
                next.addClass('active');
                this.changeBtnState(next.index(), obj);

                $(obj.container).css('left', '-' + (next.position().left) + 'px');
            },

            //move to specified position
            moveTo: function (index, obj) {
                obj = obj || this.current;
                this.changeBtnState(index, obj);

                var position = $(obj.sliderGroup)
                    .removeClass('active')
                    .eq(index)
                    .addClass('active').position().left;

                $(obj.container).css('left', '-' + (position) + 'px');
            },

            changeBtnState: function (index, obj) {
                obj = obj || this.current;
                $(obj.switchBtn)
                    .removeClass("active")
                    .eq(index)
                    .addClass("active");
            },

            //Create groups from a bunch of slides,
            //this way it's easier to change slides
            groupSlides: function (obj) {
                obj = obj || this.current;

                if ($(obj.sliderGroup).length) {
                    $(obj.slide).unwrap();
                }

                let slidesNumCanView = Math.floor($(obj.slider).width() / $(obj.slide).width());
                obj.slidesNumCanView = slidesNumCanView;
                var length = $(obj.slide).length;
                for (let i = 0; i < length / slidesNumCanView; i++) {
                    this.wrapGroup();
                }

                //in case there is offset of the first slide or
                // there are little slides in the last group
                // add slides to the last group
                this.addSlides(obj);

                this.makeLastGroupFull();

                //make the first group active
                $(obj.sliderGroup).slice(0, 1).addClass('active');

            },

            wrapGroup: function (obj) {
                obj = obj || this.current;

                let slideGroup = $(obj.container).children(".b-slide")
                    .slice(0, obj.slidesNumCanView);
                $(slideGroup).wrapAll("<div class='b-slider_group' />");
            },

            //generate slider buttons
            makeSliderBtn: function (obj) {
                obj = obj || this.current;

                var insertHtml = '<div class="active"></div>';

                //need to refresh the selector
                var groupNum = $(obj.container).children().length;

                for (let i = 1; i < groupNum; i++) {
                    insertHtml += '<div></div>';
                }

                $(obj.switchContainer).empty().append(insertHtml);
            },

            //make draggable for desktop
            makeDraggable: function (obj) {
				return; // disable touch on all
                obj = obj || this.current;
                //disable slider touch event
                if (obj.touch === false) return;
                //if ($(window).width() < 768)  return;

                $(obj.container).draggable({
                    axis: "x",

                    start: function (event, ui) {
                        start = ui.position.left;
                    },
                    stop: function (event, ui) {
                        stop = ui.position.left;
                        (start < stop) ? $(obj.leftArr).click() : $(obj.rightArr).click();
                    }
                });
            },

            //if the size of the window changed and new buttons appeared,
            //detach events and attach one more time
            events: function (obj) {
                obj = obj || this.current;

                var that = this;
                $(obj.rightArr).off().click(function () {
                    that.moveRight(obj);
                });

                $(obj.leftArr).off().click(function () {
                    that.moveLeft(obj);
                });

                $(obj.switchBtn).off().click(function () {
                    var index = $(obj.switchBtn).index(this);
                    that.moveTo(index, obj);
                });

                var resizeTimer;
                //on resize regenerate sliders
                $(window).resize(function (e) {
                    //remove previously created copies of slides
                    that.removeSlides();
                    clearTimeout(resizeTimer);
                    resizeTimer = setTimeout(function () {
                        app.page.main.slider.init();
                        app.page.main.sliderInterior.init();
                        app.page.main.sliderAboutUs.init();
                        app.page.catalog.sliders.init();
                        app.page.product.sliderRelated.init();
                        //app.page.catalog.stretchSlide();
                        //hover event
                        app.events.mainPage();
                    }, 1000);
                });
            },

            //prevent too ofter initialization of the slider
            checkDelay: function () {
                if (this.current.lastInit == undefined) {
                    this.current.lastInit = +new Date();
                    return true;
                }

                if (Math.floor(new Date() - this.current.lastInit) > 2000) {
                    this.current.lastInit = +new Date();
                    return true;
                }

                return false;

            },

            addSlides: function (obj) {
                obj = obj || this.current;

                if (obj.add === false) return;

                //if offset exists in the slides
                let add = 0;
                if (obj.offset) add = 2;


                let last_group_slides = $(obj.sliderGroup).last().children().length;

                if (last_group_slides >= obj.slidesNumCanView + add) return;

                let slides = $(obj.slide);
                let iterations = obj.slidesNumCanView - last_group_slides + add;

                //if there are too few slides
                if (slides.length < 2) return;

                for (let i = 0; iterations >= i; ++i) {
                    let slide = slides.slice(i, i + 1).clone().addClass('b-slide-copy');
                    $(obj.sliderGroup).last().append(slide);
                }
            },

            //remove previously duplicated slides
            removeSlides: function () {
                $('.b-slide-copy').remove();
            },

            //make the last group of slides full,
            //avoid blank space on the right side
            makeLastGroupFull: function (obj) {
                obj = obj || this.current;

                if (obj.lastFull !== true) return;

                //if space on the right exists
                let lastGroupSliderNum = $(obj.sliderGroup + ':last').find('.b-slide').length;
                if (lastGroupSliderNum >= obj.slidesNumCanView) return;

                let beforeLastContainer = $(obj.sliderGroup + ':last').prev();
                let addSlidesNum = obj.slidesNumCanView - lastGroupSliderNum;

                let slidesToAdd = beforeLastContainer.find('.b-slide').slice(0, addSlidesNum);

                $(obj.sliderGroup + ':last').prepend(slidesToAdd);
            }
        },

        validEmail: function (email) {
            let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (email.match(mailformat)) {
                return true;
            }

            return false;
        },

        csrf: function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        },

        contactForm: function (form) {
            form.find("input").css('border', '1px solid #e0e0e0');
            form.find("select").css('border', '1px solid #e0e0e0');

            let name = form.find("input[name=client_name]").val();
            let phone = form.find("input[name=phone]").val();
            let email = form.find("input[name=email]").val();

            if (name.length < 1) {
                form.find("input[name=client_name]").css('border', '1px solid red');
                swal({
                    title: "Введите имя",
                    type: "error",
                    timer: 3000,
                });
                return false;
            }

            if (phone.length < 5) {
                form.find("input[name=phone]").css('border', '1px solid red');
                swal({
                    title: "Введите номер телефона",
                    type: "error",
                    timer: 3000,
                });
                return false;
            }

            if (!app.shared.validEmail(email)) {
                form.find("input[name=email]").css('border', '1px solid red');
                swal({
                    title: "Указан неверный email",
                    type: "error",
                    timer: 3000,
                });
                return false;
            }

            if (!app.shared.captchaChecked()) {
                app.shared.captchaShow($(form));
                return;
            }
            $.fancybox.close();

            $.ajax({
                url: '/ajax/contact',
                method: 'POST',
                dataType: "json",
                data: {
                    client_name: name,
                    phone: phone,
                    email: email,
                    'g-recaptcha-response': app.shared.captchaField(),
                    comment: form.find("textarea").val(),
                    repair_type: form.find("select").val(),
                }
            })
                .done(function (msg) {
                    swal({
                        title: "С Вами скоро свяжутся наши специалисты",
                        type: "info",
                        timer: 3000,
                    });

                    $(form).find(".fancybox-close-small").click();
                    form.find("button").prop("disabled", true);
                })
                .fail(function (msg) {
                    swal({
                        title: "Указаны неверные данные",
                        type: "error",
                        timer: 3000,
                    });
                }),

                grecaptcha.reset();
        },

        footer: function () {
            setInterval(function () {
                let effects = ['tada', 'zoomIn', 'bounce', 'rubberBand'];
                let pickEffect = function () {
                    return effects[Math.floor(Math.random() * 4)];
                };

                $(".contact-btn i").animateCss(pickEffect());
            }, 3000);

            this.footerBtnPosition();


            //pop up special offer
            var special_offer = {
                src: '#special_offer',
                type: 'inline',
                autoSize: false,
                width: "auto",
                opts: {
                    afterShow: function (instance, current) {
                        $('#special_offer').removeClass('hidden');
                    },
                    afterClose: function () {
                        $(".b-sticky-offer").removeClass('hidden');
                        $('#special_offer').addClass('hidden');
                    }
                }
            };

            //show special offer pop up
            setTimeout(function () {
                if (!$("#special_offer").data('show')) {
                    $(".b-sticky-offer").removeClass('hidden');
                    return;
                }

                $.fancybox.open(special_offer);
            }, 15000);

            //close special offer popup
            setTimeout(function () {
                $.fancybox.close();
            }, 30000);

            $(".b-sticky-offer").click(function () {
                $.fancybox.open(special_offer);
            });

            if (!$("#special_offer").data('show')) {
                $(".b-sticky-offer").removeClass('hidden');
            }

        },

        //stick button to container right corner
        footerBtnPosition: function () {
            let right = $('.container').width() + $('.container').offset().left;
            $('.contact-btn').css('left', (right - 100) + 'px');

        },

        searchActiveMenu: function () {
            //if there is no sidebar, just return;
            if ($(".b-left-sidebar_menu").length <= 0) return false;

            //search category to open
            let pathArray = window.location.pathname.split('/');
            let category = $(".b-left-sidebar_menu a[href='" + pathArray.join('/') + "']");

            //for category pages
            if (category.length > 0) {
                return category
                    .closest('li')
                    .addClass('active');
            }

            //for filter
            if ($(".b-left-sidebar_menu .active_filter").length > 0) {
                return $(".b-left-sidebar_menu .active_filter")
                    .closest('li')
                    .addClass('active');
            }

            //for products
            pathArray.pop();
            return $(".b-left-sidebar_menu a[href='" + pathArray.join('/') + "']")
                .parent('li')
                .addClass('active');
        },

        openActiveMenu: function () {
            let active = this.searchActiveMenu();

            if (!active) return;

            //open parent categories
            active
                .closest('.b-left-child')
                .prev()
                .find('i')
                .click()
                .closest('.b-left-child')
                .prev()
                .find('i')
                .click();

            //open children
            active
                .find('>i')
                .click();

        },

        captchaChecked: function () {
            if ($(".google_captcha_process").length > 0) return true;

            return typeof grecaptcha !== 'undefined' && grecaptcha && grecaptcha.getResponse().length !== 0;
        },

        captchaShow: function (form) {
            if ($("div.fancybox-is-open").length > 0
                && form.attr('id') != 'free_consult'
                && form.attr('id') != 'product_request_form') {
                return;
            }

            form.addClass('google_captcha_process');
            jQuery(".google_captcha_opener").click();
            this.captchaSubmit();
        },

        captchaField: function () {
            return jQuery('.g-recaptcha-response').val();
        },

        captchaSubmit: function () {
            if ($(document).hasClass('google_captcha_process')) return;
            jQuery("#google_captcha .btn").off('click').click(function () {
                jQuery('.google_captcha_process button').click().prop('disabled', true);
            })
        }

    };

    app.events = {

        init: function () {
            this.mainPage();
            this.catalog();
            this.leftSidebar();
            this.product();
            this.menu();
            this.menuXs();
            this.contactForm();
            this.products();
            this.designStudio();
            this.footer();
            this.object();
            this.portfolio();
        },

        mainPage: function () {
            $(".b-subscribe .b-subscribe_phone, .b-subscribe .b-subscribe_email").click(function (e) {
                e.preventDefault();
                app.page.main.toggleSubscribe();
            });

            $(".b-subscribe .btn-subscribe").off('click').click(function (e) {
                e.preventDefault();
                app.page.main.subscribe();
            });

            //hover on main slider
            $(".b-slider .b-slide .b-slide__border").hover(function () {
                $(this).next().next().addClass('hovered');
            }, function () {
                $(this).next().next().removeClass('hovered');
            });

        },

        catalog: function () {
            $(".b-catalog .opener").click(function () {
                app.page.catalog.toggleCategories(this);
            });

            $(".b-category .opener").click(function () {
                if (!$(this).parent().next().hasClass('b-category-items')) {
                    $(this).addClass('disabled closed');
                    return;
                }
                app.page.category.toggleCategory(this);
            });

            $(".b-category .opener").click().click();

            $(window).resize(function () {
                $(".b-left-sidebar_lev1 > .opener").each(function (index, value) {
                    $(value).click().click();
                });
            });

            $(".b-category .b-category-filter_row select").change(function () {
                $(this).parent().parent().submit();
            })
        },

        leftSidebar: function () {
            $(".b-left-sidebar_list > ul li, .b-left-sidebar_lev2 > ul li").click(function (e) {
                e.stopPropagation();
                app.page.catalog.toggleSidebar(this);
            })
        },


        product: function () {
            $(".b-product a[data-fancybox]").fancybox({
                //options
            });

            $(".b-product .b-product_images__main .fa").click(function () {
                $(this).prev().click();
            });

            $(".b-product .b-product_btn__vol").click(function () {
                $(".b-product .b-product_btn .active").removeClass("active");
                $(this).addClass("active");
                $('.b-product .b-product_btn input[name=volume]').val($(this).data('volume'));
            });

            $(".b-product_btn_quantity_minus").click(function (e) {
                e.preventDefault();
                app.page.product.setQuantity(this, false);
            });

            $(".b-product_btn_quantity_plus").click(function (e) {
                e.preventDefault();
                app.page.product.setQuantity(this);
            });

            $(".b-product .b-product_tabs_nav a").click(function (e) {
                e.preventDefault();
                app.page.product.tabs(this);
            });

            $(".b-product .b-product_tab__color div").click(function () {
                $(this).prev().prev().click();
            });

            $(".b-product_btn_request").fancybox({
                'scrolling': 'no',
                'titleShow': false,
                'onClosed': function () {
                }
            });

            $("#product_request_form").submit(function (e) {
                e.preventDefault();
                app.page.product.requestProduct(this);
            });

            $(".b-product .b-product_btn__color").click(function () {
                $(this).hide();
                $(".b-product .b-product_btn__color-select").show();
            });

            $(".b-product .b-product_btn__color-select").change(function () {
                $(this).hide();
                $(".b-product .b-product_btn__color").show();
                app.page.product.selectColor(this);
            });

            //open the first tab
            let tab = $(".b-product .b-product_tabs_nav a").first();
            app.page.product.tabs(tab);

            $(".b-defer_contact form").submit(function (e) {
                if (!app.shared.captchaChecked()) {
                    app.shared.captchaShow($(this));
                    e.preventDefault();
                    return;
                }

                $.fancybox.close();

                if ($(this).find('#g-recaptcha-response').length > 0) return;

                let captchaField = '<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" value="' + app.shared.captchaField() + '"/>';
                $(this).prepend(captchaField);
                $(this).submit();

                return true;
            })

        },

        menu: function () {
            $(".b-menu .no-link").click(function (e) {
                e.preventDefault();
            });

            $(".b-menu ul li, .b-menu .b-menu_level1 li").hover(function () {
                app.page.menu.onHover(this);
            }, function () {
                app.page.menu.offHover(this);
            });
        },

        menuXs: function () {
        //    $(".b-menu-xs .no-link").click(function (e) {
        //        e.preventDefault();
        //    });
		
            $(".b-menu-xs .no-link .fa").click(function (e) {
                e.preventDefault();
            });

    //        $(".b-menu-xs .nav > li > a").click(function () {
    //            $(this)
    //                .toggleClass("active")
    //                .parent()
    //                .find("> ul")
    //                .toggleClass("hidden");
    //        }); 

			$(".b-menu-xs .nav > li > a > i ").click(function () {
                $(this)
				.parent()
                    .toggleClass("active")
                    .parent()
                    .find("> ul")
                    .toggleClass("hidden");
            });

            $(".b-menu-xs_level1 > li").click(function () {
                $(".b-menu-xs_level1").show();
                $(this)
                    .find("a")
                    .toggleClass("active")
                    .end()
                    .find("> ul")
                    .toggleClass("hidden");
            })
        },

        contactForm: function () {
            $(".b-consult_form button").click(function (e) {
                e.preventDefault();
                app.shared.contactForm($(".b-consult_form form"));
            });

            $(".b-left-sidebar_consult button").click(function (e) {
                e.preventDefault();
                app.shared.contactForm($(".b-left-sidebar_consult form"));
            });

            $("#free_consult button").click(function (e) {
                e.preventDefault();
                app.shared.contactForm($("#free_consult"));
            });

            $("#portfolio_want button").click(function (e) {
                e.preventDefault();
                app.shared.contactForm($("#portfolio_want"));
            });
        },

        products: function () {
            $('.b-products .b-products_item_list').click(function () {
                $(this).find('a').click();
            });

            $('.b-products .b-products_item_plus').click(function () {
                $(this).find('form').submit();
            });

            $('.b-products-filter #field_id').change(function () {
                $(this).parent().parent().submit();
            })
        },

        designStudio: function () {
            $(".b-design-studio .b-design-studio__border2").hover(function () {
                $(this).prevAll().addClass("hovered");
            }, function () {
                $(".b-design-studio .b-design-studio_block .hovered").removeClass("hovered");
            });

        },

        footer: function () {
            $(".contact-btn").draggable({
                containment: ".container"
            });

            app.shared.footer();

            $(window).resize(function () {
                app.shared.footerBtnPosition();
            });

            $(".b-free-consult_btn").fancybox({
                'scrolling': 'no',
                'titleShow': false,
                'onClosed': function () {
                }
            });

            $(".b-free-consult_btn .contact-btn").hover(function () {
                $(this).find('div').toggleClass('hidden');
                $(this).find('span').toggleClass('hidden');
            }, function () {
                $(this).find('div').toggleClass('hidden');
                $(this).find('span').toggleClass('hidden');
            });

            $(".google_captcha_opener").fancybox({
                'scrolling': 'no',
                'titleShow': false,
                'hideOnOverlayClick': false,
                'hideOnContentClick': false,
                'overlayShow': true,
                'modal': true,
                'closeClick': false,
                'onClosed': function () {
                }
            });

            $("#google_captcha .close").click(function () {
                $.fancybox.close();
                $(".google_captcha_process").removeClass("google_captcha_process");
            })


        },

        object: function () {
            $(".b-object .b-object_images a[data-fancybox]").fancybox();

            $(".b-object .b-object_images__right").click(function (e) {
                e.preventDefault();
                app.page.portfolio.next();
            });

            $(".b-object .b-object_images__left").click(function (e) {
                e.preventDefault();
                app.page.portfolio.prev();
            });

            $(".b-object .b-object_images__left").click();
            $(".b-object .b-object_images__right").click();

            $(".b-object .b-object__want").fancybox({
                'scrolling': 'no',
                'titleShow': false,
                'onClosed': function () {
                }
            });

            $("#portfolio_want").submit(function (e) {
                e.preventDefault();
                app.page.product.requestForm(this);
            });
        },

        portfolio: function () {
            $(".b-object-list .b-object-list_item__image").hover(function () {
                $(this).addClass('hovered');
            }, function () {
                $(this).removeClass('hovered');
            })
        }
    };


    //extends jquery
    $.fn.extend({
        animateCss: function (animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function () {
                $(this).removeClass('animated ' + animationName);
            });
            return this;
        }
    });

    app.page.main.slider.init();
    app.page.main.sliderInterior.init();
    app.page.main.sliderAboutUs.init();
    app.page.main.interior();
    app.page.catalog.sliders.init();
    //app.page.catalog.stretchSlide();
    app.page.product.sliderRelated.init();
    app.events.init();
    app.shared.csrf();
    app.shared.openActiveMenu();

    window.app = app;
})
(jQuery);



