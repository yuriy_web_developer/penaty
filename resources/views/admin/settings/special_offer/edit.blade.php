@extends('admin.app')

@section('content')
    <div class="panel panel-default b-settings_offer">
        <div class="panel-heading">Редактировать всплывающее окно с акцией</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('settings.offer.update')}}">
                <div class="form-group">
                    <label for="content">Содержание</label>
                    <textarea name="content"
                              id="b-settings__offer"
                              class="form-control">{!!$offer->value!!}</textarea>
                </div>
                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
