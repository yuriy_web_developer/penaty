@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Редактировать</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('settings.general.update')}}">
                <div class="row">
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <label for="email">Email для уведомлений</label>
                        <input type="text" name="settings[adminEmail]" id="email" class="form-control"
                               value="{{ $data['adminEmail'] }}">
                    </div>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
