@extends('admin.app')

@section('content')
    <div class="panel panel-default b-product">
        <div class="panel-heading">Добавить товар</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('product.store')}}">
                <div class="b-product_tabs">
                    <ul>
                        <li><a href="#b-product_tab1">Общая информация</a></li>
                        <li><a href="#b-product_tab2">Характеристики</a></li>
                        <li><a href="#b-product_tab3">Цвета</a></li>
                        <li><a href="#b-product_tab4">Описание</a></li>
                        <li><a href="#b-product_tab5">Инструкции</a></li>
                        <li><a href="#b-product_tab6">Видео</a></li>
                        <li><a href="#b-product_tab7">Фото</a></li>
                    </ul>
                    <div id="b-product_tab1">
                        <div class="form-group">
                            <label for="title">Заглавие</label>
                            <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                        </div>

                        <div class="form-group">
                            <label for="url">Url адрес</label>
                            <div class="text-muted small">
                                Для автоматической генерации оставьте поле пустым
                            </div>
                            <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}">
                        </div>

                        <div class="form-group">
                            <label for="keywords">Ключевые слова(мета данные)</label>
                            <input type="text"
                                   name="keywords"
                                   id="keywords"
                                   class="form-control"
                                   value="{{ old('keywords') }}">
                        </div>

                        <div class="form-group">
                            <label for="description">Описание(мета данные)</label>
                            <input type="text"
                                   name="description"
                                   id="description"
                                   class="form-control"
                                   value="{{ old('description') }}">
                        </div>


                        <div class="form-group">
                            <label for="b-product_short-info">Краткое описание</label>
                            <textarea name="short_info" id="b-product_short-info"
                                      class="form-control">{{ old('content') }}</textarea>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <label for="category">Категории</label>
                                    <div class="text-muted small">
                                        Для выбора нескольких категорий удерживайте клавишу CTRL
                                    </div>
                                    <select name="categories[]"
                                            id="category"
                                            class="form-control"
                                            multiple>
                                        <option value="0">Без категории</option>
                                        @foreach($category->firstLevel() as $first)
                                            <option value="{{$first->id}}">
                                                {{$first->page->title}}
                                            </option>
                                            @if ($first->hasChildren())
                                                @foreach($first->children() as $second)
                                                    <option value="{{$second->id}}">
                                                        &nbsp;&nbsp;{{$second->page->title}}
                                                    </option>
                                                    @if($second->hasChildren())
                                                        @foreach($second->children() as $third)
                                                            <option value="{{$third->id}}">
                                                                &nbsp;&nbsp;&nbsp;&nbsp;{{$third->page->title}}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{--<div class="checkbox">--}}
                            {{--<label><input type="checkbox" name="published" value="1">Опубликовать</label>--}}
                        {{--</div>--}}
                        {{--<div class="checkbox">--}}
                            {{--<label><input type="checkbox" name="archived" value="1">Архив</label>--}}
                        {{--</div>--}}

                        <div class="checkbox">
                            <label><input type="checkbox" name="hit" value="1" >Хит продаж</label>
                        </div>
                    </div>

                    <div id="b-product_tab2">
                        <div class="form-group">
                            <label for="basement">Основа</label>
                            <input type="text" name="basement" id="basement" class="form-control"
                                   value="{{ old('basement') }}">
                        </div>

                        <div class="form-group">
                            <label for="instrument">Инструмент</label>
                            <input type="text" name="instrument" id="instrument" class="form-control"
                                   value="{{ old('instrument') }}">
                        </div>

                        <div class="form-group">
                            <label for="consumption">Расход</label>
                            <input type="text" name="consumption" id="consumption" class="form-control"
                                   value="{{ old('consumption') }}">
                        </div>

                        <div class="form-group">
                            <label for="layers">Количество слоев</label>
                            <input type="text" name="layers" id="layers" class="form-control"
                                   value="{{ old('layers') }}">
                        </div>

                        <div class="form-group">
                            <label for="dilution">Разбавление</label>
                            <input type="text" name="dilution" id="dilution" class="form-control"
                                   value="{{ old('dilution') }}">
                        </div>

                        <div class="form-group">
                            <label for="producer">Производитель</label>
                            <input type="text" name="producer" id="producer" class="form-control"
                                   value="{{ old('producer') }}">
                        </div>

                        <div class="form-group">
                            <label for="volume">Объем</label>
                            <div class="text-muted small">
                                Для выбора нескольких пунктов удерживайте клавишу CTRL
                            </div>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                    <select name="volume[]" id="volume" class="form-control" multiple>
                                        @for($i = 1; $i <= 50; $i++)
                                            <option value="{{$i}}">{{$i}}л</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div id="b-product_tab3">
                        <div class="b-product-colors_dropzone dropzone"></div>

                        <div class="row b-product_colors">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="b-product_search-color">Поиск цвета по названию или адресу</label>
                                    <input type="text"
                                           name=""
                                           id="b-product_search-color"
                                           class="form-control"
                                           value="">
                                    <a href="/admin_assets/filemanager/dialog.php?type=0&field_id=b-product-colors_filemanager&relative_url=1"
                                       class="btn btn-primary">Открыть галерею</a>
                                    <input type="hidden"
                                           id="b-product-colors_filemanager"
                                           name="b-product-colors_filemanager"
                                           value="">
                                    <img src="/img/loading.gif" alt="" width="30" class="b-product_colors-loading">
                                </div>
                            </div>
                        </div>
                        <div class="text-muted small">
                            Перетащите фото, чтобы изменить их порядок.
                        </div>
                        <ul class="b-product_current-colors"></ul>
                    </div>
                    <div id="b-product_tab4">
                        <div class="form-group">
                            <textarea
                                    name="tab_description"
                                    id="tab_description"
                                    class="form-control">{{ old('tab_description') }}</textarea>
                        </div>
                    </div>
                    <div id="b-product_tab5">
                        <div class="form-group">
                            <textarea
                                    name="tab_instruction"
                                    id="tab_instruction"
                                    class="form-control">{{ old('tab_instruction') }}</textarea>
                        </div>
                    </div>
                    <div id="b-product_tab6">
                        <div class="form-group">
                            <label for="tab_video">Поля для кода с видео</label>
                            <textarea
                                    name="tab_video"
                                    id="tab_video"
                                    class="form-control">{{ old('tab_video') }}</textarea>
                        </div>
                    </div>
                    <div id="b-product_tab7">
                        <div class="b-product-photos_dropzone dropzone"></div>

                        <div class="row b-product_photos">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label for="b-product_search-photo">Поиск фото по названию или адресу</label>
                                    <input type="text"
                                           name="b-product_search-photo"
                                           id="b-product_search-photo"
                                           class="form-control"
                                           value="">
                                    <a href="/admin_assets/filemanager/dialog.php?type=0&field_id=b-product-photos_filemanager&relative_url=1"
                                       class="btn btn-primary">Открыть галерею</a>
                                    <input type="hidden"
                                           id="b-product-photos_filemanager"
                                           name="b-product-photos_filemanager"
                                           value="">
                                    <img src="/img/loading.gif" alt="" width="30" class="b-product_photos-loading">
                                </div>
                            </div>
                        </div>
                        <div class="text-muted small">
                            Перетащите фото, чтобы изменить их порядок.
                            Первое фото - используется для миниатюры товара
                        </div>
                        <ul class="b-product_current-photos"></ul>
                    </div>
                </div>

                {{csrf_field()}}
                <hr>
                <button type="submit" class="btn btn-default">Создать</button>

            </form>
        </div>
    </div>
@endsection


