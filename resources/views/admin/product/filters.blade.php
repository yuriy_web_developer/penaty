@if(!empty($filters))
    @foreach($filters as $filter)
        <div class="form-group">
            <label for="description">{{$filter->name}}</label>
            <label for="filter_fields"></label>
            <select
                    name="filter[]"
                    id="filter_fields"
                    class="form-control"
                    size="10"
                    multiple>
                <option value="" selected>Не выбрано</option>
                @foreach($filter->fields as $field)
                    <option value="{{$field->id}}"
                            {{$product->hasFilterField($field->id) ? ' selected ':''}}>{{$field->name}}</option>
                @endforeach
            </select>
        </div>
    @endforeach
@endif

