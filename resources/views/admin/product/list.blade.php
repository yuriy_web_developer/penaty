@extends('admin.app')

@section('content')
    <div class="panel panel-default b-product">
        <div class="panel-heading">Товары</div>

        <div class="panel-body">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <form method="GET" class=" form-inline"
                          action="{{route('product.index')}}">
                        <div class="form-group">
                            <input type="text"
                                   placeholder="Поиск по заглавию."
                                   name="search_word"
                                   id="search_word"
                                   class="form-control"
                                   value="{{ request('search_word') }}">
                        </div>
                        <input type="hidden" name="sort" value="{{request('sort')}}">
                        <button type="submit" class="btn btn-primary">Поиск</button>
                    </form>
                    <form method="GET"
                          class="form-inline"
                          action="{{route('product.index')}}">
                        <div class="form-group">
                            <select name="sort" id="sort" class="form-control">
                                <option value="1"
                                        {{ request('sort') == 1 ? "selected" : '' }}>
                                    Дате создания
                                </option>
                                <option value="2" {{ request('sort') == 2 ? "selected" : '' }}>
                                    Дате изменения
                                </option>
                                <option value="3" {{ request('sort') == 3 ? "selected" : '' }}>
                                    Заглавию
                                </option>
                            </select>
                        </div>
                        <input type="hidden" name="search_word" value="{{request('search_word')}}">
                        <button type="submit" class="btn btn-primary">Сортировать</button>
                    </form>
                    <div class="pull-right">
                        <div class="b-menu_btn">
                            <form method="GET" action="{{route('product.create')}}" class="form-inline">
                                <button type="submit" class="btn btn-primary">Добавить товар</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th class="col-lg-1 col-md-1 col-sm-1 col-xs-1">#</th>
                    <th class="col-lg-3 col-md-3 col-sm-3 col-xs-3">Заглавие</th>
                    <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">Адрес</th>
                    <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">Посл. изм.</th>
                    <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">Статус</th>
                    <th class="col-lg-2 col-md-2 col-sm-2 col-xs-2">Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($products as $index=>$product)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td><a href="{{route('product.edit', $product->id)}}">{{$product->page->title}}</a></td>
                        <td>{{$product->page->url}}</td>
                        <td>{{$product->updated_at}}</td>
                        <td>{{$product->page->published == 1? 'Опубликован ' : ''}}
                            {{$product->page->archived == 1? ' Архив ' : ''}}
                        </td>
                        <td>
                            <a class="btn btn-default" href="{{route('product.edit', $product->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-product_delete"
                                  action="{{route('product.destroy', $product->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $products->links() }}
            </div>

        </div>
    </div>
@endsection