<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <!-- Styles -->
    <link href="{{ asset('admin_assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/css/temp.css') }}" rel="stylesheet">
    <script src="{{ asset('admin_assets/js/libs.js') }}"></script>
    <script src="{{ asset('admin_assets/js/app.js') }}"></script>
    <script src="{{asset('admin_assets/js/tinymce/tinymce.min.js')}}"></script>
</head>
