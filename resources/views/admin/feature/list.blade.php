@extends('admin.app')

@section('content')
    <div class="panel panel-default b-feature">
        <div class="panel-heading">Характеристики товаров</div>

        <div class="panel-body">

            <form method="GET" action="{{route('feature.create')}}" class="form-inline">
                <button type="submit" class="btn btn-primary">Добавить характеристику</button>
            </form>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    {{--<th>Значение по умолчанию</th>--}}
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($features as $feature)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$feature->id }} </td>
                        <td><a href="{{route('feature.edit', $feature->id)}}">
                                {{$feature->name}}
                            </a></td>
                        {{--<td>{{$feature->default_value}}</td>--}}
                        <td>
                            <a class="btn btn-default" title="Редактировать"
                               href="{{route('feature.edit', $feature->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>

                            <form method="POST"
                                  class="form-inline b-filter-field__delete"
                                  action="{{route('feature.destroy', $feature->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="field_id" value="{{$feature->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection