@extends('admin.app')

@section('content')
    <div class="panel panel-default b-filter">
        <div class="panel-heading">Создать характеристику товара</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('feature.store')}}">
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                </div>

                {{--<div class="form-group">--}}
                    {{--<label for="default_value">Значение по умолчанию</label>--}}
                    {{--<input type="text" name="default_value" id="default_value" class="form-control" value="{{ old('default_value') }}">--}}
                {{--</div>--}}

                <div class="form-group">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <label for="category">Категории</label>
                            <div class="text-muted small">
                                Для выбора нескольких категорий удерживайте клавишу CTRL
                            </div>
                            <select name="categories[]"
                                    id="category"
                                    class="form-control"

                                    multiple>
                                <option value="0">Без категории</option>
                                @foreach($category->firstLevel() as $first)
                                    <option value="{{$first->id}}">
                                        {{$first->page->title}}
                                    </option>
                                    @if ($first->hasChildren())
                                        @foreach($first->children() as $second)
                                            <option value="{{$second->id}}" >
                                                &nbsp;&nbsp;{{$second->page->title}}
                                            </option>
                                            @if($second->hasChildren())
                                                @foreach($second->children() as $third)
                                                    <option value="{{$third->id}}">
                                                        &nbsp;&nbsp;&nbsp;&nbsp;{{$third->page->title}}
                                                    </option>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection

