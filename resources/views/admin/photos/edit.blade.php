@extends('admin.app')

@section('content')
    <div class="panel panel-default b-photos">
        <div class="panel-heading">Редактировать фото</div>

        <div class="panel-body col6">
            <form method="POST"
                  action="{{route('photos.update', $photo->id)}}"
                  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

                {{method_field('PUT')}}
                {{csrf_field()}}

                <div>
                    <img src="/{{$photo->thumbnail_path}}" alt="">
                </div>

                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{$photo->name}}">
                </div>

                <div class="form-group">
                    <label for="name">Адрес</label>
                    <div class="">/{{$photo->path}}</div>
                </div>

                <div class="form-group">
                    <label for="name">Размеры</label>
                    <div class="">{{$width}} x {{$height}}</div>
                </div>

                <div class="form-group">
                    <label for="name">Загружено</label>
                    <div class="">{{$photo->created_at}}</div>
                </div>

                <button type="submit" class="btn btn-default">Обновить</button>

            </form>
        </div>
    </div>
@endsection


