@extends('admin.app')

@section('content')
    <div class="panel panel-default b-photos">
        <div class="panel-heading">Все фото</div>

        <div class="panel-body">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 b-photos-menu">
                <div class="row">
                    <form method="GET" class=" form-inline"
                          action="{{route('photos.index')}}">
                        <div class="form-group">
                            <input type="text"
                                   placeholder="Поиск..."
                                   name="search_word"
                                   id="search_word"
                                   class="form-control"
                                   value="{{ old('search_word') }}">
                        </div>
                        <button type="submit" class="btn btn-primary">Поиск</button>
                    </form>
                    <form method="GET"
                          class="form-inline"
                          action="{{route('photos.index')}}">
                        <div class="form-group">
                            <select name="sort" id="sort" class="form-control">
                                <option value="1"
                                        {{ old('sort') == 1 ? "selected" : '' }}>
                                    Дате создания
                                </option>
                                <option value="2" {{ old('sort') == 2 ? "selected" : '' }}>
                                    Дате изменения
                                </option>
                                <option value="3" {{ old('sort') == 3 ? "selected" : '' }}>
                                    Имени
                                </option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Сортировать</button>
                    </form>
                    <div class="pull-right">
                        <a href="#" class="btn btn-primary b-photos__multi">Выбрать несколько фото</a>
                        <form method="POST"
                              class="form-inline"
                              action="{{route('photos.delete.multi')}}">

                            {{csrf_field()}}

                            <button type="submit" class="btn btn-default b-photos__delete hidden">
                                Удалить выбранные
                            </button>
                        </form>
                    </div>
                </div>
            </div>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 b-photos_gallery">
                @foreach($photos->chunk(6) as $set)
                    <div class="row">
                        @foreach($set as $photo)
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                {{--used for multiple selection--}}
                                <div class="b-photos__checkbox hidden">
                                    <input type="checkbox" data-id="{{$photo->id}}">
                                </div>
                                <div class="b-photos_gallery__name">{{$photo->name}}</div>
                                <form method="POST"
                                      action="{{route('photos.destroy', $photo->id)}}">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <button type="submit"
                                            class="btn btn-default b-photos_remove">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </form>
                                <a href="{{route('photos.edit', $photo->id)}}"
                                   class="btn btn-default b-photos_edit"
                                   title="Редактировать">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </a>
                                <a href="/{{$photo->path}}"
                                   class="btn btn-default b-photos_copy"
                                   title="Копировать адрес">
                                    <span class="glyphicon glyphicon-copy"></span>
                                </a>
                                <a href="/{{$photo->path}}" data-lity>
                                    <img src="/{{$photo->thumbnail_path}}">
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $photos->links() }}
            </div>

        </div>
    </div>
@endsection