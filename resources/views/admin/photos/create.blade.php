@extends('admin.app')

@section('content')
    <div class="panel panel-default b-photos">
        <div class="panel-heading">Добавить фото</div>

        <div class="panel-body">
            <form method="POST"
                  id="addPhotos"
                  class="dropzone"
                  enctype="multipart/form-data"
                  action="{{route('photos.store')}}">

                {{csrf_field()}}
            </form>
        </div>
    </div>
@endsection


