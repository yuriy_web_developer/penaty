@extends('admin.app')

@section('content')
    <div class="panel panel-default b-email">
        <div class="panel-heading">Редактировать шаблон</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('email.update', $email->id)}}">

                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $email->title }}">
                </div>

                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ $email->name }}">
                </div>

                <div class="form-group">
                    <label for="body">Шаблон</label>
                    <textarea name="body" id="body" class="form-control">{!! $email->body !!}</textarea>
                </div>

                <div class="form-group">
                    <div class="text-muted">Доступные шорткоды:</div>
                    @if(array_key_exists($email->name, $email->shortCode))
                        @foreach($email->shortCode[$email->name] as $code)
                            |||{{$code}}||| <br>
                        @endforeach
                        <br>
                    @endif
                    <div class="small">Для добавления новых шорткодов обратитесь к разработчикам.</div>

                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
