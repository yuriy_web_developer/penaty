@extends('admin.app')

@section('content')
    <div class="panel panel-default b-email">
        <div class="panel-heading">Создать шаблон</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('email.store')}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="body">Шаблон</label>
                    <textarea name="body" id="body" class="form-control">{!! old('body') !!}</textarea>
                </div>

                {{csrf_field()}}

                <div class="text-muted">После создания шаблона обратитесь в разработчикам.</div><br>
                <button type="submit" class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection