@extends('admin.app')

@section('content')
    <div class="panel panel-default b-email">
        <div class="panel-heading">Шаблоны писем</div>

        <div class="panel-body">

            <form method="GET" action="{{route('email.create')}}" class="form-inline">
                <button type="submit" class="btn btn-primary">Добавить шаблон</button>
            </form>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($emails as $email)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$email->id }} </td>
                        <td><a href="{{route('email.edit', $email->id)}}">
                                {{$email->title}}
                            </a></td>
                        <td>
                            <a class="btn btn-default" title="Редактировать"
                               href="{{route('email.edit', $email->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>

                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('email.destroy', $email->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="field_id" value="{{$email->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection