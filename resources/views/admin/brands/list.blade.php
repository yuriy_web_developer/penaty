@extends('admin.app')

@section('content')
    <div class="panel panel-default b-brand">
        <div class="panel-heading">Брэнды</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('brand.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Добавить брэнд</button>
                </form>
                <form method="POST" action="{{route('brand.order')}}" class="form-inline b-brand_btn__order">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary">Сохранить порядок брэндов</button>
                </form>
            </div>

            <div class="text-muted small">
                Перетащите брэнды, чтобы изменить их порядок.
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($brands as $brand)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$brand->order_num }} </td>
                        <td><a href="{{route('brand.edit', $brand->id)}}">{{$brand->title}}</a></td>
                        <td>{{$brand->created_at}}</td>
                        <td>
                            <a class="btn btn-default" title="Редактировать" href="{{route('brand.edit', $brand->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-brand_delete"
                                  action="{{route('brand.destroy', $brand->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="brand_id" value="{{$brand->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection