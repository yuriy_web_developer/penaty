@extends('admin.app')

@section('content')
    <div class="panel panel-default b-brand">
        <div class="panel-heading">Добавить брэнд</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('brand.update', $brand->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $brand->title }}">
                </div>

                <div class="form-group">
                    <label for="link">Ссылка</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ $brand->link }}">
                </div>

                <div class="form-group">
                    <label for="b-brand_tinymce-image">Изображение</label>
                    <textarea name="photo" id="b-brand_tinymce-image"
                              class="form-control">@if($brand->photo)
                            <img src="/{{$brand->photo->path}}" alt="">
                        @endif
                    </textarea>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Сохранить</button>
            </form>
        </div>
    </div>
@endsection
