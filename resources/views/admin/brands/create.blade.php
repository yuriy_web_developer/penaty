@extends('admin.app')

@section('content')
    <div class="panel panel-default b-brand">
        <div class="panel-heading">Добавить брэнд</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('brand.store')}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="link">Ссылка</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ old('link') }}">
                </div>

                <div class="form-group">
                    <label for="b-brand_tinymce-image">Изображение</label>
                    <textarea name="photo" id="b-brand_tinymce-image"
                              class="form-control">{{ old('photo') }}</textarea>
                </div>

                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Добавить</button>
            </form>
        </div>
    </div>
@endsection
