@extends('admin.app')

@section('content')
    <div class="panel panel-default b-news">
        <div class="panel-heading">Создать фильтр</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('filter.store')}}">
                <div class="form-group">
                    <label for="name">Заглавие</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="type"></label>
                    <select name="type" id="type" class="form-control">
                        <option></option>
                        <option value="slider">Слайдер</option>
                        <option value="select">Выпадающий список</option>
                        <option value="button">Кнопка</option>
                    </select>
                </div>


                <div class="form-group">
                    <label for="filter_category">Прикрепить к категориям</label>
                    <div class="muted">Удерживайте ctrl для выбора нескольких категорий</div>
                    <select
                            name="category[]"
                            id="filter_category"
                            class="form-control"
                            size="10"
                            multiple>
                        <option value="" disabled selected>Выберите категория</option>
                        {{--@foreach($categories as $category)--}}
                            {{--<option value="{{$category->id}}">{{$category->page->title}}</option>--}}
                        {{--@endforeach--}}
                        <option value="0">Без категории</option>
                        @foreach($categories[0]->firstLevel() as $first)
                            <option value="{{$first->id}}">
                                {{$first->page->title}}
                            </option>
                            @if ($first->hasChildren())
                                @foreach($first->children() as $second)
                                    <option value="{{$second->id}}">
                                        &nbsp;&nbsp;{{$second->page->title}}
                                    </option>
                                    @if($second->hasChildren())
                                        @foreach($second->children() as $third)
                                            <option value="{{$third->id}}">
                                                &nbsp;&nbsp;&nbsp;&nbsp;{{$third->page->title}}
                                            </option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>

                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection
