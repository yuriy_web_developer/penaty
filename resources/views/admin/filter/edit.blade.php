@extends('admin.app')

@section('content')
    <div class="panel panel-default b-filter">
        <div class="panel-heading">Редактировать фильтр</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('filter.update', $filter->id)}}">
                <div class="form-group">
                    <label for="name">Заглавие</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ $filter->name  }}">
                </div>

                <div class="form-group">
                    <label for="type"></label>
                    <select name="type" id="type" class="form-control">
                        <option></option>
                        <option value="slider"
                                {{$filter->type == 'slider' ? ' selected ':''}}>Слайдер</option>
                        <option value="select"
                                {{$filter->type == 'select' ? ' selected ':''}}>Выпадающий список</option>
                        <option value="button"
                                {{$filter->type == 'button' ? ' selected ':''}}>Кнопка</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="filter_category">Прикрепить к категориям</label>
                    <div class="muted">Удерживайте ctrl для выбора нескольких категорий</div>
                    <select
                            name="category[]"
                            id="filter_category"
                            class="form-control"
                            size="10"
                            multiple>
                        <option value="">Без категории</option>
                        @foreach($categories[0]->firstLevel() as $first)
                            <option value="{{$first->id}}"
                                    {{$filter->hasCategory($first) ? ' selected ': '' }}>
                                {{$first->page->title}}
                            </option>
                            @if ($first->hasChildren())
                                @foreach($first->children() as $second)
                                    <option value="{{$second->id}}"
                                            {{$filter->hasCategory($second) ? ' selected ': '' }}>
                                        &nbsp;&nbsp;{{$second->page->title}}
                                    </option>
                                    @if($second->hasChildren())
                                        @foreach($second->children() as $third)
                                            <option value="{{$third->id}}"
                                                    {{$filter->hasCategory($third) ? ' selected ': '' }}>
                                                &nbsp;&nbsp;&nbsp;&nbsp;{{$third->page->title}}
                                            </option>
                                        @endforeach
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>
                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
