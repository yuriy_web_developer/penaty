@extends('admin.app')

@section('content')
    <div class="panel panel-default b-filter">
        <div class="panel-heading">Фильтры</div>

        <div class="panel-body">


            <form method="GET" action="{{route('filter.create')}}" class="form-inline b-filter__create">
                <button type="submit" class="btn btn-primary">Создать фильтр</button>
            </form>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Посл. изменения</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($filters as $filter)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$filter->id }} </td>
                        <td><a href="{{route('filter.edit', $filter->id)}}">{{$filter->name}}</a></td>
                        <td>{{$filter->updated_at}}</td>
                        <td>
                            <a class="btn btn-default" title="Редактировать"
                               href="{{route('filter.edit', $filter->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>

                            <form method="POST"
                                  class="form-inline b-filter_delete"
                                  action="{{route('filter.destroy', $filter->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="filter_id" value="{{$filter->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection