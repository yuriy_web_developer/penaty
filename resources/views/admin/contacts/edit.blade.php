@extends('admin.app')

@section('content')
    <div class="panel panel-default b-main-menu">
        <div class="panel-heading">Изменить контактную инорфмацию</div>

        <div class="panel-body">
            <form method="POST"
                  class="col-lg-6 col-md-6 col-sm-6 col-xs-6"
                  action="{{route('contacts.update')}}">

                <div class="form-group">
                    <label for="phone_header">Телефон в шапке</label>
                    <textarea rows="6"
                              name="phone_header"
                              id="phone_header"
                              class="form-control">{{$contacts['phone_header']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="email_header">Email в шапке</label>
                    <textarea rows="6"
                              name="email_header"
                              id="email_header"
                              class="form-control">{{$contacts['email_header']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="address_header">Адрес в шапке</label>
                    <textarea rows="6"
                              name="address_header"
                              id="address_header"
                              class="form-control">{{$contacts['address_header']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="address_footer">Адрес в футере</label>
                    <textarea rows="6"
                              name="address_footer"
                              id="address_footer"
                              class="form-control">{{$contacts['address_footer']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="time_footer">Время работы в футере</label>
                    <textarea rows="6"
                              name="time_footer"
                              id="time_footer"
                              class="form-control">{{$contacts['time_footer']}}</textarea>
                </div>


                <div class="form-group">
                    <label for="email_footer">Email в футере</label>
                    <textarea rows="2"
                              name="email_footer"
                              id="email_footer"
                              class="form-control">{{$contacts['email_footer']}}</textarea>
                </div>

                <div class="form-group">
                    <label for="phone_footer">Телефон в футере</label>
                    <textarea rows="5"
                              name="phone_footer"
                              id="phone_footer"
                              class="form-control">{{$contacts['phone_footer']}}</textarea>
                </div>
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
