@extends('admin.app')

@section('content')
    <div class="panel panel-default b-portfolio">
        <div class="panel-heading">Добавить объект</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('portfolio.store')}}"
                  enctype="multipart/form-data">

                <div class="form-group">
                    <div>Фото</div>
                    <div class="">
                    </div>
                </div>
                <a href="/admin_assets/filemanager/dialog.php?type=0&field_id=b-portfolio__photo&relative_url=1"
                   class="btn btn-primary b-portfolio__link">Открыть галерею</a>
                <input type="hidden" name="b-portfolio__photo" id="b-portfolio__photo" value="">

                <div class="b-portfolio_list">
                    <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    </ul>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 row">
                    <div class="form-group">
                        <label for="title">Заглавие</label>
                        <textarea rows="6" name="title" id="title" class="form-control">{{ old('title') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="keywords">Ключевые слова (мета-данные)</label>
                        <textarea rows="6" name="keywords" id="keywords" class="form-control">{{ old('keywords') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="description">Описание (мета-данные)</label>
                        <textarea rows="6" name="description" id="description" class="form-control">{{ old('description') }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="url">Адрес</label>
                        <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}">
                    </div>

                    <div class="form-group">
                        <label for="short_content">Описание объекта</label>
                        <textarea rows="6" name="short_content" id="short_content" class="form-control">{{ old('short_content') }}</textarea>
                    </div>

                    {{csrf_field()}}

                    <button type="submit" class="btn btn-default">Добавить</button>
                </div>
            </form>
        </div>
    </div>
@endsection

