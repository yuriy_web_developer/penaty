@extends('admin.app')

@section('content')
    <div class="panel panel-default b-portfolio">
        <div class="panel-heading">Объекты</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('portfolio.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Добавить объект</button>
                </form>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Заглавие</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($objects as $object)
                    <tr>
                        <td><a href="{{route('portfolio.edit', $object->id)}}">{{$object->page->title}}</a></td>
                        <td>{{$object->created_at}}</td>
                        <td>
                            <a class="btn btn-default" href="{{route('portfolio.edit', $object->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('portfolio.destroy', $object->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{{$objects->links()}}</div>
        </div>
    </div>
@endsection