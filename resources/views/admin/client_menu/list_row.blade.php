<div class="row">
    <div class="b-menu_sortable__title col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <a href="{{route('menu.edit', $item->id)}}">
            {{$loop->iteration}}. {{$item['item_name']}}
        </a>
    </div>
    <div class="b-menu_sortable_btn col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <a class="btn btn-default b-menu_sortable__edit"
           href="{{route('menu.edit', $item->id)}}">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <form method="POST"
              class="form-inline"
              action="{{route('menu.destroy', $item->id)}}"
              onsubmit="return confirm('Удалить пункт меню?')">

            {{method_field('DELETE')}}
            {{csrf_field()}}

            {{--this field is used for saving items order--}}
            <input type="hidden" name="item_id" value="{{$item->id}}" data-parent="{{$item->parent_id}}">

            <button type="submit" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </form>
    </div>
</div>