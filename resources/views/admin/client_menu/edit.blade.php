@extends('admin.app')

@section('content')
    <div class="panel panel-default b-main-menu">
        <div class="panel-heading">Редактировать пункт в меню</div>

        <div class="panel-body">
            <form method="POST"
                  class="col-lg-6 col-md-6 col-sm-6 col-xs-6"
                  action="{{route('menu.update', $item->id)}}">
                <div class="form-group">
                    <label for="item_name">Заглавие</label>
                    <input type="text" name="item_name" id="item_name" class="form-control"
                           value="{{ $item->item_name }}">
                </div>
                <div class="form-group">
                    <label for="url">Адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{ $item->url }}">
                </div>

                <div class="form-group">
                    <label for="parent_id">Родительский раздел</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option value="0">Нет родительского раздела</option>
                        @foreach($menu->getFirstLevel() as $first)
                            <option value="{{$first->id}}"
                                    {{$item->parent_id == $first->id ? ' selected ': '' }}
                                    {{$item->id == $first->id ? ' disabled ': '' }}>
                                {{$first->item_name}}
                            </option>
                            @if ($first->getChildren($first->id))
                                @foreach($first->getChildren($first->id) as $second)
                                    <option value="{{$second->id}}"
                                            {{$item->parent_id == $second->id ? ' selected ': '' }}
                                            {{$item->id == $second->id ? ' disabled ': '' }}>
                                        &nbsp;&nbsp;{{$second->item_name}}
                                    </option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>

                {{csrf_field()}}
                {{method_field('PUT')}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
