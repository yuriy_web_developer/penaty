@extends('admin.app')
{{--@inject('menu', '\App\Models\Menu')--}}

@section('content')
    <div class="panel panel-default b-menu">
        <div class="panel-heading">Главное меню</div>

        <div class="panel-body">
            <div class="b-menu_btn">
                <form method="GET" action="{{route('menu.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Создать раздел меню</button>
                </form>
                <form method="POST" action="{{route('menu.order')}}" class="form-inline b-menu_btn__order">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary">Сохранить порядок пунктов</button>
                </form>
            </div>

            <div class="text-muted small">
                Перетащите пункты меню, чтобы изменить их порядок.
            </div>

            <ul class="b-menu_sortable_level0">
                @foreach($items as $first)
                    <li>
                        @include('admin.client_menu.list_row', ['item'=>$first])
                        @if($first->hasChildren($first->id))
                            <ul class="b-menu_sortable_level1">
                                @foreach($first->getChildren($first->id) as $second)
                                    <li>
                                        @include('admin.client_menu.list_row', ['item'=>$second])
                                    </li>

                                    @if($second->hasChildren($second->id))
                                        <ul class="b-menu_sortable_level2">
                                            @foreach($second->getChildren($second->id) as $third)
                                                <li>
                                                    @include('admin.client_menu.list_row', ['item'=>$third])
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach

                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection