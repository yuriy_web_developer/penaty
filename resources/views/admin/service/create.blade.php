@extends('admin.app')

@section('content')
    <div class="panel panel-default b-service">
        <div class="panel-heading">Создать слайд</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('service.store')}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <textarea rows="6" name="title" id="title" class="form-control">{{ old('title') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <textarea rows="6" name="description" id="description" class="form-control">{{ old('description') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="photo">Фото</label>
                    <textarea rows="6" name="photo" id="photo" class="form-control">{{ old('photo') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="link">Ссылка для кнопки подробнее</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ old('link') }}">
                </div>

                <input type="hidden" name="name" value="{{request('name')}}">

                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Создать</button>

            </form>
        </div>
    </div>
@endsection
