@extends('admin.app')

@section('content')
    <div class="panel panel-default b-service">
        <div class="panel-heading">Услуги</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('service.create', ['name'=>'main'])}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Добавить блок</button>
                </form>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($services as $service)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$loop->index+1}} </td>
                        <td><a href="{{route('service.edit', $service->id)}}">{!! $service->title !!}</a></td>
                        <td>{{$service->created_at}}</td>
                        <td>
                            <a class="btn btn-default" href="{{route('service.edit', $service->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('service.destroy', $service->id)}}">
                                <input type="hidden" name="name" value="{{$service->name}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection