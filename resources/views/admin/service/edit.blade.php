@extends('admin.app')

@section('content')
    <div class="panel panel-default b-service">
        <div class="panel-heading">Редактировать блок "Услуги"</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('service.update', $service->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <textarea rows="6" name="title" id="title" class="form-control">{!! $service->title  !!}</textarea>
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <textarea rows="6" name="description" id="description" class="form-control">{!! $service->description !!}</textarea>
                </div>

                <div class="form-group">
                    <label for="photo">Фото</label>
                    <textarea rows="6" name="photo" id="photo" class="form-control">
                        @if($service->photo)
                            <img src="/{{ $service->photo->path }}" alt="">
                        @endif
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="link">Ссылка для кнопки подробнее</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ $service->link }}">
                </div>

                <input type="hidden" name="name" value="{{$service->name}}">

                {{csrf_field()}}
                {{method_field('PUT')}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
