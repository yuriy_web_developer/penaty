@extends('admin.app')

@section('content')
    <div class="panel panel-default b-callback">
        <div class="panel-heading">Обратная связь</div>

        <div class="panel-body">
            <table class="table table-bordered table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th>ФИО</th>
                    <th>Телефон</th>
                    <th>Email</th>
                    <th>Тип ремонта</th>
                    <th>Город</th>
                    <th>Комментарий</th>
                    <th>Создан</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach($callbacks as $index=>$callback)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td>{{$callback->client_name}}</td>
                        <td>{{$callback->phone}}</td>
                        <td>{{$callback->email}}</td>
                        <td>{{$callback->repair_type}}</td>
                        <td>{{$callback->city}}</td>
                        <td>{{$callback->comment}}</td>
                        <td>{{$callback->created_at}}</td>
                        <td>
                            <form method="POST"
                                  class="form-inline b-callback_delete"
                                  action="{{route('callback.destroy', $callback->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $callbacks->links() }}
            </div>
        </div>
    </div>
@endsection