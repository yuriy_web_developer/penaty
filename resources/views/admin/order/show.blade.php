@extends('admin.app')

@section('content')
    <div class="panel panel-default b-order">
        <div class="panel-heading">Заказ</div>

        <div class="panel-body">

            <table class="table table-bordered table-hover">
                <tbody>
                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>ФИО</b></td>
                    <td>{{$order->client_name}}</td>
                </tr>
                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>Телефон</b></td>
                    <td>{{$order->phone}}</td>
                </tr>

                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>Email</b></td>
                    <td>{{$order->email}}</td>
                </tr>
                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>Город</b></td>
                    <td>{{$order->city}}</td>
                </tr>
                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>Товары</b></td>
                    <td>
                        @foreach($order->products as $product)
                            {{$loop->index+1}}.{{$product->page->title}}<br>
                            Кол-во: {{$product->pivot->quantity}}шт.<br>
                            Объем:{{$product->pivot->volume}} л.<br>
                            Цвет:
                            @if($product->pivot->color_id)
                                {{$product
                                ->colors()
                                ->find($product->pivot->color_id)
                                ->pivot
                                ->color_name}}
                            @endif
                            <br><br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td class="col-lg-2 col-md-2 col-sm-2 col-xs-2"><b>Комментарий</b></td>
                    <td>{{$order->comment}}</td>
                </tr>
                </tbody>
            </table>


        </div>
    </div>
@endsection