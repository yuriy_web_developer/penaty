@extends('admin.app')

@section('content')
    <div class="panel panel-default b-order">
        <div class="panel-heading">Заказы</div>

        <div class="panel-body">
            <table class="table table-bordered table-hover">

                <thead>
                <tr>
                    <th>#</th>
                    <th>ФИО</th>
                    <th>Телефон</th>
                    <th>Email</th>
                    <th>Город</th>
                    <th>Создан</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                @foreach($orders as $index=>$order)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td><a href="{{route('order.show', $order->id)}}">{{$order->client_name}}</a></td>
                        <td>{{$order->phone}}</td>
                        <td>{{$order->email}}</td>
                        <td>{{$order->city}}</td>
                        <td>{{$order->created_at}}</td>
                        <td>
                            <form method="POST"
                                  class="form-inline b-order_delete"
                                  action="{{route('order.destroy', $order->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection