@extends('admin.app')

@section('content')
    <div class="panel panel-default b-design-studio">
        <div class="panel-heading">Дизайн студия</div>

        <div class="panel-body">
            <div class="b-menu_btn">
                <form method="GET" action="{{route('design_studio.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Создать блок</button>
                </form>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Заглавие</th>
                    <th>Адрес</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($blocks as $block)
                    <tr>
                        <td><a href="{{route('design_studio.edit', $block->id)}}">{{$block->title}}</a></td>
                        <td><a href="{{route('design_studio.edit', $block->id)}}">{{$block->url}}</a></td>
                        <td>
                            <a class="btn btn-default" title="Редактировать" href="{{route('design_studio.edit', $block->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('design_studio.destroy', $block->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="block_id" value="{{$block->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection