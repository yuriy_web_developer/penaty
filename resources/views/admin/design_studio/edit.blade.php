@extends('admin.app')

@section('content')
    <div class="panel panel-default b-design_studio">
        <div class="panel-heading">Редактировать блок</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('design_studio.update', $block->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $block['title'] }}">
                </div>

                <div class="form-group">
                    <label for="title">Url адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{$block['url'] }}">
                </div>

                <div class="form-group">
                    <label for="b-design_studio-image">Изображение</label>
                    <textarea name="photo" id="b-design_studio-image"
                              class="form-control">
                        @if ($block->photo)
                            <img src="/{{$block->photo->path}}" alt="">
                        @endif
                    </textarea>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
