@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Редактировать подписчика</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('subscriber.update', $subscriber->id)}}">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ $subscriber->email }}">
                </div>

                <div class="form-group">
                    <label for="phone">Телефон</label>
                    <input type="text" name="phone" id="phone" class="form-control" value="{{ $subscriber->phone }}">
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection


