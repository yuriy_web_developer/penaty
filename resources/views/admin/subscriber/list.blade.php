@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Подписчики</div>

        <div class="panel-body">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <form method="GET" class=" form-inline"
                          action="{{route('subscriber.index')}}">
                        <div class="form-group">
                            <input type="text"
                                   placeholder="Поиск по email или тел."
                                   name="search_word"
                                   id="search_word"
                                   class="form-control"
                                   value="{{ request('search_word') }}">
                        </div>
                        <input type="hidden" name="sort" value="{{request('sort')}}">
                        <button type="submit" class="btn btn-primary">Поиск</button>
                    </form>
                    <form method="GET"
                          class="form-inline"
                          action="{{route('subscriber.index')}}">
                        <div class="form-group">
                            <select name="sort" id="sort" class="form-control">
                                <option value="1"
                                        {{ request('sort') == 1 ? "selected" : '' }}>
                                    Дате создания
                                </option>
                                <option value="2" {{request('sort') == 2 ? "selected" : '' }}>
                                    Телефону
                                </option>
                                <option value="3" {{ request('sort') == 3 ? "selected" : '' }}>
                                    Email
                                </option>
                            </select>
                        </div>
                        <input type="hidden" name="search_word" value="{{request('search_word')}}">
                        <button type="submit" class="btn btn-primary">Сортировать</button>
                    </form>
                </div>
            </div>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Email</th>
                    <th>Телефон</th>
                    <th>Создана</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($subscribers as $index=>$subscriber)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td><a href="{{route('subscriber.edit', $subscriber->id)}}">{{$subscriber->email}}</a></td>
                        <td><a href="{{route('subscriber.edit', $subscriber->id)}}">{{$subscriber->phone}}</a></td>
                        <td>{{$subscriber->created_at}}</td>


                        <td>
                            <a class="btn btn-default" href="{{route('subscriber.edit', $subscriber->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-subscriber_delete"
                                  action="{{route('subscriber.destroy', $subscriber->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $subscribers->links() }}
            </div>

        </div>
    </div>
@endsection