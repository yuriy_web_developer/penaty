@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Редактировать страницу</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('info_page.update', $page->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $page->title }}">
                </div>

                <div class="form-group">
                    <label for="title">Url адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{ $page->url }}">
                </div>

                <div class="form-group">
                    <label for="title">Ключевые слова</label>
                    <input type="text"
                           name="keywords"
                           id="keywords"
                           class="form-control"
                           value="{{ $page->keywords }}">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text"
                           name="description"
                           id="description"
                           class="form-control"
                           value="{{ $page->description }}">
                </div>

                <div class="form-group">
                    <label for="b-info-page_tinymce-image">Изображение</label>
                    <textarea name="photo" id="b-info-page_tinymce-image"
                              class="form-control">
                         @if($page->photo)
                            <img src="/{{ $page->photo->path}}" alt="">
                        @endif
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="text">Содержание</label>
                    <textarea name="content" id="b-info-page_tinymce"
                              class="form-control">{!! $page->content !!}</textarea>
                </div>

                <div class="checkbox">
                    <label><input
                                type="checkbox"
                                name="published"
                                value="1"
                                {{$page->published == 1 ? 'checked' : ''}}>Опубликовать</label>
                </div>
                <div class="checkbox">
                    <label><input
                                type="checkbox"
                                name="archived"
                                value="1"
                                {{$page->archived == 1 ? 'checked' : ''}}>Архив</label>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection


