@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Создать страницу</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('info_page.store')}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="title">Url адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}">
                </div>


                <div class="form-group">
                    <label for="title">Ключевые слова</label>
                    <input type="text"
                           name="keywords"
                           id="keywords"
                           class="form-control"
                           value="{{ old('keywords') }}">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text"
                           name="description"
                           id="description"
                           class="form-control"
                           value="{{ old('description') }}">
                </div>

                <div class="form-group">
                    <label for="b-info-page_tinymce-image">Изображение</label>
                    <textarea name="photo" id="b-info-page_tinymce-image" class="form-control">{{ old('photo') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="b-info-page_tinymce">Содержание</label>
                    <textarea name="content" id="b-info-page_tinymce" class="form-control">{{ old('content') }}</textarea>
                </div>

                <div class="checkbox">
                    <label><input type="checkbox" name="published" value="1">Опубликовать</label>
                </div>
                <div class="checkbox">
                    <label><input type="checkbox" name="archived" value="1">Архив</label>
                </div>

                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection


