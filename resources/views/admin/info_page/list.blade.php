@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Информационные страницы</div>

        <div class="panel-body">


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <form method="GET" class=" form-inline"
                          action="{{route('info_page.index')}}">
                        <div class="form-group">
                            <input type="text"
                                   placeholder="Поиск по заглавию."
                                   name="search_word"
                                   id="search_word"
                                   class="form-control"
                                   value="{{ request('search_word') }}">
                        </div>
                        <input type="hidden" name="sort" value="{{request('sort')}}">
                        <button type="submit" class="btn btn-primary">Поиск</button>
                    </form>
                    <form method="GET"
                          class="form-inline"
                          action="{{route('info_page.index')}}">
                        <div class="form-group">
                            <select name="sort" id="sort" class="form-control">
                                <option value="1"
                                        {{ array_key_exists('sort', $req) && $req['sort'] == 1 ? "selected" : '' }}>
                                    Дате создания
                                </option>
                                <option value="2" {{ array_key_exists('sort', $req) && $req['sort'] == 2 ? "selected" : '' }}>
                                    Дате изменения
                                </option>
                                <option value="3" {{ array_key_exists('sort', $req) && $req['sort'] == 3 ? "selected" : '' }}>
                                    Заглавию
                                </option>
                            </select>
                        </div>
                        <input type="hidden" name="search_word" value="{{request('search_word')}}">
                        <button type="submit" class="btn btn-primary">Сортировать</button>
                    </form>
                    <div class="pull-right">
                        <div class="b-menu_btn">
                            <form method="GET" action="{{route('info_page.create')}}" class="form-inline">
                                <button type="submit" class="btn btn-primary">Создать страницу</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Посл. изменения</th>
                    <th>Создана</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($pages as $index=>$page)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td><a href="{{route('info_page.edit', $page->id)}}">{{$page->title}}</a></td>
                        <td>{{$page->updated_at}}</td>
                        <td>{{$page->created_at}}</td>
                        <td>{{$page->published == 1? 'Опубликована ' : ''}}
                            {{$page->archived == 1? 'Архив ' : ''}}
                        </td>
                        <td>
                            <a class="btn btn-default" href="{{route('info_page.edit', $page->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-info-page_delete"
                                  action="{{route('info_page.destroy', $page->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="news_id" value="{{$page->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $pages->links() }}
            </div>

        </div>
    </div>
@endsection