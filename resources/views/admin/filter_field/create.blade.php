@extends('admin.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Добавить пункт к фильтру</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('filter_field.store')}}">
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="b-filter_tinymce">Миниатюра</label>
                    <textarea name="photo"
                              id="b-filter_tinymce"
                              class="form-control">
                        {{--@if($category->photo)--}}
                            {{--<img src="/{{$category->photo->path}}">--}}
                        {{--@endif--}}
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="filter_id">Прикрепить к фильтру</label>
                    <select
                            name="filter_id"
                            id="filter_id"
                            class="form-control"
                            size="10">
                        <option value="" disabled selected>Выберите фильтр</option>
                        @foreach($filters as $filter)
                            <option value="{{$filter->id}}">{{$filter->name}}</option>
                        @endforeach
                    </select>
                </div>

                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection
