@extends('admin.app')

@section('content')
    <div class="panel panel-default b-filter">
        <div class="panel-heading">Пункты для фильтра</div>

        <div class="panel-body">

            <form method="GET" action="{{route('filter_field.create')}}" class="form-inline">
                <button type="submit" class="btn btn-primary">Добавить пункт в фильтр</button>
            </form>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Посл. изменения</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($fields as $field)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$field->id }} </td>
                        <td><a href="{{route('filter_field.edit', $field->id)}}">
                                @if($field->filter)
                                    {{$field->filter->name}} -
                                @endif
                                {{$field->name}}
                            </a></td>
                        <td>{{$field->updated_at}}</td>
                        <td>
                            <a class="btn btn-default" title="Редактировать"
                               href="{{route('filter_field.edit', $field->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>

                            <form method="POST"
                                  class="form-inline b-filter-field__delete"
                                  action="{{route('filter_field.destroy', $field->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="field_id" value="{{$field->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $fields->links() }}
            </div>

        </div>
    </div>
@endsection