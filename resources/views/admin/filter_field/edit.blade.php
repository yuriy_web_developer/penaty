@extends('admin.app')

@section('content')
    <div class="panel panel-default b-filter">
        <div class="panel-heading">Редактировать пункт для фильтра</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('filter_field.update', $field->id)}}">
                <div class="form-group">
                    <label for="name">Название</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ $field->name }}">
                </div>

                <div class="form-group">
                    <label for="b-filter_tinymce">Миниатюра</label>
                    <textarea name="photo"
                              id="b-filter_tinymce"
                              class="form-control">@if($field->photo)
                            <img src="/{{$field->photo->path}}">
                        @endif</textarea>
                </div>

                <div class="form-group">
                    <label for="filter_id">Прикрепить к фильтру</label>
                    <select
                            name="filter_id"
                            id="filter_id"
                            class="form-control"
                            size="10">
                        <option value="" disabled selected>Выберите фильтр</option>
                        @foreach($filters as $filter)
                            <option value="{{$filter->id}}"
                                    {{($field->filter_id == $filter->id) ?' selected ': ' '}}>{{$filter->name}}</option>
                        @endforeach
                    </select>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
