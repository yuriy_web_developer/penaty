@extends('admin.app')
@inject('pageObj', 'App\Models\Page')

@section('content')
    <div class="panel panel-default b-meta-data">
        <div class="panel-heading">Метаданные страниц</div>

        <div class="panel-body">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <form method="GET" class=" form-inline"
                          action="{{route('meta_data.index')}}">
                        <div class="form-group">
                            <input type="text"
                                   placeholder="Поиск по заглавию."
                                   name="search_word"
                                   id="search_word"
                                   class="form-control"
                                   value="{{ request('search_word') }}">
                        </div>
                        <input type="hidden" name="sort" value="{{request('sort')}}">
                        <button type="submit" class="btn btn-primary">Поиск</button>
                    </form>
                    <form method="GET"
                          class="form-inline"
                          action="{{route('meta_data.index')}}">
                        <div class="form-group">
                            <select name="sort" id="sort" class="form-control">
                                <option value="1"
                                        {{ array_key_exists('sort', $req) && $req['sort'] == 1 ? "selected" : '' }}>
                                    Дате создания
                                </option>
                                <option value="2" {{ array_key_exists('sort', $req) && $req['sort'] == 2 ? "selected" : '' }}>
                                    Дате изменения
                                </option>
                                <option value="3" {{ array_key_exists('sort', $req) && $req['sort'] == 3 ? "selected" : '' }}>
                                    Заглавию
                                </option>
                            </select>
                        </div>
                        <input type="hidden" name="search_word" value="{{request('search_word')}}">
                        <button type="submit" class="btn btn-primary">Сортировать</button>
                    </form>

                </div>
            </div>


            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Тип</th>
                    <th>Заглавие</th>
                    <th>Ключ.сл.</th>
                    <th>Описание</th>
                </tr>
                </thead>
                <tbody>

                @foreach($pages as $index=>$page)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td>{{$pageObj->getTypeName($page->type)}}</td>
                        <td><a href="{{route('meta_data.edit', $page->id)}}">{{$page->title}}</a></td>
                        <td>{{$page->keywords}}</td>
                        <td>{{$page->description}}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $pages->links() }}
            </div>

        </div>
    </div>
@endsection