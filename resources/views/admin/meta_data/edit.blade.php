@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Редактировать метаданные</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('meta_data.update', $page->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $page->title }}">
                </div>

                <div class="form-group">
                    <label for="keywords">Ключевые слова</label>
                    <input type="text" name="keywords" id="keywords" class="form-control"
                           value="{{ $page->metadata ? $page->metadata->keywords : '' }}">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <textarea name="description" id="description" rows="5"
                              class="form-control">{{ $page->metadata ? $page->metadata->description: '' }}</textarea>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection


