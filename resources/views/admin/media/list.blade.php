@extends('admin.app')

@section('content')
    <div class="panel panel-default b-media">
        <div class="panel-heading">Медиа файлы</div>

        <div class="panel-body">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <a class="btn btn-primary" href="{{route('media.create')}}">Добавить</a>
                </div>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($media as $index=>$one)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$lastPageNum + $index}}
                        </td>
                        <td><a href="/{{$one->path}}">{{$one->name}}</a></td>
                        <td>{{$one->created_at}}</td>
                        <td>
                            {{--<a class="btn btn-default" href="{{route('media.edit', $one->id)}}">--}}
                                {{--<span class="glyphicon glyphicon-pencil"></span>--}}
                            {{--</a>--}}
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('media.destroy', $one->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                {{ $media->links() }}
            </div>

        </div>
    </div>
@endsection