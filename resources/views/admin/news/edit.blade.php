@extends('admin.app')

@section('content')
    <div class="panel panel-default b-main-menu">
        <div class="panel-heading">Редактировать новость</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('news.update', $news->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $news->title }}">
                </div>

                <div class="form-group">
                    <label for="title">Url адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{ $news->url }}">
                </div>

                <div class="form-group">
                    <label for="title">Ключевые слова</label>
                    <input type="text"
                           name="keywords"
                           id="keywords"
                           class="form-control"
                           value="{{ $news->keywords }}">
                </div>

                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text"
                           name="description"
                           id="description"
                           class="form-control"
                           value="{{ $news->description }}">
                </div>

                <div class="form-group">
                    <label for="b-news_tinymce-image">Изображение</label>
                    <textarea name="photo" id="b-news_tinymce-image"
                              class="form-control">
                         @if($news->photo)
                            <img src="/{{ $news->photo->path}}" alt="">
                        @endif
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="short_content">Краткое содержание</label>
                    <textarea rows="6" name="short_content" id="short_content"
                              class="form-control">{{ $news->short_content }}</textarea>
                </div>

                <div class="form-group">
                    <label for="text">Содержание</label>
                    <textarea name="content" id="b-news_tinymce" class="form-control">{!! $news->content !!}</textarea>
                </div>

                <div class="checkbox">
                    <label><input
                                type="checkbox"
                                name="published"
                                value="1"
                                {{$news->published == 1 ? 'checked' : ''}}>Опубликовать
                    </label>
                </div>
                <div class="checkbox">
                    <label><input
                                type="checkbox"
                                name="archived"
                                value="1"
                                {{$news->archived == 1 ? 'checked' : ''}}>Архив
                    </label>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
