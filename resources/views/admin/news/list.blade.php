@extends('admin.app')

@section('content')
    <div class="panel panel-default b-news">
        <div class="panel-heading">Новости</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('news.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Создать новость</button>
                </form>
                <form method="POST" action="{{route('news.order')}}" class="form-inline b-news_btn__order">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary">Сохранить порядок пунктов</button>
                </form>
            </div>

            <div class="text-muted small">
                Перетащите новости, чтобы изменить их порядок.
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Посл. изменения</th>
                    <th>Создана</th>
                    <th>Статус</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($news as $news_one)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$news_one->order_num }} </td>
                        <td><a href="{{route('news.edit', $news_one->id)}}">{{$news_one->title}}</a></td>
                        <td>{{$news_one->updated_at}}</td>
                        <td>{{$news_one->created_at}}</td>
                        <td>{{$news_one->published == 1? 'Опубликована ' : ''}}
                            {{$news_one->archived == 1? 'Архив ' : ''}}
                        </td>
                        <td>
                            <a class="btn btn-default" title="Редактировать" href="{{route('news.edit', $news_one->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a class="btn btn-default" title="Дублировать" href="{{route('news.copy', $news_one->id)}}">
                                <span class="glyphicon glyphicon-duplicate"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-news_delete"
                                  action="{{route('news.destroy', $news_one->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <input type="hidden" name="news_id" value="{{$news_one->id}}">

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection