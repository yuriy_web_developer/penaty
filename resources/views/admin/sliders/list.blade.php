@extends('admin.app')

@section('content')
    <div class="panel panel-default b-slider">
        <div class="panel-heading">Слайды</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('slider.create')}}" class="form-inline">
                    <input type="hidden" name="slider" value="{{$name}}">
                    <button type="submit" class="btn btn-primary">Добавить слайд</button>
                </form>
                <form method="POST" action="{{route('slider.order')}}" class="form-inline b-slider_order-form">
                    {{csrf_field()}}
                    <input type="hidden" name="slider" value="{{$name}}">
                    <button type="submit" class="btn btn-primary">Сохранить порядок слайдов</button>
                </form>
            </div>

            <div class="text-muted small">
                Перетащите слайды, чтобы изменить их порядок.
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заглавие</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($slides as $slide)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$slide->order_num}} </td>
                        <td><a href="{{route('slider.edit', $slide->id)}}?slider={{$name}}">{{$slide->title}}</a></td>
                        <td>{{$slide->created_at}}</td>
                        <td>
                            <a class="btn btn-default" href="{{route('slider.edit', $slide->id)}}?slider={{$name}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline b-news_delete"
                                  action="{{route('slider.destroy', $slide->id)}}">
                                <input type="hidden" name="slide_id" value="{{$slide->id}}">
                                <input type="hidden" name="slider" value="{{$name}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection