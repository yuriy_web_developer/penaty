@extends('admin.app')

@section('content')
    <div class="panel panel-default b-slider">
        <div class="panel-heading">Создать слайд</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('slider.store', ['slider'=>$name])}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                </div>

                <div class="form-group">
                    <label for="details_link">Ссылка для кнопки подробнее</label>
                    <input type="text" name="link" id="link" class="form-control" value="{{ old('link') }}">
                </div>

                <div class="form-group">
                    <label for="text_short">Фото</label>
                    <textarea rows="6" name="image" id="b-slider__tinymce-image" class="form-control">{{ old('image') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="text_short">Описание</label>
                    <textarea rows="6" name="description" id="b-slider__tinymce-text" class="form-control">{{ old('description') }}</textarea>
                </div>

                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection
