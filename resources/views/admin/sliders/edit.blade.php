@extends('admin.app')

@section('content')
    <div class="panel panel-default b-slider">
        <div class="panel-heading">Редактировать слайд</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('slider.update', $slide->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ $slide->title }}">
                </div>

                <div class="form-group">
                    <label for="details_link">Ссылка для кнопки подробнее</label>
                    <input type="text" name="link" id="link" class="form-control"
                           value="{{ $slide->link }}">
                </div>

                <div class="form-group">
                    <label for="text_short">Фото</label>
                    <textarea rows="6" name="image" id="b-slider__tinymce-image" class="form-control">
                        @if($slide->photo)
                            <img src="/{{ $slide->photo->path}}" alt="" width="300">
                        @endif
                    </textarea>
                </div>

                <div class="form-group">
                    <label for="text_short">Описание</label>
                    <textarea rows="6" name="description" id="b-slider__tinymce-text"
                              class="form-control">{{ $slide->description }}</textarea>
                </div>

                <input type="hidden" name="slider" value="{{$name}}">

                {{csrf_field()}}
                {{method_field('PUT')}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
