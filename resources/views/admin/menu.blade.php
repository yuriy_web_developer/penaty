<div id="sidebar">
    <nav class="navbar navbar-inverse navbar-fixed-top b-menu_wrapper">
        <ul class="nav sidebar-nav">
            <li class="sidebar-brand">
                <a href="{{ url('/') }}">
                    {{ config('app.name') }}
                </a>
            </li>

            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Главная страница
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('menu.index')}}">Меню</a></li>
                    <li><a href="{{route('brand.index')}}">Брэнды</a></li>
                    <li><a href="{{route('contacts.edit')}}">Контакты</a></li>
                    <li><a href="{{route('interior.edit')}}">Дизайн интерьера</a></li>
                </ul>
            </li>

            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Страницы <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    {{--<li class="dropdown-header">Заглавие подраздела</li>--}}
                    <li><a href="{{route('info_page.index')}}">Инф. страницы</a></li>
                    <li><a href="{{route('meta_data.index')}}">Метаданные</a></li>
                    <li><a href="{{route('design_studio.index')}}">Дизайн студия</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Слайдеры <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('slider.index', ['slider'=>'main'])}}">Слайдер в шапке</a></li>
                    <li><a href="{{route('slider.index', ['slider'=>'interior'])}}">Слайдер интерьеры</a></li>
                    <li><a href="{{route('slider.index', ['slider'=>'about_us'])}}">Слайдер про нас</a></li>
                </ul>
            </li>

            <li>
                <a href="{{route('news.index')}}">Новости</a>
            </li>

            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Галерея
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('photos.index')}}">Все фото</a></li>
                    <li><a href="{{route('photos.create')}}">Добавить</a></li>
                </ul>
            </li>

            <li>
                <a href="{{route('subscriber.index')}}">Подписчики</a>
            </li>

            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Каталог
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('product.index')}}">Товары</a></li>
                </ul>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('category.index')}}">Категории</a></li>
                </ul>
            </li>

            <li>
                <a href="{{route('order.index')}}">Заказы</a>
            </li>

            <li>
                <a href="{{route('callback.index')}}">Обратная связь</a>
            </li>

            <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Настройки
                    <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="{{route('settings.offer.edit')}}">Акция</a></li>
                </ul>
            </li>


        </ul>
        <button type="button" class="b-sidebar__btn is-closed" >
            <span class="hamb-top"></span>
            <span class="hamb-middle"></span>
            <span class="hamb-bottom"></span>
        </button>
    </nav>
</div>

