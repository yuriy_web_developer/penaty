@include('admin.head')
<body>

@include('admin.nav.main')

@if (!Auth::guest())
    @include('admin.menu')
@endif

<div class="container-fluid b-content">
    @include('flash::message')
    @include('partials.errors')
    @yield('content')
</div>

<script src="{{ asset('admin_assets/js/app_footer.js') }}"></script>

</body>
</html>
