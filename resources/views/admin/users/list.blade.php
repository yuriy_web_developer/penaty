@extends('admin.app')

@section('content')
    <div class="panel panel-default b-users">
        <div class="panel-heading">Пользователи</div>

        <div class="panel-body">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <a href="{{route('register')}}" class="btn btn-primary">Добавить пользователя</a>
                </div>
            </div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($users as $user)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                            {{$loop->index +1}}
                        </td>
                        <td><a href="{{route('user.edit', $user->id)}}">{{$user->name}}</a></td>
                        <td><a href="{{route('user.edit', $user->id)}}">{{$user->email}}</a></td>
                        <td>
                            <a class="btn btn-default" href="{{route('user.edit', $user->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('user.destroy', $user->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection