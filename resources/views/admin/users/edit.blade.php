@extends('admin.app')

@section('content')
    <div class="panel panel-default b-info-page">
        <div class="panel-heading">Редактировать пользователя</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('user.update', $user->id)}}">
                <div class="form-group">
                    <label for="email">Email/логин</label>
                    <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}" disabled>
                </div>

                <div class="form-group">
                    <label for="password">Сменить пароль</label>
                    <input type="password"
                           name="password_old"
                           id="password_old"
                           class="form-control"
                           placeholder="Старый пароль">
                    <br>
                    <input type="password"
                           name="password"
                           id="password"
                           class="form-control"
                           placeholder="Пароль, не менее 6 символов">
                    <br>
                    <input type="password"
                           name="password_confirmation"
                           id="password_confirmation"
                           class="form-control"
                           placeholder="Повторить пароль">
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection


