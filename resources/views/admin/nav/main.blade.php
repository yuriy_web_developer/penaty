<nav class="navbar navbar-default navbar-static-top">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    @include('admin.nav.guest')
                @else
                    @include('admin.nav.admin')
                @endif
            </ul>
        </div>
    </div>
</nav>

