<div class="row">
    <div class="b-category_sortable__title col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <a href="{{route('category.edit', $item->id)}}">
            {{$loop->iteration}}. {{$item->page->title}}
        </a>
    </div>
    <div class="b-category_sortable_btn col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <a class="btn btn-default b-category_sortable__edit"
           href="{{route('category.edit', $item->id)}}">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <form method="POST"
              class="form-inline"
              action="{{route('category.destroy', $item->id)}}" >

            {{method_field('DELETE')}}
            {{csrf_field()}}

            {{--this field is used for saving items order--}}
            <input type="hidden" name="item_id" value="{{$item->id}}" data-parent="{{$item->parent_id}}">

            <button type="submit" class="btn btn-danger">
                <span class="glyphicon glyphicon-remove"></span>
            </button>
        </form>
    </div>
</div>