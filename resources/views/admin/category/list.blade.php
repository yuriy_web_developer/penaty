@extends('admin.app')

@section('content')
    <div class="panel panel-default b-category">
        <div class="panel-heading">Список категорий</div>

        <div class="panel-body">
            <div class="b-category_btn">
                <form method="GET" action="{{route('category.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Создать категорию</button>
                </form>
                <form method="POST" action="{{route('category.order')}}" class="form-inline b-category_btn__order">
                    {{csrf_field()}}
                    <button type="submit" class="btn btn-primary">Сохранить порядок пунктов</button>
                </form>
            </div>

            <div class="text-muted small">
                Перетащите пункты, чтобы изменить их порядок.
            </div>

            <ul class="b-category_sortable_level0">
                @foreach($categories as $category)
                    <li>
                        @include('admin.category.list_row', ['item'=>$category])
                        @if($category->hasChildren())
                            <ul class="b-category_sortable_level1">
                                @foreach($category->children() as $second)
                                    <li>
                                        @include('admin.category.list_row', ['item'=>$second])
                                    </li>

                                    @if($second->hasChildren())
                                        <ul class="b-category_sortable_level2">
                                            @foreach($second->children() as $third)
                                                <li>
                                                    @include('admin.category.list_row', ['item'=>$third])
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection