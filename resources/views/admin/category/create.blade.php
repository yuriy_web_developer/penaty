@extends('admin.app')

@section('content')
    <div class="panel panel-default b-category">
        <div class="panel-heading">Создать категорию</div>

        <div class="panel-body">
            <form method="POST"
                  class="col-lg-6 col-md-6 col-sm-6 col-xs-6"
                  action="{{route('category.store')}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{ old('title') }}">
                </div>
                <div class="form-group">
                    <label for="url">Адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{ old('url') }}">
                </div>

                <div class="form-group">
                    <label for="title">Ключевые слова</label>
                    <input type="text"
                           name="keywords"
                           id="keywords"
                           class="form-control" value="{{ old('keywords') }}">
                </div>


                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text"
                           name="description"
                           id="description"
                           class="form-control"
                           value="{{ old('description') }}">
                </div>

                <div class="form-group">
                    <label for="b-category_tinymce-miniature">Миниатюра</label>
                    <textarea name="miniature" id="b-category_tinymce-miniature" class="form-control">{{ old('miniature') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="b-info-page_tinymce-image">Изображение в шапке</label>
                    <textarea name="photo" id="b-category_tinymce-image" class="form-control">{{ old('photo') }}</textarea>
                </div>

                <div class="form-group">
                    <label for="parent_id">Родительский раздел</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option value="0">Нет родительского раздела</option>
                        @foreach($category->firstLevel() as $first)
                            <option value="{{$first->id}}">
                                {{$first->page->title}}
                            </option>
                            @if ($first->hasChildren())
                                @foreach($first->children() as $second)
                                    <option value="{{$second->id}}">
                                        &nbsp;&nbsp;{{$second->page->title}}
                                    </option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>

                {{csrf_field()}}

                <button type="submit"  class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection
