@extends('admin.app')
@inject('menu', 'App\Models\Category')

@section('content')
    <div class="panel panel-default b-category">
        <div class="panel-heading">Редактировать категорию</div>

        <div class="panel-body">
            <form method="POST"
                  class="col-lg-6 col-md-6 col-sm-6 col-xs-6"
                  action="{{route('category.update', $category->id)}}">
                <div class="form-group">
                    <label for="title">Заглавие</label>
                    <input type="text" name="title" id="title" class="form-control" value="{{$category->page->title}}">
                </div>
                <div class="form-group">
                    <label for="url">Адрес</label>
                    <input type="text" name="url" id="url" class="form-control" value="{{$category->page->url}}">
                </div>

                <div class="form-group">
                    <label for="title">Ключевые слова</label>
                    <input type="text"
                           name="keywords"
                           id="keywords"
                           class="form-control" value="{{$category->page->keywords}}">
                </div>


                <div class="form-group">
                    <label for="description">Описание</label>
                    <input type="text"
                           name="description"
                           id="description"
                           class="form-control"
                           value="{{$category->page->description}}">
                </div>

                <div class="form-group">
                    <label for="b-category_tinymce-miniature">Миниатюра</label>
                    <textarea name="miniature"
                              id="b-category_tinymce-miniature"
                              class="form-control">@if($category->photo)
                            <img src="/{{$category->photo->path}}">
                        @endif</textarea>
                </div>

                <div class="form-group">
                    <label for="b-category_tinymce-image">Фото в шапке</label>
                    <textarea name="photo"
                              id="b-category_tinymce-image"
                              class="form-control">@if($category->page->photo)
                            <img src="/{{$category->page->photo->path}}">
                        @endif</textarea>
                </div>

                <div class="form-group">
                    <label for="parent_id">Родительский раздел</label>
                    <select name="parent_id" id="parent_id" class="form-control">
                        <option value="0">Нет родительского раздела</option>
                        @foreach($menu->firstLevel() as $first)
                            <option value="{{$first->id}}"
                                    {{$category->parent_id == $first->id ? ' selected ': '' }}
                                    {{$category->id == $first->id ? ' disabled ': '' }}>
                                {{$first->page->title}}
                            </option>
                            @if ($first->hasChildren())
                                @foreach($first->children() as $second)
                                    <option value="{{$second->id}}"
                                            {{$category->parent_id == $second->id ? ' selected ': '' }}
                                            {{$category->id == $second->id ? ' disabled ': '' }}>
                                        &nbsp;&nbsp;{{$second->page->title}}
                                    </option>
                                @endforeach
                            @endif
                        @endforeach
                    </select>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
