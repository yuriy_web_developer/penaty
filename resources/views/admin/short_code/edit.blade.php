@extends('admin.app')

@section('content')
    <div class="panel panel-default b-codes">
        <div class="panel-heading">Редактировать шорт-код</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('short_code.update', $code->id)}}">
                <div class="form-group">
                    <label for="code">Название</label>
                    <input type="text" name="code" id="code" class="form-control" value="{{$code->code}}">
                </div>

                <div class="form-group">
                    <label for="replace">Заменить на</label>
                    <textarea rows="6" name="replace" id="replace"
                              class="form-control">{!! $code->replace !!}</textarea>
                </div>

                {{method_field('PUT')}}
                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Обновить</button>
            </form>
        </div>
    </div>
@endsection
