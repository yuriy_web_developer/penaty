@extends('admin.app')

@section('content')
    <div class="panel panel-default b-codes">
        <div class="panel-heading">Шорт-коды</div>

        <div class="panel-body">

            <div class="b-menu_btn">
                <form method="GET" action="{{route('short_code.create')}}" class="form-inline">
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
            <div class="text-muted">Скопируйте название и вставьте, где необходимо заменить шорт-код</div>

            <table class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Создан</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <tbody>

                @foreach($codes as $code)
                    <tr>
                        <td class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{$code->id}} </td>
                        <td>|||{!! $code->code !!}|||</td>
                        <td>{{$code->created_at}}</td>
                        <td>
                            <a class="btn btn-default" href="{{route('short_code.edit', $code->id)}}">
                                <span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <form method="POST"
                                  class="form-inline"
                                  action="{{route('short_code.destroy', $code->id)}}">

                                {{method_field('DELETE')}}
                                {{csrf_field()}}

                                <button type="submit" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach

                </tbody>
            </table>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{{$codes->links()}}</div>
        </div>
    </div>
@endsection