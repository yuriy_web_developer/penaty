@extends('admin.app')

@section('content')
    <div class="panel panel-default b-codes">
        <div class="panel-heading">Создать шорт-код</div>

        <div class="panel-body">
            <form method="POST"
                  action="{{route('short_code.store')}}">
                <div class="form-group">
                    <label for="code">Название</label>
                    <input type="text" name="code" id="code" class="form-control" value="{{ old('code') }}">
                </div>

                <div class="form-group">
                    <label for="replace">Заменить на</label>
                    <textarea rows="6" name="replace" id="replace"
                              class="form-control">{!! old('description') !!}</textarea>
                </div>

                {{csrf_field()}}

                <button type="submit" class="btn btn-default">Создать</button>
            </form>
        </div>
    </div>
@endsection