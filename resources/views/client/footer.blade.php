@inject('news', 'App\Models\News')
@inject('brands', 'App\Models\Brand')

<div class="b-footer">
    <div class="row">
        <div class="b-bottom-line"></div>
        <div class="b-contact col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="b-footer__title b-footer_frame">КОНТАКТЫ</div>
            <div class="b-footer_separator hidden-xs">
                <div></div>
            </div>
            <div class="b-footer__background"></div>
            <div class="b-contact_item">
                <div class="icon icon-location"></div>
                {!! $contacts['address_footer'] !!}
            </div>
            <div class="b-contact_item">
                <div class="icon icon-time"></div>
                {!! $contacts['time_footer'] !!}
            </div>
            <div class="b-contact_item">
                <div class="icon icon-mail"></div>
                <a href="mailto:{!! $contacts['email_footer'] !!}">{!! $contacts['email_footer'] !!}</a>
            </div>
            <div class="b-contact_item">
                <div class="icon icon-phone"></div>
                {!! $contacts['phone_footer'] !!}
            </div>
            <div class="b-social b-contact_item pull-left">
                <a href="https://www.facebook.com/Салон-Пенаты-636751189752202/" target="_blank">
                    <div class="b-social__facebook"></div>
                </a>
                <a href="https://www.instagram.com/salonpenaty/" target="_blank">
                    <div class="b-social__instagram"></div>
                </a>
            </div>
        </div>

        <div class="b-news col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="b-footer__title b-footer_frame">ПОСЛЕДНИЕ НОВОСТИ</div>
            <div class="b-footer_separator hidden-xs">
                <div></div>
            </div>
            <div class="b-footer__background"></div>
            @foreach($news->getLatest() as $one)
                <div class="b-news_item">
                    <div>{{$one->getOnlyDate()}}</div>
                    <a href="/news/{{$one->url}}">{{$one->title}}</a>
                </div>
            @endforeach

            <div class="b-news__archive text-center">
                <a href="/news">Архив новостей</a>
            </div>
        </div>

        <div class="b-brands col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="b-footer__title b-footer_frame">О БРЕНДАХ</div>
            <div class="b-footer_separator hidden-xs">
                <div></div>
            </div>
            <div class="b-footer__background"></div>
            @foreach($brands->getOrdered() as $brand)
                @if($brand->photo)
                    <div class="b-brand_item">
                        <a href="{{$brand->link}}">
                            <img src="/{{$brand->photo->path}}" alt="">
                        </a>
                    </div>
                @endif
            @endforeach
        </div>

        <div class="b-about col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <div class="b-footer__title b-footer_frame">О НАС</div>
            <div class="b-footer__background"></div>
            <div class="b-about_slider">
                @foreach($slider->getSlidesByName('about_us') as $slide)
                    <div class="b-slide">
                        <img src="/{{$slide->photo->path}}" alt="">
                        {!! $slide->description !!}
                        <br>
                        <a href="{{$slide->link}}" class="pull-right">Подробнее</a>
                    </div>
                @endforeach
            </div>
            <div class="b-about_switch">
                <div class="active"></div>
            </div>

            <div class="b-about_slide__left"></div>
            <div class="b-about_slide__right"></div>
        </div>
    </div>

    <div class="row">
        <div class="b-logo">
            <img src="/img/logo_footer_v3.png" alt="">
        </div>
    </div>
    <div class="row">
        <div class="b-rights">
            &copy; ПЕНАТЫ, 2003–<?php echo date('Y') ?>. Все права защищены.
        </div>
    </div>
    {{--contact form, free consult--}}

    <div style="display:none">
        <form id="free_consult" method="post" action="">
            <div class="b-footer">
                <div class="b-free-consult">
                    <div class="b-free-consult__title text-left">
                        Получить бесплатную консультацию
                    </div>
                    <form method="POST" action="/defer/send">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="ФИО"
                                           type="text"
                                           name="client_name"
                                           id="client_name" class="form-control"
                                           value="{{ old('client_name') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Email"
                                           type="text"
                                           name="email"
                                           id="email" class="form-control"
                                           value="{{ old('email') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Телефон"
                                           type="text"
                                           name="phone"
                                           id="phone" class="form-control"
                                           value="{{ old('phone') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <select name="repair_type" class="form-control">
                                        <option selected disabled>Объект ремонта</option>
                                        <option value="Кафе">Кафе</option>
                                        <option value="Коттедж">Коттедж</option>
                                        <option value="Офис">Офис</option>
                                        <option value="Квартира">Квартира</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                            <textarea name="comment"
                                      id="comment"
                                      rows="7"
                                      class="form-control">{{ old('comment') }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="b-free-consult__required">
                                    <div>Поля обязательны к заполнению</div>
                                    <div>Ваши данные не будут переданы третьим лицам</div>
                                    <div class="required"></div>
                                </div>

                                {{csrf_field()}}
                                <button type="submit" class="btn btn-default">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </form>
    </div>
</div>

<a class="b-free-consult_btn" href="#free_consult">
    <div class="contact-btn">
        <span class="hidden">Напишите нам</span>
        <div>
            <i class="fa fa-envelope animated" aria-hidden="true"></i>
        </div>

    </div>
</a>

<div id="special_offer" class="hidden">
    {!!$settings->where('name', 'special_offer')->first()->value!!}
</div>




