<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/fix.css') }}">
    <script src="{{ asset('js/app.js') }}"></script>
    <title>{{$page ? $page->title : env('APP_NAME') }}</title>
    <meta name="description" content="{{$page ? $page->description : '' }}">
    <meta name="keywords" content="{{$page ? $page->keywords : '' }}">
</head>