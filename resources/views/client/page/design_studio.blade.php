@extends('client.app')

@section('content')
    @include('client.page.header')

    <div class="b-design-studio col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="b-design-studio_blocks row">
            @foreach($blocks as $block)
            <div class="b-design-studio_block">
                <img src="/{{$block->photo->path}}" alt="">
                <div class="b-design-studio__border1"></div>
                <div class="b-design-studio__border2"></div>

                <div class="b-design-studio__title">
                    <a href="{{$block->url}}">{{$block->title}}</a>
                </div>
                <div class="b-design-studio__details">
                    <a class="btn" href="{{$block->url}}">Подробнее</a>
                </div>
            </div>
            @endforeach
        </div>

        <div class="b-design-studio__line"></div>

        @include('client.main.slider_interior')

        <div class="pull-right b-design-studio_decoration hidden-xs">
            <img src="/img/services_background.png" alt="">
        </div>

        <div class="hidden-xs pull-left b-design-studio_decoration b-design-studio_decoration-left">
            <img src="/img/services_background.png" alt="">
        </div>
        {{--{!! $page->content !!}--}}
        {{--@include('partials.social')--}}
    </div>

@endsection
