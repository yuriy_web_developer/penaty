@extends('client.app')

@section('content')
    @include('client.page.header')

    <div class="b-info-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="b-info-content">
            {!! $page->content !!}
            @include('partials.social')
        </div>
    </div>

@endsection


