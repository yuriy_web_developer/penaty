<div class="b-header-image">
    <div class="b-header-image__file">
        @if($page->photo)
            <img src="/{{ $page->photo->path}}"
                 alt="{{$page->photo->name}}"
                 width="100%">
        @endif
    </div>
    <div class="b-header-image_title">
        <div>{{ $page->title}}</div>
    </div>
    <div class="b-header-image__border"></div>

    <div class="b-header-image__frame-left-top">
        <img src="/img/header_frame.png" alt="">
    </div>
    <div class="b-header-image__frame-right-bottom">
        <img src="/img/header_frame.png" alt="">
    </div>


    {{--<div class="b-header-image__right-line"></div>--}}
    {{--<div class="b-header-image__left-line"></div>--}}
</div>
