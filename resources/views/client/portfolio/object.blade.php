@extends('client.app')

@section('content')
    @include('client.catalog.header')
    <div class="b-object">
        <div class="b-object__title">{{$object->page->title}}</div>
        <div class="b-object__nav">
            <a href="/portfolio/{{$object->prev()->first()->page->url}}" title="Предыдущий">
                <div class="b-object__next">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
            </a>
            <a href="/portfolio/{{$object->next()->first()->page->url}}" title="Следующий">
                <div class="b-object__prev">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </a>
        </div>
        <hr>
        <div class="row">
            <div class="b-object_images col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="b-object_images_viewable">
                    <div class="b-object_images_container">
                        @if($object->photo->isNotEmpty())
                            @foreach($object->photo as $photo)
                                <a href="/{{$photo->withLogo()}}" class="text-center" data-fancybox="group">
                                    <img src="/{{$photo->withLogo()}}">
                                </a>
                            @endforeach
                        @else
                            <a href="/img/no_image.png" data-fancybox="group">
                                <img src="/img/no_image.png">
                            </a>
                        @endif
                    </div>
                </div>
                <div class="b-object_images__left">
                    <img src="/img/object_arrow.png">
                </div>
                <div class="b-object_images__right">
                    <img src="/img/object_arrow.png">
                </div>

                <div class="b-object_images_tn">
                    @if($object->photo->isNotEmpty())
                        @foreach($object->photo as $photo)
                            <div class="{{$loop->index == 0 ? 'active': ''}}">
                                <a href="/{{$photo->withLogo()}}" data-fancybox="group">
                                    <img src="/{{$photo->thumbnail_path}}">
                                </a>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>

            <div class="b-object__content col-lg-4 col-md-4 col-sm-12 col-xs-12">
                {!! $object->page->short_content !!}
                <a href="#portfolio_want" class="btn b-object__want text-center">Хочу такой же</a>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @include('partials.social')
            </div>
        </div>

        @include('client.portfolio.form')

    </div>
@endsection
