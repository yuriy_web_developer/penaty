@extends('client.app')

@section('content')
    @include('client.catalog.header')

    <div class="b-object-list">
        <div class="b-object-list__title">Наши объекты</div>
        <div class="row">
            @foreach($objects->chunk(4) as $set)
                <div class="b-object-list_row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @foreach($set as $object)
                        <a href="/portfolio/{{$object->page->url}}"
                           class="b-object-list_item col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="b-object-list_item__image">
                                @if($object->photo->isNotEmpty())
                                    <img src="/{{$object->photo->first()->withLogo()}}">
                                @else
                                    <img src="/img/no_image_260_200.png" alt="">
                                @endif
                                <div>+</div>
                            </div>
                            <div class="b-object-list_item__title">{{$object->page->title}}</div>
                        </a>
                    @endforeach
                </div>
            @endforeach
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">{{$objects->links()}}</div>
    </div>
@endsection
