<div style="display:none">
    <form id="portfolio_want" method="post" action="/portfolio/send">
        <div class="b-product">
            <div class="b-product_contact">
                <div class="b-product_contact__title text-left">
                    Бесплатная консультация
                </div>
                <form method="POST" action="/">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="ФИО"
                                       type="text"
                                       name="client_name"
                                       id="client_name" class="form-control"
                                       value="">
                                <div class="required"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Email"
                                       type="text"
                                       name="email"
                                       id="email" class="form-control"
                                       value="">
                                <div class="required"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Телефон"
                                       type="text"
                                       name="phone"
                                       id="phone" class="form-control"
                                       value="">
                                <div class="required"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Город"
                                       type="text"
                                       name="city"
                                       id="city" class="form-control"
                                       value="">
                                <div class="required"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                            <textarea name="comment"
                                      id="comment"
                                      rows="7"
                                      class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="b-product_contact__required">
                                <div>Поля обязательны к заполнению</div>
                                <div>Ваши данные не будут переданы третьим лицам</div>
                                <div class="required"></div>
                            </div>

                            {{csrf_field()}}

                            <button type="submit" class="btn btn-default">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </form>
</div>



