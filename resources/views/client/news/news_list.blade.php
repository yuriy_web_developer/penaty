@extends('client.app')

@section('content')
    @include('client.page.header')

    @include('client.sidebar.left_sidebar')

    <div class="b-info-content b-news_list col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="b-info-content">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @foreach($news as $one)
                    <div class="row b-news_list_row">
                        <a href="/news/{{$one->url}}">
                            <div class="b-news_list__date">{{$one->getOnlyDate()}}</div>
                            {{$one->title}}</a>
                    </div>
                @endforeach
            </div>
            @include('partials.social')
        </div>
    </div>

@endsection


