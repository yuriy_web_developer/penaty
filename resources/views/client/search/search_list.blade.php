@extends('client.app')

@section('content')
    @include('client.catalog.header')

    @include('client.sidebar.left_sidebar')

    <div class="b-search-list col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="lead row">Поисковая фраза "<b>{{request('term')}}</b>"</div>
                @foreach($pages as $index=>$page)
                    <div class="row b-search-list__item">
                        <a href="{{$url->make($page)}}">
                            {{$lastPageNum + $index}}. {{$page->title}}</a>
                    </div>
                @endforeach
                <div class="text-center">{{ $pages->links()}}</div>
            </div>
            @include('partials.social')
    </div>

@endsection


