@extends('client.app')

@section('content')
    @include('client.catalog.header')

    <div class="b-info-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="b-info-content b-confirm-email">
            <h3><p class="lead">Ссылка не действительна, попробуйте подписать еще раз.</p></h3>

            <h4>Через 10 секунд Вы будете перенаправлены на главную страницу</h4>
            <script>
                setTimeout(function () {
                    window.location.href = "/";
                }, 10000);
            </script>
        </div>
        <div class="b-info__background-left hidden-xs"></div>
        <div class="b-info__background-right hidden-xs"></div>
    </div>

@endsection


