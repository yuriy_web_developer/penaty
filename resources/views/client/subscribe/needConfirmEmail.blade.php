@extends('client.app')

@section('content')
    @include('client.catalog.header')

    <div class="b-info-content col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="b-info-content b-confirm-email">
            <h3><p class="lead">На почтовый ящик {{$email}} было отправлено письмо с подтверждением активации подписки</p></h3>
        </div>
        <div class="b-info__background-left hidden-xs"></div>
        <div class="b-info__background-right hidden-xs"></div>
    </div>

@endsection


