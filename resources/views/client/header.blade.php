<div class="b-header">
    <div class="b-header__logo">
        <a href="{{ url('/') }}"><img src="/img/logo_v3.jpg" alt=""></a>
        <div class="b-contact col-md-5 col-sm-5 col-xs-12 pull-right text-right visible-xs">
            <a href="tel:{!! $contacts['phone_header'] !!}">{!! $contacts['phone_header'] !!}</a><br>
            <a href="mailto:{!! $contacts['email_header'] !!}">{!! $contacts['email_header'] !!}</a><br>
            {!! $contacts['address_header'] !!}
        </div>
    </div>

    <div class="b-subscribe col-lg-4 col-md-6 col-sm-6 col-xs-12 pull-left">
        <div class="b-subscribe__title">Подписаться на новости</div>
        <form method="POST" action="/" autocomplete="false">
            <button type="button" class="btn b-subscribe_phone">
                <img src="/img/icon_phone.png" alt="">
            </button>
            <button type="button" class="btn active b-subscribe_email">
                <span class="b-subscribe__at">@</span>
            </button>
            <input type="text"
                   name="email"
                   id="subscribe_field"
                   placeholder="E-mail"
                   value="{{ old('subscribe_field') }}">

            <button type="submit" name="" id="" class="btn btn-subscribe">Подписаться</button>
            <div class="checkbox">
                <label><input type="checkbox" name="conditions_agreed" id="conditions_agreed">
                    <a href="/">Согласен(на) с условиями подписки</a>
                </label>
            </div>

        </form>
    </div>
    <div class="b-contact col-lg-3 col-md-5 col-sm-5 col-xs-12 pull-right text-right hidden-xs">
        <div class="b-social">
            <a href="https://www.facebook.com/Салон-Пенаты-636751189752202/" target="_blank">
                <div class="b-social__facebook"></div>
            </a>
            <a href="https://www.instagram.com/salonpenaty/" target="_blank">
                <div class="b-social__instagram"></div>
            </a>
        </div>
        <a href="tel:{!! $contacts['phone_header'] !!}">{!! $contacts['phone_header'] !!}</a><br>
        <a href="mailto:{!! $contacts['email_header'] !!}">{!! $contacts['email_header'] !!}</a><br>
        {!! $contacts['address_header'] !!}
    </div>
</div>