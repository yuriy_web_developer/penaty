<div class="b-left-sidebar_consult">
    <form method="POST" action="/">

        <div class="b-left-sidebar_consult_padding">
            <div class="b-left-sidebar_consult__title-line pull-right"></div>
            <div class="b-left-sidebar_consult__title  text-left">
                Получить бесплатную консультацию
                <img src="/img/left_sidebar_background.png" alt="">
            </div>

            <div class="form-group b-left-sidebar_consult__first-row">
                <div class="b-left-sidebar_icon"></div>
                <input type="text"
                       name="client_name"
                       placeholder="Имя"
                       id="client_name"
                       class="form-control">
            </div>

            <div class="form-group">
                <div class="b-left-sidebar_icon"></div>
                <input type="text"
                       name="phone"
                       placeholder="Ваш телефон"
                       id="phone"
                       class="form-control">
            </div>

            <div class="form-group">
                <div class="b-left-sidebar_icon"></div>
                <input type="text"
                       name="email"
                       placeholder="Ваш E-mail"
                       id="email"
                       class="form-control">
            </div>

            <div class="form-group">
                <select name="repair_type" class="form-control">
                    <option selected disabled>Объект ремонта</option>
                    <option>Кафе</option>
                    <option>Коттедж</option>
                    <option>Офис</option>
                    <option>Квартира</option>
                </select>
            </div>

            <div class="form-group">
                <textarea name="comment"
                          id="comment"
                          rows="4"
                          placeholder="Комментарий"
                          class="form-control"></textarea>
            </div>
            <div class="form-group b-left-sidebar_explain">
                <div class="b-left-sidebar__required"></div>
                <span class="b-left-sidebar_explain-first">Поля обязательны к заполнению <br></span>
                <span>Ваши данные не будут переданы третьим лицам</span>
            </div>
        </div>

        <button type="submit"
                name="consult_btn"
                id="consult_btn"
                class="btn btn-default">Отправить заявку
        </button>

    </form>
</div>
