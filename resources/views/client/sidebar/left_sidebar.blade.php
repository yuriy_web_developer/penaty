@inject('categories', 'App\Models\Category')
@inject('defer', 'App\Http\Controllers\ProductDeferController')
<div class="b-left-sidebar col-lg-3 col-md-12 col-sm-12 col-xs-12">
    <div class="b-left-sidebar_menu">

        <div class="b-left-sidebar_list">
            <ul>
                @foreach($categories->firstLevel() as $first)
                    <li class="b-left-sidebar_lev1">
                        <div class="b-left-sidebar_background">
                            @if($first->page->photo)
                                <img
                                        src="/{{ $first->page->photo->path}}"
                                        alt="{{$first->page->photo->name}}">
                            @else
                                <div class="b-left-sidebar_no-background"></div>
                            @endif
                        </div>

                        <div class="b-left-sidebar_content">
                            <a href="{{$url->makeCategoryUrl($first)}}">{{$first->page->title}}</a>
                        </div>

                        @if($first->hasChildren())
                            <i class="opener closed fa fa-chevron-down" aria-hidden="true"></i>
                        @endif

                    </li>

                    @if($first->hasChildren())
                        <li class="b-left-sidebar_lev2 hidden b-left-child">
                            <div class="b-left-sidebar_lev2__background">
                                @if($first->photo)
                                    <img src="/{{$first->photo->path}}">
                                @endif
                            </div>
                            <ul>
                                @foreach($first->children() as $second)
                                    <li>
                                        {{--<div>{{$second->page->title}}</div>--}}
                                        <div>
                                            <a href="{{$url->makeCategoryUrl($second)}}">{{$second->page->title}}</a>
                                        </div>
                                        @if($second->hasChildren())
                                            <i class="opener closed fa fa-chevron-down" aria-hidden="true"></i>
                                        @endif
                                        <div class="bullet"></div>
                                    </li>
                                    @if($second->hasChildren())
                                        <li class="b-left-sidebar_lev3 hidden b-left-child">
                                            <ul>
                                                @foreach($second->children() as $third)
                                                    <li>
                                                        <a href="{{$url->makeCategoryUrl($third)}}">{{$third->page->title}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                            <img src="/img/sidebar_decoration.png"
                                 class="b-left-sidebar_decoration">
                            <div class="b-left-sidebar_decoration_bottom"></div>
                            <div class="b-left-sidebar_decoration_left"></div>

                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
    </div>
    @if($defer->getDeferred())
        <a href="/defer" class="btn btn-default">Отложенные товары: {{count($defer->getDeferred())}}шт.</a>
    @endif
</div>
