@inject('menu', '\App\Models\Menu')

<div class="b-menu hidden-xs">
    <div class="b-menu__line"></div>
    <ul class="">
        @foreach($menu->getFirstLevel() as $first)

            <li><a href="{{starts_with($first->url, 'http')? $first->url :  starts_with($first->url, '/')? $first->url : '/'.$first->url }}"
                        {{--class="{{$first->hasChildren($first->id) ? 'no-link' : '' }}"--}}
                >
                    {{$first->item_name}}
                    @if($first->hasChildren($first->id) || $first->url == '/catalog')
                        <i class="fa fa-chevron-right opener" aria-hidden="true"></i>
                    @endif
                </a>
                @if($first->url == '/catalog')
                    @include('client.menus.catalog_menu')
                    @continue
                @endif

                @if($first->hasChildren($first->id))
                    <ul class="b-menu_level1">
                        @foreach($first->getChildren($first->id) as $second)
                            <li><a
                                        {{--class="{{$second->hasChildren($second->id) ? 'no-link' : '' }}"--}}
                                        href="{{starts_with($second->url, 'http')? $second->url :  starts_with($second->url, '/')? $second->url : '/'.$second->url }}">
                                    {{$second->item_name}}
                                    @if($second->hasChildren($second->id))
                                        <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                    @endif
                                </a>
                                @if($second->hasChildren($second->id))
                                    <ul class="b-menu_level2">
                                        @foreach($second->getChildren($second->id) as $third)
                                            <li><a href="{{$third->url}}">{{$third->item_name}}</a></li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
    </ul>
</div>

<div class="b-menu-xs visible-xs">
    <div class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Открыть меню</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/')}}">Меню</a>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    @foreach($menu->getFirstLevel() as $first)
                        <li><a href="{{starts_with($first->url, 'http')? $first->url :  starts_with($first->url, '/')? $first->url : '/'.$first->url }}"
                               class="{{($first->hasChildren($first->id) || $first->url == '/catalog') ? 'no-link' : '' }}"
                            >
                                {{$first->item_name}}
                                @if($first->hasChildren($first->id) || $first->url == '/catalog')
                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                @endif
                            </a>

                            @if($first->url == '/catalog')
                                @include('client.menus.catalog_menu_mobile')
                                @continue
                            @endif

                            @if($first->hasChildren($first->id))
                                <ul class="b-menu-xs_level1 hidden">
                                    @foreach($first->getChildren($first->id) as $second)
                                        <li><a
                                                    class="{{$second->hasChildren($second->id) ? 'no-link' : '' }}"
                                                    href="{{starts_with($second->url, 'http')? $second->url :  starts_with($second->url, '/')? $second->url : '/'.$second->url }}">
                                                {{$second->item_name}}
                                                @if($second->hasChildren($second->id))
                                                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                                                @endif
                                            </a>
                                            @if($second->hasChildren($second->id))
                                                <ul class="b-menu-xs_level2 hidden">
                                                    @foreach($second->getChildren($second->id) as $third)
                                                        <li><a href="{{$third->url}}">{{$third->item_name}}</a></li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>