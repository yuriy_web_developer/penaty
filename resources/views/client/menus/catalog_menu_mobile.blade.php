@inject('categories', 'App\Models\Category')

<ul class="b-menu-xs_level1 hidden">
    @foreach($categories->firstLevel() as $category)
        <li><a
                    class="{{$category->hasChildren() ? 'no-link' : '' }}"
                    href="{{$url->makeCategoryUrl($category)}}">
                {{$category->page->title}}
                <i class="fa fa-chevron-right" aria-hidden="true"></i>

            </a>
            @if($category->hasChildren())
                <ul class="b-menu-xs_level2 hidden">
                    @foreach($category->children() as $subcategory)
                        <li>
                            <a href="{{$url->makeCategoryUrl($subcategory)}}">
                                {{$subcategory->page->title}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>

