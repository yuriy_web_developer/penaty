@inject('categories', 'App\Models\Category')

<ul class="b-menu_level1">
    @foreach($categories->firstLevel() as $category)
        <li>
            <a href="{{$url->makeCategoryUrl($category)}}">{{$category->page->title}}
                @if($category->hasChildren())
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                @endif
            </a>
            @if($category->hasChildren())
                <ul class="b-menu_level2">
                    @foreach($category->children() as $subcategory)
                        <li><a href="{{$url->makeCategoryUrl($subcategory)}}">
                                {{$subcategory->page->title}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>
