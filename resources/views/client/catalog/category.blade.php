@extends('client.catalog.index')

@section('catalog_content')
    <div class="b-category col-lg-9 col-md-12 col-sm-12 col-xs-12">

        @include('client.catalog.hit')

        @foreach($category->children() as $first)

            <div class="row b-category_row">
                <img src="/img/categories_title.png" class="hidden-xs">
                <div class="b-category__title">{{$first->page->title}}
                    @if($first->hasChildren())
                        <i class="opener fa fa-chevron-down pull-right" aria-hidden="true"></i>
                    @endif
                </div>
            </div>

            @include('client.catalog.catalog_items', ['category'=>$first])

        @endforeach

        <div class="b-category__social">
            @include('partials.social')
        </div>
    </div>
@endsection




