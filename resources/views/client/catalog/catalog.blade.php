@extends('client.app')

@section('content')
    @include('client.page.header')

    <div class="b-catalog">
        @foreach($catalog->chunk(3) as $categories)
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @foreach($categories as $first)
                        <div class="b-catalog_item col-lg-4 col-md-6 col-sm-12 col-xs-12">
                            <a href="{{$url->makeCategoryUrl($first)}}">
                            <div class="b-catalog_image">
                                @if($first->photo)
                                    <img src="/{{$first->photo['path']}}" alt="">
                                @else
                                    <img src="/img/no_image.png" alt="">
                                @endif
                                <div class="b-catalog_image__frame"></div>
                            </div>
                            </a>
                            <div class="b-catalog_content">
                                <div class="b-catalog_content_lev1" data-level="1">
                                    <div>
                                        <a href="{{$url->makeCategoryUrl($first)}}"
                                           title="{{$first->page->title}}">
                                            {{$first->page->title}}
                                        </a>
                                    </div>
                                    @if($first->hasChildren())
                                        <i class="opener closed fa fa-chevron-down" aria-hidden="true"></i>
                                    @endif
                                </div>
                                @if($first->hasChildren())
                                    @foreach($first->children() as $second)
                                        <div class="b-catalog_content_lev2 hidden" data-level="2">
                                            <div>
                                                <a href="{{$url->makeCategoryUrl($second)}}"
                                                   title="{{$second->page->title}}">
                                                    {{$second->page->title}}
                                                </a>
                                            </div>
                                            @if($second->hasChildren())
                                                <i class="opener closed fa fa-chevron-down" aria-hidden="true"></i>
                                            @endif
                                        </div>
                                        @if($second->hasChildren())
                                            <div class="b-catalog_content_lev3 hidden" data-level="3">
                                                @foreach($second->children() as $third)
                                                    <a href="{{$url->makeCategoryUrl($third)}}"
                                                       title="{{$third->page->title}}">
                                                        {{$third->page->title}}
                                                    </a>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
            <div class="b-catalog__social">
                @include('partials.social')
            </div>

    </div>

@endsection


