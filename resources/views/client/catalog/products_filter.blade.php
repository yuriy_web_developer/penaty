@extends('client.catalog.index')

@section('catalog_content')

    <div class="b-products col-lg-9 col-md-12 col-sm-12 col-xs-12">
        <div class="b-category">
            @include('client.catalog.filter')
        </div>
        @foreach($products->chunk(4) as $set)
            <div class="row b-products_items">
                @foreach($set as $product)
                    <div class="b-products_item">
                        <a href="{{$url->makeProductUrl($product)}}">
                            <div class="b-products_item-image">
                                <div class="circle">
                                    <div class="frame">
                                        <div></div>
                                    </div>
                                    @if(!$product->photos->isEmpty())
                                        <img src="/{{$product->photos()->first()->thumbnail_path}}" alt="">
                                    @else
                                        <img src="/img/no_image_400_400.png" alt="">
                                    @endif
                                </div>
                                <div class="b-products_item__bottom"></div>
                            </div>
                        </a>

                        <div class="b-products_item__title text-center">
                            <a href="{{$url->makeProductUrl($product)}}">
                                {{str_limit($product->page->title, 35)}}
                            </a>
                        </div>

                        <div class="b-products_item__details text-center">
                            <a class="btn" href="{{$url->makeProductUrl($product)}}">Подробнее
                                <i class="fa fa-angle-right pull-right" aria-hidden="true"></i>
                            </a>
                        </div>

                    </div>
                @endforeach
            </div>
        @endforeach
        <div class="b-products__social">
            @include('partials.social')
        </div>
    </div>
@endsection
