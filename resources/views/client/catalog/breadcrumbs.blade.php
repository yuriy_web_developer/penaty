<div class="b-header-breadcrumbs hidden-xs">
    <div class="b-header-breadcrumbs__item">
        <a href="/">Главная</a>
    </div>
    <div class="divider"></div>

    <div class="b-header-breadcrumbs__item">
        <a href="/catalog">Каталог</a>
    </div>

    @if($page)
        @foreach($breadcrumbs->make($page) as $part)
            <div class="divider"></div>
            <div class="b-header-breadcrumbs__item">
                <a href="{{$url->makePageUrl($part)}}">{{str_limit($part->title, 30)}}</a>
            </div>
        @endforeach
    @endif
</div>

