@if($filter->type=="select" && $filter->hasFields())
    <form method="GET"
          action="{{route('catalog.filter')}}"
          class="b-category-filter_row pull-right">
        <div class="form-group">
            <select name="field_ids[]" class="form-control">
                <option value="" disabled selected>{{$filter->name}}</option>
                @foreach($filter->fields as $field)
                    <option value="{{$field->id}}" {{$field->requestHasUniqueField($filter) ? ' selected ': ''}}>
                        {{$field->name}}
                    </option>
                @endforeach
            </select>
            @if(!empty($field->getFieldsWithExclude($filter->fields)))
                @foreach($field->getFieldsWithExclude($filter->fields) as $one)
                    <input type="hidden" name="field_ids[]" value="{{$one}}">
                @endforeach
            @endif
            <input type="hidden" name="category_id" value="{{$category->id}}">
        </div>
    </form>
@endif

