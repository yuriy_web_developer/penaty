@if($filter->type=="button" && $filter->hasFields())
    <a class="btn b-category-filter__btn b-category-filter_row pull-right
       {{($filter->fields[0]->requestHasField()) ? ' active ':'' }}"
       href="{{$filter->fields[0]->makeLink($category)}}">
        {{$filter->fields[0]->name}}
    </a>
@endif



