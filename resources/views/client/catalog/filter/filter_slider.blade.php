@if($filter->type=="slider")
    <div class="b-category__title">
        <img class="hidden-xs" src="/img/categories_title.png">
        <div>{{$filter->name}}</div>
        <i class="opener fa fa-chevron-down" aria-hidden="true"></i>
    </div>
    <div class="b-category-filter b-category-filter-slider{{$loop ? $loop->index:''}}"
         data-index="{{$loop ? $loop->index:''}}">
        <div class="b-category-filter_container" style="width:2000%">
            @foreach($filter->fields()->orderBy('name', 'asc')->get() as $field)
                <div class="b-slide {{$field->requestHasField() ? ' active ': ''}}">
                    <a href="{{$field->makeLink($category)}}">
                        <div class="b-slide-image">
                            {{--<div class="circle">--}}
                                {{--<div class="frame">--}}
                                    {{--<div></div>--}}
                                {{--</div>--}}
                                @if($field->photo)
                                    <img src="/{{$field->photo->thumbnail_path}}" alt="">
                                @else
                                    <img src="/img/no_image_260_200.png" height="100%" width="100%" alt="">
                                @endif
                            {{--</div>--}}
                        </div>
                    </a>
                    <div class="b-category-filter__content">
                        <a href="{{$field->makeLink($category)}}" class="title">{{$field->name}}</a>
                    </div>
                    @if($field->requestHasField())
                        <a href="{{$field->makeLink($category)}}" class="b-category-filter__delete">
                            <i class="fa fa-times" aria-hidden="true"></i>
                        </a>
                    @endif

                </div>
            @endforeach

        </div>
        <div class="b-category-filter__arrow-left">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
        <div class="b-category-filter__arrow-right">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
    </div>
@endif

