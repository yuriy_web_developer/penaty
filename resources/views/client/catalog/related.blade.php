@if($product->related->isNotEmpty())
    <div class="b-product-related">
        <div class="lead">С этим товаром также покупают</div>
        <div class="b-product-related_container" style="width:2000%">
            @foreach($product->related as $product)
                <div class="b-slide">
                    <a href="{{$url->makeProductUrl($product)}}">
                        <div class="b-slide-image">
                            <div class="circle">
                                <div class="frame">
                                    <div></div>
                                </div>
                                @if(!$product->photos->isEmpty())
                                    <img src="/{{$product->photos[0]->thumbnail_path}}" alt="">
                                @else
                                    <img src="/img/no_image_260_200.png" height="100%" width="100%" alt="">
                                @endif
                            </div>
                        </div>
                    </a>
                    <div class="b-product-related__content">
                        <a href="{{$url->makeProductUrl($product)}}" class="title">{{$product->page->title}}</a>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="b-product-related__arrow-left">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
        <div class="b-product-related__arrow-right">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
    </div>

@endif

