@extends('client.catalog.index')

@section('catalog_content')

    <div class="b-product col-lg-9 col-md-12 col-sm-12 col-xs-12">
        @include('flash::message')
        <div class="row">
            <div class="b-product_images col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="b-product_images__main">
                    @if(!$product->photos->isEmpty())
                        <a href="/{{$product->photos[0]->path}}"
                           data-fancybox="group"
                           data-caption="/{{$product->photos[0]->title}}">
                            <img src="/{{$product->photos[0]->thumbnail_path}}" alt="">
                        </a>
                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                    @else
                        <img src="/img/no_image_260_200.png" alt="">
                    @endif
                </div>
                @foreach($product->photos as $photo)
                    <div class="b-product_images__tn">
                        <a href="/{{$photo->path}}"
                           data-fancybox="group"
                           data-caption="{{$photo->title}}">
                            <img src="/{{$photo->thumbnail_path}}" alt="">
                        </a>
                    </div>
                @endforeach

            </div>
            <div class="b-product_info col-lg-7 col-md-12 col-sm-12 col-xs-12">
                <div class="b-product__title text-left">{{$product->page->title}}</div>
                <div class="b-product__desc">
                    {{$product->short_info}}
                </div>
                <hr>
                <form method="POST" action="/defer/add">
                    <div class="b-product_btn">
                        @if(is_array($product->volume))
                            @foreach($product->volume as $one)
                                <div class="b-product_btn__vol" data-volume="{{$one}}">{{$one}}л</div>
                            @endforeach
                        @endif
                        <input type="hidden" name="volume" value="1">

                        @if(!$product->colors->isEmpty())
                            <div class="b-product_btn__color">
                                <img src="/{{$product->colors[0]->thumbnail_path}}" alt="">
                                {{$product->colors[0]->pivot->color_name}}
                            </div>
                        @endif

                        <div class="form-group">
                            <select name="color_id" class="b-product_btn__color-select form-control">
                                @foreach($product->colors as $color)
                                    <option value="{{$color->pivot->color_name}}">
                                        {{$color->pivot->color_name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="b-product_btn_desc">
                            Основа: <span>{{$product->basement}}</span>
                        </div>

                    </div>
                    <div class="b-product_btn">
                        <div class="b-product_btn_quantity">
                            <div class="b-product_btn_counter">1<span>шт</span></div>
                            <input type="hidden" name="quantity" value="1">
                            <button class="b-product_btn_quantity_plus">+</button>
                            <button class="b-product_btn_quantity_minus">-</button>
                        </div>
                        <button class="b-product_btn_defer">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                            Отложить
                        </button>
                        {{csrf_field()}}
                        <input type="hidden" name="product_id" value="{{$product->id}}">
                        <a class="b-product_btn_request" title="Запросить" href="#product_request_form">
                            <i class="fa fa-list" aria-hidden="true"></i>
                            Запросить
                        </a>
                    </div>
                </form>
                <hr>

                <div class="b-product_details">
                    <div class="">Инструменты:<span>{{$product->instrument}}</span></div>
                    <div class="">Расход:<span>{{$product->consumption}}</span></div>
                    <div class="">Количество слоев:<span>{{$product->layers}}</span></div>
                    <div class="">Разбавление:<span>{{$product->dilution}}</span></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="b-product_tabs">
                    <div class="b-product_tabs_nav">
                        <a href="#" class="b-product_tabs_nav__link active">
                            <span>Цвета</span>
                            <i class="fa fa-eyedropper" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="b-product_tabs_nav__link">
                            <span>Описание</span>
                            <i class="fa fa-info" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="b-product_tabs_nav__link">
                            <span>Инструкции</span>
                            <i class="fa fa-book" aria-hidden="true"></i>
                        </a>
                        <a href="#" class="b-product_tabs_nav__link">
                            <span>Видео</span>
                            <i class="fa fa-film" aria-hidden="true"></i>
                        </a>
                    </div>

                    <div class="b-product_tab active">
                        <div class="b-product_tab__content">
                            @foreach($product->colors as $color)
                                <div class="b-product_tab__color">
                                    <a data-fancybox="color"
                                       data-caption="{{$color->pivot->color_name}}"
                                       href="/{{$color->path}}">
                                        <img src="/{{$color->thumbnail_path}}" alt="">
                                    </a>
                                    <span>{{$color->pivot->color_name}}</span>
                                    <div>
                                        <i class="fa fa-search-plus" aria-hidden="true"></i>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="b-product_tab">
                        <div class="b-product_tab__content">{!! $product->tab_description !!}</div>
                    </div>

                    <div class="b-product_tab">
                        <div class="b-product_tab__content">{!! $product->tab_instruction !!}</div>
                    </div>
                    <div class="b-product_tab">
                        <div class="b-product_tab__content">{!! $product->tab_video !!}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-product__social">
            @include('partials.social')
        </div>
    </div>

    <div style="display:none">
        <form id="product_request_form" method="post" action="">
            <div class="b-product">
                <div class="b-product_contact">
                    <div class="b-product_contact__title text-left">
                        Запрос для подтверждения цены и наличия товара
                    </div>
                    <form method="POST" action="/defer/send">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="ФИО"
                                           type="text"
                                           name="client_name"
                                           id="client_name" class="form-control"
                                           value="{{ old('client_name') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Email"
                                           type="text"
                                           name="email"
                                           id="email" class="form-control"
                                           value="{{ old('email') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Телефон"
                                           type="text"
                                           name="phone"
                                           id="phone" class="form-control"
                                           value="{{ old('phone') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Город"
                                           type="text"
                                           name="city"
                                           id="city" class="form-control"
                                           value="{{ old('city') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                            <textarea name="comment"
                                      id="comment"
                                      rows="7"
                                      class="form-control">{{ old('comment') }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="b-product_contact__required">
                                    <div>Поля обязательны к заполнению</div>
                                    <div>Ваши данные не будут переданы третьим лицам</div>
                                    <div class="required"></div>
                                </div>

                                {{csrf_field()}}
                                <button type="submit" class="btn btn-default">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </form>
    </div>


@endsection

