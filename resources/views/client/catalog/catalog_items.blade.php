@if($category->hasChildren())
    <div class="category_items_block">
        @foreach($category->children()->chunk(4) as $set)

            <div class="row b-category_items">
                @foreach($set as $item)
                    <div class="b-category_item">
                        <a href="{{$url->makeCategoryUrl($item)}}">
                            <div class="b-category_item-image">
                                <div class="circle">
                                    <div class="frame">
                                        <div></div>
                                    </div>
                                    @if($item->photo)
                                        <img src="/{{$item->photo['path']}}" alt="">
                                    @else
                                        <img src="/img/no_image_400_400.png" alt="">
                                    @endif
                                </div>
                                <div class="b-category_item__bottom"></div>
                            </div>
                        </a>
                        <div class="b-category_item__title text-center">
                            <a href="{{$url->makeCategoryUrl($item)}}">
                                {{str_limit($item->page->title, 60)}}
                            </a>
                        </div>

                        <div class="b-category_item__details text-center">
                            <a class="btn" href="{{$url->makeCategoryUrl($item)}}">Подробнее
                                <i class="fa fa-angle-right pull-right" aria-hidden="true"></i>
                            </a>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach
    </div>
@endif

