@if($category->hasFilters())
    @foreach($category->allFilters() as $filter)
        @include('client.catalog.filter.filter_select')
        @include('client.catalog.filter.filter_button')
        @include('client.catalog.filter.filter_slider')

    @endforeach
    <script>
        if ($('.b-category-filter_row').length > 0)
            $('.b-category-filter_row')
                .wrapAll('<div class="row">')
                .parent();
        @if(isset($filter) && $filter->needResetBtn())
            $('.b-category-filter_row')
            .parent()
            .prepend('<a href="{{$filter->resetLink()}}" ' +
                'class="btn btn-default pull-right">Сбросить</a>');
        @endif
    </script>
@endif
