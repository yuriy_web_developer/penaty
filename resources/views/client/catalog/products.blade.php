@extends('client.catalog.index')

@section('catalog_content')

    <div class="b-products col-lg-9 col-md-12 col-sm-12 col-xs-12">

        <div class="row b-products-filter">
            <form class="form-inline pull-right"
                  method="GET"
                  action="#">
                <div class="form-group">
                    <label for="sort_by">Сортировать по</label>
                    <select name="sort_by" id="sort_by" class="form-control">
                        <option value="" selected></option>
                        <option disabled>Дате</option>
                        <option disabled>Цене</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="brands">Бренды</label>
                    <select name="brands"
                            id="brands"
                            class="form-control"
                            onchange="$(this).parent().parent().submit()">
                        <option value="" selected></option>
                        @foreach($category->getProducers($category) as $producer)
                            <option value="{{$producer->producer}}"
                                    {{request()->get('brands') == $producer->producer ? 'selected' : ''}}
                            >{{$producer->producer}}</option>
                        @endforeach
                    </select>
                </div>
            </form>
        </div>

        <div class="b-category">
            @include('client.catalog.hit')
        </div>

        @foreach($category->whereProducer($category)->chunk(4) as $set)

            <div class="row b-products_items">
                @foreach($set as $product)
                    <div class="b-products_item">
                        <a href="{{$url->makeProductUrl($product)}}">
                            <div class="b-products_item-image">
                                <div class="circle">
                                    <div class="frame">
                                        <div></div>
                                    </div>
                                    @if(!$product->photos->isEmpty())
                                        <img src="/{{$product->photos()->first()->thumbnail_path}}" alt="">
                                    @else
                                        <img src="/img/no_image_400_400.png" alt="">
                                    @endif
                                </div>
                                <div class="b-products_item__bottom"></div>
                            </div>
                        </a>

                        <div class="b-products_item__title text-center">
                            <a href="{{$url->makeProductUrl($product)}}">
                                {{str_limit($product->page->title, 35)}}
                            </a>
                        </div>

                        <div class="b-products_item__details text-center">
                            <a class="btn" href="{{$url->makeProductUrl($product)}}">Подробнее
                                <i class="fa fa-angle-right pull-right" aria-hidden="true"></i>
                            </a>
                        </div>

                    </div>
                @endforeach

            </div>

        @endforeach

        <div class="b-products__social">
            @include('partials.social')
        </div>
    </div>
    <div style="display:none">
        <form id="product_request_form" method="post" action="">
            <div class="b-product">
                <div class="b-product_contact">
                    <div class="b-product_contact__title text-left">
                        Запрос для подтверждения цены и наличия товара
                    </div>
                    <form method="POST" action="/defer/send">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="ФИО"
                                           type="text"
                                           name="client_name"
                                           id="client_name" class="form-control"
                                           value="{{ old('client_name') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Email"
                                           type="text"
                                           name="email"
                                           id="email" class="form-control"
                                           value="{{ old('email') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Телефон"
                                           type="text"
                                           name="phone"
                                           id="phone" class="form-control"
                                           value="{{ old('phone') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <input placeholder="Город"
                                           type="text"
                                           name="city"
                                           id="city" class="form-control"
                                           value="{{ old('city') }}">
                                    <div class="required"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                <div class="form-group">
                            <textarea name="comment"
                                      id="comment"
                                      rows="7"
                                      class="form-control">{{ old('comment') }}</textarea>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <div class="b-product_contact__required">
                                    <div>Поля обязательны к заполнению</div>
                                    <div>Ваши данные не будут переданы третьим лицам</div>
                                    <div class="required"></div>
                                </div>

                                {{csrf_field()}}
                                <button type="submit" class="btn btn-default">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </form>
    </div>

@endsection
