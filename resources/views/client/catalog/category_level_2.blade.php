@extends('client.catalog.index')

@section('catalog_content')

    <div class="b-category col-lg-9 col-md-12 col-sm-12 col-xs-12">

        @include('client.catalog.hit')

        @include('client.catalog.catalog_items')

        <div class="b-category__social">
            @include('partials.social')
        </div>
    </div>

@endsection


