@if($category->allCategoryProducts($category))
    <div class="row b-category_row">
        <img src="/img/categories_title.png" class="hidden-xs">
        <div class="b-category__title">Хиты продаж</div>
    </div>
    <div class="b-category-hit">
        <div class="b-category-hit_container" style="width:2000%">
            @foreach($category->allCategoryProducts($category) as $product)
                <div class="b-slide">
                    <a href="{{$url->makeProductUrl($product)}}">
                        <div class="b-slide-image">
                            <div class="circle">
                                <div class="frame">
                                    <div></div>
                                </div>
                                @if(!$product->photos->isEmpty())
                                    <img src="/{{$product->photos[0]->thumbnail_path}}" alt="">
                                @else
                                    <img src="/img/no_image_260_200.png" alt="">
                                @endif
                            </div>
                        </div>
                    </a>
                    <div class="b-category-hit__content">
                        <a href="{{$url->makeProductUrl($product)}}" class="title">{{$product->page->title}}</a>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="b-category-hit__arrow-left">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
        <div class="b-category-hit__arrow-right">
            <img src="/img/slider_interior_arrow.png" alt="">
        </div>
    </div>

@endif

