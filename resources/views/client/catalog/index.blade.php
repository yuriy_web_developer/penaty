@extends('client.app')

@section('content')
    @include('client.page.header')

    @include('client.catalog.breadcrumbs')

    @include('client.sidebar.left_sidebar')

    @yield('catalog_content')

@endsection


