@extends('client.catalog.index')

@section('catalog_content')

    <div class="b-defer col-lg-9 col-md-12 col-sm-12 col-xs-12">
        @include('flash::message')

        @foreach($products as $key=>$product)
            <div class="b-defer_row">
                <div class="b-defer__image col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <a href="{{$url->makeProductUrl($product['product'])}}">
                    @if(!$product['product']->photos->isEmpty())
                        <img src="/{{$product['product']->photos[0]->thumbnail_path}}" alt="">
                    @else
                        <img src="/img/no_image_260_200.png" alt="">
                    @endif
                    </a>
                </div>
                <div class="b-defer_row_info col-lg-10 col-md-10 col-sm-10 col-xs-12">
                    <div class="b-defer_title row">
                        {{$product['product']->page->title}}
                        <a href="/defer/delete?id={{$key}}">
                            <i class="fa fa-times pull-right" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="b-defer_row_btn row">
                        @if (array_key_exists('volume', $product) && $product['volume'])
                            <div class="b-defer__volume">{{$product['volume']}}л</div>
                        @endif
                        <div class="b-defer__quantity">{{$product['quantity']}}шт</div>
                        <div class="b-defer__color">
                            @if($product['color'])
                                <img src="/{{$product['color']->thumbnail_path}}" alt="">
                                <div>{{$product['color']->pivot->color_name}}</div>
                            @endif
                        </div>
                        <div class="b-defer__change">
                            <i class="fa fa-chevron-left" aria-hidden="true"></i>
                            <a href="{{$url->makeProductUrl($product['product'])}}">Изменить</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        @if(!empty($products))
            @include('partials.errors')
            <div class="b-defer_contact">
                <div class="b-defer_contact__title text-left">
                    Запрос для подтверждения цены и наличия Вашего заказа
                </div>
                <form method="POST" action="/defer/send">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="ФИО"
                                       type="text"
                                       name="client_name"
                                       id="client_name" class="form-control"
                                       value="{{ old('client_name') }}">
                                <div class="required"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Email"
                                       type="text"
                                       name="email"
                                       id="email" class="form-control"
                                       value="{{ old('email') }}">
                                <div class="required"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Телефон"
                                       type="text"
                                       name="phone"
                                       id="phone" class="form-control"
                                       value="{{ old('phone') }}">
                                <div class="required"></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input placeholder="Город"
                                       type="text"
                                       name="city"
                                       id="city" class="form-control"
                                       value="{{ old('city') }}">
                                <div class="required"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                            <textarea name="comment"
                                      id="comment"
                                      rows="5"
                                      class="form-control">{{ old('comment') }}</textarea>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="b-defer_contact__required">
                                <div>Поля обязательны к заполнению</div>
                                <div>Ваши данные не будут переданы третьим лицам</div>
                                <div class="required"></div>
                            </div>

                            {{csrf_field()}}
                            <button type="submit" class="btn btn-default">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        @endif

    </div>

@endsection

