<!DOCTYPE html>
<html>

@include('client.head')

<body>
<div class="container">

    @include('client.header')
    @include('client.sidebar.menu')

    @yield('content')

    @include('client.footer')

</div>

<script src="{{ asset('js/app_footer.js') }}"></script>
</body>

</html>
