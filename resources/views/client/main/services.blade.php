<div class="b-services">
    <div class="b-services__title text-center">
        <div>Наши услуги</div>
    </div>

    <div class="b-services_blocks">
        <div class="b-services_block">
            <img src="img/services.jpg" alt="">
            <div class="icon icon-paint"></div>
            <div class="b-service_content">
                <div class="title">Поставка материалов для декоративной отделки</div>
                <div class="content">Роспись в интерьере – это всегда радостно.
                    Вряд ли еще что-то может сравниться с ней по живости и вдохновенности.
                    Среди фабричных материалов, рукотворное произведение,
                </div>
                <form method="GET" action="/postavka-materialov-dlya-dekorativnoj-otdelki">
                    <button type="submit" name="" id="" class="btn">Подробнее</button>
                </form>

            </div>
        </div>
        <div class="b-services_block">
            <img src="img/services2.jpg" alt="">
            <div class="icon icon-sofa"></div>
            <div class="b-service_content">
                <div class="title">Шеф-дизайн <br>проектов</div>
                <div class="content">Роспись в интерьере – это всегда радостно.
                    Вряд ли еще что-то может сравниться с ней по живости и вдохновенности.
                    Среди фабричных материалов, рукотворное произведение,
                </div>
                <form method="GET" action="/shef-dizajn-proektov">
                    <button type="submit" name="" id="" class="btn">Подробнее</button>
                </form>
            </div>
        </div>
        <div class="b-services_block">
            <img src="img/services3.jpg" alt="">
            <div class="icon icon-fire"></div>
            <div class="b-service_content">
                <div class="title">Отделочные работы <br> и декорирование</div>
                <div class="content">Роспись в интерьере – это всегда радостно.
                    Вряд ли еще что-то может сравниться с ней по живости и вдохновенности.
                    Среди фабричных материалов, рукотворное произведение,
                </div>
                <form method="GET" action="/otdelochnye-raboty-i-dekorirovanie">
                    <button type="submit" name="" id="" class="btn">Подробнее</button>
                </form>
            </div>
        </div>
    </div>

    <div class="b-services__left hidden-xs">
        <img src="/img/services_background.png">
    </div>
    <div class="b-services__right hidden-xs">
        <img src="/img/services_background.png" alt="">
    </div>
</div>

