<div class="b-slider-interior">
    <div class="b-slider-interior_container" style="width:2000%">
        @foreach($slider->getSlidesByName('interior') as $slide)
            <div class="b-slide">
                <div class="b-slide-image">
                    <div class="circle">
                        <div class="frame">
                            <div></div>
                        </div>
                        <img src="{{$slide->photo->path}}" alt="">
                    </div>
                    @if($loop->index % 4 == 0 )
                        <div class="quarter-right-bottom"></div>
                    @elseif($loop->index % 3 == 0)
                        <div class="quarter-left-top"></div>
                    @elseif($loop->index % 2 == 0)
                        <div class="quarter-left-bottom"></div>
                    @else
                        <div class="quarter-right-top"></div>
                    @endif

                </div>
                <div class="b-slider-interior__content">
                    <div class="title">{{$slide->title}}</div>
                    <div class="b-slider-interior__text">{!!$slide->description!!}</div>
                    <div class="text-center b-slider-interior__details">
                        <form method="GET" action="{{$slide->url}}">
                            <button type="submit" class="btn">&rarr; Подробнее</button>
                        </form>
                    </div>
                </div>

            </div>
        @endforeach
    </div>
    <div class="b-slider-interior__arrow-left">
        <img src="img/slider_interior_arrow.png" alt="">
    </div>
    <div class="b-slider-interior__arrow-right">
        <img src="img/slider_interior_arrow.png" alt="">
    </div>

    <div class="b-slider_switch text-center">
        <div class="active"></div>
    </div>
</div>

