<div class="b-slider">
    <div class="b-slider_wrap">
        <div class="b-slider_container">
            @foreach($slider->getSlidesByName('main') as $slide)
                <div class="b-slide">
                    <div class="b-slide__border">
                        <div class="b-slider__frame"></div>
                    </div>
                    @if($slide->photo)
                        <img src="{{$slide->photo->path}}" alt="">
                    @endif
                    <div class="b-slide__desc">
                        <a href="/">{{$slide->title}}</a>
                    </div>
                    <div class="b-slide_details">
                        <a href="{{$slide->link}}">Подробнее</a>
                    </div>
                </div>
            @endforeach

            {{--@foreach($slider->getSlidesByName('main') as $slide)--}}
                {{--<div class="b-slide">--}}
                    {{--<div class="b-slide__border">--}}
                        {{--<div class="b-slider__frame"></div>--}}
                    {{--</div>--}}
                    {{--<img src="{{$slide->photo->path}}" alt="">--}}
                    {{--<div class="b-slide__desc">--}}
                        {{--<a href="/">{{$slide->title}}</a>--}}
                    {{--</div>--}}
                    {{--<div class="b-slide_details">--}}
                        {{--<a href="{{$slide->link}}">Подробнее</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--@endforeach--}}
        </div>
    </div>
    <div class="b-slider__arrow-right">
        <img src="img/slider_arrow.png" alt="">
    </div>
    <div class="b-slider__arrow-left">
        <img src="img/slider_arrow.png" alt="">
    </div>
    <div class="b-slider_switch">
        <div class="active"></div>
    </div>

    <div class="b-slider_background-left hidden-xs"></div>
    <div class="b-slider_background-right hidden-xs"></div>
</div>

