@extends('client.app')

@section('content')
    @include('client.main.main_slider')

    <div class="b-design">
        <div class="b-design__background">
            <img src="/img/design_background.jpg" alt="">
        </div>
        <div class="b-design_photo"
             data-delay="{{$interior['delay']}}"
             data-inEffect="{{$interior['inEffect']}}"
             data-outEffect="{{$interior['outEffect']}}">
            @foreach($interior['photo_ids'] as $photo)
                <img class="{{$loop->index==0 ? '' : 'hidden'}}" src="/{{$photo->path}}">
            @endforeach
        </div>
        <div class="b-design_border1"></div>
        <div class="b-design_border2"></div>
        <div class="b-design_border3"></div>

        <div class="b-design__text">
            <a href="{{$interior['link']}}">Дизайн интерьеров</a>
        </div>
    </div>

    @include('client.main.slider_interior')

    @include('client.main.services')

    @include('client.main.free_consult')
@endsection

