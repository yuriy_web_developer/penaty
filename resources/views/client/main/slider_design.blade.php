<div class="b-design">
    <div class="b-design__background">
        <img src="/img/design_background.jpg" alt="">
    </div>
    <div class="b-design_photo"
         data-delay="{{$interior['delay']}}"
         data-inEffect="{{$interior['inEffect']}}"
         data-outEffect="{{$interior['outEffect']}}">
        @foreach($interior['photo_ids'] as $photo)
            <img class="{{$loop->index==0 ? '' : 'hidden'}}" src="/{{$photo->path}}">
        @endforeach
    </div>
    <a href="{{$interior['link']}}">
        <div class="b-design_border1"></div>
        <div class="b-design_border2"></div>
        <div class="b-design_border3"></div>
    </a>
    {{--<div class="b-design__text">--}}
        {{--<a href="{{$interior['link']}}">Дизайн интерьеров</a>--}}
    {{--</div>--}}
</div>