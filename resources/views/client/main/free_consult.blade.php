<div class="b-consult">
    <div class="b-consult__title text-center">
        <div>Получить бесплатную консультацию</div>
    </div>
    <div class="row">
        <div class="b-consult_form__frame"></div>
        <div class="b-consult_form col-lg-7 col-md-7 col-sm-7 col-xs-12 center-block">
            <form method="POST" action="/">
                <div class="form-group col-xs-12">
                    <div class="b-consult__required"></div>
                    <input type="text" name="client_name" class="form-control" placeholder="Ваше имя">
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 b-consult__phone">
                        <div class="b-consult__required"></div>
                        <input type="text" name="phone" placeholder="Ваш телефон" class="form-control">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 b-consult__email">
                        <div class="b-consult__required"></div>
                        <input type="text" name="email" placeholder="Ваш E-mail" class="form-control">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-7 col-md-7 col-sm-7 col-xs-12 b-consult__comment">
                        <textarea name="comment" placeholder="Комментарий"
                                  class="form-control" rows="6"></textarea>
                    </div>
                    <div class="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12 b-consult__repair">
                        <select name="repair_type" class="form-control">
                            <option selected disabled>Объект ремонта</option>
                            <option value="Кафе">Кафе</option>
                            <option value="Коттедж">Коттедж</option>
                            <option value="Офис">Офис</option>
                            <option value="Квартира">Квартира</option>
                        </select>
                        <div class="b-consult__desc">
                            <div></div>
                            <span>Поля обязательны к заполнению <br>
                                    Ваши данные не будут переданы третьим лицам</span>
                        </div>
                        <button type="submit" class="btn">Отправить заявку</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>