<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProductsAddColumnsForTabs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('basement')->nullable()->after('short_info');
            $table->string('instrument')->nullable()->after('basement');
            $table->string('consumption')->nullable()->after('instrument');
            $table->string('layers')->nullable()->after('consumption');
            $table->string('dilution')->nullable()->after('layers');
            $table->text('tab_description')->nullable()->after('layers');
            $table->text('tab_instruction')->nullable()->after('tab_description');
            $table->text('tab_video')->nullable()->after('tab_instruction');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn([
                'basement',
                'instrument',
                'consumption',
                'layers',
                'dilution',
                'tab_description',
                'tab_instruction',
                'tab_video',
            ]);
        });
    }
}
