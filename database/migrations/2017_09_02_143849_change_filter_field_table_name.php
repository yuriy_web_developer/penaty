<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFilterFieldTableName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('filter_data', function (Blueprint $table) {
            Schema::rename('filter_data', 'filter_field');
            $table->renameColumn('filter_data_id', 'filter_field_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('filter_field', function (Blueprint $table) {
            Schema::dropIfExists('filter_field');
        });
    }
}
