<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Models\InfoPage::class, function (Faker\Generator $faker) {
    return [
        'url' => $faker->url,
        'title' => $faker->sentence,
        'content' => $faker->paragraph(),
        'published' =>$faker->numberBetween(0, 1),
        'archived' =>$faker->numberBetween(0, 1),
        'keywords' => $faker->sentence,
        'description' => $faker->sentence,
        'type' => 'info',
    ];
});


$factory->define(App\Models\News::class, function (Faker\Generator $faker) {
    return [
        'url' => $faker->url,
        'title' => $faker->sentence,
        'content' => $faker->paragraph(),
        'published' =>$faker->numberBetween(0, 1),
        'archived' =>$faker->numberBetween(0, 1),
        'keywords' => $faker->sentence,
        'description' => $faker->sentence,
        'type' => 'news',

    ];
});


$factory->define(App\Models\Subscriber::class, function (Faker\Generator $faker) {
    return [
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
    ];
});


$factory->define(App\Models\Product::class, function (Faker\Generator $faker) {
    return [
        'title'=>$faker->sentence,
        'url'=>$faker->url,
        'category_id' =>$faker->numberBetween(0,100),
        'available' =>$faker->numberBetween(0,1),
        'published' =>$faker->numberBetween(0,1),
    ];
});



