<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{

    /**
     * Tables that should be cleaned
     * @var array
     */
    protected $tables = ['pages', 'subscribers', 'products'];

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $this->cleanTables();
        $this->call(InfoPageSeeder::class);
        $this->call(NewsSeeder::class);
        $this->call(SubscriberSeeder::class);
        $this->call(ProductsSeeder::class);
    }

    /**
     * Clean tables before seeding them.
     */
    public function cleanTables()
    {
        foreach ($this->tables as $table) {
            DB::table($table)->truncate();
        }
    }

}
