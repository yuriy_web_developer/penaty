<?php

use Illuminate\Database\Seeder;

class InfoPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\InfoPage::class, 50)->create();
    }
}
