//noinspection JSAnnotator
const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.sass('resources/assets/sass/client/app.scss', 'public/css');


mix.styles([
    'resources/assets/css/libs/jquery-ui.css',
    'resources/assets/css/libs/sweetalert.css',
    'resources/assets/css/libs/lity.css',
    'resources/assets/css/libs/font-awesome.css',
    'resources/assets/css/libs/fancybox.css',
    'resources/assets/css/libs/animate.css',
    'public/css/app.css'
], 'public/css/app.css');


mix.scripts([
    'resources/assets/js/libs/jquery-3.2.1.js',
    'resources/assets/js/libs/bootstrap.js',
    'resources/assets/js/libs/jquery-ui.js',
    'resources/assets/js/libs/jquery-ui-touch.js',
    'resources/assets/js/libs/sweetalert.min.js',
    'resources/assets/js/libs/lity.js',
    'resources/assets/js/libs/fancybox.js',
], 'public/js/app.js');


//copy client fonts
mix.copyDirectory('resources/assets/fonts', 'public/fonts');


//admin js and styles
mix.scripts([
    'resources/assets/js/libs/jquery-3.2.1.js',
    'resources/assets/js/libs/bootstrap.js',
    'resources/assets/js/libs/jquery-ui.js',
    'resources/assets/js/libs/jquery.cookie.js',
    'resources/assets/js/libs/dropzone.js',
    'resources/assets/js/libs/sweetalert.min.js',
    'resources/assets/js/libs/lity.js',
    'resources/assets/js/libs/fancybox.js',
], 'public/admin_assets/js/libs.js');

mix.copyDirectory('resources/assets/js/libs/tinymce', 'public/admin_assets/js/tinymce');

mix.sass('resources/assets/sass/admin/app.scss', 'public/admin_assets/css');

mix.styles([
    'resources/assets/css/libs/jquery-ui.css',
    'resources/assets/css/libs/dropzone.css',
    'resources/assets/css/libs/sweetalert.css',
    'resources/assets/css/libs/lity.css',
    'resources/assets/css/libs/fancybox.css',
    'public/admin_assets/css/app.css',
], 'public/admin_assets/css/app.css');




