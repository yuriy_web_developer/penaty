<?php
namespace App\Modules\Media;

use Auth;
use File;
use Carbon\Carbon;
use App\Models\Media;
use Illuminate\Http\UploadedFile;
use App\Modules\Url\UrlTransform;
use App\Modules\Photo\PhotoUploader;

/**
 * Media uploader
 *
 * Class MediaUploader
 * @package App\Modules\Media
 */
class MediaUploader
{
    /**
     * Media save folder
     *
     * @var string
     */
    public $baseDir = 'downloads';

    /**
     * Media instance to save
     *
     * @var Media
     */
    protected $media;

    /**
     * MediaUploader constructor.
     */
    public function __construct()
    {
        $this->media = new Media;
    }

    /**
     * Upload a file
     *
     * @param UploadedFile $file
     * @return array|mixed
     */
    public function upload(UploadedFile $file)
    {
        $ext = $file->guessExtension();

        //extension is allowed to upload
        if (!$this->media->isAllowed($ext)) return false;

        //if media is a photo
        if ($this->media->isPhoto($ext))
            return (new PhotoUploader)->makePhoto($file);

        //for other media
        return $this->makeMedia($file);
    }

    /**
     * Process the photo upload
     *
     * @param UploadedFile $file
     * @return mixed
     */
    public function makeMedia(UploadedFile $file)
    {
        $uploaded = $this->named($file->getClientOriginalName())
                         ->move($file)
                         ->save();

        return [
            'location' => '/' . $uploaded->media->path,
            'id'       => $uploaded->media->id,
        ];
    }

    /**
     * Wrapper to instantiate a class and set a filename
     *
     * @param $name string
     * @return $this
     */
    public function named($name)
    {
        return $this->setNames($name);
    }

    /**
     * Move uploaded file and thumbnail
     *
     * @param UploadedFile $file
     * @return $this
     */
    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir, $this->media->name);

        return $this;
    }

    /**
     * Save media model
     *
     * @return $this
     */
    public function save()
    {
        $this->media->uploaded_by = Auth::id();

        $this->media->save();

        return $this;
    }

    /**
     * Set filename and folder name
     *
     * @param $name
     * @return $this
     */
    protected function setNames($name)
    {
        static::makeSaveDir();

        //transliterate filename
        $name = $this->transliterateFilename($name);

        $this->media->name = sprintf('%s-%s', Carbon::create()->format('Y_m_d_H_i_s'), $name);
        $this->media->path = sprintf('%s/%s', $this->baseDir, $this->media->name);

        return $this;
    }

    /**
     * Organize folders according to year and month
     *
     */
    protected function makeSaveDir()
    {
        $yearMonthPath = Carbon::create()->format('Y/m');

        $baseDirWithDate = public_path() . '/' . $this->baseDir . '/' . $yearMonthPath;
        $isBaseDirCreated = File::makeDirectory($baseDirWithDate, 0775, true, true);
        if ($isBaseDirCreated || file_exists($baseDirWithDate))
            $this->baseDir = $this->baseDir . '/' . $yearMonthPath;

    }

    /**
     * Transliterate filename
     *
     * @param string $name
     * @return string
     */
    protected function transliterateFilename($name)
    {
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $filename = pathinfo($name, PATHINFO_FILENAME);

        $filename = (new UrlTransform)->makeFriendlyUrl($filename);

        return $filename . '.' . $ext;
    }
}