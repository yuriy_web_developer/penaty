<?php

namespace App\Modules\Subscription;

use App\Models\EmailConfirm;
use Illuminate\Validation\Rule;
use App\Mail\SubscribeEmailConfirm;
use App\Http\Controllers\Controller;

class SubscribeEmail extends Controller
{

    /**
     * @var EmailConfirm
     */
    protected $confirm;

    /**
     * Email to subscribe
     *
     * @var string
     */
    protected $email;

    public function __construct()
    {
        $this->confirm = new EmailConfirm;
        $this->email = request('email', '');
    }

    /**
     * Process subscription wrapper
     */
    public function subscribe()
    {
        if (!request()->has('email')) return;

        $this->validateRequest()
             ->addEmail()
             ->sendConfirmEmail();
    }

    /**
     * Validate the incoming request
     *
     * @return $this
     */
    private function validateRequest()
    {
        $this->validate(request(), [
            'email' => Rule::unique('subscribers')->where(function ($query) {
                return $query
                    ->whereNull('deleted_at')
                    ->where('email', request('email'))->get();
            }),
        ]);

        return $this;
    }

    /**
     * Send email with confirmation link
     *
     * @return mixed
     */
    private function sendConfirmEmail()
    {
        $link = url('/subscribe/email/confirm?hash=') . $this->confirm->hash;

        return \Mail::to($this->email)
                    ->send(new SubscribeEmailConfirm(['link' => $link]));
    }

    /**
     * Generate hash and store email
     *
     * @return $this
     */
    private function addEmail()
    {
        $hash = md5(microtime());

        $this->confirm = $this->confirm->create([
            'email' => $this->email,
            'hash'  => $hash,
            'page'  => array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '',
            'ip'    => request()->ip(),
        ]);

        return $this;
    }
}