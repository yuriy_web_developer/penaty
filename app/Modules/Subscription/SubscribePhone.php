<?php

namespace App\Modules\Subscription;

use App\Models\Subscriber;
use App\Mail\ClientSubscribed;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;

class SubscribePhone extends Controller
{

    /**
     * @var string
     */
    protected $phone;

    public function __construct()
    {
        $this->phone = request('phone', '');
    }

    /**
     * Phone subscription
     *
     * @return bool
     */
    public function subscribe()
    {
        if (!request()->has('phone')) return false;

        $this->validateRequest()
             ->addPhone()
             ->successEmail();

        return true;
    }

    /**
     * Validate the incoming request
     *
     * @return $this
     */
    private function validateRequest()
    {
        $this->validate(request(), [
            'phone' => Rule::unique('subscribers')->where(function ($query) {
                return $query
                    ->whereNull('deleted_at')
                    ->where('phone', request('phone'))->get();
            }),
        ]);

        return $this;
    }

    /**
     * Save phone number
     *
     * @return $this
     */
    private function addPhone()
    {
        Subscriber::create([
            'phone' => $this->phone,
            'page'  => array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '',
            'ip'    => request()->ip(),
        ]);
        return $this;
    }

    /**
     * Send success email
     *
     * @return $this
     */
    private function successEmail()
    {
        $replace = [
            'phone' => $this->phone,
            'email' => '',
            'page'  => array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '',
            'ip'    => request()->ip(),
        ];
        \Mail::to('test@test.com')->send(new ClientSubscribed($replace));

        return $this;
    }


}