<?php

namespace App\Modules\Subscription;

use App\Mail\ClientSubscribed;
use App\Models\EmailConfirm as Confirm;
use App\Models\Subscriber;
use Carbon\Carbon;

class EmailConfirm
{
    /**
     * Confirm email
     *
     * @return bool
     */
    public function confirm()
    {
        $subs = $this->hashExists();

        if (!$subs) return false;

        $this->addSubscriber($subs)
             ->successEmail($subs);

        return true;
    }

    /**
     * Check if hash exists
     *
     * @return bool|Confirm
     */
    private function hashExists()
    {
        $subs = Confirm::where([['hash', '=', request('hash', '')],
            ['created_at', '>=', Carbon::now()->subDay()],
        ])->first();

        if ($subs && $subs->isEmpty) return false;

        return $subs;
    }

    /**
     * Add subscribe to the store
     *
     * @param Confirm $subs
     * @return $this
     */
    private function addSubscriber(Confirm $subs)
    {
        Subscriber::create([
            'email' => $subs->email,
            'page'  => $subs->page,
            'ip'    => $subs->ip,
        ]);

        Confirm::where('email', $subs->email)->delete();

        return $this;
    }

    /**
     * Send success email
     *
     * @param Confirm $subs
     * @return $this
     */
    private function successEmail(Confirm $subs)
    {
        $replace = [
            'phone' => '',
            'email' => $subs->email,
            'page'  => $subs->page,
            'ip'    => $subs->ip,
        ];

        \Mail::to($subs->email)->send(new ClientSubscribed($replace));

        return $this;
    }
}