<?php

namespace App\Modules\Search;

use App\Models\Page;

class SearchSite
{

    /**
     * Search pages or products
     *
     * @param $term  string
     * @return mixed
     */
    public function search($term)
    {
        $term = urldecode($term);

        $pages = (new Page)
            ->orWhere('title', 'like', '%' . $term . '%')
            ->orWhere('content', 'like', '%' . $term . '% ')
            ->orWhere('short_content', 'like', '%' . $term . '%')
            ->limit(50)
            ->paginate(10)
            ->appends(
                ['term' => $term]
            );

        return $pages;
    }

}