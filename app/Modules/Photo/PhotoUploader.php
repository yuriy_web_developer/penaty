<?php

namespace App\Modules\Photo;

use Auth;
use File;
use Carbon\Carbon;
use App\Models\Photo;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image;

/**
 * Photo Uploader
 *
 * Class PhotoUploader
 * @package App\Modules\Photo
 */
class PhotoUploader
{
    /**
     * Path to a photo folder. Can be changed in method "makeSaveDir".
     *
     * @var string
     */
    protected $baseDir = 'img/uploaded/full';

    /**
     * Path to a thumbnail folder. Can be changed in method "makeSaveDir".
     *
     * @var string
     */
    protected $baseThumbnailDir = 'img/uploaded/tn';

    /**
     * Photo model
     *
     * @var \App\Models\Photo
     */
    public $photo;

    public function __construct()
    {
        $this->photo = new Photo;
    }

    /**
     * Wrapper to instantiate a class and set a filename
     *
     * @param $name
     * @return mixed
     */
    public static function named($name)
    {
        return (new static)->setNames($name);
    }

    /**
     * Upload photo from ajax request;
     *
     * @param UploadedFile $file
     * @return array
     */
    public static function fromAjax(UploadedFile $file)
    {
        $saved = (new static)->makePhoto($file);

        return [
            'location' => '/' . $saved->photo->path,
            'tn'       => '/' . $saved->photo->thumbnail_path,
            'id'       => $saved->photo->id,
        ];
    }

    /**
     * Process the photo upload
     *
     * @param UploadedFile $file
     * @return mixed
     */
    public function makePhoto(UploadedFile $file)
    {
        return self::named($file->getClientOriginalName())
                   ->move($file)
                   ->save();
    }

    /**
     * Set filename and folder name
     *
     * @param $name
     * @return $this
     */
    private function setNames($name)
    {
        $this->makeSaveDir();

        $this->photo->name = sprintf('%s-%s', Carbon::create()->format('Y_m_d_H_i_s'), $name);
        $this->photo->path = sprintf('%s/%s', $this->baseDir, $this->photo->name);
        $this->photo->thumbnail_path = sprintf('%s/%s', $this->baseThumbnailDir, $this->photo->name);

        return $this;
    }

    /**
     * Move uploaded file and thumbnail
     *
     * @param UploadedFile $file
     * @return $this
     */
    public function move(UploadedFile $file)
    {
        $file->move($this->baseDir, $this->photo->name);

        $this->makeThumbnail();

        return $this;
    }

    /**
     * Save photo model
     *
     * @return $this
     */
    public function save()
    {
        $this->photo->uploaded_by = Auth::id();

        $this->photo->save();

        return $this;
    }

    /**
     * Make and save thumbnail
     *
     */
    public function makeThumbnail()
    {
        Image::make($this->photo->path)
             ->fit(200)
             ->save($this->photo->thumbnail_path);
    }

    /**
     * Organize folders according to year and month
     *
     */
    private function makeSaveDir()
    {
        $yearMonthPath = Carbon::create()->format('Y/m');

        $baseDirWithDate = public_path() . '/' . $this->baseDir . '/' . $yearMonthPath;
        $isBaseDirCreated = File::makeDirectory($baseDirWithDate, 0775, true, true);
        if ($isBaseDirCreated || file_exists($baseDirWithDate))
            $this->baseDir = $this->baseDir . '/' . $yearMonthPath;

        $baseThumbnailDirWithDate = public_path() . '/' . $this->baseThumbnailDir . '/' . $yearMonthPath;
        $isBaseThumbnailDirCreated = File::makeDirectory($baseThumbnailDirWithDate, 0775, true, true);

        if ($isBaseThumbnailDirCreated || file_exists($baseThumbnailDirWithDate))
            $this->baseThumbnailDir = $this->baseThumbnailDir . '/' . $yearMonthPath;
    }

}