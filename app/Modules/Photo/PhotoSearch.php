<?php

namespace App\Modules\Photo;

use App\Models\Photo;

class PhotoSearch
{

    /**
     * Number of results for pagination
     *
     * @var int
     */
    public static $paginateNum = 24;

    /**
     * Columns to search
     *
     * @var array
     */
    public $searchColumns = [];

    /**
     * Words to search
     *
     * @var array
     */
    public $searchWords = [];

    /**
     * Sort column
     *
     * @var string
     */
    public $sortColumn = 'id';

    /**
     * Desc or asc sorting
     *
     * @var string
     */
    public $sortType = 'desc';

    /**
     * Search query
     *
     * @var \Illuminate\Database\Eloquent\Builder
     */
    protected $query;

    public function __construct()
    {
        $this->query = (new Photo)->newQuery();
    }

    /**
     * Start a search
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getQuery()
    {
        return (new static)
            ->searchData()
            ->makeQuery();
    }

    /**
     * Form a query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function makeQuery()
    {
        $this->setOrderBy();

        foreach ($this->searchColumns as $column) {
            foreach ($this->searchWords as $word) {

                if (!$word) continue;

                $this->query->orWhere($column, 'like', '%' . $word . '%');
            }
        }

        return $this->query;
    }

    /**
     * Set order by.
     */
    public function setOrderBy()
    {
        if ($this->getSortColumnName()) {
            $this->query->orderBy($this->sortColumn, $this->sortType);
        }
    }

    /**
     * By what column in the database to sort results
     *
     * @return bool|string
     */
    private function getSortColumnName()
    {
        $sort = request()->get('sort');

        switch ($sort) {
            case '1':
                $this->sortColumn = 'created_at';
                break;
            case '2' :
                $this->sortColumn = 'updated_at';
                break;
            case '3' :
                $this->sortColumn = 'name';
                break;
        }

        return false;
    }

    /**
     * Get data to search from request
     *
     * @return $this
     */
    private function searchData()
    {
        if (request()->has('search_word')) {
            $this->searchColumns[] = 'name';
            $this->searchWords[] = request()->get('search_word');
        }

        return $this;
    }


    /**
     * Search photos with ajax calls
     *
     * @return mixed
     */
    public function fromAjax()
    {
        return (new Photo)
            ->select(['name', 'id', 'path', 'thumbnail_path'])
            ->orWhere('path', 'like', '%' . trim(request()->get('term'), '/') . '%')
            ->orWhere('name', 'like', '%' . request()->get('term'))
            ->limit(5)->get()->toJson();

    }

    /**
     * Data to be appended in pagination
     *
     * @return array
     */
    public static function appendsData()
    {
        return ['search_word' => request()->get('search_word'),
                'sort'        => request()->get('sort')];
    }

}