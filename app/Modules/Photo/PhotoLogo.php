<?php

namespace App\Modules\Photo;

use Intervention\Image\Facades\Image;

class PhotoLogo
{

    /**
     * Path to the original photo
     *
     * @var string
     */
    private $original;

    /**
     * Absolute path to the original photo
     *
     * @var string
     */
    private $originalAbsolutePath;

    /**
     * Path to a photo with a logo
     *
     * @var string
     */
    private $fileWithLogo;

    /**
     * Where to place a logo
     *
     * @var string
     */
    private $logoPosition = 'bottom-right';

    /**
     * Path to file with logo
     *
     * @var string
     */
    private $logoPath = 'img/logo_photo.png';

    /**
     * Absolute path to logo
     *
     * @var string
     */
    private $logoPathAbsolute;

    /**
     * Logo size in percents, relative to a photo
     *
     * @var int
     */
    private $scale = 30;

    /**
     * PhotoLogo constructor.
     * @param $path string
     */
    public function __construct($path, $pathWithLogo)
    {
        $this->original = $path;
        $this->originalAbsolutePath = public_path() . '/' . $path;
        $this->logoPathAbsolute = public_path() . '/' . $this->logoPath;
        $this->fileWithLogo = public_path() . '/' . $pathWithLogo;
    }

    /**
     * Control the process of inserting a logo
     *
     * @return $this
     */
    public function make()
    {
        if (!$this->logoExists()) return $this;

        $logo = $this->fitLogo();

        $this->insert($logo);

        return $this;
    }

    /**
     * Where logo should be placed.
     *
     * @param string $corner
     * @return $this
     */
    public function position($corner)
    {
        //available position for intervention library
        $available = [
            'top-left',
            'top',
            'top-right',
            'left',
            'center',
            'right',
            'bottom-left',
            'bottom',
            'bottom-right',
        ];

        if (array_key_exists((string)$corner, $available))
            $this->logoPosition = $corner;

        return $this;
    }

    /**
     * Set scale size
     *
     * @param $size
     * @return $this
     */
    public function scale($size)
    {
        $size = (int)$size;

        if ($size > 0 && $size < 100) $this->scale = $size;

        return $this;
    }

    /**
     * Get path with logo
     *
     * @return string
     */
    public function get()
    {
        return $this->fileWithLogo ? $this->fileWithLogo : $this->original;
    }

    /**
     * Check if file with logo exists
     *
     * @return bool
     */
    private function logoExists()
    {
        if (file_exists($this->logoPathAbsolute)) return true;

        return false;
    }

    /**
     * Fit logo size to image
     *
     * @return \Intervention\Image\Image
     */
    private function fitLogo()
    {
        $width = Image::make($this->originalAbsolutePath)->width();

        $logoWidth = ($this->scale / 100) * $width;

        return Image::make($this->logoPathAbsolute)
                    ->resize($logoWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
    }

    /**
     * Insert the logo to the photo
     *
     * @param \Intervention\Image\Image $logo
     */
    private function insert($logo)
    {
        return Image::make($this->originalAbsolutePath)
                    ->insert($logo, $this->logoPosition)
                    ->save($this->fileWithLogo);
    }


}