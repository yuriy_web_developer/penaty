<?php

namespace App\Modules\Product;

use App\Models\Feature;
use App\Models\Product;
use Illuminate\Support\Collection;

class ProductFeature
{
    /**
     * features that belong to a product
     * @var
     */
    protected $features;

    protected $currentFeatures;

    /**
     * Product instance
     *
     * @var Product
     */
    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
        $this->features = collect();
        $this->currentFeatures = $product->currentFeatures;
    }

    /**
     * All features that belong to a product including parent features.
     *
     * @return Collection
     */
    public function all()
    {
        return $this
            ->retrieve()
            ->unique()
            ->sorted()
            ->get();
    }

    /**
     * Getter for features
     *
     * @return Collection
     */
    public function get()
    {
        return $this->features;
    }

    /**
     * Attach features to a product
     *
     * @param $data
     */
    public function attach($data)
    {
        $toAttach = [];

        foreach ($data as $key => $one) {
            //do not attach defaults
            $default = Feature::find($key);
            if ($default->default_value == $one || empty($one)) continue;

            $toAttach[$key] = ['value' => $one];
        }

        //dd($toAttach);
        $this->product->currentFeatures()->sync($toAttach);
    }

    /**
     * Get current or default value of a feature
     *
     * @param $id int
     * @return string
     */
    public function value($id)
    {
        $feature = $this->featureExists($id);

        if(!$feature) return '';

        return $feature->pivot->value ? $feature->pivot->value : $feature->default_value;
    }

    /**
     * Check by id if feature exists
     *
     * @param int $id
     * @return bool
     */
    private function featureExists($id)
    {
        $found = ($this->currentFeatures->search(function ($item) use ($id) {
            return $item->id == $id;
        }));

        return ($found === false) ? false : $this->currentFeatures[$found];
    }

    /**
     * Select only unique features
     * @return $this
     */
    private function unique()
    {
        $this->features = collect($this->features)->unique(function ($item) {
            return $item->id;
        });

        return $this;
    }

    /**
     * Get all features
     *
     * @return $this
     */
    private function retrieve()
    {
        foreach ($this->product->category as $category) {
            $this->features->push($category->feature);
            foreach ($category->parents(null, true) as $parent) {
                $this->features->push($parent->category->feature);
            }
        }

        $this->features = $this->features->flatten();

        return $this;
    }

    /**
     * Sort features
     *
     * @return $this
     */
    private function sorted()
    {
        $this->features = $this->features->sortBy('name');
        return $this;
    }

}