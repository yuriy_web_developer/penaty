<?php

namespace App\Modules\ShortCode;

use App\Models\ShortCode as ShortCodeModel;

/**
 * |||shorcode||| syntax is used in all templates
 *
 * Class ShortCode
 * @package App\Modules\ShortCode
 */
class ShortCode
{

    /**
     * Html where replacement is made
     *
     * @var string
     */
    private $html;

    /**
     * Original html
     *
     * @var string
     */
    private $original;

    /**
     * Apply global shortcodes
     *
     * @var bool
     */
    private $withGlobal = false;


    /**
     * Status
     *
     * @var int
     */
    public $counter = 0;

    public function __construct($html)
    {
        $this->original = $this->html = $html;
    }

    /**
     * Replace the shortcode in html
     *
     * @param array $replace
     * @return $this
     */
    public function replace($replace)
    {
        foreach ($replace as $shortCode => $value) {
            $this->html = str_ireplace('|||' . $shortCode . '|||', $value, $this->html);
            $this->counter++;
        }
        return $this;
    }

    /**
     * Getter of html
     *
     * @return mixed
     */
    public function get()
    {
        return $this->html;
    }

    /**
     * Replace all available shortcodes
     *
     * @return $this
     */
    public function globalCodesReplace()
    {
        $codes = ShortCodeModel::all();

        foreach ($codes as $code) {
            $this->replace([$code->code => $code->replace]);
        }
        return $this;
    }

    /**
     * Apply global shortcodes
     *
     * @return $this
     */
    public function withGlobal()
    {
        $this->withGlobal = true;
        return $this;
    }
}