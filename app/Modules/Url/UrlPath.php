<?php

namespace App\Modules\Url;

use App\Models\Category;
use App\Models\Page;
use App\Models\Product;

class UrlPath
{
    /**
     * Create a url for a product
     *
     * @param Product $product
     * @param Category|null $parent
     * @return string
     */
    public function makeProductUrl(Product $product, Category $parent = null)
    {
        if (!$parent) $parent = $product->category()->first();

        return $this->makeCategoryUrl($parent).'/'.$product->page->url;
    }

    /**
     * Create a category url
     *
     * @param Category $current
     * @return string
     */
    public function makeCategoryUrl(Category $current)
    {
        $parents = $current->parents($current);

        $url = '/'.Category::$slug . '/';
        $url .= $parents ? implode('/', $parents) . '/' : '';
        $url .= $current->page->url;

        return $url;
    }

    /**
     * Create a page url from Page model
     *
     * @param Page $page
     * @return string
     */
    public function makePageUrl(Page $page)
    {
        if (!$page->category) return '';

        return $this->makeCategoryUrl($page->category);
    }

}