<?php

namespace App\Modules\Catalog;

use App\Models\Product;
use App\Models\Category;
use App\Models\FilterField;
use App\Models\ProductFilter;
use Illuminate\Support\Collection;

class FilterCatalog
{
    private $filterFields;

    public function __construct()
    {
        $this->filterFields = new FilterFields;
    }

    /**
     * Filter products
     *
     * @param $category Category
     * @return Collection
     */
    public function filter($category)
    {
        //todo caching db queries
        $products = $this->getProducts();
        $products = $this->productsBelongToCategory($products, $category);
        $products = $this->mustHaveAllFields($products);

        return $products;
    }

    /**
     * Get all products that belong to filter fields ids
     *
     * @return Collection
     */
    private function getProducts()
    {
        $products = collect();
        if ($this->getFieldsIds()) {
            $fields = FilterField::find($this->getFieldsIds());
        } else {
            $fields = FilterField::all();
        }

        foreach ($fields as $field) {
            $products->push($field->products);
        }

        return $products
            ->flatten()
            ->unique(function ($item) {
                return $item->id;
            });
    }

    /**
     * Reject products that do not belong to the category
     *
     * @param $products
     * @param Category $currentCategory
     * @return mixed
     */
    private function productsBelongToCategory($products, $currentCategory)
    {
        $categories = $currentCategory->allChildren();
        //add current category
        $categories->push($currentCategory);

        return $products->reject(function ($product) use ($categories, $currentCategory) {
            //all children
            $found = $categories->filter(function ($category) use ($product) {
                $cacheName = 'productBelongToCategory' . $product->id . '_' . $category->id;
                return \Cache::remember($cacheName, config('cache.time'), function () use ($product, $category) {
                    //return ($product->hasCategory($category) || $product->hasAsAnyParent($category));
                    return ($product->hasCategory($category));
                });
            });

            return $found->isEmpty() ? true : false;
        });
    }

    /**
     * Product must have all filter fields
     *
     * @param Collection $products
     * @return Collection
     */
    private function mustHaveAllFields(Collection $products)
    {
        $filters = $this->getFilters();
        return $products->reject(function ($product) use ($filters) {
            foreach ($filters as $filter) {
                if ($this->passFilter($filter, $product)) continue;
                return true;
            }

            return false;
        });
    }

    /**
     * Get field ids
     *
     * @return array
     */
    public function getFieldsIds()
    {
        return (new FilterFields)->getFields();
    }

    /**
     * Get filters by filter fields
     *
     * @return Collection
     */
    public function getFilters()
    {
        $fields = $this->getFieldsIds() ?
            FilterField::find($this->getFieldsIds()) : FilterField::all();

        $filters = collect();

        foreach ($fields as $field) {
            $filters->push($field->filter);
        };

        return $filters->unique(function ($filter) {
            return $filter->id;
        });
    }

    /**
     * Check if product pass a filter
     *
     * @param ProductFilter $filter
     * @param Product $product
     * @return bool
     */
    private function passFilter(ProductFilter $filter, Product $product)
    {
        //if fields are not specified
        if (!$this->getFieldsIds()) return true;

        $ids = $this->filterFields->getCurrentFilterFields($filter);
        foreach ($ids as $id) {
            if (array_search($id, $product->filterFieldIds()) !== false) return true;
        }

        return false;
    }


}