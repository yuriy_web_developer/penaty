<?php

namespace App\Modules\Catalog;

use Cache;
use App\Models\Page;
use App\Models\Product;
use App\Models\Category;

class SearchCatalog
{
    /**
     * search categories for the first level
     *
     * @param $first string
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function searchFirstLevel($first)
    {
        $parent = Cache::remember('searchFirstLevel' . $first, config('cache.time'),
            function () use ($first) {
                return Page::where([
                    'url'  => $first,
                    'type' => 'category',
                ])->first();
            });

        if (!$parent) return redirect()->route('catalog.index');

        return $parent;
    }

    /**
     * search categories for the second level
     *
     * @param $first string
     * @param $second string
     * @return Category|\Illuminate\Http\RedirectResponse
     */
    public function searchSecondLevel($first, $second)
    {
        $parent = $this->searchFirstLevel($first)->category;
        $children = $this->searchChildren($parent, $second);

        return $children ? $children : redirect()->route('catalog.index');
    }

    /**
     * Show many products
     *
     * @param $first string
     * @param $second string
     * @param $third string
     * @return Category|\Illuminate\Http\RedirectResponse
     */
    public function searchManyProducts($first, $second, $third)
    {
        $parent = $this->searchSecondLevel($first, $second);
        $category = $this->searchChildren($parent, $third);

        return $category ? $category : redirect()->route('catalog.index');
    }

    /**
     * Search one product
     *
     * @param $first string
     * @param $second string
     * @param $third string
     * @param $product_slug string
     * @return \Illuminate\Http\RedirectResponse|Product
     */
    public function searchProduct($first, $second, $third, $product_slug)
    {
        $parent = $this->searchSecondLevel($first, $second);
        $category = $this->searchChildren($parent, $third);

        foreach ($category->product as $product)
            if ($product->page->url == $product_slug) return $product;

        return redirect()->route('catalog.index');
    }

    /**
     * Search children by slug
     *
     * @param Category $category
     * @param $slug string
     * @return Category|bool
     */
    private function searchChildren(Category $category, $slug)
    {
        if ($category->hasChildren()) {
            foreach ($category->children() as $child) {
                if ($child->page->url == $slug) {
                    return $child;
                }
            }
        }

        return false;
    }

}