<?php

namespace App\Modules\Catalog;

use App\Models\FilterField;
use App\Models\ProductFilter;

class FilterFields
{

    /**
     * List of current ids
     *
     * @var array
     */
    private $currentIds = [];

    /**
     * Get fields that exists in the request
     * and belong to the specified filter
     *
     * @param ProductFilter $filter
     * @return mixed
     */
    public function getCurrentFilterFields(ProductFilter $filter)
    {
        if (!$this->getFields()) return [];

        $result = $filter->fields
            ->reject(function ($value) {
                //if no fields specified, accept all fields
                if (!$this->getFields()) return false;
                //search fields that exist in request
                if (array_search($value->id, $this->getFields()) !== false) return false;

                return true;
            })->transform(function ($item) {
                return $item->id;
            })->toArray();

        return $result;
    }

    /**
     * Get array with current filter field ids
     *
     * @return array
     */
    public function getFields()
    {
        $input = request('field_ids');

        if (is_string($input)) $input = explode(',', $input);
        $ids = (is_array($input)) ? $input : [];
        $ids = array_map('intval', array_filter($ids));
        $ids = !empty($ids) ? $ids : [];

        $this->currentIds = $ids = array_unique($ids);

        return $this->uniqueFieldsForSlider($ids);
    }

    /**
     * Prepare ids from request for links
     *
     * @param $toggleId
     * @param array $exclude
     * @param array $include
     * @return array
     */
    public function idsForLink($toggleId, $exclude = [], $include = [])
    {
        //make array from existing ids
        $current = $this->getFields();
        $current = $this->uniqueFieldsForSlider($this->currentIds, $toggleId);

        //if value exists, delete it and otherwise
        //(array_search($toggleId, $current) === false) ?
        //    $include[] = $toggleId : $exclude[] = $toggleId;

        //filter arrays
        $exclude = array_map('intval', array_filter($exclude));
        $include = array_map('intval', array_filter($include));

        //apply include and exclude
        $current = array_where($current, function ($value) use ($exclude) {
            if (array_search($value, $exclude) !== false) return false;
            return !!(int)$value;
        });

        if ($include) foreach ($include as $value) $current[] = $value;



        return array_unique($current);
    }

    /**
     * Get only unique values per filter
     *
     * @param ProductFilter $filter
     * @return mixed
     */
    public function uniqueFieldsPerFilter(ProductFilter $filter)
    {
        return array_first($this->getCurrentFilterFields($filter));
    }

    /**
     * Get only unique value for slider filter
     *
     * @param array $ids
     * @param null|int $current
     * @return array
     */
    private function uniqueFieldsForSlider($ids, $current = null)
    {
        if (!empty($current)) $ids[] = $current;

        $fields = !empty($ids) ?
            FilterField::find($ids) : collect();

        //split slider and non slider fields
        $nonSliderFields = $fields->filter(function ($value) {
            if ($value->filter->type != 'slider') return true;
            return false;
        });

        $sliderFields = $fields->filter(function ($value) {
            if ($value->filter->type == 'slider') return true;
            return false;
        });

        //group by filters
        $sliderGrouped = $sliderFields->groupBy('filter_id');

        $sliderFields = $sliderGrouped
            ->map(function ($value) use ($current) {
                $found = $value->search(function ($subValue) use ($current) {
                    if ($subValue->id == $current) return true;
                    return false;
                });

                if($found !== false) return $value[$found];

                return $value->last();
            })->flatten();

        return $sliderFields
            ->merge($nonSliderFields)
            ->map(function ($value) {
                return $value->id;
            })
            ->toArray();
    }
}