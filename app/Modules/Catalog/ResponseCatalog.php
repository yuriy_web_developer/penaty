<?php

namespace App\Modules\Catalog;

use App\Http\Controllers\CatalogController;
use App\Modules\Product\ProductFeature;
use Illuminate\Support\Facades\View;

class ResponseCatalog
{

    /**View name
     *
     * @var
     */
    public $view;

    /**
     * Search results
     *
     * @var SearchCatalog
     */
    private $search;

    /**
     * Constructor
     *
     * @param SearchCatalog $search
     */
    public function __construct(SearchCatalog $search)
    {
        $this->search = $search;
    }

    /**
     * Static wrapper to create an object
     *
     * @param SearchCatalog $result
     * @return View
     */
    public static function get(SearchCatalog $result)
    {
        return (new self($result))->getResponse();
    }

    /**
     * Get response with view and data
     *
     * @return View
     */
    public function getResponse()
    {
        $this->setView();
        //if result is valid
        if ($this->search->shouldRedirect() || !$this->isValidView()) {
            return redirect()->route('catalog.index');
        }

        switch ($this->search->contentType) {
            case 'product':
                return $this->productResponse();
            case 'products':
                return $this->manyProductsResponse();
            case 'category':
                return $this->showCategories();
            case 'categoryLevel2':
                return $this->showCategories();
        }

        //default response
        return redirect()->route('catalog.index');

    }

    /**
     * Prepare product response
     *
     * @return View
     */
    private function productResponse()
    {
        $features = (new ProductFeature($this->search->product))->all();
        $page = $this->search->product->page;
        $product = $this->search->product;

        return view($this->view, compact('features', 'page', 'product'));
    }

    /**
     * Show a listing of products
     *
     * @return View
     */
    private function manyProductsResponse()
    {
        $category = last($this->search->categories);

        return view($this->view, [
            'category' => $category, 'page' => $category->page,
        ]);
    }

    /**
     * Prepare response with list of child categories
     *
     * @return View
     */
    private function showCategories()
    {
        $category = last($this->search->categories);

        return view($this->view, [
            'category' => $category, 'page' => $category->page,
        ]);
    }

    /**
     *  Set view name
     *
     */
    private function setView()
    {
        foreach (CatalogController::$contentView as $name=>$view){
            if($this->search->contentType == $name) $this->view = $view;
        }
    }

    /**
     * if the view exists
     *
     * @return bool
     */
    private function isValidView()
    {
        if (View::exists($this->view)) {
            return true;
        }

        return false;
    }
 
}