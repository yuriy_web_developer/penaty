<?php

namespace App\Modules\Notification;

use App\Models\EmailTemplate;
use App\Modules\ShortCode\ShortCode;

/**
 * Email notification with shortcodes
 *
 * Class EmailNotification
 * @package App\Modules\Notification
 */
class EmailNotification
{
    public $html = '';

    /**
     * Replacements
     *
     * @var array
     */
    public $replace;

    /**
     * Template name
     *
     * @var
     */
    private $template;

    /**
     * Email title
     *
     * @var string
     */
    public $subject;


    /**
     * EmailNotification constructor.
     *
     * @param string $template
     * @param array $replace
     */
    public function __construct($template, $replace = [])
    {
        $this->template = $template;
        $this->replace = $replace;
    }

    /**
     * Make replacement in html
     *
     * @param string|null $html
     * @return $this
     */
    public function doShortCode($html = null)
    {
        $html = $html ? $html : $this->html;

        $this->html = (new ShortCode($html))
            ->replace($this->replace)
            ->get();

        return $this;
    }

    /**
     * Retrieve template
     *
     * @return $this
     */
    public function prepare()
    {
        $rawHtml = EmailTemplate::where('name', $this->template)
                                ->first();

        if ($rawHtml) {
            $this->html = $rawHtml->body;
            $this->subject = $rawHtml->title;
        }

        return $this;
    }

    /**
     * Getter for the object
     *
     * @return $this
     */
    public function get()
    {
        return $this;
    }
}