<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductRequest extends Mailable
{
    use Queueable, SerializesModels, CustomEmailTrait;

    /**
     * Template name
     *
     * @var string
     */
    private $template = 'request';

    protected $adminTemplate = 'adminRequest';

}
