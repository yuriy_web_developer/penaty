<?php

namespace App\Mail;

use App\Models\EmailTemplate;
use App\Models\Settings;
use App\Modules\Notification\EmailNotification;

trait CustomEmailTrait
{
    /**
     * @var EmailNotification
     */
    public $email;

    /**
     * @var EmailNotification
     */
    public $adminEmail;

    /**
     * Data to replace in shortcodes
     *
     * @var array
     */
    public $replace = [];

    /**
     * Custom Email constructor.
     * @param array $replace
     */
    public function __construct($replace = [])
    {
        $this->replace = $this->addPageUrl($replace);
        $this->email = $this->prepare($this->template);
        $this->adminEmail = $this->prepare($this->adminTemplate);
    }

    /**
     * Prepare template
     *
     * @param string $name
     * @return EmailNotification
     */
    public function prepare($name)
    {
        return (new EmailNotification($name, $this->replace))
            ->prepare()
            ->doShortCode()
            ->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->sendToAdmin();

        return $this->view('emails.custom', ['html' => $this->email->html])
                    ->subject($this->email->subject);
    }

    /**
     * Send notification to admin
     *
     */
    private function sendToAdmin()
    {
        if (!$this->adminTemplate) return;

        \Mail::send('emails.custom', ['html' => $this->adminEmail->html], function ($message) {
            $message->to((new Settings)->adminEmail());
            $message->subject($this->adminEmail->subject);
        });
    }


    /**
     * Add page url where request came from
     *
     * @param array $replace
     * @return array
     */
    private function addPageUrl($replace)
    {

        if (array_key_exists('page', $replace)) return $replace;

        $replace['page'] = array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '';

        return $replace;
    }
}