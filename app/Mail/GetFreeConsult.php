<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GetFreeConsult extends Mailable
{
    use Queueable, SerializesModels, CustomEmailTrait;

    /**
     * Template name
     *
     * @var string
     */
    private $template = 'consult';

    protected $adminTemplate = 'adminConsult';

}
