<?php

namespace App\Providers;

use App\Helpers\Breadcrumbs;
use App\Models\MainPage;
use App\Models\Settings;
use App\Models\Slider;
use App\Modules\Url\UrlPath;
use DB;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Helpers;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        View::share('contacts', Helpers::getContacts());
        View::share('interior', (new MainPage())->getInterior());
        View::share('menu', Helpers::getMenu());
        View::share('page', Helpers::getPage());
        View::share('slider', (new Slider()));
        View::share('url', (new UrlPath()));
        View::share('breadcrumbs', (new Breadcrumbs));
        View::share('settings', (new Settings));
        View::share('settings', (new Settings));
        //DB::enableQueryLog();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
