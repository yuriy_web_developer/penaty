<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'category_id',
        'page_id',
        'short_info',
        'basement',
        'instrument',
        'consumption',
        'layers',
        'dilution',
        'producer',
        'tab_description',
        'tab_instruction',
        'tab_video',
        'available',
        'published',
        'volume',
        'hit',
    ];

    protected $casts = ['volume' => 'array'];
    //current categories
    protected $categories_ids = [];

    //TODO extract relations to a trait

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id')->withDefault();
    }

    public function colors()
    {
        return $this
            ->belongsToMany(Photo::class, 'product_colors', 'product_id', 'photo_id')
            ->withPivot(['color_name'])
            ->withTimestamps();
    }

    public function photos()
    {
        return $this
            ->belongsToMany(Photo::class, 'product_photos', 'product_id', 'photo_id')
            ->withTimestamps();
    }

    public function category()
    {
        return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, 'order_product', 'product_id', 'order_id')
                    ->withPivot(['color_id', 'volume', 'quantity'])
                    ->withTimestamps();
    }


    public function delete()
    {
        $this->page()->delete();
        $this->colors()->sync([]);
        $this->photos()->sync([]);
        parent::delete();
    }

    /**
     * Build a query depending on request parameters
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */

    //TODO extract search method to a class

    public function getQuery()
    {
        $query = $this->newQuery();
        if ($this->getSortColumn()) {
            $query->orderBy($this->getSortColumn(), 'desc');
        }

        if (request()->exists('search_word')) {
            //add select as id column is overwritten
            $query->select("products.*")
                  ->join('pages', 'products.page_id', '=', 'pages.id')
                  ->where('pages.title', 'like', '%' . request()->get('search_word') . '%');
        }

        return $query;
    }

    /**
     * By what column in the database to sort results
     *
     * @return bool|string
     */
    private function getSortColumn()
    {
        $sort = request()->get('sort');

        if ($sort == '1') return 'created_at';
        if ($sort == '2') return 'updated_at';
        if ($sort == '3') return 'name';

        return false;
    }

    /**
     * Check if a product belong to the specified category
     * @param $category
     * @return bool
     */
    public function hasCategory($category)
    {
        if (is_object($category) && is_a($category, Category::class)) $category = $category->id;

        if (!$this->categories_ids) {
            $categories = $this->category()->get()->toArray();
            foreach ($categories as $one) $this->categories_ids[] = $one['id'];
        }

        if (array_search($category, $this->categories_ids) !== false)
            return true;

        return false;
    }

    /**
     * Check if product exists by id
     *
     * @param $id
     * @return bool
     */
    public static function exists($id)
    {
        return (new self)->find($id)->count() > 0 ? true : false;
    }

}
