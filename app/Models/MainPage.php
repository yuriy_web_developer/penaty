<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainPage extends Model
{
    protected $table = 'main_page';

    protected $fillable = ['meta_key', 'meta_value'];

    protected $casts = ['meta_value' => 'json'];


    /**
     *  Get data for the main page by key
     *
     * @param string $key
     * @return mixed
     */
    public function getMetaByKey($key)
    {
        return $this
            ->where(['meta_key' => (string)$key])
            ->first();
    }

    /**
     * Get only values by key
     *
     * @param string $key
     * @return array
     */
    public function getArrayMetaByKey($key)
    {
        $meta = $this->getMetaByKey($key);

        return $meta['meta_value'];
    }


    /**
     * Convert new lines to br
     *
     * @param array $items
     * @return array $contacts
     */
    public function newLinesToBr(array $items)
    {
        foreach ($items as $key => $item) {
            $items[$key] = nl2br($item);
        }

        return $items;
    }

    /**
     * Convert br to new lines
     *
     * @param array $items
     * @return array $contacts
     */
    public function brToNewLines(array $items)
    {
        foreach ($items as $key => $item) {
            $items[$key] = str_replace('<br />', '\r\n', $item);
        }

        return $items;
    }


    public function getInterior()
    {
        $interior = $this->getArrayMetaByKey('interior');
        foreach ($interior['photo_ids'] as $key => $photo) {
            $interior['photo_ids'][$key] = Photo::find($photo);
        }

        return $interior;
    }


}
