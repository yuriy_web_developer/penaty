<?php

namespace App\Models;

use Illuminate\Support\Collection;
use App\Modules\Catalog\FilterFields;
use Illuminate\Database\Eloquent\Model;

class FilterField extends Model
{
    protected $table = 'filter_field';

    protected $fillable = ['filter_id', 'name', 'photo_id'];


    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_filter_field', 'filter_field_id', 'product_id');
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public function filter()
    {
        return $this->belongsTo(ProductFilter::class);
    }

    /**
     * Make a link for a filter field
     *
     * @param Category $category
     * @param array $exclude
     * @param array $include
     * @return string
     */
    public function makeLink(Category $category, $exclude = [], $include = [])
    {
        if ($this->requestHasField()) $exclude[] = $this->id;

        $ids = (new FilterFields)->idsForLink($this->id, $exclude, $include);

        return '/' . Category::$slug . '/filter/?field_ids='
            . implode(',', $ids) . '&category_id=' . $category->id;
    }

    /**
     * Make link without field ids
     *
     * @return string
     */
    public function makeRawLink()
    {
        return '/' . Category::$slug . '/filter/?category_id=' . request('category_id');
    }

    /**
     * Check if field exists in the request
     *
     * @return bool
     */
    public function requestHasField()
    {
        return (array_search($this->id, $this->getFields()) !== false);
    }

    /**
     * Check if field exists in unique-per-filter list
     *
     * @param ProductFilter $filter
     * @return bool
     */
    public function requestHasUniqueField(ProductFilter $filter)
    {

        $uniqueId = (new FilterFields)->uniqueFieldsPerFilter($filter);

        return $this->id == $uniqueId;
    }

    /**
     * Get array with current filter field ids
     *
     * @return array
     */
    public function getFields()
    {
        return (new FilterFields)->getFields();
    }

    /**
     * Get array with current filter field ids
     * and exclude the specified ids
     *
     * @param Collection $exclude
     * @return array
     */
    public function getFieldsWithExclude(Collection $exclude)
    {
        $exclude = $exclude->pluck('id')->toArray();

        return (new FilterFields)->idsForLink('', $exclude);
    }

}
