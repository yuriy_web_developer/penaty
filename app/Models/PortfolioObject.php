<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioObject extends Model
{
    protected $table = 'portfolio';

    protected $fillable = ['page_id'];

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id');
    }

    public function photo()
    {
        return $this
            ->belongsToMany(Photo::class, 'portfolio_photo', 'portfolio_id', 'photo_id')
            ->orderBy('portfolio_photo.id')
            ->withTimestamps();
    }

    public function get($slug)
    {
        $page = Page::where(['type' => 'portfolio', 'url' => $slug])->first();

        return $page ? $page->portfolio : false;
    }

    /**
     * If portfolio object exists
     *
     * @param string $slug
     * @return bool
     */
    public function exists($slug)
    {
        $object = Page::where(['type' => 'portfolio', 'url' => $slug])->get();

        return $object->isNotEmpty();
    }

    /**
     * Get next portfolio object
     *
     * @return PortfolioObject
     */
    public function next()
    {
        $next = $this
            ->where('id', '>', $this->id)
            ->orderBy('id', 'asc')
            ->limit(1)
            ->get();

        if ($next->isEmpty()) $next = $this->first();

        return $next;
    }

    /**
     * Get previous portfolio object
     *
     * @return PortfolioObject
     */
    public function prev()
    {
        $prev = $this
            ->where('id', '<', $this->id)
            ->orderBy('id', 'desc')
            ->limit(1)
            ->get();

        if ($prev->isEmpty())
            $prev = $this->orderBy('id', 'desc')
                         ->limit(1)
                         ->get();
        return $prev;
    }

}
