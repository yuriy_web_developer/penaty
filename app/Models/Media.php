<?php

namespace App\Models;

use App\Modules\Media\MediaUploader;
use File;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    protected $table = 'media';

    protected $fillable = ['path', 'name', 'uploaded_by'];

    /**
     * All accepted extensions
     *
     * @var array
     */
    protected $ext = ['jpg', 'jpeg', 'png', 'bmp', 'pdf', 'doc', 'docx'];

    /**
     * Allowed image extensions
     *
     * @var array
     */
    protected $imageExt = ['jpg', 'jpeg', 'png', 'bmp',];

    /**
     * Check if media is a photo
     *
     * @param $ext string
     * @return bool
     */
    public function isPhoto($ext)
    {
        return array_search($ext, $this->imageExt) !== false;
    }

    /**
     * Extension is allowed to upload
     *
     * @param $ext
     * @return bool
     */
    public function isAllowed($ext)
    {
        return array_search($ext, $this->ext) !== false;
    }

    /**
     * Delete media
     */
    public function delete()
    {
        File::delete([
            $this->path
        ]);

        parent::delete();
    }

}
