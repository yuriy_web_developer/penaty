<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    protected $table = 'product_colors';

    protected $fillable = ['photo_id', 'color_name', 'product_id'];

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function products()
    {
        return $this
            ->belongsToMany(Product::class, 'product_colors', 'product_id', 'id')
            ->withTimestamps();
    }


}
