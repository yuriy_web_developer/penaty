<?php

namespace App\Models;

use App\Modules\Product\ProductFeature;
use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    protected $table = 'features';

    protected $fillable = ['name', 'default_value'];

    public function category()
    {
        return $this
            ->belongsToMany(Category::class, 'feature_category', 'feature_id', 'category_id')
            ->withTimestamps();
    }

    public function product()
    {
        return $this
            ->belongsToMany(Product::class, 'feature_product', 'feature_id', 'feature_id')
            ->withTimestamps();
    }

    /**
     * Check if product category has a feature
     *
     * @param Category $category
     * @return bool
     */
    public function hasCategory(Category $category)
    {
        $found = $this->category->search(function ($item) use ($category) {
            return $item->id == $category->id;
        });

        return ($found === false) ? false : true;
    }


    /**
     * Get product value
     *
     * @param Product $product
     * @param $id
     * @return string
     */
    public function productValue(Product $product, $id)
    {
        return (new ProductFeature($product))->value($id);
    }

}
