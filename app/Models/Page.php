<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'title',
        'keywords',
        'description',
        'url',
        'type',
        'published',
        'archived',
    ];

    protected $types = [
        'news'     => 'Новость',
        'info'     => 'Инф. страница',
        'main'     => 'Главная стр.',
        'product'  => 'Товар',
        'category' => 'Категория товара',
    ];

    /**
     * Pages protected from delete
     * @var array
     */
    //TODO add method that prevents delete

    protected $nonDelete = [
        74 => 'dizajn',
        71 => '/',
    ];


    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'id', 'page_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'id', 'page_id');
    }

    /**
     * Get a readable type
     *
     * @param null $name
     * @return bool|mixed
     */
    public function getTypeName($name = null)
    {
        if (!$name) $name = $this->type;

        if (array_key_exists($name, $this->types))
            return $this->types[$name];

        return false;
    }

    /**
     * Set type of the page
     *
     * @param null $type
     * @return bool
     */
    public function setType($type = null)
    {
        if (array_key_exists($type, $this->types)) {
            $this->type = $type;
            return true;
        }

        return false;
    }
}
