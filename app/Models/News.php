<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'url',
        'title',
        'keywords',
        'description',
        'short_content',
        'content',
        'order_num',
        'type',
        'published',
        'archived'
    ];

    protected $attributes = ['type' => 'news'];

    /**
     * Add global scope to select only news
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('type', 'news');
        });
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Get the latest news
     *
     * @param int $num
     * @return mixed
     */
    public function getLatest($num = 3)
    {
        return $this
            ->where('published', 1)
            ->orderBy('order_num', 'asc')
            ->limit($num)
            ->get();
    }


    /**
     * Get only dates from the given timestamp.
     *
     * @return bool|string
     */
    public function getOnlyDate()
    {
        $parsed = Carbon::parse($this->updated_at);
        if ($parsed) return $parsed->toDateString();
        return false;
    }
}
