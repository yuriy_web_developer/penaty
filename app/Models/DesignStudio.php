<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DesignStudio extends Model
{
    protected $table = 'design_studios';

    protected $fillable = ['title', 'url', 'photo_id'];

    static $slug = 'dizajn';


    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Get design studio page
     *
     * @return mixed null|Page
     */
    public function getPage()
    {
        return Page::where('url', self::$slug)->first();
    }
}
