<?php

namespace App\Models;

use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;


/**
 * Top menu in the header
 */
class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable = ['item_name', 'url', 'order_num', 'parent_id'];


    /**
     * get the last item in the menu
     *
     * @return mixed
     */
    public function getLastItem()
    {
        return $this->orderBy('order_num', 'desc')->first();
    }

    /**
     * Get top level menu
     *
     * @return mixed
     */
    public function getFirstLevel()
    {
        return Cache::remember('menuFirstLevel', config('cache.time'), function () {
            return $this->where(['parent_id' => 0])
                        ->orderBy('order_num', 'asc')
                        ->get();
        });
    }

    /**
     * Get menu children
     *
     * @param $id
     * @return mixed
     */
    public function getChildren($id)
    {
        return Cache::remember('menuChildren'.$id, config('cache.time'), function () use($id) {
            return $this->where(['parent_id' => $id])
                        ->orderBy('order_num', 'asc')
                        ->get();
        });

    }

    /**
     * Check if menu item has children
     *
     * @param $id
     * @return bool
     */
    public function hasChildren($id)
    {
        return !$this->getChildren($id)->isEmpty();
    }


}
