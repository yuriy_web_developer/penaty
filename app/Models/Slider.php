<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Photo;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $fillable = ['name', 'title', 'link', 'description', 'photo_id', 'order_num'];


    /**
     * Relations with Photo model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Get last slide
     *
     * @param $slider
     * @return mixed
     */
    public function getLastSlideNum($slider)
    {
        return $this
            ->where(['name' => $slider])
            ->orderBy('order_num', 'desc')
            ->limit(1)
            ->get();
    }

    /**
     * Get slides by slider name
     *
     * @param $name
     * @return mixed
     */
    public function getSlidesByName($name)
    {
        return $this
            ->where(['name' => $name])
            ->orderBy('order_num', 'asc')
            ->get();
    }
}
