<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShortCode extends Model
{
    protected $table = 'short_code';

    protected $fillable = ['code', 'replace'];
}
