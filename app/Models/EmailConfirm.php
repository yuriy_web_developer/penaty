<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailConfirm extends Model
{
    protected $table = 'subscribe_email_confirm';

    protected $fillable = ['email', 'hash', 'page', 'ip'];

}
