<?php

namespace App\Models;

use App\Modules\Catalog\FilterFields;
use App\Modules\Url\UrlPath;
use Illuminate\Database\Eloquent\Model;

class ProductFilter extends Model
{
    protected $table = 'filters';

    protected $fillable = ['name', 'type'];

    protected $types = ['select', 'slider', 'button'];

    public function category()
    {
        return $this->belongsToMany(Category::class, 'filter_category', 'filter_id', 'category_id');
    }

    public function fields()
    {
        return $this->hasMany(FilterField::class, 'filter_id', 'id');

    }

    /**
     * Check if filter is applied to a category
     *
     * @param Category $category
     * @return bool
     */
    public function hasCategory(Category $category)
    {
        return $this
            ->category()
            ->where('category_id', $category->id)
            ->get()
            ->isNotEmpty();
    }

    /**
     * Make link for reset button
     *
     * @return string
     */
    public function resetLink()
    {
        $category = Category::find(request('category_id'));

        if (!$category) return '/';

        return (new UrlPath)->makeCategoryUrl($category);
    }

    /**
     * Check if  reset button should be displayed
     *
     * @return bool
     */
    public function needResetBtn()
    {
        if (!empty(request('field_ids'))) return true;

        return false;
    }

    /**
     * Check if filter has fields
     *
     * @return bool
     */
    public function hasFields()
    {
        return $this->fields->isNotEmpty();
    }

    /**
     * Get filters that belong to categories
     *
     * @param $categories array|Category
     * @return mixed
     */
    public function getCategoryFilters($categories)
    {
        //normalize input
        if (!is_array($categories)) $categories[] = $categories;
        //make one level array
        $categories = array_flatten($categories);

        $filters = [];

        foreach ($categories as $category) {

            if ($category instanceof Page) $category = $category->category;
            if (!$category instanceof Category) return false;
            $filters[] = $category->filter;
        }

        return array_flatten($filters);
    }

}
