<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class InfoPage extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'url',
        'title',
        'keywords',
        'description',
        'short_content',
        'content',
        'order_num',
        'type',
        'published',
        'archived'
    ];

    protected $attributes = ['type' => 'info'];

    /**
     * Add global scope to select only info pages
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('type', function (Builder $builder) {
            $builder->where('type', 'info');
        });
    }

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Build a query depending on request parameters
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        $query = $this->newQuery();
        if ($this->getSortColumn()) {
            $query->orderBy($this->getSortColumn(), 'desc');
        }

        if (request()->exists('search_word')) {
            $query->where('title', 'like', '%'.request('search_word').'%');
        }

        return $query;
    }

    /**
     * By what column in the database to sort results
     *
     * @return bool|string
     */
    private function getSortColumn()
    {
        $sort = request('sort');

        if ($sort == '1') return 'created_at';
        if ($sort == '2') return 'updated_at';
        if ($sort == '3') return 'title';

        return false;
    }

}
