<?php

namespace App\Models;

use DB;
use Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['parent_id', 'page_id', 'photo_id', 'order_num'];

    protected $parents = [];

    //catalog slug
    public static $slug = 'catalog';

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public function page()
    {
        return $this->hasOne(Page::class, 'id', 'page_id');
    }


    public function product()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id');
    }

    /**
     * Get category children
     *
     * @param Category $category
     * @return mixed
     */
    public function children(Category $category = null)
    {
        if (!$category) $category = $this;

        if (!Auth::guest()) {
            return $this
                ->where(['parent_id' => $category->id])
                ->orderBy('order_num', 'asc')
                ->get();
        }

        return Cache::remember('categoryChildren' . $category->id, config('cache.time'),
            function () use ($category) {
                return $this
                    ->where(['parent_id' => $category->id])
                    ->orderBy('order_num', 'asc')
                    ->get();
            });
    }

    /**
     * Get all parents for a given category
     *
     * @param Category $category
     * @param bool $obj
     * @return array
     */
    public function parents(Category $category, $obj = false)
    {
        $this->parents = [];

        for ($i = 0; $i < 5; $i++) {
            $parent = $this->where(['id' => $category->parent_id])->first();

            if (!$parent) break;

            $category = $parent;

            if ($obj) {
                $this->parents[] = $parent->page;
                continue;
            }

            $this->parents[] = $parent->page->url;
        }

        return array_reverse($this->parents, false);
    }

    /**
     * Check if a cagegory has children
     *
     * @param Category $category
     * @return bool
     */
    public function hasChildren(Category $category = null)
    {
        return !$this->children($category)->isEmpty();
    }

    /**
     * Get top level categories
     *
     * @return mixed
     */
    public function firstLevel()
    {
        //disable cache for admin
        if (!Auth::check()) {
            return $this
                ->where(['parent_id' => 0])
                ->orderBy('order_num', 'asc')
                ->get();
        }

        return Cache::remember('categoryFirstLevel', config('cache.time'),
            function () {
                return $this
                    ->where(['parent_id' => 0])
                    ->orderBy('order_num', 'asc')
                    ->get();
            });
    }

    /**
     * Get distinct producers from the given category
     *
     * @param Category $category
     * @return array
     */
    public
    function getProducers(Category $category)
    {
        return DB::table('products')
                 ->select('products.producer')
                 ->join('product_categories', 'products.id', '=', 'product_categories.product_id')
                 ->where(['product_categories.category_id' => $category->id])
                 ->where('products.producer', '<>', '')
                 ->groupBy('products.producer')
                 ->orderBy('products.producer')
                 ->get()
                 ->toArray();

    }

    /**
     * Get products that belong to the category
     *
     * @param Category $category
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function whereProducer(Category $category)
    {
        if (request()->has('brands')) {
            return $category
                ->product()
                ->where('producer', request()->get('brands'))
                ->get();
        }

        return $category
            ->product()
            ->get();
    }

    /**
     * Get all products that belong to a given category
     *
     * @param Category $category
     * @return array|mixed $products
     */
    public function allCategoryProducts(Category $category)
    {
        $products = [];

        if ($this->hasChildren($category)) {
            foreach ($category->children($category) as $child) {
                if ($child->hasChildren($child)) {
                    foreach ($child->children($child) as $subchild) {
                        $products[] = $subchild->product;
                    }
                    continue;
                }
                $products[] = $child->product;
            }
        } else {
            $products[] = $category->product;
        }

        //dd($products);

        return array_flatten($products);

    }


}
