<?php

namespace App\Models;

use File;
use App\Modules\Url\UrlTransform;
use App\Modules\Photo\PhotoSearch;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

    protected $table = 'photos';

    protected $fillable = ['path', 'thumbnail_path', 'name', 'uploaded_by'];


    public function slider()
    {
        return $this->belongsTo(Slider::class);
    }

    public function info_header()
    {
        return $this->belongsTo(InfoPage::class);
    }

    public function products()
    {
        return $this
            ->belongsToMany(Product::class, 'product_photos', 'photo_id', 'product_id')
            ->withTimestamps();
    }


    public function delete()
    {
        File::delete([
            $this->path,
            $this->thumbnail,
        ]);

        parent::delete();
    }

    /**
     * Search a photo by path
     *
     * @param $path
     * @return mixed
     */
    public function getIdByPath($path)
    {
        if (!$path) return false;

        $photo = $this->where(['path'=>urldecode($path)])->first();
        return $photo ? $photo->id : false;
    }

    /**
     * Search a photo by id
     *
     * @param $id
     * @return mixed
     */
    public function getPathById($id)
    {
        if (!$id) return null;

        $photo = $this->find((int)$id);

        return $photo ? $photo->path : null;
    }

    /**
     * Parse image url from the visual editor
     *
     * @param $value
     * @return bool|string
     */
    public static function getImageUrl($value)
    {
        preg_match('/<img(.*?)src="(.*?)"(.*?)\/>/i', $value, $match);

        if (array_key_exists(2, $match)) {
            $url = trim($match[2], '/');
            $url = (new UrlTransform)->deleteQuery($url);

            return $url;
        }

        return false;
    }

    /**
     * Get a query for searching a photo
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function searchQuery()
    {
        return PhotoSearch::getQuery();
    }

    /**
     * Get photo id from the given html
     *
     * @param $html string
     * @return mixed
     */
    public function getIdFromHtml($html)
    {
        $url = self::getImageUrl($html);
        return $this->getIdByPath($url);
    }

}
