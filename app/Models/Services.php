<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';

    protected $fillable = ['name', 'title', 'description', 'link', 'photo_id', 'order_num'];

    //possible name for service names
    protected $names = ['main', 'design'];

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    /**
     * Set name for a service block
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        if (array_search($name, $this->names)!== false) $this->name = $name;

        return $this;
    }

    /**
     * Scope for services name
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $name
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNamed($query, $name)
    {
        if (array_search($name, $this->names)!== false)
            return $query->where('name', $name);

        return $query;
    }
}
