<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Callback extends Model
{
    protected $table = 'callbacks';

    protected $fillable = [
        'client_name',
        'phone',
        'email',
        'repair_type',
        'comment',
        'city',
    ];
}
