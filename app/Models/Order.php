<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'client_name',
        'phone',
        'email',
        'city',
        'comment',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'order_product', 'order_id', 'product_id')
                    ->withPivot(['color_id', 'volume', 'quantity'])
                    ->withTimestamps();
    }


    /**
     * Make an order
     *
     * @param $order_info
     * @param $products
     */
    public function make($order_info, $products)
    {
        $order = $this->create($order_info);

        //save order items
        foreach ($products as $product_info) {
            $product = Product::find($product_info['product_id']);

            if ($product->count() <= 0) continue;

            $color = $product->colors()
                             ->where('color_name', $product_info['color_id'])
                             ->first();

            $order->products()->attach($product_info['product_id'],
                [
                    'color_id' => $color->id,
                    'quantity' => $product_info['quantity'],
                    'volume'   => $product_info['volume'],
                ]);
        }
    }


    /**
     * Delete an order
     *
     */
    public function delete()
    {
        $this->products()->sync([]);
        parent::delete();
    }
}
