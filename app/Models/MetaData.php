<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetaData extends Model
{
    protected $table = 'pages';

    protected $fillable = ['title', 'keywords', 'description'];

    /**
     * Build a query depending on request parameters
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        $query = $this->newQuery();
        if ($this->getSortColumn()) {
            $query->orderBy($this->getSortColumn(), 'desc');
        }

        if (request()->exists('search_word')) {
            $query->where('title', 'like', '%'.request('search_word').'%');
        }

        return $query;
    }

    /**
     * By what column in the database to sort results
     *
     * @return bool|string
     */
    private function getSortColumn()
    {
        $sort = request('sort');

        if ($sort == '1') return 'created_at';
        if ($sort == '2') return 'updated_at';
        if ($sort == '3') return 'title';

        return false;
    }

}
