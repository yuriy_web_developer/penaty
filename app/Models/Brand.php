<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brands';

    protected $fillable = ['photo_id', 'link', 'title'];

    public function photo()
    {
        return $this->hasOne(Photo::class, 'id', 'photo_id');
    }

    public function getOrdered()
    {
        return $this->orderBy('order_num')->get();
    }

}
