<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $table = 'subscribers';

    protected $fillable = ['email', 'phone'];

    /**
     * Build a query depending on request parameters
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        $query = $this->newQuery();

        if ($this->getSortColumn()) {
            $query->orderBy($this->getSortColumn(), 'desc');
        }

        if (request()->exists('search_word')) {
            $query->where(function ($query) {
                $query->where('email', 'like', '%'.request('search_word').'%')
                    ->orWhere('phone', 'like', '%'.request('search_word').'%');
            });
        }

        return $query;
    }

    /**
     * By what column in the database to sort results
     *
     * @return bool|string
     */
    private function getSortColumn()
    {
        $sort = request('sort');

        if ($sort == '1') return 'created_at';
        if ($sort == '2') return 'phone';
        if ($sort == '3') return 'email';

        return 'created_at';
    }

}
