<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = 'emails';

    protected $fillable = ['name', 'body', 'title'];

    /**
     * Shortcode that can be used in tinymce.
     * Syntax - |||variable|||
     *
     * @var array
     */
    public $shortCode = [
        'subscribe'      => ['page', 'email', 'phone'],
        'adminSubscribe' => ['page', 'email', 'phone', 'ip'],
        'consult'        => ['name', 'email', 'phone', 'comment', 'page'],
        'adminConsult'   => ['name', 'email', 'phone', 'comment', 'page'],
        'order'          => ['name', 'email', 'phone', 'comment', 'city', 'page', 'order'],
        'adminOrder'     => ['name', 'email', 'phone', 'comment', 'city', 'page', 'order'],
        'request'        => ['name', 'email', 'phone', 'comment', 'city', 'page', 'order'],
        'adminRequest'   => ['name', 'email', 'phone', 'comment', 'city', 'page', 'order'],
        'confirm'        => ['link'],
    ];
}