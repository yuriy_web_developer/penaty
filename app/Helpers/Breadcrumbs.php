<?php

namespace App\Helpers;


use App\Models\Page;

class Breadcrumbs
{
    public function make(Page $page)
    {
        $parents = [];
        $category = $page->category;

        if ($page->url == 'catalog') return [];
        if ($page->type == 'product') $category = $page->product->category->first();

        if($category) $parents = $category->parents($category, true);

        if ($page->type == 'product') $parents[] = $category->page;

        $parents[] = $page;

        return $parents;
    }
}