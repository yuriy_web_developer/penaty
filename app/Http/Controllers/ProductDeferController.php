<?php

namespace App\Http\Controllers;


use App\Models\Order;
use App\Models\Page;
use App\Models\Product;

class ProductDeferController extends Controller
{
    public function getDeferred()
    {
        return $this->deleteDuplicate();
    }

    public function addDeferred()
    {
        $data = request()->only(['color_id', 'quantity', 'volume', 'product_id']);
        session()->push('defer', $data);
        $this->deleteDuplicate();

        flash('Товар отложен. <br><a href="/defer"><b>Открыть отложенные товары</b></a>')->important();

        return back();
    }

    public function defer()
    {
        $deferred = $this->deleteDuplicate();

        foreach ($deferred as $index => $one) {
            $product = Product::find($one['product_id']);
            $deferred[$index]['product'] = $product;
            $deferred[$index]['color'] = $product->colors()
                ->where('photo_id', $one['color_id'])
                ->first();
        }

        $page = Page::where(['url' => 'defer'])->first();

        return view('client.catalog.defer', [
            'products' => $deferred,
            'message'  => 'Нет отложенный товаров',
            'page'     => $page,
        ]);
    }

    public function deleteDuplicate()
    {
        $products = [];

        foreach (session('defer', []) as $one) {
            $products[$one['product_id']] = $one;
        }

        return array_values($products);

    }

    public function deleteAll()
    {
        session()->forget('defer');

        return redirect('/defer');

    }

    public function sendForm()
    {
        $this->validate(request(), [
            'client_name' => 'required',
            'email'       => 'required|email',
            'phone'       => 'required',
            'city'        => 'required',
        ]);

        $products = $this->getDeferred();
        $order = request()->only(['client_name', 'email', 'phone', 'city', 'comment']);
        (new Order)->make($order, $products);

        $this->deleteAll();

        flash('С Вами скоро свяжутся наши специалисты');

        return back();
    }

    public function delete()
    {
        $id = request()->get('id');
        $products = [];

        foreach (session()->pull('defer', []) as $key => $one) {
            if ($key == $id) continue;
            $products[$key] = $one;
        }

        session(['defer' => $products]);

        return redirect()->action('ProductDeferController@defer');
    }

}

