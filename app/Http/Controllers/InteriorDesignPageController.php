<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\MainPage;

class InteriorDesignPageController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param MainPage $page
     * @return \Illuminate\Http\Response
     */
    public function edit(MainPage $page)
    {
        $interior = $page->getArrayMetaByKey('interiorDesign');
        $photo = new Photo;

        return view('admin.interior_design_page.edit', compact('interior', 'photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MainPage $page
     * @return \Illuminate\Http\Response
     */
    public function update(MainPage $page)
    {
        $data = $this->prepareInteriorData();

        //get meta value
        $interior = $page->getMetaByKey('interiorDesign');
        $interior->meta_value = $data;

        $interior->save();

        flash('Данные обновлены')->important();

        return redirect()->route('design_studio.index');
    }

    /**
     * Prepare data for interior design model
     *
     * @return array
     */
    private function prepareInteriorData()
    {
        $interior = request()->only([
            'photo_ids',
            'link',
            'inEffect',
            'outEffect',
            'delay',
        ]);

        return $interior;

    }
}
