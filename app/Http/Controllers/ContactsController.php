<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MainPage;

class ContactsController extends Controller
{
    /**
     * @param MainPage $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(MainPage $page)
    {
        $contacts = $page->getArrayMetaByKey('contacts');

        return view('admin.contacts.edit', compact('contacts'));
    }

    /**
     * Update contacts
     *
     * @param Request $req
     * @param MainPage $page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $req, MainPage $page)
    {
        $new_contacts = $req->only([
            'phone_header',
            'email_header',
            'address_header',
            'address_footer',
            'time_footer',
            'email_footer',
            'phone_footer'
        ]);

        $contacts = $page->getMetaByKey('contacts');
        $contacts->meta_value = $new_contacts;
        $contacts->save();

        flash('Данные обновлены')->important();

        return redirect()->route('contacts.edit');
    }

}
