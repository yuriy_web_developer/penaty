<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Modules\Catalog\SearchCatalog;
use App\Modules\Url\UrlPath;

class CatalogController extends Controller
{

    /**
     * Show catalog index
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('client.catalog.catalog', ['catalog' => (new Category)->firstLevel()]);
    }

    /**
     * Show first level categories
     *
     * @param $first string
     * @param SearchCatalog $search
     * @return \Illuminate\Http\Response
     */
    public function firstLevel($first, SearchCatalog $search)
    {
        $parent = $search->searchFirstLevel($first);

        //TODO remove page variable
        return view('client.catalog.category_level_2', [
            'category' => $parent->category, 'page' => $parent
        ]);
    }

    /**
     * Catalog second level
     *
     * @param $first string
     * @param $second string
     * @param SearchCatalog $search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function secondLevel($first, $second, SearchCatalog $search)
    {
        $category = $search->searchSecondLevel($first, $second);

        //TODO remove page variable
        return view('client.catalog.category_level_2', [
            'category' => $category, 'page' => $category->page
        ]);
    }

    /**
     * Catalog third level, many products
     *
     * @param $first string
     * @param $second string
     * @param $third string
     * @param SearchCatalog $search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function thirdLevel($first, $second, $third, SearchCatalog $search)
    {
        $category = $search->searchManyProducts($first, $second, $third);
        //TODO remove page variable
        return view('client.catalog.products', [
            'category' => $category, 'page' => $category->page
        ]);
    }

    /**
     * Show one product
     *
     * @param $first string
     * @param $second string
     * @param $third string
     * @param $product string
     * @param SearchCatalog $search
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProduct($first, $second, $third, $product, SearchCatalog $search)
    {
        $product = $search->searchProduct($first, $second, $third, $product);

        return view('client.catalog.product', [
            'product' => $product, 'page' => $product->page
        ]);
    }
}
