<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;
use App\Modules\Photo\PhotoSearch;
use App\Modules\Photo\PhotoUploader;
use Intervention\Image\Facades\Image;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::searchQuery()
                       ->paginate(PhotoSearch::$paginateNum)
                       ->appends(PhotoSearch::appendsData());

        request()->flash();

        return view('admin.photos.list', compact('photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.photos.create');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Photo $photo)
    {
        $img = Image::make($photo->path);
        $width = $img->width();
        $height = $img->height();

        return view('admin.photos.edit', compact('photo', 'width', 'height'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        $photo->fill($request->all());
        $photo->save();

        flash('Информация обновлена')->important();

        return redirect()->route('photos.edit', $photo->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        $photo->delete();

        flash('Фото удалено')->important();

        return redirect()->route('photos.index');
    }

    /**
     * Upload photos from ajax calls.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAjax()
    {
        $file = array_first(request()->file());

        return response()->json(PhotoUploader::fromAjax($file));
    }

    /**
     * Search photos with ajax request
     *
     * @return mixed
     */
    public function searchAjax()
    {
        return (new PhotoSearch)->fromAjax();
    }

    /**
     * Delete multiple photos by ids
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyMulti()
    {
        Photo::destroy(request()->ids);

        flash('Фото удалены')->important();

        return redirect()->route('photos.index');
    }


}
