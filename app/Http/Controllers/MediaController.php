<?php

namespace App\Http\Controllers;

use App\Models\Media;
use App\Modules\Media\MediaUploader;

class MediaController extends Controller
{
    /**
     *  Show all media files
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $media = Media::paginate(20);

        $lastPageNum = $media->currentPage() * $media->perPage() - (20 - 1);

        return view('admin.media.list', compact('media', 'lastPageNum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.photos.create');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $media = Media::findOrFail($id);

        $media->delete();

        flash('Файл удален')->important();

        return redirect()->route('media.index');
    }

    /**
     * Upload media from ajax request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAjax()
    {
        $file = array_first(request()->file());
        $uploaded = (new MediaUploader)->upload($file);

        return response()->json($uploaded);
    }


}
