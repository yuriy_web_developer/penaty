<?php

namespace App\Http\Controllers;

use App\Helpers;
use App\Models\Category;
use App\Models\Page;
use App\Models\Photo;
use App\Modules\Url\UrlTransform;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = (new Category)->firstLevel();

        return view('admin.category.list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;

        return view('admin.category.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Page $page
     * @param Photo $photo
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function store(Page $page, Photo $photo, Category $category)
    {
        $this->makeFriendlyUrl(request());

        $this->validate(request(),[
            'title'=>'required',
        ]);

        //TODO image logic refactor
        $page->setType('category');
        $photo_url = $photo->getImageUrl(request()->get('photo'));
        $page->photo_id = $photo->getIdByPath($photo_url);
        $page->fill(request()->all())->save();

        $photo_url = $photo->getImageUrl(request()->get('miniature'));
        $category->photo_id = $photo->getIdByPath($photo_url);
        $category->page_id = $page->id;

        $category->fill(request()->all())->save();

        flash('Категория создана')->important();

        return redirect()->route('category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('admin.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param Photo $photo
     * @return \Illuminate\Http\Response
     */
    public function update($id, Photo $photo)
    {
        $category = Category::find($id);

        $this->makeFriendlyUrl(request());

        $this->validate(request(),[
            'title'=>'required',
        ]);

        //TODO image logic refactor
        $page = $category->page;
        $page->setType('category');
        $photo_url = $photo->getImageUrl(request()->get('photo'));
        $page->photo_id = $photo->getIdByPath($photo_url);
        $page->fill(request()->all())->save();

        $photo_url = $photo->getImageUrl(request()->get('miniature'));
        $category->photo_id = $photo->getIdByPath($photo_url);
        $category->page_id = $page->id;

        $category->fill(request()->all())->save();

        flash('Категория обновлена')->important();

        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if ($category->hasChildren()){
            flash('Удалите сначала вложенные категории')->error();

            return redirect()->route('category.index');
        }

        $category->page()->delete();
        $category->delete();

        flash('Категория удалена')->important();

        return redirect()->route('category.index');

    }

    public function updateOrder()
    {
        $categories = Category::all();
        $counter = 1;

        if (request()->has('item_id')) {
            foreach (request()->item_id as $index => $value) {
                foreach ($categories as $category) {
                    if ($category->id == $index) {
                        $category->order_num = $counter;
                        $category->save();
                        $counter++;
                    }
                }
            }
        }

        flash('Порядок категорий обновлен')->important();

        return redirect()->route('category.index');
    }

    //TODO already extract to modules
    private function makeFriendlyUrl($req)
    {
        $url = (new UrlTransform)->makeFriendlyUrl($req->url, $req->title);

        $req->replace(array_merge($req->all(), ['url' => $url]));
    }
}
