<?php

namespace App\Http\Controllers;

use App\Models\PortfolioObject;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = PortfolioObject::orderBy('id', 'desc')->paginate(16);

        return view('client.portfolio.list', compact('objects'));
    }

    /**
     * Display the portfolio object
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $object = (new PortfolioObject)->get($slug);

        if(!$object) return redirect('/');

        $page = $object->page;

        return view('client.portfolio.object', compact('object', 'page'));
    }

}
