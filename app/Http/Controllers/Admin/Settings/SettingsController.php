<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings;

class SettingsController extends Controller
{

    /**
     * Show the form for editing a special offer
     *
     * @param Settings $settingObj
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Settings $settingObj)
    {
        $data = [];

        foreach ($settingObj->settings as $setting) {
            $value = Settings::where('name', $setting)->first();

            if (empty($value)) {
                Settings::create(['name' => $setting]);
                $data[$setting] = '';
                continue;
            }

            $data[$setting] = $value->value;
        }

        return view('admin.settings.general.edit', compact('data'));
    }

    /**
     * Update the special offer
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        foreach (request('settings') as $name => $value) {
            Settings::where('name', $name)->update(['value' => $value]);
        }

        flash('Данные обновлены')->important();

        return redirect()->route('settings.general.edit');

    }
}
