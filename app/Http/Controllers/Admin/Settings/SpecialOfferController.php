<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Models\Settings;

class SpecialOfferController extends Controller
{

    private $settingName = 'special_offer';

    /**
     * Show the form for editing a special offer
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        $offer = Settings::where('name', $this->settingName)->first();

        $offer = $offer ? $offer : Settings::create(['name' => $this->settingName]);

        return view('admin.settings.special_offer.edit', compact('offer'));
    }

    /**
     * Update the special offer
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        flash('Акция обновлена')->important();

        Settings::where('name', $this->settingName)
                     ->update(['value' => request()->get('content')]);

        return redirect()->route('settings.offer.edit');

    }


}
