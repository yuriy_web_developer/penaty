<?php

namespace App\Http\Controllers\Admin\Settings;

use App\Http\Controllers\Controller;
use App\Models\EmailTemplate;

class EmailTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emails = EmailTemplate::orderBy('id')->get();

        return view('admin.email.list', compact('emails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.email.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param EmailTemplate $email
     * @return \Illuminate\Http\Response
     */
    public function store(EmailTemplate $email)
    {
        $email
            ->fill(request()->all())
            ->save();

        flash('Шаблон создан')->important();

        return redirect()->route('email.index');
    }

    /**
     * Show the form for editing the email template
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $email = EmailTemplate::findOrFail($id);

        return view('admin.email.edit', compact('email'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $email = EmailTemplate::findOrFail($id);

        $email
            ->fill(request()->all())
            ->save();
        //todo make redirect module and move two line below to static method,
        //replace in all controllers the similar logic
        flash('Шаблон обновлен')->important();

        return redirect()->route('email.index');
    }

    /**
     * Remove the email template
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EmailTemplate::destroy($id);

        flash('Шаблон удален')->important();

        return redirect()->route('email.index');
    }
}
