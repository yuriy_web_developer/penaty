<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShortCode;
use Illuminate\Http\Request;

class ShortCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $codes = ShortCode::paginate(20);

        return view('admin.short_code.list', compact('codes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.short_code.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ShortCode $code
     * @return \Illuminate\Http\Response
     */
    public function store(ShortCode $code)
    {
        $this->validate(request(), ['code' => 'required|unique:short_code,code']);

        $code->fill(request()->all())->save();

        flash('Шорт-код создан')->important();

        return redirect()->route('short_code.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $code = ShortCode::findOrFail($id);

        return view('admin.short_code.edit', compact('code'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $code = ShortCode::findOrFail($id);

        $this->validate($request, ['code' => 'required|unique:short_code,code,' . $code->id]);

        $code->fill($request->all())->save();

        flash('Шорт-код обновлен')->important();

        return redirect()->route('short_code.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ShortCode::destroy($id);

        flash('Шорт-код удален')->important();

        return redirect()->route('short_code.index');
    }
}
