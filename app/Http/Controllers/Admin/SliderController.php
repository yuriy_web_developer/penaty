<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Photo;

class SliderController extends Controller
{
    //all sliders
    protected $names = ['main', 'interior', 'about_us'];
    //name of the current slider
    protected $current;


    /**
     * Set the default slider
     */
    public function __construct()
    {
        $this->setSliderName();
    }

    /**
     * Display a listing of slides.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slider::where(['name' => $this->current])
            ->orderBy('order_num', 'asc')
            ->get();

        return view('admin.sliders.list', [
            'name'   => $this->current,
            'slides' => $slides,
        ]);
    }

    /**
     * Show the form for creating the slide.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create', [
            'name' => $this->current,
        ]);
    }

    /**
     * Store a newly created slide
     *
     * @param Slider $slider
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function store(Slider $slider)
    {
        $this->validateSlide();
        $this->fillSlideWithData($slider)->save();

        flash('Слайд удачно создан')->important();

        return redirect()->route('slider.index', ['slider' => $this->current]);
    }

    /**
     * Show the form for editing the slide
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slider::findOrFail($id);

        return view('admin.sliders.edit', [
            'name'  => $this->current,
            'slide' => $slide,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validateSlide();

        $slide = Slider::findOrFail($id);

        $this->fillSlideWithData($slide)->save();

        flash('Слайд удачно обновлен')->important();

        return redirect()->route('slider.index', ['slider' => $this->current]);
    }

    /**
     * Remove the slide
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Slider::destroy($id);

        flash('Слайд удален')->important();

        return redirect()->route('slider.index', ['slider' => $this->current]);
    }


    /**
     * Set the name of the current slider
     *
     * @param null $name
     */
    public function setSliderName($name = null)
    {
        $slider = $name ? $name : request('slider');

        if (array_search($slider, $this->names)) {
            $this->current = $slider;
            return;
        }

        $this->current = $this->names[0];
    }

    /**
     * Parse image url from the visual editor
     *
     *
     * @return bool
     */

    //TODO remove this method.
    private function getImageUrl()
    {
        preg_match('/<img(.*?)src="(.*?)"(.*?)\/>/i', request()->get('image'), $match);
        if (array_key_exists(2, $match)) return trim($match[2], '/');

        return false;
    }

    /**
     * Perform validation of request data
     */
    private function validateSlide()
    {
        $this->validate(request(), [
            'title' => 'required',
            'link'  => 'required',
            'image' => 'required',
        ]);
    }

    /**
     * Fill Slider model with data
     *
     * @param Slider $slide
     * @return Slider
     */
    private function fillSlideWithData(Slider $slide)
    {
        $slide->name = $this->current;
        $photo_path = Photo::getImageUrl(request()->get('image'));
        $slide->photo_id = (new Photo)->getIdByPath($photo_path);

        $slide->fill(request()->all());

        return $slide;
    }

    public function updateOrder(Slider $slider)
    {
        $slides = $slider->getSlidesByName($this->current);

        foreach ($slides as $index => $slide) {
            $new_position = array_search($slide->id, request('slide_ids'));
            $new_position += 1;

            if (!$new_position) continue;

            $slide->order_num = $new_position;
            $slide->save();
        }

        flash('Порядок изменен')->important();

        return redirect()->route('slider.index', ['slider' => $this->current]);
    }


}
