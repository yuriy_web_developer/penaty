<?php

namespace App\Http\Controllers\Admin;

use App\Models\Page;
use App\Models\PortfolioObject;
use App\Modules\Url\UrlTransform;
use App\Http\Controllers\Controller;

class PortfolioAdminController extends Controller
{
    /**
     * Display a listing objects.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objects = PortfolioObject::orderBy('id', 'desc')->paginate(20);

        return view('admin.portfolio.list', compact('objects'));
    }

    /**
     * Show the form for creating portfolio object
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.portfolio.create');
    }

    /**
     * Store portfolio
     *
     * @param Page $page
     * @param PortfolioObject $object
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function store(Page $page, PortfolioObject $object)
    {
        if (!$this->handleData($page, $object)) {
            flash('Объект существует')->important();

            return redirect()->back()->withInput(request()->all());
        };

        flash('Объект создан')->important();

        return redirect()->route('portfolio.index');
    }

    /**
     * Show the form for editing the portfolio object
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $object = PortfolioObject::findOrFail($id);

        return view('admin.portfolio.edit', compact('object'));
    }

    /**
     * Update the portfolio object
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function update($id)
    {
        $object = PortfolioObject::findOrFail($id);
        $page = $object->page;

        if (!$this->handleData($page, $object)) {
            flash('Объект существует')->important();

            return redirect()->back()->withInput(request()->all());
        }

        flash('Объект обновлен')->important();

        return redirect()->route('portfolio.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $object = PortfolioObject::findOrFail($id);
        $object->photo()->detach();
        $object->delete();

        flash('Объект удален')->important();

        return redirect()->route('portfolio.index');
    }

    /**
     * Persists data from request
     *
     * @param Page $page
     * @param PortfolioObject $object
     * @return PortfolioObject | bool
     */
    private function handleData(Page $page, PortfolioObject $object)
    {
        $this->validate(request(), [
            'url' => 'required',
        ]);

        $url = (new UrlTransform)->makeFriendlyUrl(request('url'), request('title'));

        //check if object exists
        if (!$page->url && $object->exists($url)) {
            return false;
        }

        //save page
        $page->setType('portfolio');
        $page->fill(request()->all())
             ->fill(['url' => $url])
             ->save();

        //save portfolio
        $object->fill(['page_id' => $page->id])->save();

        //attach photos
        if (request()->has('photo_ids')) {
            $object->photo()->detach();
            $object->photo()->attach(request('photo_ids'));
        }

        return $object;
    }

}
