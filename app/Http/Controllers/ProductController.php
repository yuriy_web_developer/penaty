<?php

namespace App\Http\Controllers;


use App\Helpers;
use App\Models\Category;
use App\Models\Product;
use App\Models\Page;
use App\Modules\Url\UrlTransform;

class ProductController extends Controller
{

    protected $perPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = (new Product())
            ->getQuery()
            ->paginate($this->perPage)
            ->appends(
                ['search_word' => request('search_word'),
                 'sort'        => request('sort')
                ]
            );

        $lastPageNum = $products->currentPage() * $products->perPage() - ($this->perPage - 1);

        return view('admin.product.list', compact('products', 'lastPageNum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;
        return view('admin.product.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Page $page
     * @param Product $product
     * @return \Illuminate\Http\Response
     */
    public function store(Page $page, Product $product)
    {   //validate
        //save page info
        //save product
        //save colors
        //save photo ids

        $this->makeFriendlyUrl(request());

        $this->validate(request(), [
            'title' => 'required',
            'url'   => 'required',
        ]);

        $page->setType('product');
        $page->fill(request()->all())->save();

        $product
            ->fill(array_add(request()->all(), 'page_id', $page->id))
            ->save();

        $product->colors()->attach(request()->get('colors'));
        $product->photos()->attach(request()->get('photos'));
        $product->category()->sync(request()->get('categories'));

        flash('Товар создан')->important();

        return redirect()->route('product.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $category = new Category;

        return view('admin.product.edit', compact('product', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->makeFriendlyUrl(request());

        $this->validate(request(), [
            'title' => 'required',
            'url'   => 'required',
        ]);

        $product = Product::findOrFail($id);
        $page = $product->page;

        $page->setType('product');
        $page->fill(request()->all());
        $page->save();

        $product
            ->fill(array_add(request()->all(), 'page_id', $page->id))
            ->save();

        //use attach/detach instead of sync to preserve the order
        if (request()->has('photos')) {
            $product->photos()->detach();
            foreach (request()->get('photos') as $photo)
                $product->photos()->attach($photo);
        }

        $product->colors()->detach();
        $product->colors()->attach(request()->get('colors'));

        $product->category()->sync(request()->get('categories'));

        flash('Товар обновлен')->important();

        return redirect()->route('product.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();

        flash('Товар удален')->important();

        return redirect()->route('product.index');
    }

    private function makeFriendlyUrl($req)
    {
        $url = (new UrlTransform)->makeFriendlyUrl($req->url, $req->title);

        $req->replace(array_merge($req->all(), ['url' => $url]));
    }
}
