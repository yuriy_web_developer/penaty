<?php

namespace App\Http\Controllers;

use App\Helpers;
use App\Models\News;
use App\Models\Page;
use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class NewsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('order_num', 'asc')->get();
        return view('admin.news.list', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->fillNewsWithData(new News)->save();

        return $this->redirectSuccessMsg('Новость создана');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $news = News::findOrFail($id);

        $this->fillNewsWithData($news)->save();

        return $this->redirectSuccessMsg('Новость обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::destroy($id);

        return $this->redirectSuccessMsg('Новость удалена');
    }

    /**
     * Change the order of news
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateOrder(Request $request)
    {
        $items = News::all();

        foreach ($items as $item) {
            $new_position = array_search($item->id, $request->item_ids);
            $new_position += 1;

            if (!$new_position) continue;

            $item->order_num = $new_position;
            $item->save();
        }

        return $this->redirectSuccessMsg('Порядок изменен');
    }


    /**
     * Clone the specified news.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function copy($id)
    {
        $news = News::findOrFail($id);

        if (!$news) {
            flash('Новость не найдена')->error();
            return redirect()->route('news.index');
        }

        $news = $news->replicate();
        $news->title = 'Копия ' . $news->title;
        $news->url = 'copy-' . $news->url;
        $news->published = 0;
        $news->save();

        return $this->redirectSuccessMsg('Копия новости создана');
    }


    /**
     * Redirect with a success message.
     *
     * @param $msg
     * @param string $route
     * @param array $param
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectSuccessMsg($msg, $route = 'news.index', $param = [])
    {
        flash($msg)->important();

        return redirect()->route($route, $param);
    }

    /**
     * Make friendly url and replace its value in request
     *
     * @param Request $req
     */
    private function makeFriendlyUrl(Request $req)
    {
        $url = Helpers::transliterate($req->url);

        if (!$url) $url = Helpers::transliterate($req->title);

        $req->replace(array_merge($req->all(), ['url' => $url]));
    }

    /**
     * Validate and fill news with request data
     *
     * @param News $news
     * @return News $news
     */
    private function fillNewsWithData(News $news)
    {
        $this->makeFriendlyUrl(request());

        $this->validate(request(), [
            'title' => 'required',
            'url'   => ['required',
                Rule::unique('pages')->ignore($news->id),
            ]
        ]);

        $news->fill(request()->all());
        $news->photo_id = (new Photo)->getIdByPath($this->getImageUrl());

        return $news;
    }

    /**
     * Parse image url from the visual editor
     *
     * @return bool
     */
    private function getImageUrl()
    {
        preg_match('/<img(.*?)src="(.*?)"(.*?)\/>/i', request()->get('photo'), $match);
        if (array_key_exists(2, $match)) return trim($match[2], '/');

        return false;
    }

    public function showForClient()
    {
        $news = (new News)->getLatest(20);
        $page = Page::where(['url' => 'news'])->first();

        return view('news_list', compact('news', 'page'));
    }

    public function showOneForClient($news)
    {
        $page = Page::where(['url' => $news])->first();

        return view('client.page.info', compact('page'));
    }

}
