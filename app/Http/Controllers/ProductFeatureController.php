<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Feature;

class ProductFeatureController extends Controller
{
    /**
     * Display a listing of all features.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $features = Feature::all();

        return view('admin.feature.list', compact('features'));
    }

    /**
     * Show the form for creating a feature.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;
        return view('admin.feature.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Feature $feature
     * @return \Illuminate\Http\Response
     */
    public function store(Feature $feature)
    {
        $this->validate(request(), ['name' => 'required']);

        $feature->fill(request()->all())->save();
        $feature->category()->sync(request()->get('categories'));

        flash('Характеристика создана')->important();

        return redirect()->route('feature.index');
    }

    /**
     * Show the form for editing the feature.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $feature = Feature::findOrFail($id);
        $category = new Category;

        return view('admin.feature.edit', compact('feature', 'category'));
    }

    /**
     * Update the specified feature
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), ['name' => 'required']);

        $feature = Feature::findOrFail($id);
        $feature->fill(request()->all())->save();
        $feature->category()->sync(request()->get('categories'));

        flash('Характеристика обновлена')->important();

        return redirect()->route('feature.index');
    }

    /**
     * Remove the specified feature.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $feature = Feature::findOrFail($id);
        $feature->product()->sync([]);
        $feature->category()->sync([]);
        $feature->delete();

        flash('Характеристика удалена')->important();

        return redirect()->route('feature.index');
    }
}
