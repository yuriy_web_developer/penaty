<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Photo;
use App\Models\DesignStudio;

class DesignStudioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blocks = DesignStudio::all();

        return view('admin.design_studio.list', compact('blocks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.design_studio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param DesignStudio $block
     * @return \Illuminate\Http\Response
     */
    public function store(DesignStudio $block)
    {
        $this->validate(request(), [
            'title' => 'required',
            'url'   => 'required',
        ]);

        $photo_id = (new Photo)->getIdFromHtml(request()->get('photo'));

        $block->create(['photo_id' => $photo_id,
                        'title'    => request()->get('title'),
                        'url'      => request()->get('url'),
        ]);

        flash('Блок создан')->important();

        return redirect()->route('design_studio.index');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $block = DesignStudio::find($id);

        return view('admin.design_studio.edit', compact('block'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'title' => 'required',
            'url'   => 'required',
        ]);

        $photo_id = (new Photo)->getIdFromHtml(request()->get('photo'));

        $block = DesignStudio::find($id);

        $block->fill(['photo_id' => $photo_id,
                      'title'    => request()->get('title'),
                      'url'      => request()->get('url'),
        ]);
        $block->save();

        flash('Блок обновлен')->important();

        return redirect()->route('design_studio.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DesignStudio::destroy($id);

        flash('Блок удален')->important();

        return redirect()->route('design_studio.index');
    }

    /**
     * Show design studio page
     *
     * @return mixed null|Page
     */
    public function showPage()
    {
        $blocks = DesignStudio::all();

        return view('client.page.design_studio', compact('blocks'));
    }
}

