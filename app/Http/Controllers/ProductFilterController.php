<?php

namespace App\Http\Controllers;


use App\Models\Category;
use App\Models\FilterField;
use App\Models\ProductFilter;

class ProductFilterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filters = ProductFilter::all();

        return view('admin.filter.list', compact('filters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = (new Category)->firstLevel();

        return view('admin.filter.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductFilter $filter
     * @return \Illuminate\Http\Response
     */
    public function store(ProductFilter $filter)
    {
        $filter->fill(request()->all())->save();

        $filter->category()->sync(request('category'));

        flash('Фильтр создан')->important();

        return redirect()->route('filter.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $filter = ProductFilter::findOrFail($id);
        $categories = (new Category)->firstLevel();
        $fields = $filter->fields;

        return view('admin.filter.edit', compact('filter', 'categories', 'fields'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $filter = ProductFilter::findOrFail($id);

        $filter->fill(request()->all())->save();

        $filter->category()->sync(request('category'));

        if (request()->has('fields')) {
            FilterField::whereNotIn('id', request('fields'))->delete();
        }

        if (request()->has('new_fields')) {
            foreach (request('new_fields') as $name) {
                FilterField::create(['filter_id' => $filter->id, 'name' => $name]);
            }
        }

        flash('Фильтр обновлен')->important();

        return redirect()->route('filter.edit', $filter->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ProductFilter::destroy($id);
        FilterField::where('filter_id', $id)->delete();

        flash('Фильтр удален')->important();

        return redirect()->route('filter.index');
    }
}
