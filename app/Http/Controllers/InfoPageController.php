<?php

namespace App\Http\Controllers;

use App\Helpers;
use App\Models\InfoPage;
use App\Models\News;
use App\Models\Page;
use App\Models\Photo;
use App\Modules\Url\UrlTransform;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class InfoPageController extends Controller
{
    protected $perPage = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = (new InfoPage)
            ->getQuery()
            ->paginate($this->perPage)
            ->appends(
                ['search_word' => request('search_word'),
                 'sort'        => request('sort')
                ]
            );

        $lastPageNum = $pages->currentPage() * $pages->perPage() - ($this->perPage - 1);
        $req = request();

        return view('admin.info_page.list', compact('pages', 'lastPageNum', 'req'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.info_page.create');
    }

    /**
     * Store an info page
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->fillPageWithData(new InfoPage)->save();

        return $this->redirectSuccessMsg('Страница создана');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = InfoPage::findOrFail($id);

        return view('admin.info_page.edit', compact('page'));
    }

    /**
     * Update the page.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $page = InfoPage::findOrFail($id);

        $this->fillPageWithData($page)->save();

        return $this->redirectSuccessMsg('Страница обновлена');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InfoPage::destroy($id);

        return $this->redirectSuccessMsg('Страница удалена');
    }

    /**
     * Redirect with a success message.
     *
     * @param $msg
     * @param string $route
     * @param array $param
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectSuccessMsg($msg, $route = 'info_page.index', $param = [])
    {
        flash($msg)->important();

        return redirect()->route($route, $param);
    }

    /**
     * Make friendly url and replace its value in request
     *
     * @param Request $req
     */
    private function makeFriendlyUrl(Request $req)
    {
        $url = (new UrlTransform())->makeFriendlyUrl($req->url, $req->title);

        $req->replace(array_merge($req->all(), ['url' => $url]));
    }

    /**
     * Validate and fill news with request data
     *
     * @param InfoPage $page
     * @return InfoPage $page
     */
    private function fillPageWithData(InfoPage $page)
    {
        $this->makeFriendlyUrl(request());

        $this->validate(request(), [
            'title' => 'required',
            'url'   => ['required',
                Rule::unique('pages')->ignore($page->id),
            ]
        ]);

        $page->fill(request()->all());

        $page->photo_id = (new Photo)->getIdByPath($this->getImageUrl());

        return $page;
    }


    /**
     * Parse image url from the visual editor
     *
     * @return bool
     */
    private function getImageUrl()
    {
        preg_match('/<img(.*?)src="(.*?)"(.*?)\/>/i', request()->get('photo'), $match);
        if (array_key_exists(2, $match)) return trim($match[2], '/');

        return false;
    }

}
