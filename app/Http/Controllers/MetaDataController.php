<?php

namespace App\Http\Controllers;

use App\Models\MetaData;

class MetaDataController extends Controller
{

    protected $perPage = '20';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = (new MetaData)
            ->getQuery()
            ->paginate($this->perPage)
            ->appends(
                ['search_word' => request('search_word'),
                 'sort'        => request('sort')
                ]
            );

        $lastPageNum = $pages->currentPage() * $pages->perPage() - ($this->perPage - 1);
        $req = request();

        return view('admin.meta_data.list', compact('pages', 'lastPageNum', 'req'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = MetaData::findOrFail($id);

        return view('admin.meta_data.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id Page id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $page = MetaData::findOrFail($id);
        $page->fill(request()->all());
        $page->save();

        flash('Метаданные обновлены')->important();

        return redirect()->route('meta_data.index');
    }

}
