<?php

namespace App\Http\Controllers;

use App\Models\FilterField;
use App\Models\Photo;
use App\Models\ProductFilter;

class ProductFilterFieldController extends Controller
{

    /**
     * Show a listing of filter fields
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fields = (new FilterField)->paginate(20);

        return view('admin.filter_field.list', compact('fields'));
    }

    /**
     * Show a form for creating a new filter field
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $filters = ProductFilter::all();

        return view('admin.filter_field.create', compact('filters'));
    }


    /**
     * Save the new filter field
     *
     * @param FilterField $field
     * @return \Illuminate\Http\Response
     */
    public function store(FilterField $field)
    {
        $this->validate(request(), [
            'name' => 'required',
        ]);

        $field->photo_id = (new Photo)->getIdFromHtml(request('photo'));
        $field->fill(request()->all());
        $field->save();

        flash('Пункт в фильтре создан')->important();

        return redirect()->route('filter_field.index');
    }

    /**
     * Show the form for editing the filter field
     *
     * @param $id int
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $field = FilterField::findOrFail($id);
        $filters = ProductFilter::all();

        return view('admin.filter_field.edit', compact('field', 'filters'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'name' => 'required',
        ]);

        $field = FilterField::find($id);

        $field->photo_id = (new Photo)->getIdFromHtml(request('photo'));
        $field->fill(request()->all());
        $field->save();

        flash('Пункт обновлен')->important();

        return redirect()->route('filter_field.edit', ['id'=>$id]);
    }

    public function destroy($id)
    {
        FilterField::destroy($id);

        flash('Пункт удален')->important();

        return redirect()->route('filter_field.index');
    }
}
