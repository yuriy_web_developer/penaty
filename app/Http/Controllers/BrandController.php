<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Photo;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::orderBy('order_num')->get();

        return view('admin.brands.list', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Brand $brand
     * @return \Illuminate\Http\Response
     */
    public function store(Brand $brand)
    {
        $this->validate(request(), [
            'title' => 'required',
            'photo'   => 'required',
        ]);

        $brand = $this->fillWithData($brand);

        $brand->save();

        flash('Брэнд добавлен');

        return redirect()->route('brand.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::findOrFail($id);

        return view('admin.brands.edit', compact('brand'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'title' => 'required',
            'photo'   => 'required',
        ]);

        $brand = Brand::findOrFail($id);

        $brand = $this->fillWithData($brand);

        $brand->save();

        flash('Брэнд обновлен');

        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Brand::destroy($id);

        flash('Брэнд удален');

        return redirect()->route('brand.index');
    }

    public function updateOrder()
    {
        $items = Brand::all();

        foreach ($items as $item) {
            $new_position = array_search($item->id, request()->get('brand_ids'));
            $new_position += 1;

            if (!$new_position) continue;

            $item->order_num = $new_position;
            $item->save();
        }

        flash('Порядок изменен');

        return redirect()->route('brand.index');
    }

    public function fillWithData(Brand $brand)
    {
        $photo = new Photo;

        $brand->fill(request()->all());
        $photo_url = $photo->getImageUrl(request()->get('photo'));
        $brand->photo_id = $photo->getIdByPath($photo_url);

        return $brand;
    }



}
