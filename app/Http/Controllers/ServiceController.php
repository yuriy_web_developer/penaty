<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use App\Models\Services;
use App\Modules\Url\UrlTransform;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display list of services
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Services::named(request('name'))->get();

        return view('admin.service.list', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.service.create');
    }

    /**
     * Store a service block
     *
     * @param Services $service
     * @return \Illuminate\Http\Response
     */
    public function store(Services $service)
    {
        $input = $this->cleanTags(request()->all());
        $input['photo_id'] = (new Photo)->getIdFromHtml($input['photo']);
        $input['link'] = UrlTransform::deleteHost($input['link']);

        $service->setName(request('name'));
        $service->fill($input)->save();

        flash('Блок "Услуги" создан')->important();

        return redirect()->route('service.edit', $service->id);
    }

    /**
     * Show the form for the service
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Services::findOrFail($id);

        return view('admin.service.edit', compact('service'));
    }

    /**
     * Update the service
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $service = Services::findOrFail($id);
        $input = $this->cleanTags(request()->all());

        $input['photo_id'] = (new Photo)->getIdFromHtml($input['photo']);
        $input['link'] = UrlTransform::deleteHost($input['link']);

        $service->setName(request('name'));
        $service->fill($input)->save();

        flash('Блок "Услуги" обновлен')->important();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Services::destroy($id);

        flash('Блок "Услуги" удален')->important();

        return redirect()->back();
    }

    /**
     * Clean input from tags
     *
     * @param $input array
     * @return array
     */
    //todo remove to helpers
    private function cleanTags($input)
    {
        foreach ($input as $key => $value) {
            $input[$key] = strip_tags($value, '<br><img><p>');
        }

        return $input;
    }

    /**
     * Update order of service blocks;
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    //todo remove to module, look through other controllers
    public function updateOrder(Request $request)
    {
        $items = Services::find($request->slide_ids);

        foreach ($items as $item) {
            $new_position = array_search($item->id, $request->slide_ids);
            $new_position += 1;

            if (!$new_position) continue;

            $item->order_num = $new_position;
            $item->save();
        }

        return redirect()->route('design_studio.index');
    }
}
