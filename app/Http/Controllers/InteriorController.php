<?php

namespace App\Http\Controllers;

use App\Models\MainPage;
use App\Models\Photo;

class InteriorController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @param MainPage $page
     * @return \Illuminate\Http\Response
     */
    public function edit(MainPage $page)
    {
        $interior = $page->getArrayMetaByKey('interior');
        $photo = new Photo;

        return view('admin.interior.edit', compact('interior', 'photo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MainPage $page
     * @return \Illuminate\Http\Response
     */
    public function update(MainPage $page)
    {
        $data = $this->prepareInteriorData();

        //get meta value
        $interior = $page->getMetaByKey('interior');
        $interior->meta_value = $data;

        $interior->save();

        flash('Данные обновлены')->important();

        return redirect()->route('interior.edit');
    }

    /**
     * Prepare data for interior design model
     *
     * @return array
     */
    private function prepareInteriorData()
    {
        $interior = request()->only([
            'photo_ids',
            'link',
            'inEffect',
            'outEffect',
            'delay',
        ]);

        return $interior;

    }

}
