<?php

namespace App\Http\Controllers;

use App\Models\Subscriber;
use Illuminate\Http\Request;

class SubscriberController extends Controller
{

    protected $perPage = 20;

    /**
     * Display a listing of subscribers
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscribers = (new Subscriber)
            ->getQuery()
            ->paginate($this->perPage)
            ->appends(
                ['search_word' => request('search_word'),
                 'sort'        => request('sort')
                ]
            );

        $lastPageNum = $subscribers->currentPage() * $subscribers->perPage() - ($this->perPage - 1);

        return view('admin.subscriber.list', compact('subscribers', 'lastPageNum'));

    }

    /**
     * Store a newly created resource in storage.
     *
     */
    public function store()
    {
        if(request()->has('email')){
            $this->validate(request(),[
                'email'=>'email|unique:subscribers,email',
            ]);
        }

        $this->validate(request(),[
            'phone'=>'unique:subscribers,phone',
        ]);

        (new Subscriber)->fill(request()->all())->save();

        return "true";
    }


    /**
     * Show the form for editing the subscriber
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subscriber = Subscriber::findOrFail($id);

        return view('admin.subscriber.edit', compact('subscriber'));
    }

    /**
     * Update the subscriber
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $subscriber = Subscriber::findOrFail($id);

        $this->validate(request(), [
            'email'=>'email'
        ]);

        $subscriber->fill(request()->all());
        $subscriber->save();

        flash('Данные обновлены')->important();

        return redirect()->route('subscriber.index');

    }

    /**
     * Remove the subscriber
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Subscriber::destroy($id);

        flash('Подписчик удален')->important();

        return redirect()->route('subscriber.index');
    }
}
