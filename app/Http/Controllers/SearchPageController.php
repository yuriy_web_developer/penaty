<?php

namespace App\Http\Controllers;

use App\Modules\Search\SearchSite;

class SearchPageController extends Controller
{
    /**
     * Search the site
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $pages = (new SearchSite)->search(request('term'));

        $lastPageNum = $pages->currentPage() * $pages->perPage() - 9;

        return view('client.search.search_list', compact('pages', 'lastPageNum'));
    }
}
