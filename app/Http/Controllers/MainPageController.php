<?php

namespace App\Http\Controllers;

class MainPageController extends Controller
{
    /**
     * Show main page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('client.main.welcome');
    }
}
