<?php

namespace App\Http\Controllers;

use App\User;
use Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display all users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.auth.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the user
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'password_old'          => 'required',
            'password'              => 'bail|required|min:6|confirmed',
            'password_confirmation' => 'bail|required|min:6',
        ]);

        $user = User::findOrFail($id);

        if (!Hash::check(request('password_old'), $user->password)) {
            flash('Неверный старый пароль')->error()->important();
            return redirect()->route('user.edit', $id);
        }

        $user->password = bcrypt(request('password'));
        //$user->save();

        flash('Данные пользователя обновлены')->important();
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::all();
        //prevent from deleting all users
        if ($users->count() <= 1) {
            flash('Нельзя удалить всех пользователей')->important();
            return redirect()->route('user.index');
        }

        User::destroy($id);

        flash('Пользователь удален')->important();
        return redirect()->route('user.index');
    }
}
