<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Page;
use DB;

class PageController extends Controller
{
    //page that should be displayed
    protected $pageToDisplay;

    protected $path;

    /**
     * Catch all the routes except the predefined in the web.php at the top
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $page = Page::where([
            'url'       => request()->path(),
            'published' => 1,
        ])
                    ->first();

        if (!$page) return redirect('/');

        return view($this->getTemplateName($page), compact('page'));
    }


    /**
     * Find out the page template
     *
     * @param Page $page
     * @return string
     */
    private function getTemplateName(Page $page)
    {
        if (!$page) return 'client.info';

        switch ($page->type) {
            case 'info':
                $view = 'client.page.info';
                break;
            case 'news':
                $view = 'client.page.info';
                break;
            case 'product':
                $view = 'client.catalog.product';
                break;
            case 'category':
                $view = $this->getCatalogTemplateName($page);
                break;
            default:
                $view = 'client.page.info';
                break;
        }

        return $view;
    }

    public function getCatalogTemplateName(Page $page)
    {
        //if page has no children categories, display products
        //otherwise display children categories
        return $page->category->hasChildren() ? 'client.catalog.category' : 'client.catalog.products';
    }


    public function catalog()
    {
        //show catalog categories
        $url = $this->getUrl();

        if (empty($url))
            return view('client.catalog.catalog', ['catalog' => (new Category)->firstLevel()]);

        $parent = Page::where(['url' => $this->getUrl()[0]])->first();
        $this->setPage($parent);

        if ($this->isProduct($parent)) {
            $this->setPage($parent);
            return view('client.catalog.catalog', ['page' => $this->pageToDisplay->product]);
        }


        //first part is already found
        $this->cutPath();
        $parent = $parent->category;

        for ($i = 0; $i < 5; $i++) {
            $parent = $this->checkChildren($parent);
            if (!$parent) break;
        }

        $this->lastPartIsProduct();

        //dd($this->CatalogPageNotFound());
        $template = $this->getTemplateName($this->pageToDisplay);

        return view($template, [
            'product'  => $this->pageToDisplay->product,
            'page'     => $this->pageToDisplay,
            'category' => $this->pageToDisplay->category]);

    }

    public function isProduct($page)
    {
        if (!is_object($page) || !is_a($page, Page::class)) return false;

        if ($page->type == 'product') return true;
        return false;
    }

    public function getUrl()
    {
        $this->path = explode('/', request()->path());
        //cut off the first part 'catalog'.
        $this->cutPath();

        return $this->path;
    }

    public function checkChildren($category)
    {
        if (!$this->isCategory($category)) return false;

        if (!$this->validPath()) return false;

        if ($category->hasChildren()) {
            foreach ($category->children() as $child) {
                if ($child->page->url == $this->path[0]) {
                    $this->setPage($child->page);
                    $this->cutPath();
                    return $child;
                }
            }
        }
        return false;

    }

    public function validPath()
    {
        if (!array_key_exists(0, $this->path)) return false;
        return true;
    }

    public function cutPath($path = [])
    {
        $path = $path ? $path : $this->path;

        array_shift($path);

        $this->path = $path;

        return $path;
    }

    public function setPage(Page $page)
    {
        $this->pageToDisplay = $page;
    }

    public function lastPartIsProduct()
    {
        if (!$this->validPath()) return false;
        if (!$this->pageToDisplay) return false;

        $products = $this->pageToDisplay->category->product;

        foreach ($products as $product) {
            if ($product->page->url == $this->path[0]) {
                $this->setPage($product->page);
            }
        }
        return false;
    }


    public function isCategory($category)
    {
        if (!is_object($category) || !is_a($category, Category::class)) return false;

        return true;
    }


    private function CatalogPageNotFound()
    {
        if (count($this->path) > 0) return true;

        return false;
    }

    public function catalogSecondLevel()
    {
        $parent = Page::where(['url' => $this->getUrl()[0]])->first();

        $this->cutPath();
        $parent = $parent->category;


        for ($i = 0; $i < 5; $i++) {
            $parent = $this->checkChildren($parent);
            if (!$parent) break;
        }

        return view('category2', [
            'category' => $this->pageToDisplay->category]);
    }

}
