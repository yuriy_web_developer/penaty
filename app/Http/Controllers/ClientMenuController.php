<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Menu;

class ClientMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = (new Menu)->getFirstLevel();
        return view('admin.client_menu.list', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.client_menu.create');
    }

    /**
     * Save menu item
     *
     * @param  \Illuminate\Http\Request $request
     * @param Menu $menu
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'item_name' => 'required',
            'url'       => 'required',
        ]);

        $last_item = $menu->getLastItem() ? $menu->getLastItem()->order_num : 0;
        $menu->order_num = $last_item + 1;
        $menu->fill($request->all());
        $menu->save();

        return $this->redirectSuccessWithMsg('Пункт меню удачно создан');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Menu::find($id);

        $menu = new Menu;

        return view('admin.client_menu.edit', compact('item', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $item = Menu::findOrFail($id);
        $item->fill($request->all());
        $item->save();

        return $this->redirectSuccessWithMsg('Пункт меню удачно обновлен');
    }

    /**
     * Remove the specified menu item
     *
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Menu::destroy($id);

        return $this->redirectSuccessWithMsg('Пункт меню удален');
    }


    /**
     * Update the order of items in the menu
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateOrder()
    {
        $menu = Menu::all();
        $counter = 1;

        if (request()->has('item_id')) {
            foreach (request()->item_id as $index => $new_menu) {
                foreach ($menu as $current_menu) {
                    if ($current_menu->id == $index) {
                        $current_menu->order_num = $counter;
                        $current_menu->save();
                        $counter++;
                    }
                }
            }
        }

        return $this->redirectSuccessWithMsg('Порядок изменен');

    }

    /**
     * Redirect with success message.
     *
     * @param $msg
     * @param string $route
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectSuccessWithMsg($msg, $route = 'menu.index')
    {
        flash($msg)->important();

        return redirect()->route($route);

    }
}
