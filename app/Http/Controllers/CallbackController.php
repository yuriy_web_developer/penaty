<?php

namespace App\Http\Controllers;

use App\Models\Callback;
use Illuminate\Http\Request;

class CallbackController extends Controller
{
    protected $perPage = 20;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $callbacks = (new Callback)
            ->orderBy('id', 'desc')
            ->paginate($this->perPage);

        $lastPageNum = $callbacks->currentPage() * $callbacks->perPage() - ($this->perPage - 1);

        return view('admin.callback.list', compact('callbacks', 'lastPageNum'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Callback::destroy($id);

        flash('Заявка удалена')->important();

        return back();
    }


    public function makeFromAjax()
    {
        $this->validate(request(), [
            'email' => 'email',
            'phone' => 'required',
            'client_name' => 'required',
        ]);

        (new Callback)->create(request()->all());

        return "true";
    }
}
