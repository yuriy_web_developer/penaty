<?php

namespace App\Http\Middleware;

use App\Modules\ShortCode\ShortCode;
use Closure;

class AfterShortCodeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if ($this->disableShortCodes($response)) {
            return $response;
        }
        $content = $response->getContent();
        $content = (new ShortCode($content))->globalCodesReplace()->get();

        return $response->setContent($content);
    }

    /**
     * When to disable shortcodes
     *
     * @param $response
     * @return bool
     */
    private function disableShortCodes($response)
    {
        if (is_object($response) && method_exists('status', $response) && $response->status() != 200)
            return true;

        return request()->is('admin/*') || request()->is('ajax/*');
    }
}
