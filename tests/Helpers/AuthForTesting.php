<?php

namespace tests\Helpers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Dusk\Browser;

/**
 * Auth helpers for testing admin panel.
 *
 * Class Auth
 * @package tests\Browser\Pages
 */
class AuthForTesting
{
    //test users
    protected $users = [];

    /**
     * Create a test user
     *
     * @param array $data
     * @return mixed
     */
    public function createUser($data = [])
    {
        $user = factory(User::class)->create($data);

        $this->users[] = $user;

        return $user;
    }

    /**
     * login a user
     */
    public function login()
    {
        Auth::login($this->createUser());
    }

    /**
     * Clean up database from test users
     *
     */
    public function deleteUsers()
    {
        foreach ($this->users as $user) {
            $user->delete();
        }
    }

    /**
     * Login for browser tests
     *
     * @param Browser $browser
     * @param User|null $user
     * @return Browser
     */
    public function loginBrowser(Browser $browser, User $user = null)
    {
        if (!$user) $user = (new self)->createUser();

        $browser->loginAs($user->id)
                ->visit('/admin')
                ->assertSee('Главное меню');

        $this->deleteUsers();

        return $browser;
    }

}

