<?php

namespace Tests\Browser\Client;

use App\Models\Callback;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class FreeConsultPopupTest extends DuskTestCase
{
    public function test_popup()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/catalog')
                    ->pause(100)
                    ->click('.contact-btn')
                    ->assertVisible('#free_consult')
                    ->with('#free_consult', function ($form) {
                        $form->type('client_name', 'Client Name')
                             ->type('phone', '123-45-78')
                             ->type('email', 'email@email.com')
                             ->type('comment', 'comment example')
                             ->select('repair_type')
                             ->click('button[type=submit]');
                    })
                    ->pause(1000);

            $this->assertDatabaseHas('callbacks', [
                'email' => 'email@email.com',
            ]);

            //delete test callback request
            Callback::where('email', 'email@email.com')->delete();
        });
    }
}

