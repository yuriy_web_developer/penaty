<?php

namespace Test\Browser\Client;

use App\Models\Order;
use App\Models\Product;
use App\Modules\Url\UrlPath;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class ProductTest extends DuskTestCase
{

    public function test_product_is_visible()
    {
        $url = (new UrlPath)->makeProductUrl(Product::first());

        $this->browse(function (Browser $browser) use ($url) {
            $browser->visit($url)
                    ->assertVisible('.b-product .b-product__title');
        });
    }

    public function test_product_category_is_opened_in_menu()
    {
        $url = (new UrlPath)->makeProductUrl(Product::first());

        $this->browse(function (Browser $browser) use ($url) {
            $category = implode('/', array_slice(explode('/', $url), 0, -1));
            $browser->visit($url)
                    ->assertVisible(".b-left-sidebar a[href='" . $category . "']");
        });
    }

    public function test_defer_product()
    {
        $url = (new UrlPath)->makeProductUrl(Product::first());

        $this->browse(function (Browser $browser) use ($url) {
            $browser->visit($url)
                    ->click('.b-product_btn_defer')
                    ->assertSee('Товар отложен')
                    ->assertSee('Отложенные товары:')
                    ->visit('/defer')
                    ->assertVisible('.b-defer_title')
                    ->with('.b-defer', function ($form) {
                        $form->type('client_name', 'Client Name')
                             ->type('phone', '123-45-78')
                             ->type('email', 'email@email.com')
                             ->type('comment', 'comment example')
                             ->type('city', 'City')
                             ->click('button[type=submit]')
                             ->assertSee('С Вами скоро свяжутся наши специалисты');
                    });
        });

        (new Order)->where('email', 'email@email.com')->delete();

    }
}