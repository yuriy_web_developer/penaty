<?php

namespace Tests\Browser\Client;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class DesignStudioTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/dizajn')
                    ->assertSee('О ДИЗАЙН СТУДИИ');
        });
    }
}
