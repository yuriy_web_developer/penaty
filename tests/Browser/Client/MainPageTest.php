<?php

namespace Tests\Browser\Client;

use App\Models\Callback;
use App\Models\Subscriber;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class LoginTest extends DuskTestCase
{

    public function test_main_page_is_available()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertVisible('.b-footer');
        });
    }

    public function test_free_consult_form()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->with('.b-consult', function ($form) {
                        $form->type('client_name', 'Client Name')
                             ->type('phone', '123-45-78')
                             ->type('email', 'email@email.com')
                             ->type('comment', 'comment example')
                             ->select('repair_type')
                             ->click('button');
                    })
                    ->pause(1000);
        });

        $this->assertDatabaseHas('callbacks', [
            'client_name' => 'Client Name',
        ]);

        //delete test callback request
        Callback::where('email', 'email@email.com')->delete();
    }

    public function test_subscribe_email()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                // ->click(".b-subscribe__at")
                    ->pause(100)
                    ->with('.b-subscribe', function ($form) {
                        $form->type('#subscribe_field', 'email@email.com')
                             ->check('conditions_agreed')
                             ->click('.btn-subscribe');
                    })
                    ->pause(1000);
        });

        $this->assertDatabaseHas('subscribers', [
            'email' => 'email@email.com',
        ]);

        //delete a test subscriber
        Subscriber::where('email', 'email@email.com')->delete();
    }


    public function test_subscribe_phone()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                 ->click(".b-subscribe_phone")
                    ->pause(100)
                    ->with('.b-subscribe', function ($form) {
                        $form->type('#subscribe_field', '123456789')
                             ->check('conditions_agreed')
                             ->click('.btn-subscribe');
                    })
                    ->pause(1000);
        });

        $this->assertDatabaseHas('subscribers', [
            'phone' => '123456789',
        ]);

        //delete a test subscriber
        Subscriber::where('phone', '123456789')->delete();
    }
}
