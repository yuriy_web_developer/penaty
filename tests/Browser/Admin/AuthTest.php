<?php

namespace tests\Browser\Admin;

use Laravel\Dusk\Browser;
use Tests\Helpers\AuthForTesting;
use Tests\DuskTestCase;

class AuthTest extends DuskTestCase
{
    public function test_login_login_out()
    {
        $this->browse(function (Browser $browser) {
            (new AuthForTesting)->loginBrowser($browser)
                                ->click('.dropdown > a')
                                ->click('.dropdown-menu a')
                                ->waitForLocation('/admin/login')
                                ->assertSee('Авторизация');
        });

    }

}

