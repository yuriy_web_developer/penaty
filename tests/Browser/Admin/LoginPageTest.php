<?php

namespace tests\Browser\Admin;

use App\User;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LoginPage extends DuskTestCase
{
    public function test_register_a_user()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/admin/register')
                    ->type("#name", 'test')
                    ->type('email', 'test@test.com')
                    ->type('password', '123456')
                    ->type('password_confirmation', '123456')
                    ->click('.panel-body button');
        });

        $this->assertDatabaseHas('users', [
            'email' => 'test@test.com',
        ]);

        User::where('email', 'test@test.com')->delete();
    }

}

