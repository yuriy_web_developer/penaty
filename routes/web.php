<?php
//client routes
Route::get('/', 'MainPageController@index');

Route::post('/defer/add', 'ProductDeferController@addDeferred');
Route::post('/defer/send', 'ProductDeferController@sendForm');
Route::post('/defer/delete_all', 'ProductDeferController@deleteAll');
Route::get('/defer/delete', 'ProductDeferController@delete');
Route::get('/defer', 'ProductDeferController@defer');

Route::get('news', 'NewsController@showForClient');
Route::get('news/{news}', 'NewsController@showOneForClient');

Route::get('dizajn', 'DesignStudioController@showPage');

//catalog
Route::get('catalog', 'CatalogController@index')->name('catalog.index');
Route::get('catalog/{first}', 'CatalogController@firstLevel');
Route::get('catalog/{first}/{second}', 'CatalogController@secondLevel');
Route::get('catalog/{first}/{second}/{third}', 'CatalogController@thirdLevel');
Route::get('catalog/{first}/{second}/{third}/{product}', 'CatalogController@showProduct');


//ajax calls
Route::group(['prefix' => 'ajax',], function () {
    Route::post('/subscribe', 'SubscriberController@store');
    Route::post('/contact', 'CallbackController@makeFromAjax');
});

//admin login
Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('logout', 'Auth\LoginController@logout');
    Auth::routes();
});

//admin routes
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', 'ClientMenuController@index');

    Route::post('menu/order', 'ClientMenuController@updateOrder')->name('menu.order');
    Route::resource('menu', 'ClientMenuController');

    Route::get('contacts/edit', 'ContactsController@edit')->name('contacts.edit');
    Route::post('contacts/update', 'ContactsController@update')->name('contacts.update');

    Route::get('interior/edit', 'InteriorController@edit')->name('interior.edit');
    Route::post('interior/update', 'InteriorController@update')->name('interior.update');

    Route::get('news/copy/{id}', 'NewsController@copy')->name('news.copy');
    Route::post('news/order', 'NewsController@updateOrder')->name('news.order');
    Route::resource('news', 'NewsController');

    Route::post('photos/ajax', 'PhotosController@uploadAjax');
    Route::post('photos/deleteMulti', 'PhotosController@destroyMulti')->name('photos.delete.multi');
    Route::resource('photos', 'PhotosController');

    Route::any('ajax/imageUpload', 'PhotosController@uploadAjax');
    Route::any('ajax/searchPhoto', 'PhotosController@searchAjax');

    Route::resource('slider', 'Admin\SliderController');
    Route::post('slider/order', 'Admin\SliderController@updateOrder')->name('slider.order');

    Route::resource('info_page', 'InfoPageController');
    Route::resource('meta_data', 'MetaDataController');

    Route::resource('subscriber', 'SubscriberController');

    Route::resource('product', 'ProductController');

    Route::resource('category', 'CategoryController');
    Route::post('category/order', 'CategoryController@updateOrder')->name('category.order');

    Route::resource('order', 'OrderController');

    Route::resource('callback', 'CallbackController');

    Route::post('brand/order', 'BrandController@updateOrder')->name('brand.order');
    Route::resource('brand', 'BrandController');

    Route::group(['prefix' => 'settings'], function () {
        Route::put('settings/offer/update','Admin\Settings\SpecialOfferController@update')->name('settings.offer.update');
        Route::get('settings/offer/edit','Admin\Settings\SpecialOfferController@edit')->name('settings.offer.edit');

    });

    Route::resource('design_studio', 'DesignStudioController');

});

Route::get('/{any}', 'PageController@index')->where('any', '(.*)');